# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v4.4.224
  - d72237c1e00f8 Linux 4.4.224
    ACK: iwamatsu
  - fbc09f1ef0482 scsi: iscsi: Fix a potential deadlock in the timeout handler
    ACK: iwamatsu
  - 8291f17dc2fe7 Makefile: disallow data races on gcc-10 as well
    ACK: iwamatsu
  - 4143cea599730 KVM: x86: Fix off-by-one error in kvm_vcpu_ioctl_x86_setup_mce
    ACK: iwamatsu
  - 07fbc97743b9d ARM: dts: r8a7740: Add missing extal2 to CPG node
    ACK: iwamatsu
  - 011449cfc2540 Revert "ALSA: hda/realtek: Fix pop noise on ALC225"
    ACK: iwamatsu
  - dea7c8ebf7303 usb: gadget: legacy: fix error return code in cdc_bind()
    ACK: iwamatsu
  - 82fc349bfd8bf usb: gadget: legacy: fix error return code in gncm_bind()
    ACK: iwamatsu
  - 2f0919d6788f8 usb: gadget: audio: Fix a missing error return value in audio_bind()
    ACK: iwamatsu
  - 6dfdd767cffb4 usb: gadget: net2272: Fix a memory leak in an error handling path in 'net2272_plat_probe()'
    ACK: iwamatsu
  - 84cd70984ed2e exec: Move would_dump into flush_old_exec
    ACK: iwamatsu
  - afa0b39ebe580 x86: Fix early boot crash on gcc-10, third try
    ACK: iwamatsu
  - bb5809183912b ARM: dts: imx27-phytec-phycard-s-rdk: Fix the I2C1 pinctrl entries
    ACK: iwamatsu
  - c18a8b0d7b8fd USB: gadget: fix illegal array access in binding with UDC
    ACK: iwamatsu
  - 41d7b565440db ALSA: rawmidi: Initialize allocated buffers
    ACK: iwamatsu
  - 718eede1eeb60 ALSA: rawmidi: Fix racy buffer resize under concurrent accesses
    ACK: iwamatsu
  - ac1eb6222b6b1 ALSA: hda/realtek - Limit int mic boost for Thinkpad T530
    ACK: iwamatsu
  - b8ff52e4bdaab netlabel: cope with NULL catmap
    ACK: iwamatsu
  - 98ee7d0d6a2b4 net: ipv4: really enforce backoff for redirects
    ACK: iwamatsu
  - 62b4ebf58fe98 net: fix a potential recursive NETDEV_FEAT_CHANGE
    ACK: iwamatsu
  - 5a1dbe6317971 gcc-10: avoid shadowing standard library 'free()' in crypto
    ACK: iwamatsu, fix for gcc-10 issue
  - 16ff1ecfbac80 x86/paravirt: Remove the unused irq_enable_sysexit pv op
    ACK: iwamatsu
  - 4764810c45447 blk-mq: Allow blocking queue tag iter callbacks
    ACK: iwamatsu
  - fa9355afd5b07 blk-mq: sync the update nr_hw_queues with blk_mq_queue_tag_busy_iter
    ACK: iwamatsu
  - f06f7a923d1de blk-mq: Allow timeouts to run while queue is freezing
    ACK: iwamatsu
  - 5fb835b9561f3 block: defer timeouts to a workqueue
    ACK: iwamatsu
  - 030a196c1da44 gcc-10: disable 'restrict' warning for now
    ACK: iwamatsu, fix for gcc-10 issue
  - a41a8515926d4 gcc-10: disable 'stringop-overflow' warning for now
    ACK: iwamatsu, fix for gcc-10 issue
  - 9c622c6ec0562 gcc-10: disable 'array-bounds' warning for now
    ACK: iwamatsu, fix for gcc-10 issue
  - 2de6a8a83ff0d gcc-10: disable 'zero-length-bounds' warning for now
    ACK: iwamatsu, fix for gcc-10 issue
  - f566668e19598 Stop the ad-hoc games with -Wno-maybe-initialized
    ACK: iwamatsu
  - c50c2c2ed69e4 kbuild: compute false-positive -Wmaybe-uninitialized cases in Kconfig
    ACK: iwamatsu
  - bd395069dda80 gcc-10 warnings: fix low-hanging fruit
    ACK: iwamatsu, fix for gcc-10 issue
  - 14aca7c0ed496 pnp: Use list_for_each_entry() instead of open coding
    ACK: iwamatsu
  - b559ea48c8992 IB/mlx4: Test return value of calls to ib_get_cached_pkey
    ACK: iwamatsu
  - b5dad703517e4 netfilter: conntrack: avoid gcc-10 zero-length-bounds warning
    ACK: iwamatsu
  - 36f7889943fac net/mlx5: Fix driver load error flow when firmware is stuck
    ACK: iwamatsu
  - 536918c1d6837 i40e: avoid NVM acquire deadlock during NVM update
    ACK: iwamatsu
  - bde533c656f3d scsi: qla2xxx: Avoid double completion of abort command
    ACK: iwamatsu
  - 3a3c1ab554378 mm/memory_hotplug.c: fix overflow in test_pages_in_a_zone()
    ACK: iwamatsu
  - 0ac3143d79ebe gre: do not keep the GRE header around in collect medata mode
    ACK: iwamatsu
  - 887daaacb5415 net: openvswitch: fix csum updates for MPLS actions
    ACK: iwamatsu
  - 77dbb2fa31d5e ipc/util.c: sysvipc_find_ipc() incorrectly updates position index
    ACK: iwamatsu
  - ac8087ffca522 drm/qxl: lost qxl_bo_kunmap_atomic_page in qxl_image_init_helper()
    ACK: iwamatsu
  - 4db73f4467eaf dmaengine: mmp_tdma: Reset channel error on release
    ACK: iwamatsu
  - 329a8c9d417bb dmaengine: pch_dma.c: Avoid data race between probe and irq handler
    ACK: iwamatsu
  - 41f53bf3898ad cifs: Fix a race condition with cifs_echo_request
    ACK: iwamatsu
  - ffd63cf20b2df cifs: Check for timeout on Negotiate stage
    ACK: iwamatsu
  - 3549e7aaa2094 spi: spi-dw: Add lock protect dw_spi rx/tx to prevent concurrent calls
    ACK: iwamatsu
  - af9a86cc9f1ac scsi: sg: add sg_remove_request in sg_write
    ACK: iwamatsu
  - cd00b981447eb drop_monitor: work around gcc-10 stringop-overflow warning
    ACK: iwamatsu
  - 1ad07995cd9bb net: moxa: Fix a potential double 'free_irq()'
    ACK: iwamatsu
  - 3c8c200bba4a7 net/sonic: Fix a resource leak in an error handling path in 'jazz_sonic_probe()'
    ACK: iwamatsu
  - 178af2f97dcae net: handle no dst on skb in icmp6_send
    ACK: iwamatsu
  - a9acf25d6c38a ptp: free ptp device pin descriptors properly
    ACK: iwamatsu
  - 6f5e3bb7879ee ptp: fix the race between the release of ptp_clock and cdev
    ACK: iwamatsu
  - 6091936487a95 ptp: Fix pass zero to ERR_PTR() in ptp_clock_register
    ACK: iwamatsu
  - d79d7d5c87880 chardev: add helper function to register char devs with a struct device
    ACK: iwamatsu
  - e5a654c9935cf ptp: create "pins" together with the rest of attributes
    ACK: iwamatsu
  - e95a7fb08693f ptp: use is_visible method to hide unused attributes
    ACK: iwamatsu
  - f212dbd2b2e84 ptp: do not explicitly set drvdata in ptp_clock_register()
    ACK: iwamatsu
  - ab33f6abb29a8 blktrace: fix dereference after null check
    ACK: iwamatsu
  - 3d5d64aea941a blktrace: Protect q->blk_trace with RCU
    ACK: iwamatsu
  - cd13258a62313 blktrace: fix trace mutex deadlock
    ACK: iwamatsu
  - 4e6958563b29d blktrace: fix unlocked access to init/start-stop/teardown
    ACK: iwamatsu
  - aee60ffbceeb0 blktrace: Fix potential deadlock between delete & sysfs ops
    ACK: iwamatsu
  - 7c9d04e1c3ed5 net: ipv6_stub: use ip6_dst_lookup_flow instead of ip6_dst_lookup
    ACK: iwamatsu
  - be901a0e5550a net: ipv6: add net argument to ip6_dst_lookup_flow
    ACK: iwamatsu
  - e3f8a70a95fa6 ext4: add cond_resched() to ext4_protect_reserved_inode
    ACK: iwamatsu
  - d48539bf92c72 binfmt_elf: Do not move brk for INTERP-less ET_EXEC
    ACK: iwamatsu
  - 5956537779b05 phy: micrel: Ensure interrupts are reenabled on resume
    ACK: iwamatsu
  - e0ff8cd6ff26c scripts/decodecode: fix trapping instruction formatting
    ACK: iwamatsu
  - 2b0aa04993325 batman-adv: fix batadv_nc_random_weight_tq
    ACK: iwamatsu
  - b9764baaebbe9 USB: serial: garmin_gps: add sanity checking for data length
    ACK: iwamatsu, add device ID
  - c2ad91a808434 USB: uas: add quirk for LaCie 2Big Quadra
    ACK: iwamatsu, add device ID
  - e61d5650ded77 Revert "IB/ipoib: Update broadcast object if PKey value was changed in index 0"
    ACK: iwamatsu
  - 42a7e784184cd x86/apm: Don't access __preempt_count with zeroed fs
    ACK: iwamatsu
  - 93567229892d4 binfmt_elf: move brk out of mmap when doing direct loader exec
    ACK: iwamatsu
  - 48a238024893b ipv6: fix cleanup ordering for ip6_mr failure
    ACK: iwamatsu
  - 4537e64a91678 enic: do not overwrite error code
    ACK: iwamatsu
  - d3e3e34f212aa Revert "ACPI / video: Add force_native quirk for HP Pavilion dv6"
    ACK: iwamatsu
  - 9aa2c8807b694 sch_choke: avoid potential panic in choke_reset()
    ACK: iwamatsu
  - 116555b32d992 sch_sfq: validate silly quantum values
    ACK: iwamatsu
  - 96a1e05f7b21e net/mlx4_core: Fix use of ENOSPC around mlx4_counter_alloc()
    ACK: iwamatsu
  - 405a8cc9b4ed8 dp83640: reverse arguments to list_add_tail
    ACK: iwamatsu
  - 7bdfb137c352a Revert "net: phy: Avoid polling PHY with PHY_IGNORE_INTERRUPTS"
    ACK: iwamatsu
  - 0fffb8dbdae34 USB: serial: qcserial: Add DW5816e support
    ACK: iwamatsu, add device ID
