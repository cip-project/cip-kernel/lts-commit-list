# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v5.10.32
  - aea70bd5a455 Linux 5.10.32
  - 6ac98ee9cb7c net: phy: marvell: fix detection of PHY on Topaz switches
  ACK: pavel
  - fbe6603e7cab bpf: Move sanitize_val_alu out of op switch
  UR: pavel -- just cleanups
  - 7723d3243857 bpf: Improve verifier error messages for users
  UR: pavel -- nontrivial, changes error returns
  - 55565c307908 bpf: Rework ptr_limit into alu_limit and add common error path
  UR: pavel --a cleanup, not a bugfix
  - 496e2fabbbe3 arm64: mte: Ensure TIF_MTE_ASYNC_FAULT is set atomically
  ACK: pavel
  - cada2ed0bb70 ARM: 9071/1: uprobes: Don't hook on thumb instructions
  ACK: pavel
  - 480d875f1242 bpf: Move off_reg into sanitize_ptr_alu
  UR: pavel --! cleanup, not a bugfix
  - 589fd9684dfa bpf: Ensure off_reg has no mixed signed bounds for all types
  ACK: pavel
  - b2df20c0f19f r8169: don't advertise pause in jumbo mode
  ACK: pavel -- just a performance issue
  - 154fb9cb3e6f r8169: tweak max read request size for newer chips also in jumbo mtu mode
  ACK: pavel
  - 7f64753835a7 KVM: VMX: Don't use vcpu->run->internal.ndata as an array index
  ACK: pavel
  - c670ff84fac9 KVM: VMX: Convert vcpu_vmx.exit_reason to a union
  UR: pavel -- preparation/cleanup. not a bugfix
  - 4f3ff11204ea bpf: Use correct permission flag for mixed signed bounds arithmetic
  ACK: pavel
  - 8d7906c548aa arm64: dts: allwinner: h6: beelink-gs1: Remove ext. 32 kHz osc reference
  UR: pavel -- deserves a comment?
  - 286c39d08664 arm64: dts: allwinner: Fix SD card CD GPIO for SOPine systems
  ACK: pavel
  - 4f90db2e92d2 ARM: OMAP2+: Fix uninitialized sr_inst
  UR: pavel --! no. All this consts...
  - 1fc087fdb98d ARM: footbridge: fix PCI interrupt mapping
  ACK: pavel
  - 11a718ef953f ARM: 9069/1: NOMMU: Fix conversion for_each_membock() to for_each_mem_range()
  ACK: pavel
  - a13d4a1228ab ARM: OMAP2+: Fix warning for omap_init_time_of()
  ACK: pavel -- just a warning fix
  - 9143158a6bd3 gro: ensure frag0 meets IP header alignment
  ACK: pavel
  - fd766f792a56 ch_ktls: do not send snd_una update to TCB in middle
  ACK: pavel
  - 65bdd564b387 ch_ktls: tcb close causes tls connection failure
  ACK: pavel
  - 5f3c278035c0 ch_ktls: fix device connection close
  ACK: pavel
  - 8d5a9dbd2116 ch_ktls: Fix kernel panic
  UR: pavel -- c/e
  - 976da1b08784 ibmvnic: remove duplicate napi_schedule call in open function
  ACK: pavel -- cleanup, not a bugfix
  - 008885a880dc ibmvnic: remove duplicate napi_schedule call in do_reset function
  ACK: pavel -- cleanup, not a bugfix
  - 685bc730e3a9 ibmvnic: avoid calling napi_disable() twice
  ACK: pavel -- I don't think bug can happen in practice
  - e154b5060aa1 ia64: tools: remove inclusion of ia64-specific version of errno.h header
  - f8f01fc8c653 ia64: remove duplicate entries in generic_defconfig
  UR: pavel
  - 1aec111c944f ethtool: pause: make sure we init driver stats
  ACK: pavel
  - 44ef38c0a2b3 i40e: fix the panic when running bpf in xdpdrv mode
  ACK: pavel
  - 35d7491e2f77 net: Make tcp_allowed_congestion_control readonly in non-init netns
  ACK: pavel -- just an interface tweak
  - 76af8126a6e4 mm: ptdump: fix build failure
  ACK: pavel
  - 33f3dab42ae2 net: ip6_tunnel: Unregister catch-all devices
  ACK: pavel
  - ea0340e632ba net: sit: Unregister catch-all devices
  ACK: pavel
  - 154ac84d497a net: davicom: Fix regulator not turned off on failed probe
  ACK: pavel
  - e072247938a8 net/mlx5e: Fix setting of RS FEC mode
  ACK: pavel
  - dc1732baa9da netfilter: nft_limit: avoid possible divide error in nft_limit_init
  ACK: pavel
  - cda5507d234f net/mlx5e: fix ingress_ifindex check in mlx5e_flower_parse_meta
  ACK: pavel
  - 40ed1d29f151 net: macb: fix the restore of cmp registers
  ACK: pavel
  - 7f8e59c4c5e5 libbpf: Fix potential NULL pointer dereference
  ACK: pavel
  - 7824d5a9935a netfilter: arp_tables: add pre_exit hook for table unregister
  UR: pavel --a is it neccessary in 5.10?
  - 4d26865974fb netfilter: bridge: add pre_exit hooks for ebtable unregistration
  UR: pavel
  - eb82199e377a libnvdimm/region: Fix nvdimm_has_flush() to handle ND_REGION_ASYNC
  ACK: pavel
  - a2af8a0f38e4 ice: Fix potential infinite loop when using u8 loop counter
  ACK: pavel
  - 783645e65b57 netfilter: conntrack: do not print icmpv6 as unknown via /proc
  ACK: pavel -- just interface tweak
  - 394c81e36e49 netfilter: flowtable: fix NAT IPv6 offload mangling
  ACK: pavel
  - be07581aacae ixgbe: fix unbalanced device enable/disable in suspend/resume
  UR: pavel -- c/e
  - 0ef9919a06a3 scsi: libsas: Reset num_scatter if libata marks qc as NODATA
  ACK: pavel
  - 6a70ab9769cd riscv: Fix spelling mistake "SPARSEMEM" to "SPARSMEM"
  ACK: pavel
  - f66d695c06f4 vfio/pci: Add missing range check in vfio_pci_mmap
  UR: pavel -- not a minimum fix; removes some robustness
  - e6177990e17d arm64: alternatives: Move length validation in alternative_{insn, endif}
  UR: pavel -- commit being fixed is not in 5.10
  - e2931f05eb32 arm64: fix inline asm in load_unaligned_zeropad()
  UR: pavel
  - 957f83a138f1 readdir: make sure to verify directory entry for legacy interfaces too
  ACK: pavel
  - 2b8308741cf5 dm verity fec: fix misaligned RS roots IO
  UR: pavel -- we have macros for the bit check... check expressions
  - 18ba387261ea HID: wacom: set EV_KEY and EV_ABS only for non-HID_GENERIC type of devices
  ACK: pavel
  - dedf75aec8fc Input: i8042 - fix Pegatron C15B ID entry
  ACK: pavel
  - 8b978750dcd2 Input: s6sy761 - fix coordinate read bit shift
  ACK: pavel
  - 955da2b5cd98 lib: fix kconfig dependency on ARCH_WANT_FRAME_POINTERS
  ACK: pavel
  - 024f9d048000 virt_wifi: Return micros for BSS TSF values
  ACK: pavel
  - cc413b375c6d mac80211: clear sta->fast_rx when STA removed from 4-addr VLAN
  ACK: pavel
  - 2e08d9a56838 pcnet32: Use pci_resource_len to validate PCI resource
  ACK: pavel -- just fixes driver on unusual hardware
  - 248b9b61b951 net: ieee802154: forbid monitor for add llsec seclevel
  ACK: pavel
  - b97c7bc42d8d net: ieee802154: stop dump llsec seclevels for monitors
  UR: pavel
  - ab9f9a1d5874 net: ieee802154: forbid monitor for del llsec devkey
  ACK: pavel
  - 4846c2debb2c net: ieee802154: forbid monitor for add llsec devkey
  ACK: pavel
  - 07714229e0e2 net: ieee802154: stop dump llsec devkeys for monitors
  UR: pavel -- check
  - 4c1775d6ea86 net: ieee802154: forbid monitor for del llsec dev
  - 813b13155d14 net: ieee802154: forbid monitor for add llsec dev
  - 2f80452951b5 net: ieee802154: stop dump llsec devs for monitors
  ACK: pavel
  - 08744a622faa net: ieee802154: forbid monitor for del llsec key
  ACK: pavel
  - 7edf4d2baa8a net: ieee802154: forbid monitor for add llsec key
  ACK: pavel
  - c09075df5e4d net: ieee802154: stop dump llsec keys for monitors
  ACK: pavel
  - 8b9485b651d4 iwlwifi: add support for Qu with AX201 device
  ACK: pavel
  - c836374bacfa scsi: scsi_transport_srp: Don't block target in SRP_PORT_LOST state
  ACK: pavel
  - d9fc084067f5 ASoC: fsl_esai: Fix TDM slot setup for I2S mode
  ACK: pavel
  - 79ef0e6c0cf8 drm/msm: Fix a5xx/a6xx timestamps
  ACK: pavel
  - d61238aa6482 ARM: omap1: fix building with clang IAS
  ACK: pavel
  - 505c48942f04 ARM: keystone: fix integer overflow warning
  ACK: pavel -- just a warning fix
  - 0d0ad98bee39 neighbour: Disregard DEAD dst in neigh_update
  ACK: pavel
  - 7a1cd9044da4 gpu/xen: Fix a use after free in xen_drm_drv_init
  UR: pavel -- c/e
  - bfb5a1523f17 ASoC: max98373: Added 30ms turn on/off time delay
  ACK: pavel
  - 58d59d9ae56f ASoC: max98373: Changed amp shutdown register as volatile
  ACK: pavel
  - b2f8476193eb xfrm: BEET mode doesn't support fragments for inner packets
  ACK: pavel
  - 806addaf8dfd iwlwifi: Fix softirq/hardirq disabling in iwl_pcie_enqueue_hcmd()
  ACK: pavel
  - b448a6a2fc5a arc: kernel: Return -EFAULT if copy_to_user() fails
  ACK: pavel
  - f12e8cf6b180 lockdep: Add a missing initialization hint to the "INFO: Trying to register non-static key" message
  ACK: pavel -- just a printk tweak
  - a55de4f0d1d4 ARM: dts: Fix moving mmc devices with aliases for omap4 & 5
  ACK: pavel -- just a device order change, will break stuff for people relying on the new order
  - 9f399a9d7006 ARM: dts: Drop duplicate sha2md5_fck to fix clk_disable race
  UR: pavel -- I don't see the duplicity. did it remove only copy?
  - f338b8fffd75 ACPI: x86: Call acpi_boot_table_init() after acpi_table_upgrade()
  ACK: pavel
  - e5eb9757fe4c dmaengine: idxd: fix wq cleanup of WQCFG registers
  ACK: pavel
  - 4c59c5c8668e dmaengine: plx_dma: add a missing put_device() on error path
  ACK: pavel
  - ac030f5c5680 dmaengine: Fix a double free in dma_async_device_register
  UR: pavel -- check chan->dev is freed properly?
  - 56f9c04893fb dmaengine: dw: Make it dependent to HAS_IOMEM
  UR: pavel -- PCI should already depend on that?
  - 4ecf25595273 dmaengine: idxd: fix wq size store permission state
  ACK: pavel
  - db23b7b5ca3e dmaengine: idxd: fix opcap sysfs attribute output
  UR: pavel --! ABI change, undocumented, space before newline
  - 0e3f14755111 dmaengine: idxd: fix delta_rec and crc size field for completion record
  ACK: pavel
  - a5ad12d5d69c dmaengine: idxd: Fix clobbering of SWERR overflow bit on writeback
  NAK: pavel -- it uses and where it should be using or
  - f567fde02baa gpio: sysfs: Obey valid_mask
  ACK: pavel -- just an interface tweak
  - dfed481e62e5 Input: nspire-keypad - enable interrupts only when opened
  UR: pavel
  - b80ea54e1e71 mtd: rawnand: mtk: Fix WAITRDY break condition and timeout
  ACK: pavel -- two fixes in one
  - 5a627026be4a net/sctp: fix race condition in sctp_destroy_sock
  UR: pavel -- c/e
