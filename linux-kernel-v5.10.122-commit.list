# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v5.10.122
  - 5754c570a569 Linux 5.10.122
  - 9ba2b4ac3593 tcp: fix tcp_mtup_probe_success vs wrong snd_cwnd
  - 5e34b4975669 dmaengine: idxd: add missing callback function to support DMA_INTERRUPT
  - b8c17121f05b zonefs: fix handling of explicit_open option on mount
  ACK: pavel
  - ef51997771d6 PCI: qcom: Fix pipe clock imbalance
  UR: pavel -- check requisites
  - 63bcb9da91eb md/raid0: Ignore RAID0 layout if the second zone has only one device
  ACK: pavel -- just an interfcace tweak
  - 418db40cc753 interconnect: Restore sync state by ignoring ipa-virt in provider count
  ACK: pavel -- just a power tweak
  - bcae8f8338ab interconnect: qcom: sc7180: Drop IP0 interconnects
  UR: pavel -- check e have dependencies
  - fe6caf512261 powerpc/mm: Switch obsolete dssall to .long
  ACK: pavel -- just a llvm tweak
  - 3be74fc0afbe powerpc/32: Fix overread/overwrite of thread_struct via ptrace
  ACK: pavel
  - fa0d3d71dc08 drm/atomic: Force bridge self-refresh-exit on CRTC switch
  ACK: pavel
  - dbe04e874d4f drm/bridge: analogix_dp: Support PSR-exit to disable transition
  ACK: pavel
  - 61297ee0c329 Input: bcm5974 - set missing URB_NO_TRANSFER_DMA_MAP urb flag
  ACK: pavel
  - 2dba96d19d25 ixgbe: fix unexpected VLAN Rx in promisc mode on VF
  ACK: pavel
  - 91620cded92d ixgbe: fix bcast packets Rx on VF after promisc removal
  ACK: pavel
  - cdd9227373f2 nfc: st21nfca: fix incorrect sizing calculations in EVT_TRANSACTION
  ACK: pavel -- not a minimum fix
  - 54423649bc0e nfc: st21nfca: fix memory leaks in EVT_TRANSACTION handling
  ACK: pavel
  - 4f0a2c46f588 nfc: st21nfca: fix incorrect validating logic in EVT_TRANSACTION
  ACK: pavel
  - c4e4c07d86db net: phy: dp83867: retrigger SGMII AN when link change
  ACK: pavel -- just a hw bug workaround
  - 133c9870cd6b mmc: block: Fix CQE recovery reset success
  ACK: pavel
  - 0248a8c844a4 ata: libata-transport: fix {dma|pio|xfer}_mode sysfs files
  ACK: pavel -- not a minimum fix
  - 471a41320170 cifs: fix reconnect on smb3 mount types
  ACK: pavel
  - 9023ecfd3378 cifs: return errors during session setup during reconnects
  UR: pavel -- c/e, goes to different place?
  - b423cd2a81e8 ALSA: hda/realtek: Fix for quirk to enable speaker output on the Lenovo Yoga DuetITL 2021
  ACK: pavel
  - 94bd216d1718 ALSA: hda/conexant - Fix loopback issue with CX20632
  ACK: pavel
  - 13639c970fdb scripts/gdb: change kernel config dumping method
  ACK: pavel
  - b6ea26873edb vringh: Fix loop descriptors check in the indirect cases
  ACK: pavel
  - 362e3b3a5953 nodemask: Fix return values to be unsigned
  - a262e1255b91 cifs: version operations for smb20 unneeded when legacy support disabled
  - 01137d898039 s390/gmap: voluntarily schedule during key setting
  - f72df77600a4 nbd: fix io hung while disconnecting device
  - 122e4adaff24 nbd: fix race between nbd_alloc_config() and module removal
  - c0868f6e728c nbd: call genl_unregister_family() first in nbd_cleanup()
  - cb8da20d71f9 jump_label,noinstr: Avoid instrumentation for JUMP_LABEL=n builds
  - 320acaf84a64 x86/cpu: Elide KCSAN for cpu_has() and friends
  - 82876878210a modpost: fix undefined behavior of is_arm_mapping_symbol()
  - fee8ae0a0bb6 drm/radeon: fix a possible null pointer dereference
  - 3e5768683022 ceph: allow ceph.dir.rctime xattr to be updatable
  - 7fa8312879f7 Revert "net: af_key: add check for pfkey_broadcast in function pfkey_process"
  - ebfe2797253f scsi: myrb: Fix up null pointer access on myrb_cleanup()
  - 7eb32f286e68 md: protect md_unregister_thread from reentrancy
  - 668c3f9fa2dd watchdog: wdat_wdt: Stop watchdog when rebooting the system
  - e20bc8b5a292 kernfs: Separate kernfs_pr_cont_buf and rename_lock.
  - 1e3b3a5762a9 serial: msm_serial: disable interrupts in __msm_console_write()
  - ff727ab0b7d7 staging: rtl8712: fix uninit-value in r871xu_drv_init()
  - 33ef21d55418 staging: rtl8712: fix uninit-value in usb_read8() and friends
  - f3f754d72d2d clocksource/drivers/sp804: Avoid error on multiple instances
  - abf3b222614f extcon: Modify extcon device to be created after driver data is set
  - 41ec9466944f misc: rtsx: set NULL intfdata when probe fails
  - 5b0c0298f7c3 usb: dwc2: gadget: don't reset gadget's driver->bus
  - 468fe959eab3 sysrq: do not omit current cpu when showing backtrace of all active CPUs
  - f4cb24706ca4 USB: hcd-pci: Fully suspend across freeze/thaw cycle
  - ffe9440d6982 drivers: usb: host: Fix deadlock in oxu_bus_suspend()
  - 6e2273eefab5 drivers: tty: serial: Fix deadlock in sa1100_set_termios()
  - ee105039d365 USB: host: isp116x: check return value after calling platform_get_resource()
  - 0f69d7d5e918 drivers: staging: rtl8192e: Fix deadlock in rtllib_beacons_stop()
  - 66f769762f65 drivers: staging: rtl8192u: Fix deadlock in ieee80211_beacons_stop()
  - cb7147afd328 tty: Fix a possible resource leak in icom_probe
  - d68d5e68b7f6 tty: synclink_gt: Fix null-pointer-dereference in slgt_clean()
  - 61ca1b97adb9 lkdtm/usercopy: Expand size of "out of frame" object
  - 7821d743abb3 iio: st_sensors: Add a local lock for protecting odr
  UR: pavel --! I don't think we need it in stable
  - 5a89a92efc34 staging: rtl8712: fix a potential memory leak in r871xu_drv_init()
  - 8caa4b7d411c iio: dummy: iio_simple_dummy: check the return value of kstrdup()
  pavel -- version of this patch for older release reviewed
  - f091e29ed872 drm: imx: fix compiler warning with gcc-12
  ACK: pavel -- just a cleanup
  - 96bf5ed057df net: altera: Fix refcount leak in altera_tse_mdio_create
  UR: pavel -- check, we may now be doing put() too many times
  - fbeb8dfa8b87 ip_gre: test csum_start instead of transport header
  ACK: pavel
  - 1981cd7a774e net/mlx5: fs, fail conflicting actions
  ACK: pavel
  - 652418d82b7d net/mlx5: Rearm the FW tracer after each tracer event
  ACK: pavel
  - 5d9c1b081ad2 net: ipv6: unexport __init-annotated seg6_hmac_init()
  ACK: pavel -- just a robustness
  - be3884d5cd04 net: xfrm: unexport __init-annotated xfrm4_protocol_init()
  ACK: pavel -- just a robustness
  - 7759c3222815 net: mdio: unexport __init-annotated mdio_bus_init()
  ACK: pavel -- just a robustness
  - b585b87fd5c7 SUNRPC: Fix the calculation of xdr->end in xdr_get_next_encode_buffer()
  ACK: pavel
  - 3d8122e1692b net/mlx4_en: Fix wrong return value on ioctl EEPROM query failure
  ACK: pavel
  - c2ae49a113a5 net: dsa: lantiq_gswip: Fix refcount leak in gswip_gphy_fw_list
  ACK: pavel
  - 0cf7aaff290c bpf, arm64: Clear prog->jited_len along prog->jited
  ACK: pavel -- may be just robustness
  - c61848500a3f af_unix: Fix a data-race in unix_dgram_peer_wake_me().
  ACK: pavel -- just a warning workaround
  - be9581f4fda7 xen: unexport __init-annotated xen_xlate_map_ballooned_pages()
  ACK: pavel -- just a warning fix
  - 86c87d2c0338 netfilter: nf_tables: bail out early if hardware offload is not supported
  ACK: pavel
  - 330c0c6cd215 netfilter: nf_tables: memleak flow rule from commit path
  ACK: pavel
  - 67e2d448733c netfilter: nf_tables: release new hooks on unsupported flowtable flags
  UR: pavel -- c/e
  - 19cb3ece1454 ata: pata_octeon_cf: Fix refcount leak in octeon_cf_probe
  ACK: pavel
  - ec5548066d34 netfilter: nf_tables: always initialize flowtable hook list in transaction
  ACK: pavel -- just a robustness
  - 7fd03e34f01f powerpc/kasan: Force thread size increase with KASAN
  ACK: pavel -- just a config tweak
  - 7a248f9c74f9 netfilter: nf_tables: delete flowtable hooks via transaction list
  ACK: pavel -- just an API tweak
  - 9edafbc7ec29 netfilter: nat: really support inet nat without l3 address
  ACK: pavel
  - 8dbae5affbdb xprtrdma: treat all calls not a bcall when bc_serv is NULL
  UR: pavel -- c/e
  - 8b3d5bafb188 video: fbdev: pxa3xx-gcu: release the resources correctly in pxa3xx_gcu_probe/remove()
  UR: pavel -- c/e, tricky
  - c09b873f3f39 video: fbdev: hyperv_fb: Allow resolutions with size > 64 MB for Gen1
  ACK: pavel -- just enables new configuration
  - 0ee5b9644f06 NFSv4: Don't hold the layoutget locks across multiple RPC calls
  ACK: pavel
  - 95a0ba85c1b5 dmaengine: zynqmp_dma: In struct zynqmp_dma_chan fix desc_size data type
  UR: pavel -- not a minimum fix
  - 2c08cae19d5d m68knommu: fix undefined reference to `_init_sp'
  ACK: pavel
  - d99f04df3236 m68knommu: set ZERO_PAGE() to the allocated zeroed page
  ACK: pavel
  - 344a55ccf5ec i2c: cadence: Increase timeout per message if necessary
  UR: pavel --! easy to overflow?
  - 32bea51fe4c6 f2fs: remove WARN_ON in f2fs_is_valid_blkaddr
  UR: pavel --! that fixes nothing
  - 54c1e0e3bbca iommu/arm-smmu-v3: check return value after calling platform_get_resource()
  UR: pavel -- there's memory leak just below that?
  - 3660db29b030 iommu/arm-smmu: fix possible null-ptr-deref in arm_smmu_device_probe()
  ACK: pavel
  - 9e801c891aa2 tracing: Avoid adding tracer option before update_tracer_options
  ACK: pavel
  - 1788e6dbb612 tracing: Fix sleeping function called from invalid context on RT kernel
  ACK: pavel -- not really problem as rt is not supported on 5.10/4.19
  - 2f452a33067d bootconfig: Make the bootconfig.o as a normal object file
  ACK: pavel -- just a cleanup
  - c667b3872a4c mips: cpc: Fix refcount leak in mips_cpc_default_phys_base
  - 76b226eaf055 dmaengine: idxd: set DMA_INTERRUPT cap bit
  ACK: pavel -- just a feature enablement
  - 32be2b805a1a perf c2c: Fix sorting in percent_rmt_hitm_cmp()
  - 71cbce75031a driver core: Fix wait_for_device_probe() & deferred_probe_timeout interaction
  ACK: pavel
  - b8fac8e32104 tipc: check attribute length for bearer name
  UR: pavel -- c/e, res initialized?
  - c1f018702590 scsi: sd: Fix potential NULL pointer dereference
  ACK: pavel
  - d2e297eaf456 afs: Fix infinite loop found by xfstest generic/676
  ACK: pavel
  - 04622d631826 gpio: pca953x: use the correct register address to do regcache sync
  ACK: pavel
  - 0a0f7f841484 tcp: tcp_rtx_synack() can be called from process context
  ACK: pavel
  - e05dd93826e1 net: sched: add barrier to fix packet stuck problem for lockless qdisc
  ACK: pavel
  - e9fe72b95d7f net/mlx5e: Update netdev features after changing XDP state
  ACK: pavel
  - b50eef7a38ed net/mlx5: correct ECE offset in query qp output
  UR: pavel --! field naming is now very confusing
  - ea5edd015feb net/mlx5: Don't use already freed action pointer
  ACK: pavel
  - bf2af9b24313 sfc: fix wrong tx channel offset with efx_separate_tx_channels
  UR: pavel -- check, do we have requirements?
  - 8f81a4113e1e sfc: fix considering that all channels have TX queues
  ACK: pavel
  - 7ac3a034d96a nfp: only report pause frame configuration for physical device
  ACK: pavel
  - 630e0a10c020 net/smc: fixes for converting from "struct smc_cdc_tx_pend **" to "struct smc_wr_tx_pend_priv *"
  ACK: pavel
  - b97550e380ca riscv: read-only pages should not be writable
  ACK: pavel
  - 8f49e1694cbc bpf: Fix probe read error in ___bpf_prog_run()
  ACK: pavel
  - 6d8d3f68cbec ubi: ubi_create_volume: Fix use-after-free when volume creation failed
  UR: pavel -- introduces memory leak?
  - f413e4d7cdf3 ubi: fastmap: Fix high cpu usage of ubi_bgt by making sure wl_pool not empty
  UR: pavel
  - 3252d327f977 jffs2: fix memory leak in jffs2_do_fill_super
  ACK: pavel
  - 741e49eacdcd modpost: fix removing numeric suffixes
  UR: pavel -- explicitely says not for stable
  - 42658e47f1ab net: dsa: mv88e6xxx: Fix refcount leak in mv88e6xxx_mdios_register
  ACK: pavel
  - f7ba2cc57f40 net: ethernet: ti: am65-cpsw-nuss: Fix some refcount leaks
  ACK: pavel
  - 71ae30662ec6 net: ethernet: mtk_eth_soc: out of bounds read in mtk_hwlro_get_fdir_entry()
  UR: pavel --! does it need to use nospec variant?
  - 503a3fd6466d net: sched: fixed barrier to prevent skbuff sticking in qdisc backlog
  ACK: pavel
  - ee89d7fd49de s390/crypto: fix scatterwalk_unmap() callers in AES-GCM
  - e892a7e60f1f clocksource/drivers/oxnas-rps: Fix irq_of_parse_and_map() return value
  ACK: pavel
  - 1d7361679f0a ASoC: fsl_sai: Fix FSL_SAI_xDR/xFR definition
  UR: pavel --! wtf?
  - 910b1cdf6c50 watchdog: ts4800_wdt: Fix refcount leak in ts4800_wdt_probe
  ACK: pavel
  - b3354f2046cc watchdog: rti-wdt: Fix pm_runtime_get_sync() error checking
  ACK: pavel
  - 36ee9ffca8ef driver core: fix deadlock in __device_attach
  ACK: pavel
  - 823f24f2e329 driver: base: fix UAF when driver_attach failed
  ACK: pavel
  - 7a6337bfedc5 bus: ti-sysc: Fix warnings for unbind for serial
  ACK: pavel
  - 985706bd3bbe firmware: dmi-sysfs: Fix memory leak in dmi_sysfs_register_handle
  ACK: pavel
  - 94acaaad470e serial: stm32-usart: Correct CSIZE, bits, and parity
  ACK: pavel
  - b7e560d2ffbe serial: st-asc: Sanitize CSIZE and correct PARENB for CS7
  ACK: pavel
  - afcfc3183cfd serial: sifive: Sanitize CSIZE and c_iflag
  ACK: pavel
  - a9f6bee486e7 serial: sh-sci: Don't allow CS5-6
  ACK: pavel
  - 00456b932e16 serial: txx9: Don't allow CS5-6
  ACK: pavel
  - 22e975796f89 serial: rda-uart: Don't allow CS5-6
  ACK: pavel
  - ff4ce2979b5d serial: digicolor-usart: Don't allow CS5-6
  ACK: pavel
  - 5cd331bcf094 serial: 8250_fintek: Check SER_RS485_RTS_* only with RS485
  ACK: pavel -- just an API tweak
  - 260792d5c9d6 serial: meson: acquire port->lock in startup()
  ACK: pavel
  - 82bfea344e8f rtc: mt6397: check return value after calling platform_get_resource()
  ACK: pavel
  - d54a51b51851 clocksource/drivers/riscv: Events are stopped during CPU suspend
  ACK: pavel
  - 5b3e990f85eb soc: rockchip: Fix refcount leak in rockchip_grf_init
  ACK: pavel
  - cfe8a0967d6e extcon: ptn5150: Add queue work sync before driver release
  ACK: pavel
  - 96414e2cdc28 coresight: cpu-debug: Replace mutex with mutex_trylock on panic notifier
  ACK: pavel
  - 47ebc50dc2a7 serial: sifive: Report actual baud base rather than fixed 115200
  ACK: pavel
  - ab35308bbd33 phy: qcom-qmp: fix pipe-clock imbalance on power-on failure
  UR: pavel
  - 52f327a45c5b rpmsg: qcom_smd: Fix returning 0 if irq_of_parse_and_map() fails
  ACK: pavel
  - c10333c4519a iio: adc: sc27xx: Fine tune the scale calibration values
  ACK: pavel -- not a minimum fix
  - 3747429834d2 iio: adc: sc27xx: fix read big scale voltage not right
  ACK: pavel
  - b30f2315a3a6 iio: proximity: vl53l0x: Fix return value check of wait_for_completion_timeout
  ACK: pavel
  - 43823ceb26e6 iio: adc: stmpe-adc: Fix wait_for_completion_timeout return value check
  ACK: pavel -- cleanup, not a bugfix
  - 6f01c0fb8e44 usb: typec: mux: Check dev_set_name() return value
  UR: pavel -- c/e
  - 7027c890ff6b firmware: stratix10-svc: fix a missing check on list iterator
  UR: pavel -- where does the free of list head appeared?
  - 70ece3c5ec4f misc: fastrpc: fix an incorrect NULL check on list iterator
  ACK: pavel
  - 2a1bf8e5ad61 usb: dwc3: pci: Fix pm_runtime_get_sync() error checking
  ACK: pavel
  - 8ae4fed195c0 rpmsg: qcom_smd: Fix irq_of_parse_and_map() return value
  ACK: pavel
  - 572211d631d7 pwm: lp3943: Fix duty calculation in case period was clamped
  ACK: pavel
  - f9782b26d6f1 staging: fieldbus: Fix the error handling path in anybuss_host_common_probe()
  - b382c0c3b8cc usb: musb: Fix missing of_node_put() in omap2430_probe
  ACK: pavel
  - 6b7cf2212223 USB: storage: karma: fix rio_karma_init return
  UR: pavel --! why GFP_NOIO? Why eat return value? Memory leak!
  - e100742823c3 usb: usbip: add missing device lock on tweak configuration cmd
  ACK: pavel
  - bcbb795a9e78 usb: usbip: fix a refcount leak in stub_probe()
  ACK: pavel
  - 4e3a2d77bd0b tty: serial: fsl_lpuart: fix potential bug when using both of_alias_get_id and ida_simple_get
  ACK: pavel -- just an interface tweak
  - e27376f5aade tty: n_tty: Restore EOF push handling behavior
  ACK: pavel
  - 11bc6eff3abc tty: serial: owl: Fix missing clk_disable_unprepare() in owl_uart_probe
  ACK: pavel
  - ee6c33b29e62 tty: goldfish: Use tty_port_destroy() to destroy port
  pavel -- version of this patch for older release reviewed
  - 56ac04f35fc5 lkdtm/bugs: Check for the NULL pointer after calling kmalloc
  ACK: pavel -- makes test silently terminate in oom case
  - 03efa70eb0ee iio: adc: ad7124: Remove shift from scan_type
  ACK: pavel
  - 4610b067615f staging: greybus: codecs: fix type confusion of list iterator variable
  - 1509d2335db8 pcmcia: db1xxx_ss: restrict to MIPS_DB1XXX boards
  ACK: pavel
