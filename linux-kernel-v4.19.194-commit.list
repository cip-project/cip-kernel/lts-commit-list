# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v4.19.194
  - 9a2dc0e6c531 Linux 4.19.194
  - ed6a024f4888 xen-pciback: redo VF placement in the virtual topology
  - b5cd7f229609 sched/fair: Optimize select_idle_cpu
  - f47c2c06982e ACPI: EC: Look for ECDT EC after calling acpi_load_tables()
  - eeb48f5ca3ab ACPI: probe ECDT before loading AML tables regardless of module-level code flag
  - 2ca5f9f13a3e KVM: arm64: Fix debug register indexing
  ACK: pavel
  - 982903d43ecb KVM: SVM: Truncate GPR value for DR and CR accesses in !64-bit mode
  ACK: pavel
  - 6b678e02e63e btrfs: fix unmountable seed device after fstrim
  ACK: pavel -- not a minimum fix
  - c31789645b7b perf/core: Fix corner case in perf_rotate_context()
  - 0b4c9255a1d0 perf/cgroups: Don't rotate events for cgroups unnecessarily
  - c35461390b48 bnxt_en: Remove the setting of dev_port.
  - 1d80154040c2 selftests/bpf: Avoid running unprivileged tests with alignment requirements
  - 0c4acb93972e selftests/bpf: add "any alignment" annotation for some tests
  - 3789f9c3a4f5 bpf: Apply F_NEEDS_EFFICIENT_UNALIGNED_ACCESS to more ACCEPT test cases.
  - f22c1cd341cd bpf: Make more use of 'any' alignment in test_verifier.c
  - 1e7ee04b035a bpf: Adjust F_NEEDS_EFFICIENT_UNALIGNED_ACCESS handling in test_verifier.c
  - 878470e7f5ed bpf: Add BPF_F_ANY_ALIGNMENT.
  - b6c9e3b46c3a selftests/bpf: Generalize dummy program types
  - ac0985c8a2d8 bpf: test make sure to run unpriv test cases in test_verifier
  - 1b5c4b0669e0 bpf: fix test suite to enable all unpriv program types
  - 7de60c2d5a2a mm, hugetlb: fix simple resv_huge_pages underflow on UFFDIO_COPY
  ACK: pavel
  - c5cfa8156217 btrfs: fixup error handling in fixup_inode_link_counts
  UR: pavel
  - adaafc32d8b0 btrfs: return errors from btrfs_del_csums in cleanup_ref_head
  ACK: pavel
  - 8299bb94fae9 btrfs: fix error handling in btrfs_del_csums
  UR: pavel -- not nearly a minimal fix
  - 543a6f5284b4 btrfs: mark ordered extent and inode with error if we fail to finish
  ACK: pavel
  - 7e25cb1b22f8 x86/apic: Mark _all_ legacy interrupts when IO/APIC is missing
  ACK: pavel
  - 93e4ac2a9979 nfc: fix NULL ptr dereference in llcp_sock_getname() after failed connect
  - cec4e857ffaa ocfs2: fix data corruption by fallocate
  UR: pavel
  - d106f05432e6 pid: take a reference when initializing `cad_pid`
  ACK: pavel
  - 2132a28807cf usb: dwc2: Fix build in periphal-only mode
  ACK: pavel
  - 569496aa3776 ext4: fix bug on in ext4_es_cache_extent as ext4_split_extent_at failed
  UR: pavel
  - 1294a5d725e8 ALSA: hda: Fix for mute key LED for HP Pavilion 15-CK0xx
  ACK: pavel
  - db6e9d1cc260 ALSA: timer: Fix master timer notification
  ACK: pavel
  - cb7bb81ac98d HID: multitouch: require Finger field to mark Win8 reports as MT
  ACK: pavel
  - 9ea0ab48e755 net: caif: fix memory leak in cfusbl_device_notify
  ACK: pavel
  - 3be863c11cab net: caif: fix memory leak in caif_device_notify
  ACK: pavel
  - 758f725c392e net: caif: add proper error handling
  ACK: pavel
  - ef1461e1198e net: caif: added cfserl_release function
  ACK: pavel -- preparation, not a bugfix
  - 2b9e9c2ed0f1 Bluetooth: use correct lock to prevent UAF of hdev object
  UR: pavel -- c/e
  - 64700748e8a7 Bluetooth: fix the erroneous flush_work() order
  ACK: pavel
  - a13a42c573a4 tipc: fix unique bearer names sanity check
  UR: pavel
  - 65281d6aeca7 tipc: add extack messages for bearer/media failure
  ACK: pavel -- just adds error messages
  - 9144f434bebd ixgbevf: add correct exception tracing for XDP
  ACK: pavel -- wow, that code is ... spagethi
  - 60d59c522352 ieee802154: fix error return code in ieee802154_llsec_getparams()
  ACK: pavel
  - 813a23e0f130 ieee802154: fix error return code in ieee802154_add_iface()
  ACK: pavel
  - 8aed10cd9497 netfilter: nfnetlink_cthelper: hit EBUSY on updates if size mismatches
  ACK: pavel
  - 85e8c3b43f47 HID: i2c-hid: fix format string mismatch
  UR: pavel -- cast + %h removal? Should one be enough?
  - 173da500cfce HID: pidff: fix error return code in hid_pidff_init()
  ACK: pavel
  - 6895ac910b73 ipvs: ignore IP_VS_SVC_F_HASHED flag when adding service
  ACK: pavel
  - 2bd07ebcb949 vfio/platform: fix module_put call in error flow
  ACK: pavel
  - f12bd9caafed samples: vfio-mdev: fix error handing in mdpy_fb_probe()
  ACK: pavel -- two fixes in one
  - 2b2ca3ee36e4 vfio/pci: zap_vma_ptes() needs MMU
  ACK: pavel
  - c953aee0d8a1 vfio/pci: Fix error return code in vfio_ecap_init()
  ACK: pavel
  - dd47a33e11fd efi: cper: fix snprintf() use in cper_dimm_err_location()
  ACK: pavel
  - 5fdb418b14e4 efi: Allow EFI_MEMORY_XP and EFI_MEMORY_RO both to be cleared
  ACK: pavel -- theoretical bug, not sure this has user-visible effects
  - 1e9cd487b04f nl80211: validate key indexes for cfg80211_registered_device
  UR: pavel -- same commit in 5.10.3
  - 3f9186ee7a30 ALSA: usb: update old-style static const declaration
  ACK: pavel -- just a warning fix
  - 24ea2de9c342 net: usb: cdc_ncm: don't spew notifications
  ACK: pavel
