# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v4.19.141
  - d18b78abc0c6 Linux 4.19.141
  - 2c1684171564 drm/amdgpu: Fix bug where DPM is not enabled after hibernate and resume
  UR: pavel --! codingstyle!
  - bfdd85969948 drm: Added orientation quirk for ASUS tablet model T103HAF
  ACK: pavel
  - c4368ab3ef78 arm64: dts: marvell: espressobin: add ethernet alias
  ACK: pavel
  - 2406c45db3de khugepaged: retract_page_tables() remember to test exit
  ACK: pavel
  - 014ec97717f4 sh: landisk: Add missing initialization of sh_io_port_base
  ACK: pavel
  - df8caaf9ef87 tools build feature: Quote CC and CXX for their arguments
  ACK: pavel
  - 3b87dc3e0ba6 perf bench mem: Always memset source before memcpy
  ACK: pavel
  - 9a1b7ced0fd9 ALSA: echoaudio: Fix potential Oops in snd_echo_resume()
  ACK: pavel
  - 59547851489e mfd: dln2: Run event handler loop under spinlock
  NAK: pavel -- this fixes problems caused by 36dc01657b49, but that is not in 4.19-rc tree
  - 8e69ac04403c test_kmod: avoid potential double free in trigger_config_run_type()
  ACK: pavel
  - aefe207d95d0 fs/ufs: avoid potential u32 multiplication overflow
  ACK: pavel
  - 9f3fb90d30db fs/minix: remove expected error message in block_to_path()
  ACK: pavel -- just a printk tweak
  - c7ac366be04e fs/minix: fix block limit check for V1 filesystems
  UR: pavel --! round down seems correct
  - 95fc841ea8b6 fs/minix: set s_maxbytes correctly
  ACK: pavel
  - a906b868953a nfs: Fix getxattr kernel panic and memory overflow
  UR: pavel --! label->label may not be initialized, and it may not be zero terminated...?
  - c08be09b2c1c net: qcom/emac: add missed clk_disable_unprepare in error path of emac_clks_phase1_init
  ACK: pavel
  - ae71f731f061 drm/vmwgfx: Fix two list_for_each loop exit tests
  ACK: pavel
  - 3cfd94ed90ee drm/vmwgfx: Use correct vmw_legacy_display_unit pointer
  ACK: pavel
  - 27a545d597dd Input: sentelic - fix error return when fsp_reg_write fails
  ACK: pavel
  - ddf2b7891323 watchdog: initialize device before misc_register
  ACK: pavel -- but code needs more fixes
  - fe9a71128e3d scsi: lpfc: nvmet: Avoid hang / use-after-free again when destroying targetport
  ACK: pavel
  - ff62a4140372 openrisc: Fix oops caused when dumping stack
  UR: pavel --! error handling?
  - 49b30a64d320 i2c: rcar: avoid race when unregistering slave
  ACK: pavel
  - bb1da23aa45b tools build feature: Use CC and CXX from parent
  ACK: pavel
  - 17dc3213fbc0 pwm: bcm-iproc: handle clk_get_rate() return
  ACK: pavel
  - f0b9f54f4763 clk: clk-atlas6: fix return value check in atlas6_clk_init()
  ACK: pavel
  - 7fc9f681fad2 i2c: rcar: slave: only send STOP event when we have been addressed
  ACK: pavel
  - a5040830b489 iommu/vt-d: Enforce PASID devTLB field mask
  ACK: pavel -- robustness, is not known to fix a concrete bug
  - b9b2092af3c1 iommu/omap: Check for failure of a call to omap_iommu_dump_ctx
  ACK: pavel
  - 0b80d3cdb0fe selftests/powerpc: ptrace-pkey: Don't update expected UAMOR value
  IGN: pavel
  - a075f690c28e selftests/powerpc: ptrace-pkey: Update the test to mark an invalid pkey correctly
  IGN: pavel
  - 9f00bbb7a21a selftests/powerpc: ptrace-pkey: Rename variables to make it easier to follow code
  IGN: pavel -- cleanup, not a bugfix
  - a24e10abad8f dm rq: don't call blk_mq_queue_stopped() in dm_stop_queue()
  ACK: pavel -- not sure this causes any user-visible bug
  - c00c5131441c gpu: ipu-v3: image-convert: Combine rotate/no-rotate irq handlers
  NAK: pavel -- cleanup, not a bugfix. Prepares for patch that is not there in 4.19
  - 388a802632cf mmc: renesas_sdhi_internal_dmac: clean up the code for dma complete
  ACK: pavel -- just a cleanup
  - e9bca40189ae USB: serial: ftdi_sio: clean up receive processing
  ACK: pavel -- cleanup and preparation for next patch, not a bugfix
  - fe34945c7898 USB: serial: ftdi_sio: make process-packet buffer unsigned
  ACK: pavel -- cleanup and preparation for next patch, not a bugfix
  - d7ee731744d7 media: rockchip: rga: Only set output CSC mode for RGB input
  ACK: pavel
  - a676d83b8f87 media: rockchip: rga: Introduce color fmt macros and refactor CSC mode logic
  ACK: pavel -- cleanup, preparation for next patch
  - 2cb3b14eb6b2 RDMA/ipoib: Fix ABBA deadlock with ipoib_reap_ah()
  IGN: pavel
  - 0bcab21cf6ee RDMA/ipoib: Return void from ipoib_ib_dev_stop()
  ACK: pavel -- cleanup, preparing for next patch?
  - d12296621cf9 mfd: arizona: Ensure 32k clock is put on driver unbind and error
  ACK: pavel -- just a warning and pm fix
  - 87a56a59ad24 drm/imx: imx-ldb: Disable both channels for split mode in enc->disable()
  ACK: pavel
  - 7d3146f3b1b7 remoteproc: qcom: q6v5: Update running state before requesting stop
  ACK: pavel
  - 721df98627dc perf intel-pt: Fix FUP packet state
  ACK: pavel
  - cec9fbfe3959 module: Correctly truncate sysfs sections output
  ACK: pavel
  - 73f74fc311d4 pseries: Fix 64 bit logical memory block panic
  ACK: pavel
  - e1462b5e6a05 watchdog: f71808e_wdt: clear watchdog timeout occurred flag
  ACK: pavel
  - 49d0707efdad watchdog: f71808e_wdt: remove use of wrong watchdog_info option
  ACK: pavel
  - 203dbe7cda02 watchdog: f71808e_wdt: indicate WDIOF_CARDRESET support in watchdog_info.options
  ACK: pavel
  - 2c98c4a0c351 tracing: Use trace_sched_process_free() instead of exit() for pid tracing
  ACK: pavel
  - b3b77736dd51 tracing/hwlat: Honor the tracing_cpumask
  ACK: pavel
  - 46c9d3925ab0 kprobes: Fix NULL pointer dereference at kprobe_ftrace_handler
  ACK: pavel
  - 892fd3637a2c ftrace: Setup correct FTRACE_FL_REGS flags for module
  ACK: pavel
  - e88a72e86bd0 mm/page_counter.c: fix protection usage propagation
  ACK: pavel
  - 73cbb8af7e8a ocfs2: change slot number type s16 to u16
  ACK: pavel -- just a warning fix
  - 41d71ef2e791 ext2: fix missing percpu_counter_inc
  ACK: pavel -- just a performance optimalization, not a minimum fix
  - baa5bd366835 MIPS: CPU#0 is not hotpluggable
  ACK: pavel -- just an API tweak
  - 706695d477fb driver core: Avoid binding drivers to dead devices
  ACK: pavel
  - 4cf1d191f77f mac80211: fix misplaced while instead of if
  ACK: pavel
  - 2a72c283319c bcache: fix overflow in offset_to_stripe()
  UR: pavel --! should it to BUG_ON in case of error?
  - d6e2394ce6c9 bcache: allocate meta data pages as compound pages
  ACK: pavel -- not sure if it fixes real bug
  - 566cba3c7d1f md/raid5: Fix Force reconstruct-write io stuck in degraded raid5
  ACK: pavel
  - f90339a4eccf net/compat: Add missing sock updates for SCM_RIGHTS
  ACK: pavel
  - c334db67ebb3 net: stmmac: dwmac1000: provide multicast filter fallback
  ACK: pavel
  - 26f0092f35e2 net: ethernet: stmmac: Disable hardware multicast filter
  ACK: pavel
  - 62f8d7140899 media: vsp1: dl: Fix NULL pointer dereference on unbind
  ACK: pavel
  - e83f99c42800 powerpc: Fix circular dependency between percpu.h and mmu.h
  ACK: pavel
  - b11ac8328081 powerpc: Allow 4224 bytes of stack expansion for the signal frame
  UR: pavel --! put sigframe_max_size into header, then build_bug_on()? 
  - d9710cc6bd97 cifs: Fix leak when handling lease break for cached root fid
  UR: pavel --! what about error handling in smb2_queue_pending_open_break()? The job does not need doing at all?
  - 6ffc89cadbd0 xtensa: fix xtensa_pmu_setup prototype
  ACK: pavel
  - b86f06e13cc6 iio: dac: ad5592r: fix unbalanced mutex unlocks in ad5592r_read_raw()
  ACK: pavel
  - 0d4abc3512b0 dt-bindings: iio: io-channel-mux: Fix compatible string in example code
  ACK: pavel -- just a documentation fix
  - a34b58b5b43b btrfs: fix return value mixup in btrfs_get_extent
  ACK: pavel
  - 183af2d27dfc btrfs: fix memory leaks after failure to lookup checksums during inode logging
  ACK: pavel
  - 627fa9d8071d btrfs: only search for left_info if there is no right_info in try_merge_free_space
  ACK: pavel
  - 7c1ddfc98703 btrfs: fix messages after changing compression level by remount
  UR: pavel --!a just a printk tweaks !! fix english?
  - 35b4a28051b2 btrfs: open device without device_list_mutex
  ACK: pavel
  - fa511954694c btrfs: don't traverse into the seed devices in show_devname
  ACK: pavel -- just a tweak for confusing interface
  - 6bf983c8db01 btrfs: ref-verify: fix memory leak in add_block_entry
  ACK: pavel
  - 8eadf67bc216 btrfs: don't allocate anonymous block device for user invisible roots
  ACK: pavel
  - 3b5318a963bd btrfs: free anon block device right after subvolume deletion
  ACK: pavel
  - 54a7a9d75c07 PCI: Probe bridge window attributes once at enumeration-time
  ACK: pavel
  - dd6dc2fd6682 PCI: qcom: Add support for tx term offset for rev 2.1.0
  ACK: pavel
  - 56e2a4456647 PCI: qcom: Define some PARF params needed for ipq8064 SoC
  ACK: pavel
  - ae33b1ebbce8 PCI: Add device even if driver attach failed
  ACK: pavel
  - 71c6716cb61a PCI: Mark AMD Navi10 GPU rev 0x00 ATS as broken
  ACK: pavel
  - c59ea9bde42e PCI: hotplug: ACPI: Fix context refcounting in acpiphp_grab_context()
  ACK: pavel
  - 5c4d9eefd314 genirq/affinity: Make affinity setting if activated opt-in
  ACK: pavel
  - f4d84941832f smb3: warn on confusing error scenario with sec=krb5
  ACK: pavel -- just a printk tweak
