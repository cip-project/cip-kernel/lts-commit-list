# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v5.10.112
  - 1052f9bce629 Linux 5.10.112
  - 5c62d3bf1410 ax25: Fix UAF bugs in ax25 timers
  ACK: pavel
  - f934fa478dd1 ax25: Fix NULL pointer dereferences in ax25 timers
  ACK: pavel
  - 145ea8d213e8 ax25: fix NPD bug in ax25_disconnect
  ACK: pavel
  - a4942c6fea87 ax25: fix UAF bug in ax25_send_control()
  ACK: pavel
  - b20a5ab0f5fb ax25: Fix refcount leaks caused by ax25_cb_del()
  UR: pavel --a still buggy, fixed by next patch
  - 57cc15f5fd55 ax25: fix UAF bugs of net_device caused by rebinding operation
  UR: pavel -- buggy, fixed by next patch
  - 5ddae8d06441 ax25: fix reference count leaks of ax25_dev
  UR: pavel
  - 5ea00fc60676 ax25: add refcount in ax25_dev to avoid UAF bugs
  UR: pavel -- two puts in a row? buggy, fixed by next patch
  - 361288633bfa scsi: iscsi: Fix unbound endpoint error handling
  UR: pavel
  - 129db30599bc scsi: iscsi: Fix endpoint reuse regression
  UR: pavel
  - 26f827e095ab dma-direct: avoid redundant memory sync for swiotlb
  ACK: pavel -- just a performance tweak
  - 9a5a4d23e24d timers: Fix warning condition in __run_timers()
  ACK: pavel -- just a WARN fix
  - 84837f43e56f i2c: pasemi: Wait for write xfers to finish
  - 89496d80bf84 smp: Fix offline cpu check in flush_smp_call_function_queue()
  ACK: pavel -- just a robustness
  - cd02b2687d66 dm integrity: fix memory corruption when tag_size is less than digest size
  UR: pavel -- check for overflows?
  - 0a312ec66a03 ARM: davinci: da850-evm: Avoid NULL pointer dereference
  - 0806f1930562 tick/nohz: Use WARN_ON_ONCE() to prevent console saturation
  ACK: pavel -- just a debugging improvement
  - 0275c75955d1 genirq/affinity: Consider that CPUs on nodes can be unbalanced
  ACK: pavel
  - 1fcfe37d170a drm/amdgpu: Enable gfxoff quirk on MacBook Pro
  ACK: pavel
  - 68ae52efa132 drm/amd/display: don't ignore alpha property on pre-multiplied mode
  ACK: pavel -- just an interface tweak
  - a263712ba8c9 ipv6: fix panic when forwarding a pkt with no in6 dev
  ACK: pavel
  - 659214603bf2 nl80211: correctly check NL80211_ATTR_REG_ALPHA2 size
  ACK: pavel
  - 912797e54c99 ALSA: pcm: Test for "silence" field in struct "pcm_format_data"
  ACK: pavel
  - 48d070ca5e7e ALSA: hda/realtek: add quirk for Lenovo Thinkpad X12 speakers
  ACK: pavel
  - 163e16247130 ALSA: hda/realtek: Add quirk for Clevo PD50PNT
  - 5e4dd1799883 btrfs: mark resumed async balance as writing
  ACK: pavel
  - 1d2eda18f6ff btrfs: fix root ref counts in error handling in btrfs_get_root_ref
  ACK: pavel
  - 9b7ec35253c9 ath9k: Fix usage of driver-private space in tx_info
  ACK: pavel
  - 0f65cedae500 ath9k: Properly clear TX status area before reporting to mac80211
  pavel -- version of this patch for older release reviewed
  - cc21ae932656 gcc-plugins: latent_entropy: use /dev/urandom
  - c089ffc846c8 memory: renesas-rpc-if: fix platform-device leak in error path
  ACK: pavel
  - 342454231ee5 KVM: x86/mmu: Resolve nx_huge_pages when kvm.ko is loaded
  - 06c348fde545 mm: kmemleak: take a full lowmem check in kmemleak_*_phys()
  - 20ed94f8181a mm: fix unexpected zeroed page mapping with zram swap
  UR: pavel --! says 4.14+ but is not in 4.19; a
  - 192e507ef894 mm, page_alloc: fix build_zonerefs_node()
  ACK: pavel
  - 000b3921b4d5 perf/imx_ddr: Fix undefined behavior due to shift overflowing the constant
  - ca24c5e8f0ac drivers: net: slip: fix NPD bug in sl_tx_timeout()
  ACK: pavel
  - e8cf1e4d953d scsi: megaraid_sas: Target with invalid LUN ID is deleted during scan
  UR: pavel -- c/e
  - 5b7ce74b6bc8 scsi: mvsas: Add PCI ID of RocketRaid 2640
  ACK: pavel
  - 4b44cd584057 drm/amd/display: Fix allocate_mst_payload assert on resume
  ACK: pavel
  - 34ea097fb63d drm/amd/display: Revert FEC check in validation
  ACK: pavel
  - fa5ee7c4232c myri10ge: fix an incorrect free for skb in myri10ge_sw_tso
  ACK: pavel
  - d90df6da50c5 net: usb: aqc111: Fix out-of-bounds accesses in RX fixup
  ACK: pavel
  - 9c12fcf1d864 net: axienet: setup mdio unconditionally
  ACK: pavel
  - b643807a735e tlb: hugetlb: Add more sizes to tlb_remove_huge_tlb_entry
  ACK: pavel -- static inline would be cleaner
  - 98973d2bdd4a arm64: alternatives: mark patch_alternative() as `noinstr`
  ACK: pavel
  - 2462faffbfa5 regulator: wm8994: Add an off-on delay for WM8994 variant
  ACK: pavel
  - aa8cdedaf760 gpu: ipu-v3: Fix dev_dbg frequency output
  ACK: pavel -- just a printk tweak
  - 150fe861c57c ata: libata-core: Disable READ LOG DMA EXT for Samsung 840 EVOs
  ACK: pavel
  - 1ff5359afa5e net: micrel: fix KS8851_MLL Kconfig
  UR: pavel --! we probably don't have that commit
  - d3478709edf2 scsi: ibmvscsis: Increase INITIAL_SRP_LIMIT to 1024
  UR: pavel --a just a performance tweak? check if we have the commit
  - b9a110fa755b scsi: lpfc: Fix queue failures when recovering from PCI parity error
  ACK: pavel
  - aec36b98a1bb scsi: target: tcmu: Fix possible page UAF
  ACK: pavel
  - 43666798059c Drivers: hv: vmbus: Prevent load re-ordering when reading ring buffer
  ACK: pavel
  - 1d7a5aae884c drm/amdkfd: Check for potential null return of kmalloc_array()
  UR: pavel -- c/e
  - e5afacc826a8 drm/amdgpu/vcn: improve vcn dpg stop procedure
  - d2e0931e6d84 drm/amdkfd: Fix Incorrect VMIDs passed to HWS
  - 7fc0610ad818 drm/amd/display: Update VTEM Infopacket definition
  - 6906e05cf3ad drm/amd/display: FEC check in timing validation
  - 756c61c1680f drm/amd/display: fix audio format not updated after edid updated
  - 76e086ce7b2d btrfs: do not warn for free space inode in cow_file_range
  ACK: pavel -- just a WARN removal
  - 217190dc66ef btrfs: fix fallocate to use file_modified to update permissions consistently
  ACK: pavel -- just a hardening
  - 9b5d1b3413d7 drm/amd: Add USBC connector ID
  UR: pavel --!a just adds useless define
  - 6f9c06501d28 net: bcmgenet: Revert "Use stronger register read/writes to assure ordering"
  UR: pavel --! removes gcc12 workaround
  - 504c15f07f54 dm mpath: only use ktime_get_ns() in historical selector
  - 4e166a41180b cifs: potential buffer overflow in handling symlinks
  ACK: pavel
  - 67677050cecb nfc: nci: add flush_workqueue to prevent uaf
  - bfba9722cf2e perf tools: Fix misleading add event PMU debug message
  - 280f721edc54 testing/selftests/mqueue: Fix mq_perf_tests to free the allocated cpu set
  - eb8873b324d9 sctp: Initialize daddr on peeled off socket
  ACK: pavel
  - 45226fac4d31 scsi: iscsi: Fix conn cleanup and stop race during iscsid restart
  ACK: pavel
  - 73805795c99f scsi: iscsi: Fix offload conn cleanup when iscsid restarts
  UR: pavel -- lockin is rather crazy here
  - 699bd835c36e scsi: iscsi: Move iscsi_ep_disconnect()
  ACK: pavel -- just a preparation
  - 46f37a34a53d scsi: iscsi: Fix in-kernel conn failure handling
  UR: pavel
  - 812573896711 scsi: iscsi: Rel ref after iscsi_lookup_endpoint()
  UR: pavel
  - 22608545b834 scsi: iscsi: Use system_unbound_wq for destroy_work
  ACK: pavel -- just a cleanup
  - 4029a1e992fc scsi: iscsi: Force immediate failure during shutdown
  ACK: pavel -- just an interface tweak
  - 17d14456f626 scsi: iscsi: Stop queueing during ep_disconnect
  ACK: pavel -- new infrastructure to fix uncommon bug
  - da9cf24aa739 scsi: pm80xx: Enable upper inbound, outbound queues
  ACK: pavel -- just a support for more queues
  - e08d26971237 scsi: pm80xx: Mask and unmask upper interrupt vectors 32-63
  ACK: pavel -- just a preparation
  - 35b91e49bc80 net/smc: Fix NULL pointer dereference in smc_pnet_find_ib()
  ACK: pavel
  - 98a7f6c4ada4 drm/msm/dsi: Use connector directly in msm_dsi_manager_connector_init()
  ACK: pavel
  - 5f78ad93837c drm/msm: Fix range size vs end confusion
  ACK: pavel
  - 5513f9a0b068 cfg80211: hold bss_lock while updating nontrans_list
  ACK: pavel
  - a44938950e5e net/sched: taprio: Check if socket flags are valid
  ACK: pavel
  - 08d5e3e95453 net: ethernet: stmmac: fix altr_tse_pcs function when using a fixed-link
  ACK: pavel
  - 2ad9d890d850 net: dsa: felix: suppress -EPROBE_DEFER errors
  - f2cc341fcc42 net/sched: fix initialization order when updating chain 0 head
  ACK: pavel
  - 7a7cf8414841 mlxsw: i2c: Fix initialization error flow
  - 43e58e119a2b net: mdio: Alphabetically sort header inclusion
  UR: pavel --!
  - 9709c8b5cdc8 gpiolib: acpi: use correct format characters
  ACK: pavel -- just a warning workaround
  - d67c900f1947 veth: Ensure eth header is in skb's linear part
  ACK: pavel
  - 845f44ce3d9f net/sched: flower: fix parsing of ethertype following VLAN header
  ACK: pavel -- just an interface tweak
  - 85ee17ca21cf SUNRPC: Fix the svc_deferred_event trace class
  ACK: pavel
  - af12dd71235c media: rockchip/rga: do proper error checking in probe
  ACK: pavel
  - 563712971202 firmware: arm_scmi: Fix sorting of retrieved clock rates
  ACK: pavel
  - 16c628b0c6fa memory: atmel-ebi: Fix missing of_node_put in atmel_ebi_probe
  ACK: pavel
  - cb66641f8106 drm/msm: Add missing put_task_struct() in debugfs path
  ACK: pavel
  - 921fdc45a084 btrfs: remove unused variable in btrfs_{start,write}_dirty_block_groups()
  ACK: pavel -- just a warning fix
  - 5d131318bb87 ACPI: processor idle: Check for architectural support for LPI
  UR: pavel -- should stop on any error?
  - 503934df3108 cpuidle: PSCI: Move the `has_lpi` check to the beginning of the function
  ACK: pavel
  - cfa98ffc42f1 hamradio: remove needs_free_netdev to avoid UAF
  ACK: pavel -- just fixes up unneccessary error introduced by previous patch
  - 80a4df14643f hamradio: defer 6pack kfree after unregister_netdev
  UR: pavel --! suspect, buggy?
  - f0c31f192f38 drm/amdkfd: Use drm_priv to pass VM from KFD to amdgpu
  ACK: pavel
