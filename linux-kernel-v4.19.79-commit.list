# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v4.19.79
  - dafd634415a7 Linux 4.19.79
  - 1bd17a737c9e nl80211: validate beacon head
  - 527ba5d7634b cfg80211: Use const more consistently in for_each_element macros
  UR:pavel -- cleanup const, not a bugfix
  - ad180cace853 cfg80211: add and use strongly typed element iteration macros
  UR:pavel -- why?
  - 3dab5ba6d7ae staging: erofs: detect potential multiref due to corrupted images
  IGN: pavel
  - 8b4341f9b80b staging: erofs: add two missing erofs_workgroup_put for corrupted images
  IGN: pavel
  - 596bbc4e0edf staging: erofs: some compressed cluster should be submitted for corrupted images
  IGN: pavel
  - e7c44410387c staging: erofs: fix an error handling in erofs_readdir()
  IGN: pavel
  - 1b94c1e80ca8 coresight: etm4x: Use explicit barriers on enable/disable
  ACK: pavel -- not a minimal fix
  - effad578c23f vfs: Fix EOVERFLOW testing in put_compat_statfs64
  UR:pavel -- is it okay? Removes sizeof() check
  - d976344d27f7 arm64/speculation: Support 'mitigations=' cmdline option
  - af33d746286c arm64: Use firmware to detect CPUs that are not affected by Spectre-v2
  - 17d1acc4c61d arm64: Force SSBS on context switch
  - fe22ea561ce0 arm64: ssbs: Don't treat CPUs with SSBS as unaffected by SSB
  - dada3a4abb43 arm64: add sysfs vulnerability show for speculative store bypass
  - f41df38898ec arm64: add sysfs vulnerability show for spectre-v2
  - 9d1bb39cdd96 arm64: Always enable spectre-v2 vulnerability detection
  - b1a33cfd8034 arm64: Advertise mitigation of Spectre-v2, or lack thereof
  - 59a6dc262c85 arm64: Provide a command line to disable spectre_v2 mitigation
  - c131623b1e9d arm64: Always enable ssb vulnerability detection
  - 47a11f2eafcc arm64: enable generic CPU vulnerabilites support
  - 512158d0c67e arm64: add sysfs vulnerability show for meltdown
  - 047aac35fd1a arm64: Add sysfs vulnerability show for spectre-v1
  - edfc026626d6 arm64: fix SSBS sanitization
  - 09c22781dd2c arm64: docs: Document SSBS HWCAP
  - a59d42ac50a1 KVM: arm64: Set SCTLR_EL2.DSSBS if SSBD is forcefully disabled and !vhe
  - 1eaff33e2441 arm64: ssbd: Add support for PSTATE.SSBS rather than trapping to EL3
  UR:pavel -- new feature -- SSBD
  - d286a37471b6 riscv: Avoid interrupts being erroneously enabled in handle_exception()
  IGN: pavel
  - 5b67a4721ddd perf stat: Reset previous counts on repeat with interval
  ACK: pavel
  - 15c57bf9dcf8 perf tools: Fix segfault in cpu_cache_level__read()
  - e5331c37c08b tick: broadcast-hrtimer: Fix a race in bc_set_next
  UR:pavel
  - 140acbb09384 tools lib traceevent: Do not free tep->cmdlines in add_new_comm() on failure
  ACK: pavel
  - d1e4b4cc3bba powerpc/book3s64/radix: Rename CPU_FTR_P9_TLBIE_BUG feature flag
  IGN: pavel
  - f5f31a6ea558 powerpc/pseries: Fix cpu_hotplug_lock acquisition in resize_hpt()
  IGN: pavel
  - c688982ffaeb nbd: fix crash when the blksize is zero
  UR:pavel -- not a minimum fix; why allow blksize of zero?
  - 63bb8b76ed62 KVM: nVMX: Fix consistency check on injected exception error code
  ACK: pavel
  - 34b13ff69668 KVM: PPC: Book3S HV: XIVE: Free escalation interrupts before disabling the VP
  IGN: pavel
  - 1b155b4fe8b4 drm/radeon: Bail earlier when radeon.cik_/si_support=0 is passed
  UR:pavel -- cleanup, workaround for userland bug. 
  - 04e0c84f137d nfp: flower: fix memory leak in nfp_flower_spawn_vnic_reprs
  ACK: pavel
  - 575a5bb3d372 perf unwind: Fix libunwind build failure on i386 systems
  ACK: pavel
  - b0aaf65bb16a kernel/elfcore.c: include proper prototypes
  ACK: pavel -- just a warning fix
  - bab46480e6f9 perf build: Add detection of java-11-openjdk-devel package
  ACK: pavel -- adds support for new java version, not really a bugfix
  - 46ff0e2f869f sched/core: Fix migration to invalid CPU in __set_cpus_allowed_ptr()
  ACK: pavel
  - 6cb7aa1b4f94 sched/membarrier: Fix private expedited registration check
  ACK: pavel
  - e250f2b6aa9e sched/membarrier: Call sync_core only before usermode for same mm
  UR:pavel -- that's not a bug fix, that is optimalization that can uncover bugs...
  - 9f33b178cbb2 libnvdimm/nfit_test: Fix acpi_handle redefinition
  ACK: pavel -- just a warning fix
  - 7b4f541fcd1c fuse: fix memleak in cuse_channel_open
  ACK: pavel
  - 2e93d24ac75e libnvdimm/region: Initialize bad block for volatile namespaces
  ACK: pavel
  - 9025adf37ee8 thermal_hwmon: Sanitize thermal_zone type
  ACK: pavel
  - c01a9dbec18a thermal: Fix use-after-free when unregistering thermal zone device
  ACK: pavel
  - 55ebeb4e865d ntb: point to right memory window index
  ACK: pavel
  - 9dabade5c197 x86/purgatory: Disable the stackleak GCC plugin for the purgatory
  ACK: pavel
  - 65348659535d pwm: stm32-lp: Add check in case requested period cannot be achieved
  ACK: pavel
  - 19b1c70e911c pNFS: Ensure we do clear the return-on-close layout stateid on fatal errors
  ACK: pavel
  - 1c70ae6a91f9 drm/amdgpu: Check for valid number of registers to read
  ACK: pavel
  - e0af3b19ad77 drm/amdgpu: Fix KFD-related kernel oops on Hawaii
  ACK: pavel
  - f7ace7f25214 netfilter: nf_tables: allow lookups in dynamic sets
  ACK: pavel
  - f217883bbc92 watchdog: aspeed: Add support for AST2600
  ACK: pavel
  - 520c2a64fc78 ceph: reconnect connection if session hang in opening state
  ACK: pavel
  - 0275113fc09a ceph: fix directories inode i_blkbits initialization
  ACK: pavel
  - 2bc2a90a083a xen/pci: reserve MCFG areas earlier
  ACK: pavel
  - 18dd2b05f349 9p: avoid attaching writeback_fid on mmap with type PRIVATE
  ACK: pavel
  - 07f3596ce344 9p: Transport error uninitialized
  ACK: pavel
  - 448deb13ab9e fs: nfs: Fix possible null-pointer dereferences in encode_attrs()
  ACK: pavel
  - 4753e7a824cb ima: fix freeing ongoing ahash_request
  ACK: pavel
  - b69c3085fcc6 ima: always return negative code for error
  ACK: pavel
  - 6df3c66de09d arm64: cpufeature: Detect SSBS and advertise to userspace
  ACK: pavel -- adds support for new cpu feature, not a bugfix
  - 3a0e673305e2 cfg80211: initialize on-stack chandefs
  ACK: pavel
  - 16c75eb13a72 s390/cio: avoid calling strlen on null pointer
  IGN: pavel
  - 3f41e88f4bd4 ieee802154: atusb: fix use-after-free at disconnect
  ACK: pavel
  - 975859bb69b2 xen/xenbus: fix self-deadlock after killing user process
  ACK: pavel
  - e409b81d9ddb Revert "locking/pvqspinlock: Don't wait if vCPU is preempted"
  ACK: pavel
  - 7ed2867ceb41 mmc: sdhci-of-esdhc: set DMA snooping based on DMA coherence
  ACK: pavel
  - 4509a19d5082 mmc: sdhci: improve ADMA error reporting
  ACK: pavel -- makes debugging more useful, not a bugfix
  - 873f49d6a4e8 drm/i915/gvt: update vgpu workload head pointer correctly
  ACK: pavel
  - 198bc7040c48 drm/nouveau/kms/nv50-: Don't create MSTMs for eDP connectors
  ACK: pavel -- quite a hack
  - 7a85c8673551 drm/msm/dsi: Fix return value check for clk_get_parent
  ACK: pavel
  - 0e45633f49ef drm/omap: fix max fclk divider for omap36xx
  ACK: pavel
  - 90ac4028739c perf stat: Fix a segmentation fault when using repeat forever
  ACK: pavel
  - 22f28afd3d77 watchdog: imx2_wdt: fix min() calculation in imx2_wdt_set_timeout
  ACK: pavel
  - e7cf8cc79f93 PCI: Restore Resizable BAR size bits correctly for 1MB BARs
  ACK: pavel
  - 956ce989c41f PCI: vmd: Fix shadow offsets to reflect spec changes
  ACK: pavel
  - 06f250215beb timer: Read jiffies once when forwarding base clk
  UR:pavel --a do other places need same fix?
  - 12c6c4a50f66 usercopy: Avoid HIGHMEM pfn warning
  ACK: pavel
  - e010c9835183 tracing: Make sure variable reference alias has correct var_ref_idx
  ACK: pavel
  - 022ca58f109e power: supply: sbs-battery: only return health when battery present
  ACK: pavel
  - 5cb6dd823127 power: supply: sbs-battery: use correct flags field
  ACK: pavel -- but I wonder if it is wise to switch implementations when the old worked probably reasonably well
  - fb93ccde081e MIPS: Treat Loongson Extensions as ASEs
  IGN: pavel
  - a0dc60ac6bef crypto: ccree - use the full crypt length value
  ACK: pavel
  - f5c087a0d9a0 crypto: ccree - account for TEE not ready to report
  ACK: pavel
  - 561bf9309209 crypto: caam - fix concurrency issue in givencrypt descriptor
  ACK: pavel
  - 3683dd7074dc crypto: cavium/zip - Add missing single_release()
  ACK: pavel
  - cd8e0a5d94fb crypto: skcipher - Unmap pages after an external error
  UR:pavel -- redesigns function
  - 9349108ae499 crypto: qat - Silence smp_processor_id() warning
  ACK: pavel
  - 532920b26678 tools lib traceevent: Fix "robust" test of do_generate_dynamic_list_file
  UR:pavel -- wrong fix, should unset LC_ALL or something
  - 4aaea17d3c31 can: mcp251x: mcp251x_hw_reset(): allow more time after a reset
  UR:pavel -- changes return value, "interesting" use of usleep_range() 
  - 9124eac41a67 powerpc/book3s64/mm: Don't do tlbie fixup for some hardware revisions
  IGN: pavel
  - 19c12f12093e powerpc/powernv/ioda: Fix race in TCE level allocation
  IGN: pavel
  - 032ce7d766a9 powerpc/powernv: Restrict OPAL symbol map to only be readable by root
  IGN: pavel
  - ba3ca9fcb0e7 powerpc/mce: Schedule work from irq_work
  IGN: pavel
  - ee6eeeb88e79 powerpc/mce: Fix MCE handling for huge pages
  IGN: pavel
  - 1284f2073415 ASoC: sgtl5000: Improve VAG power and mute control
  UR:pavel -- rewrite that fixes "at least one bug"
  - 50090b75fa89 ASoC: Define a set of DAPM pre/post-up events
  UR:pavel -- just adds an empty define
  - 42b888f63333 PM / devfreq: tegra: Fix kHz to Hz conversion
  ACK: pavel
  - 9f0f39c92e4f nbd: fix max number of supported devs
  ACK: pavel
  - eff3a54aae68 KVM: nVMX: handle page fault in vmread fix
  ACK: pavel
  - 21874027e1de KVM: X86: Fix userspace set invalid CR4
  ACK: pavel
  - 30fbe0d380aa KVM: PPC: Book3S HV: Don't lose pending doorbell request on migration on P9
  IGN: pavel
  - 4faa7f05af75 KVM: PPC: Book3S HV: Check for MMU ready on piggybacked virtual cores
  IGN: pavel
  - 577a5119d7af KVM: PPC: Book3S HV: Fix race in re-enabling XIVE escalation interrupts
  IGN: pavel
  - 46cb14a57088 s390/cio: exclude subchannels with no parent from pseudo check
  IGN: pavel
  - 9aa823b3c0a6 s390/topology: avoid firing events before kobjs are created
  IGN: pavel
  - ddfef75f877b KVM: s390: Test for bad access register and size at the start of S390_MEM_OP
  IGN: pavel
  - 8b41a30f91db s390/process: avoid potential reading of freed stack
  IGN: pavel
