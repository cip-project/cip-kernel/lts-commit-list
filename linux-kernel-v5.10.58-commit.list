# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v5.10.58
  - 132a8267adab Linux 5.10.58
  - 3d7d1b0f5f41 arm64: fix compat syscall return truncation
  ACK: pavel
  - bb65051dcd1f drm/amdgpu/display: only enable aux backlight control for OLED panels
  ACK: pavel -- commented out code, little tested, not even authors are sure it is right thing
  - c8b7cfa674ee smb3: rc uninitialized in one fallocate path
  ACK: pavel
  - 8cfdd039ca18 net/qla3xxx: fix schedule while atomic in ql_wait_for_drvr_lock and ql_adapter_reset
  UR: pavel -- waiting for seconds in atomic context is wrong
  - fbbb209268e5 alpha: Send stop IPI to send to online CPUs
  ACK: pavel
  - 13d0a9b3b917 net: qede: Fix end of loop tests for list_for_each_entry
  NAK: pavel -- change is not neccessary, and may be introducing a bug
  - 1478e902bcbc virt_wifi: fix error on connect
  ACK: pavel
  - ecd8614809eb reiserfs: check directory items on read from disk
  ACK: pavel -- just a robustness
  - dbe4f82fedc6 reiserfs: add check for root_inode in reiserfs_fill_super
  ACK: pavel -- just a robustness
  - 0f05e0ffa247 libata: fix ata_pio_sector for CONFIG_HIGHMEM
  ACK: pavel
  - 11891adab23d drm/i915: avoid uninitialised var in eb_parse()
  ACK: pavel -- shows we need clear marking of upstream
  - a3e6bd0c71bb sched/rt: Fix double enqueue caused by rt_effective_prio
  ACK: pavel
  - c797b8872bb9 perf/x86/amd: Don't touch the AMD64_EVENTSEL_HOSTONLY bit inside the guest
  ACK: pavel
  - 2d94cffc94a5 soc: ixp4xx/qmgr: fix invalid __iomem access
  ACK: pavel -- just a warning fix
  - 7397034905ac drm/i915: Correct SFC_DONE register offset
  ACK: pavel -- just a debugging improvement
  - 16aecf1e36d9 interconnect: qcom: icc-rpmh: Ensure floor BW is enforced for all nodes
  ACK: pavel
  - 22b4917c85af interconnect: Always call pre_aggregate before aggregate
  ACK: pavel
  - ccfe4f62ff9f interconnect: Zero initial BW after sync-state
  ACK: pavel
  - 05565b469358 spi: meson-spicc: fix memory leak in meson_spicc_remove
  ACK: pavel
  - 1a084e78217d interconnect: Fix undersized devress_alloc allocation
  ACK: pavel
  - dcc23e58511b soc: ixp4xx: fix printing resources
  ACK: pavel -- just a warning fix
  - 37cbd27ef4b2 arm64: vdso: Avoid ISB after reading from cntvct_el0
  UR: pavel -- just an "interesting" performance trick
  - 7a2b5bb00f54 KVM: x86/mmu: Fix per-cpu counter corruption on 32-bit builds
  ACK: pavel
  - 32f55c25ee29 KVM: Do not leak memory for duplicate debugfs directories
  ACK: pavel
  - 309a31127bef KVM: x86: accept userspace interrupt only if no event is injected
  ACK: pavel -- just a ABI tweak
  - a786282b55b4 md/raid10: properly indicate failure when ending a failed write request
  ACK: pavel -- not a minimum fix
  - 3d7d2d2b069b ARM: omap2+: hwmod: fix potential NULL pointer access
  ACK: pavel
  - 9851ad2f7107 Revert "gpio: mpc8xxx: change the gpio interrupt flags."
  UR: pavel -- fix/revert mess, just for benefit of -rt
  - 57c44e7ac788 bus: ti-sysc: AM3: RNG is GP only
  ACK: pavel
  - f4984f60acc7 selinux: correct the return value when loads initial sids
  ACK: pavel -- not a minimum fix
  - 100f8396d154 pcmcia: i82092: fix a null pointer dereference bug
  ACK: pavel
  - afcd5a0e015f net/xfrm/compat: Copy xfrm_spdattr_type_t atributes
  ACK: pavel
  - f08b2d078cbb xfrm: Fix RCU vs hash_resize_mutex lock inversion
  ACK: pavel
  - 23e36a8610ca timers: Move clearing of base::timer_running under base:: Lock
  ACK: pavel
  - 9a69d0d24d69 fpga: dfl: fme: Fix cpu hotplug issue in performance reporting
  ACK: pavel
  - bfb5f1a12325 serial: 8250_pci: Avoid irq sharing for MSI(-X) interrupts.
  ACK: pavel
  - 0f30fedced7c serial: 8250_pci: Enumerate Elkhart Lake UARTs via dedicated driver
  ACK: pavel
  - 17f3c64f707b MIPS: Malta: Do not byte-swap accesses to the CBUS UART
  - 8a1624f4a8d3 serial: 8250: Mask out floating 16/32-bit bus bits
  ACK: pavel
  - c03cef67157a serial: 8250_mtk: fix uart corruption issue when rx power off
  ACK: pavel
  - a4f8bfc919ee serial: tegra: Only print FIFO error message when an error occurs
  ACK: pavel -- just a printk tweak
  - cc7300776808 ext4: fix potential htree corruption when growing large_dir directories
  ACK: pavel
  - 6b5a3d2c2b89 pipe: increase minimum default pipe size to 2 pages
  UR: pavel -- uhuh. Will cost quite a lot of memory in some cases.
  - 556e7f204d34 media: rtl28xxu: fix zero-length control request
  ACK: pavel
  - 551e0c5d6b2e drivers core: Fix oops when driver probe fails
  ACK: pavel
  - faec2c68ea5f staging: rtl8712: error handling refactoring
  - e468a357af68 staging: rtl8712: get rid of flush_scheduled_work
  - 369101e39911 staging: rtl8723bs: Fix a resource leak in sd_int_dpc
  - 1628b64efb36 tpm_ftpm_tee: Free and unregister TEE shared memory during kexec
  ACK: pavel
  - 2a879ff9719f optee: fix tee out of memory failure seen during kexec reboot
  ACK: pavel
  - ad80c25987fe optee: Refuse to load the driver under the kdump kernel
  ACK: pavel
  - 1340dc3fb75e optee: Fix memory leak when failing to register shm pages
  ACK: pavel
  - 6b2ded93d35c tee: add tee_shm_alloc_kernel_buf()
  NAK: pavel -- this is not used in 4.19
  - 5e9d82021425 optee: Clear stale cache entries during initialization
  ACK: pavel
  - e5d8fd87091c arm64: stacktrace: avoid tracing arch_stack_walk()
  ACK: pavel
  - 7799ad4d181f tracepoint: Fix static call function vs data state mismatch
  UR: pavel
  - 14673e19291c tracepoint: static call: Compare data on transition from 2->1 callees
  ACK: pavel
  - 046e12323ab4 tracing: Fix NULL pointer dereference in start_creating
  ACK: pavel
  - b2aca8daa50e tracing: Reject string operand in the histogram expression
  ACK: pavel
  - b10ccc2c5888 tracing / histogram: Give calculation hist_fields a size
  ACK: pavel
  - f97274528037 scripts/tracing: fix the bug that can't parse raw_trace_func
  ACK: pavel
  - fd3afb81f448 clk: fix leak on devm_clk_bulk_get_all() unwind
  ACK: pavel
  - 948ff2f214fb usb: otg-fsm: Fix hrtimer list corruption
  ACK: pavel
  - 8f8645de092a usb: typec: tcpm: Keep other events when receiving FRS and Sourcing_vbus events
  ACK: pavel
  - 5b4318885a43 usb: host: ohci-at91: suspend/resume ports after/before OHCI accesses
  ACK: pavel
  - 1f2015506d9c usb: gadget: f_hid: idle uses the highest byte for duration
  ACK: pavel
  - 825ac3f0bc35 usb: gadget: f_hid: fixed NULL pointer dereference
  ACK: pavel
  - 683702dff7c8 usb: gadget: f_hid: added GET_IDLE and SET_IDLE handlers
  ACK: pavel -- more missing feature than a bugfix; buggy, fixed below; interesting coding style with goto before break
  - 051518d9cfe3 usb: cdns3: Fixed incorrect gadget state
  ACK: pavel
  - 822bec5cbb05 usb: gadget: remove leaked entry from udc driver list
  ACK: pavel
  - 98c83d72614e usb: dwc3: gadget: Avoid runtime resume if disabling pullup
  ACK: pavel
  - 79e9389038c4 ALSA: usb-audio: Add registration quirk for JBL Quantum 600
  ACK: pavel
  - b7532db2d458 ALSA: usb-audio: Fix superfluous autosuspend recovery
  ACK: pavel -- may not have real impact
  - 80b7aa2651bc ALSA: hda/realtek: Fix headset mic for Acer SWIFT SF314-56 (ALC256)
  ACK: pavel
  - de30786fb25a ALSA: hda/realtek: add mic quirk for Acer SF314-42
  ACK: pavel
  - c0b626f0a29a ALSA: pcm - fix mmap capability check for the snd-dummy driver
  ACK: pavel
  - dd3f7c5c8904 drm/amdgpu/display: fix DMUB firmware version info
  ACK: pavel
  - ecb739cf15a9 firmware_loader: fix use-after-free in firmware_fallback_sysfs
  ACK: pavel
  - 5019f5812bbf firmware_loader: use -ETIMEDOUT instead of -EAGAIN in fw_load_sysfs_fallback
  ACK: pavel -- just an API tweak
  - aa3b8bc17e2a USB: serial: ftdi_sio: add device ID for Auto-M3 OP-COM v2
  ACK: pavel
  - d245a76719cf USB: serial: ch341: fix character loss at high transfer rates
  ACK: pavel
  - 0470385e63bb USB: serial: option: add Telit FD980 composition 0x1056
  ACK: pavel
  - ba4a395668b5 USB: usbtmc: Fix RCU stall warning
  ACK: pavel
  - f2f856b65ac4 Bluetooth: defer cleanup of resources in hci_unregister_dev()
  UR: pavel -- tricky, locking rewrite
  - 821e6a613354 blk-iolatency: error out if blk_get_queue() failed in iolatency_set_limit()
  ACK: pavel
  - c5a499b8607a net: vxge: fix use-after-free in vxge_device_unregister
  ACK: pavel
  - fb49d67262ca net: fec: fix use-after-free in fec_drv_remove
  ACK: pavel
  - f12b6b6bc15f net: pegasus: fix uninit-value in get_interrupt_interval
  ACK: pavel -- just a KASAN fix
  - c66d273b70fe bnx2x: fix an error code in bnx2x_nic_load()
  ACK: pavel
  - f76f9caccb46 mips: Fix non-POSIX regexp
  - f93b7b000044 MIPS: check return value of pgtable_pmd_page_ctor
  - 9b2b2f07712b net: sched: fix lockdep_set_class() typo error for sch->seqlock
  ACK: pavel
  - d1f2abe57bc1 net: dsa: qca: ar9331: reorder MDIO write sequence
  - a45ee8ed0c7d net: ipv6: fix returned variable type in ip6_skb_dst_mtu
  ACK: pavel -- just a cleanup
  - f87be69b7fe9 nfp: update ethtool reporting of pauseframe control
  ACK: pavel
  - 44f2e360e784 sctp: move the active_key update after sh_keys is added
  ACK: pavel
  - e74551ba938a RDMA/mlx5: Delay emptying a cache entry when a new MR is added to it recently
  - 1242ca9369b1 gpio: tqmx86: really make IRQ optional
  ACK: pavel
  - 4ef549dc9c1a net: natsemi: Fix missing pci_disable_device() in probe and remove
  ACK: pavel
  - 1dc3eef381c1 net: phy: micrel: Fix detection of ksz87xx switch
  ACK: pavel
  - e09dba75cafd net: dsa: sja1105: match FDB entries regardless of inner/outer VLAN tag
  ACK: pavel
  - c0b14a0e61e7 net: dsa: sja1105: be stateless with FDB entries on SJA1105P/Q/R/S/SJA1110 too
  ACK: pavel
  - 00bf923dce2a net: dsa: sja1105: invalidate dynamic FDB entries learned concurrently with statically added ones
  ACK: pavel
  - de425f1c3a60 net: dsa: sja1105: overwrite dynamic FDB entries with static ones in .port_fdb_add
  ACK: pavel
  - 74bcf85ff1e2 net, gro: Set inner transport header offset in tcp/udp GRO hook
  ACK: pavel
  - 80fd533ac3f9 dmaengine: imx-dma: configure the generic DMA type to make it work
  ACK: pavel
  - 163e6d87216d ARM: dts: stm32: Fix touchscreen IRQ line assignment on DHCOM
  ACK: pavel
  - 442f7e04d592 ARM: dts: stm32: Disable LAN8710 EDPD on DHCOM
  ACK: pavel
  - 449991df08d5 media: videobuf2-core: dequeue if start_streaming fails
  ACK: pavel
  - 3e8bba601212 scsi: sr: Return correct event when media event code is 3
  ACK: pavel
  - aaaf6e6e4174 spi: imx: mx51-ecspi: Fix low-speed CONFIGREG delay calculation
  ACK: pavel
  - cd989e119272 spi: imx: mx51-ecspi: Reinstate low-speed CONFIGREG delay
  ACK: pavel
  - 281514da66a4 dmaengine: stm32-dmamux: Fix PM usage counter unbalance in stm32 dmamux ops
  ACK: pavel
  - bbce3c99f622 dmaengine: stm32-dma: Fix PM usage counter imbalance in stm32 dma ops
  ACK: pavel
  - 84656b4c27bf clk: tegra: Implement disable_unused() of tegra_clk_sdmmc_mux_ops
  ACK: pavel
  - edf1b7911af2 dmaengine: uniphier-xdmac: Use readl_poll_timeout_atomic() in atomic state
  ACK: pavel
  - 4ebd11d1c782 omap5-board-common: remove not physically existing vdds_1v8_main fixed-regulator
  ACK: pavel
  - 9bf056b99fa0 ARM: dts: am437x-l4: fix typo in can@0 node
  ACK: pavel -- not sure if it has end-user impact
  - e79a30f71d95 clk: stm32f4: fix post divisor setup for I2S/SAI PLLs
  ACK: pavel
  - 71f39badc898 ALSA: usb-audio: fix incorrect clock source setting
  ACK: pavel
  - c4fcda128780 arm64: dts: armada-3720-turris-mox: remove mrvl,i2c-fast-mode
  ACK: pavel
  - 8d13f6a0a656 arm64: dts: armada-3720-turris-mox: fixed indices for the SDHC controllers
  ACK: pavel
  - f239369f37d9 ARM: dts: imx: Swap M53Menlo pinctrl_power_button/pinctrl_power_out pins
  ACK: pavel
  - ee6f7084324d ARM: imx: fix missing 3rd argument in macro imx_mmdc_perf_init
  ACK: pavel
  - e1011b9c597d ARM: dts: colibri-imx6ull: limit SDIO clock to 25MHz
  ACK: pavel
  - c0f61abbefdf arm64: dts: ls1028: sl28: fix networking for variant 2
  ACK: pavel
  - 54555c399668 ARM: dts: imx6qdl-sr-som: Increase the PHY reset duration to 10ms
  ACK: pavel
  - 3790f940981d ARM: imx: add missing clk_disable_unprepare()
  ACK: pavel -- buggy, fixed by 20fb73
  - a28569b510e5 ARM: imx: add missing iounmap()
  ACK: pavel
  - 9189d77f0e21 arm64: dts: ls1028a: fix node name for the sysclk
  ACK: pavel
  - d61dc8c634bb net: xfrm: fix memory leak in xfrm_user_rcv_msg
  ACK: pavel
  - 8efe3a635f22 bus: ti-sysc: Fix gpt12 system timer issue with reserved status
  NAK: pavel -- code is confused and checks for wrong error code
  - e32a291736fc ALSA: seq: Fix racy deletion of subscriber
  - b917f123b50d Revert "ACPICA: Fix memory leak caused by _CID repair function"
