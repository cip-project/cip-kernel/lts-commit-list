# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v4.19.118
  - 7edd66cf6167 Linux 4.19.118
  - e0b80b7d6466 bpf: fix buggy r0 retval refinement for tracing helpers
  UR: pavel
  - 18779eac17b5 KEYS: Don't write out to userspace while holding key semaphore
  UR: pavel
  - 9e303d524904 mtd: phram: fix a double free issue in error path
  ACK: pavel
  - 42baf547a41f mtd: lpddr: Fix a double free in probe()
  ACK: pavel
  - f966b0388b18 mtd: spinand: Explicitly use MTD_OPS_RAW to write the bad block marker to OOB
  ACK: pavel -- cleanup, does not fix a bug
  - 9c85fc004ed6 locktorture: Print ratio of acquisitions, not failures
  ACK: pavel -- just changes output, not sure if author understands what zero-day is
  - 34f5859b8f9a tty: evh_bytechan: Fix out of bounds accesses
  ACK: pavel -- quite evil hack
  - 84f77a944839 iio: si1133: read 24-bit signed integer for measurement
  ACK: pavel -- driver was returning wrong (but possibly useful) values. This fixed it
  - 7b24ff9e0082 fbdev: potential information leak in do_fb_ioctl()
  ACK: pavel
  - 65dd68c7a55b net: dsa: bcm_sf2: Fix overflow checks
  ACK: pavel
  - 8b10cf2de32c f2fs: fix to wait all node page writeback
  ACK: pavel
  - 2fdac8fd2049 iommu/amd: Fix the configuration of GCR3 table root pointer
  ACK: pavel
  - b7c5dc73e192 libnvdimm: Out of bounds read in __nd_ioctl()
  ACK: pavel
  - 8f595c7826bf power: supply: axp288_fuel_gauge: Broaden vendor check for Intel Compute Sticks.
  ACK: pavel
  - aad145830991 ext2: fix debug reference to ext2_xattr_cache
  ACK: pavel -- improves debugging; code changes are needed for this to hit
  - 5175f717c6db ext2: fix empty body warnings when -Wextra is used
  ACK: pavel -- just a warning fix, for warning not enabled by default
  - 4b602f68a95a iommu/vt-d: Fix mm reference leak
  ACK: pavel
  - a95787ed3691 drm/vc4: Fix HDMI mode validation
  ACK: pavel
  - 1c7259f74482 f2fs: fix NULL pointer dereference in f2fs_write_begin()
  ACK: pavel
  - b38f7532eb15 NFS: Fix memory leaks in nfs_pageio_stop_mirroring()
  ACK: pavel
  - 044a884072b4 drm/amdkfd: kfree the wrong pointer
  ACK: pavel
  - 67e5b7090905 x86: ACPI: fix CPU hotplug deadlock
  ACK: pavel
  - 41228f464fdf KVM: s390: vsie: Fix possible race when shadowing region 3 tables
  - 9af5777e75da compiler.h: fix error in BUILD_BUG_ON() reporting
  ACK: pavel -- theoretical error, not known to happen anywhere
  - 638350f0ac3c percpu_counter: fix a data race at vm_committed_as
  ACK: pavel -- code is correct, just a KCSAN warning workaround
  - 9b6170c5cf36 include/linux/swapops.h: correct guards for non_swap_entry()
  ACK: pavel -- not a bug, problem can not happen due to kconfig dependencies  
  - 731a3bc2be26 cifs: Allocate encryption header through kmalloc
  ACK: pavel
  - 1421615c646a um: ubd: Prevent buffer overrun on command completion
  ACK: pavel
  - 6e2fa8b3b8d6 ext4: do not commit super on read-only bdev
  ACK: pavel -- just a warning/printk improvement
  - 60cb7886942b s390/cpum_sf: Fix wrong page count in error message
  - 1081196571d5 powerpc/maple: Fix declaration made after definition
  ACK: pavel
  - ffc059b5b95c s390/cpuinfo: fix wrong output when CPU0 is offline
  - 2f5253c5e9ec NFS: direct.c: Fix memory leak of dreq when nfs_get_lock_context fails
  ACK: pavel
  - 401876dbcf6b NFSv4/pnfs: Return valid stateids in nfs_layout_find_inode_by_stateid()
  ACK: pavel
  - 65ea19acb0fd rtc: 88pm860x: fix possible race condition
  ACK: pavel
  - 195bd29b4fb1 soc: imx: gpc: fix power up sequencing
  ACK: pavel
  - 08afbff24be3 clk: tegra: Fix Tegra PMC clock out parents
  ACK: pavel
  - 6c0f9e7fdd2c power: supply: bq27xxx_battery: Silence deferred-probe error
  ACK: pavel -- just a printk fix
  - 0cc1de475c1a clk: at91: usb: continue if clk_hw_round_rate() return zero
  ACK: pavel
  - f904261ddab6 x86/Hyper-V: Report crash data in die() when panic_on_oops is set
  ACK: pavel
  - 83064464c9bd x86/Hyper-V: Report crash register data when sysctl_record_panic_msg is not set
  - cefde4e7c979 x86/Hyper-V: Trigger crash enlightenment only once during system crash.
  ACK: pavel
  - 89b0b47a6fb8 x86/Hyper-V: Free hv_panic_page when fail to register kmsg dump
  ACK: pavel -- just a memory leak in error path
  - 5e059fc0f054 x86/Hyper-V: Unload vmbus channel in hv panic callback
  ACK: pavel
  - ad8fb61c184f xsk: Add missing check on user supplied headroom size
  UR: pavel -- changes > into >=, but that's it?
  - 2b48629885b6 rbd: call rbd_dev_unprobe() after unwatching and flushing notifies
  ACK: pavel
  - 26b69a33ff03 rbd: avoid a deadlock on header_rwsem when flushing notifies
  ACK: pavel
  - adff7c6c512e video: fbdev: sis: Remove unnecessary parentheses and commented code
  ACK: pavel -- just a warning fix, not a minimum one
  - 00dd1df3c24f lib/raid6: use vdupq_n_u8 to avoid endianness warnings
  ACK: pavel -- just a warning fix, and not really obvious
  - 017917fef5c6 x86/Hyper-V: Report crash register data or kmsg before running crash kernel
  ACK: pavel
  - d55d3d74dace of: overlay: kmemleak in dup_and_fixup_symbol_prop()
  ACK: pavel
  - e04087b3f43a of: unittest: kmemleak in of_unittest_overlay_high_level()
  ACK: pavel
  - 842f7bbaf44c of: unittest: kmemleak in of_unittest_platform_populate()
  ACK: pavel
  - 3352cc2f7b71 of: unittest: kmemleak on changeset destroy
  ACK: pavel
  - c8fddc945aba ALSA: hda: Don't release card at firmware loading error
  ACK: pavel
  - 654c9adb64e1 irqchip/mbigen: Free msi_desc on device teardown
  ACK: pavel
  - 79f784c999bc netfilter: nf_tables: report EOPNOTSUPP on unsupported flags/object type
  ACK: pavel -- just a error code tweak
  - f539aa273e61 ARM: dts: imx6: Use gpc for FEC interrupt controller to fix wake on LAN.
  ACK: pavel
  - 1c9c3bc82213 arm, bpf: Fix bugs with ALU64 {RSH, ARSH} BPF_K shift by 0
  ACK: pavel
  - 097852a187d8 watchdog: sp805: fix restart handler
  ACK: pavel
  - a6375c9877a8 ext4: use non-movable memory for superblock readahead
  ACK: pavel
  - 45533ebd5e5c scsi: sg: add sg_remove_request in sg_common_write
  ACK: pavel
  - 2b91a2361d77 objtool: Fix switch table detection in .text.unlikely
  ACK: pavel
  - db9d273b6bad arm, bpf: Fix offset overflow for BPF_MEM BPF_DW
  UR: pavel
