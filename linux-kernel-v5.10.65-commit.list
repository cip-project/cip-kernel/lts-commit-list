# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v5.10.65
  - c31c2cca229a Linux 5.10.65
  - b216a075a9ab clk: kirkwood: Fix a clocking boot regression
  ACK: uli
  - 5866b1175df0 backlight: pwm_bl: Improve bootloader/kernel device handover
  ACK: pavel -- just a improvement of display flicker
  - 4c00435cb813 fbmem: don't allow too huge resolutions
  ACK: pavel
  - 34d099a330e7 IMA: remove the dependency on CRYPTO_MD5
  UR: pavel -- config tweak not
  - 5cc1ee31353b IMA: remove -Wmissing-prototypes warning
  ACK: uli -- warning fix, not a real problem
  - 131968998109 fuse: flush extending writes
  ACK: pavel
  - 8018100c5444 fuse: truncate pagecache on atomic_o_trunc
  ACK: pavel
  - a8ca1fba54be ARM: dts: at91: add pinctrl-{names, 0} for all gpios
  UR: pavel
  - c2c7eefc9371 KVM: nVMX: Unconditionally clear nested.pi_pending on nested VM-Enter
  ACK: uli
  - bf3622446335 KVM: VMX: avoid running vmx_handle_exit_irqoff in case of emulation
  ACK: uli
  - c06e6ff2fcc3 KVM: x86: Update vCPU's hv_clock before back to guest when tsc_offset is adjusted
  UR: pavel -- comment style, 80 columns
  - 1db337b10d12 KVM: s390: index kvm->arch.idle_mask by vcpu_idx
  IGN: uli
  - dc9db2a2aae4 Revert "KVM: x86: mmu: Add guest physical address check in translate_gpa()"
  ACK: uli
  - c6b42ec1c936 x86/resctrl: Fix a maybe-uninitialized build warning treated as error
  NAK: uli -- "fixes" GCC warning only enabled on RHEL that is notoriously unreliable
  - bafece6cd1f9 perf/x86/amd/ibs: Extend PERF_PMU_CAP_NO_EXCLUDE to IBS Op
  ACK: pavel
  - ae95c3a147d5 tty: Fix data race between tiocsti() and flush_to_ldisc()
  ACK: pavel
  - 4d0e6d6fe4e2 bio: fix page leak bio_add_hw_page failure
  ACK: pavel
  - 24fbd77d5a0f io_uring: IORING_OP_WRITE needs hash_reg_file set
  ACK: uli
  - 656f343d724b time: Handle negative seconds correctly in timespec64_to_ns()
  ACK: pavel
  - 611b7f9dc9f6 f2fs: guarantee to write dirty data when enabling checkpoint back
  ACK: pavel -- looks like it simply makes problem less likely
  - 75ffcd85dff5 iwlwifi Add support for ax201 in Samsung Galaxy Book Flex2 Alpha
  ACK: uli -- notebook hardware enablement
  - 3853c0c0703d ASoC: rt5682: Remove unused variable in rt5682_i2c_remove()
  ACK: uli -- warning fix
  - c4f1ad393026 ipv4: fix endianness issue in inet_rtm_getroute_build_skb()
  ACK: pavel
  - dc4ff31506f4 octeontx2-af: Set proper errorcode for IPv4 checksum errors
  ACK: uli
  - bf2991f8e783 octeontx2-af: Fix static code analyzer reported issues
  ACK: uli -- probably not a real problem
  - ee485124b7fa octeontx2-af: Fix loop in free and unmap counter
  ACK: uli
  - a67c66c1bb12 net: qualcomm: fix QCA7000 checksum handling
  ACK: uli
  - f96bc82e0348 net: sched: Fix qdisc_rate_table refcount leak when get tcf_block failed
  UR: pavel -- c/e
  - 5867e20e1808 ipv4: make exception cache less predictible
  UR: pavel
  - 8692f0bb2992 ipv6: make exception cache less predictible
  ACK: pavel
  - 4663aaef24df brcmfmac: pcie: fix oops on failure to resume and reprobe
  ACK: uli
  - e68128e078da bcma: Fix memory leak for internally-handled cores
  ACK: pavel -- two fixes in one
  - 26fae720c112 atlantic: Fix driver resume flow.
  ACK: uli
  - cb996dc9f937 ath6kl: wmi: fix an error code in ath6kl_wmi_sync_point()
  ACK: pavel -- fixes unlikely code path
  - baecab8c469f ice: Only lock to update netdev dev_addr
  ACK: uli
  - bd6d9c83f44d iwlwifi: skip first element in the WTAS ACPI table
  ACK: uli
  - 4c4f868082ed iwlwifi: follow the new inclusive terminology
  ACK: uli -- cosmetic
  - 5c305b90d8a1 ASoC: wcd9335: Disable irq on slave ports in the remove function
  ACK: uli
  - 729a459efd30 ASoC: wcd9335: Fix a memory leak in the error handling path of the probe function
  ACK: uli
  - 9c640a2bb551 ASoC: wcd9335: Fix a double irq free in the remove function
  ACK: uli
  - 8446bb0ff1d0 tty: serial: fsl_lpuart: fix the wrong mapbase value
  ACK: pavel
  - 9ee4ff8cbe39 usb: bdc: Fix a resource leak in the error handling path of 'bdc_probe()'
  UR: pavel
  - 4d2823abd1fe usb: bdc: Fix an error handling path in 'bdc_probe()' when no suitable DMA config is available
  UR: pavel -- c/e
  - 86b79054d76b usb: ehci-orion: Handle errors of clk_prepare_enable() in probe
  ACK: uli
  - f0bb63127354 i2c: xlp9xx: fix main IRQ check
  ACK: uli
  - 7ac3090e0123 i2c: mt65xx: fix IRQ check
  ACK: uli
  - 6c4857203ffa CIFS: Fix a potencially linear read overflow
  ACK: pavel -- not a minimum fix
  - b0491ab7d4c7 bpf: Fix possible out of bound write in narrow load handling
  UR: pavel -- c/e
  - cfaefbcc6bc4 mmc: moxart: Fix issue with uninitialized dma_slave_config
  ACK: pavel -- probably not a real problem
  - ced0bc748185 mmc: dw_mmc: Fix issue with uninitialized dma_slave_config
  ACK: pavel
  - 8a9f9b97558e mmc: sdhci: Fix issue with uninitialized dma_slave_config
  ACK: pavel
  - dd903083cbe4 ASoC: Intel: Skylake: Fix module resource and format selection
  UR: uli
  - b0159dbd1dd6 ASoC: Intel: Skylake: Leave data as is when invoking TLV IPCs
  ACK: uli
  - 7934c79fb0ed ASoC: Intel: kbl_da7219_max98927: Fix format selection for max98373
  UR: uli
  - 56d976f45000 rsi: fix an error code in rsi_probe()
  ACK: uli
  - 110ce7d256a3 rsi: fix error code in rsi_load_9116_firmware()
  ACK: uli
  - b4bbb77d886b gfs2: init system threads before freeze lock
  UR: pavel
  - ee029e3aa129 i2c: hix5hd2: fix IRQ check
  ACK: uli
  - d36ab9b3ee49 i2c: fix platform_get_irq.cocci warnings
  ACK: uli
  - 187705a4b1fa i2c: s3c2410: fix IRQ check
  ACK: uli
  - 3913fa307a33 i2c: iop3xx: fix deferred probing
  ACK: uli -- two (closely related) fixes in one
  - 50e6f34499a5 Bluetooth: add timeout sanity check to hci_inquiry
  ACK: pavel
  - cc59ad70cfb6 lkdtm: replace SCSI_DISPATCH_CMD with SCSI_QUEUE_RQ
  ACK: pavel -- just a fragile workaround for gcc inlining
  - 9295566a136c mm/swap: consider max pages in iomap_swapfile_add_extent
  ACK: pavel
  - a9c29bc2a578 usb: gadget: mv_u3d: request_irq() after initializing UDC
  ACK: pavel
  - b2f4dd13b211 firmware: raspberrypi: Fix a leak in 'rpi_firmware_get()'
  ACK: pavel
  - 60831f5ae6c7 firmware: raspberrypi: Keep count of all consumers
  ACK: pavel -- buggy, fixed by next patch
  - 5c68b7795b4c i2c: synquacer: fix deferred probing
  ACK: pavel
  - f577e9f58ff0 clk: staging: correct reference to config IOMEM to config HAS_IOMEM
  - 5ae5f087c9d6 arm64: dts: marvell: armada-37xx: Extend PCIe MEM space
  ACK: pavel
  - cb788d698a10 nfsd4: Fix forced-expiry locking
  ACK: pavel
  - c9773f42c1de lockd: Fix invalid lockowner cast after vfs_test_lock
  ACK: pavel
  - 2600861b9069 locking/local_lock: Add missing owner initialization
  - d5462a630f7d locking/lockdep: Mark local_lock_t
  UR: pavel -- not a minimum fix, just a robustness
  - 22b106df73c6 mac80211: Fix insufficient headroom issue for AMSDU
  ACK: pavel
  - 0ad4ddb27e2b libbpf: Re-build libbpf.so when libbpf.map changes
  - 494629ba62a9 usb: phy: tahvo: add IRQ check
  UR: pavel -- c/e
  - 46638d6941ee usb: host: ohci-tmio: add IRQ check
  UR: pavel -- c/e, looks suspect
  - 4b7874a32ec2 PM: cpu: Make notifier chain use a raw_spinlock_t
  UR: pavel --! unsuitable for 5.10
  - 471128476819 Bluetooth: Move shutdown callback before flushing tx and rx queue
  NAK: pavel -- this should move code but it duplicates it instead.
  - d993a6f137ec samples: pktgen: add missing IPv6 option to pktgen scripts
  - 2c0b826f4a79 devlink: Clear whole devlink_flash_notify struct
  ACK: pavel
  - 2aa3d5c9e19e selftests/bpf: Fix test_core_autosize on big-endian machines
  - c03bf1bc84ea usb: gadget: udc: renesas_usb3: Fix soc_device_match() abuse
  ACK: pavel
  - eabbb2e8cc41 usb: phy: twl6030: add IRQ checks
  - fa5dbfd53982 usb: phy: fsl-usb: add IRQ check
  - 99ad1be3e9cb usb: gadget: udc: s3c2410: add IRQ check
  - 0a7731458968 usb: gadget: udc: at91: add IRQ check
  UR: pavel -- c/e
  - 27f102bcee52 usb: dwc3: qcom: add IRQ check
  UR: pavel -- c/e
  - c4e0f54a56d0 usb: dwc3: meson-g12a: add IRQ check
  UR: pavel -- c/e
  - 96ba1e20e252 ASoC: rt5682: Properly turn off regulators if wrong device ID
  ACK: pavel
  - 1a2feb23043b ASoC: rt5682: Implement remove callback
  ACK: pavel -- just a preparation for next patch
  - 628acf6ee2f1 net/mlx5: Fix unpublish devlink parameters
  ACK: pavel
  - fe6322774ca2 net/mlx5: Register to devlink ingress VLAN filter trap
  UR: pavel -- why?
  - dbeb4574ddf0 drm/msm/dsi: Fix some reference counted resource leaks
  ACK: pavel
  - 059c2c09f4b7 Bluetooth: fix repeated calls to sco_sock_kill
  UR: pavel
  - 6df58421da13 ASoC: Intel: Fix platform ID matching
  UR: pavel -- check, does it have second part
  - 10dfcfda5c6f cgroup/cpuset: Fix violation of cpuset locking rule
  UR: pavel
  - cbc97661439d cgroup/cpuset: Miscellaneous code cleanup
  UR: pavel
  - 974ab0a04fe6 counter: 104-quad-8: Return error when invalid mode during ceiling_write
  - c158f9b23279 arm64: dts: exynos: correct GIC CPU interfaces address range on Exynos7
  ACK: pavel
  - 7125705623f9 drm/msm/dpu: make dpu_hw_ctl_clear_all_blendstages clear necessary LMs
  ACK: pavel
  - a6e980b110d2 drm/msm/mdp4: move HW revision detection to earlier phase
  UR: pavel -- c/e
  - 90363618b552 drm/msm/mdp4: refactor HW revision detection into read_mdp_hw_revision
  ACK: pavel -- will cause a warning?
  - 416929eaf44e selftests/bpf: Fix bpf-iter-tcp4 test to print correctly the dest IP
  - d6337dfd1e77 PM: EM: Increase energy calculation precision
  UR: pavel -- it says div64 is costly, then does it anyway?
  - 5014a8453f02 Bluetooth: increase BTNAMSIZ to 21 chars to fix potential buffer overflow
  ACK: pavel
  - afffa7b4c6e4 debugfs: Return error during {full/open}_proxy_open() on rmmod
  ACK: pavel
  - 17830b041534 soc: qcom: smsm: Fix missed interrupts if state changes while masked
  ACK: pavel
  - b8361513ac76 bpf, samples: Add missing mprog-disable to xdp_redirect_cpu's optstring
  UR: pavel -- check we have that option
  - cd6008e31af0 PCI: PM: Enable PME if it can be signaled from D3cold
  ACK: pavel
  - 3890c6e1da31 PCI: PM: Avoid forcing PCI_D0 for wakeup reasons inconsistently
  ACK: pavel -- not sure it fixes anything
  - eda4ccca906f media: venus: venc: Fix potential null pointer dereference on pointer fmt
  ACK: pavel
  - 519ad41a0989 media: em28xx-input: fix refcount bug in em28xx_usb_disconnect
  UR: pavel
  - a7dd8b778a4d leds: trigger: audio: Add an activate callback to ensure the initial brightness is set
  UR: uli -- fixes cosmetic problem on 2-in-1 notebooks
  - 917191d582f9 leds: lt3593: Put fwnode in any case during ->probe()
  ACK: uli
  - eef8496579de i2c: highlander: add IRQ check
  UR: pavel -- why is it requesting irq in the poll case; does it leak it?
  - 11dd40c18918 net/mlx5: Fix missing return value in mlx5_devlink_eswitch_inline_mode_set()
  ACK: pavel
  - b376ae5597fc devlink: Break parameter notification sequence to be before/after unload/load driver
  UR: pavel
  - 9fa9ff10408f arm64: dts: renesas: hihope-rzg2-ex: Add EtherAVB internal rx delay
  UR: pavel
  - e4da0e0006f9 arm64: dts: renesas: rzg2: Convert EtherAVB to explicit delay handling
  UR: pavel -- cleanup, not known to fix a bug
  - 61b1db235868 Bluetooth: mgmt: Fix wrong opcode in the response for add_adv cmd
  ACK: pavel
  - bca46d228393 net: cipso: fix warnings in netlbl_cipsov4_add_std
  ACK: pavel -- just a warning workaround
  - b6b5dc12bd7c drm: mxsfb: Clear FIFO_CLEAR bit
  ACK: pavel
  - 1a0014c1c62c drm: mxsfb: Increase number of outstanding requests on V4 and newer HW
  ACK: pavel
  - 46f546394063 drm: mxsfb: Enable recovery on underflow
  ACK: pavel
  - e0f3de1573fd cgroup/cpuset: Fix a partition bug with hotplug
  ACK: pavel
  - 7a0b297480dd net/mlx5e: Block LRO if firmware asks for tunneled LRO
  ACK: pavel
  - c40ed983b874 net/mlx5e: Prohibit inner indir TIRs in IPoIB
  UR: pavel
  - a11fc1cd8a31 ARM: dts: meson8b: ec100: Fix the pwm regulator supply properties
  ACK: pavel
  - 2e68547e99a7 ARM: dts: meson8b: mxq: Fix the pwm regulator supply properties
  ACK: pavel
  - 0d40e59c03b8 ARM: dts: meson8b: odroidc1: Fix the pwm regulator supply properties
  ACK: pavel
  - eda87dd4738a ARM: dts: meson8: Use a higher default GPU clock frequency
  ACK: pavel
  - a7d0a59e21ef tcp: seq_file: Avoid skipping sk during tcp_seek_last_pos
  ACK: pavel -- log explains it does not happen in practice
  - 1f60072320b5 drm/amdgpu/acp: Make PM domain really work
  UR: pavel
  - c7ebd3622bf8 6lowpan: iphc: Fix an off-by-one check of array index
  ACK: pavel
  - def6efdf91e7 Bluetooth: sco: prevent information leak in sco_conn_defer_accept()
  UR: pavel
  - e9a62740876b media: atomisp: fix the uninitialized use and rename "retvalue"
  ACK: pavel
  - b0e87701b813 media: coda: fix frame_mem_ctrl for YUV420 and YVU420 formats
  ACK: pavel
  - c062253748d8 media: rockchip/rga: fix error handling in probe
  UR: pavel -- c/e
  - dc49537334a7 media: rockchip/rga: use pm_runtime_resume_and_get()
  ACK: pavel -- three fixes in one, just a robustness
  - 94d6aa2b871f media: go7007: remove redundant initialization
  UR: pavel --! just a performance optimalization
  - ffd9c8cecbad media: go7007: fix memory leak in go7007_usb_probe
  UR: pavel -- c/e
  - fb22665c37b3 media: dvb-usb: Fix error handling in dvb_usb_i2c_init
  ACK: pavel -- not a minimum fix
  - 6b0fe6953430 media: dvb-usb: fix uninit-value in vp702x_read_mac_addr
  ACK: pavel
  - 372890e0b41e media: dvb-usb: fix uninit-value in dvb_usb_adapter_dvb_init
  UR: pavel -- c/e
  - 83f7297a4af4 ionic: cleanly release devlink instance
  ACK: pavel
  - 203537ff35ea driver core: Fix error return code in really_probe()
  ACK: pavel
  - 4225d357bc75 firmware: fix theoretical UAF race with firmware cache and resume
  ACK: pavel -- very theoretical bug
  - c4aaad8a3389 gfs2: Fix memory leak of object lsi on error return path
  ACK: pavel
  - 8c3b5028ec02 libbpf: Fix removal of inner map in bpf_object__create_map
  ACK: pavel
  - ffb887c15f7f soc: qcom: rpmhpd: Use corner in power_off
  ACK: pavel -- not a minimum fix, and not fixing any real problem
  - f32b433d8e25 i40e: improve locking of mac_filter_hash
  ACK: pavel
  - 5ac21a4e6e85 arm64: dts: renesas: r8a77995: draak: Remove bogus adv7511w properties
  UR: pavel
  - a8c1eaed2374 ARM: dts: aspeed-g6: Fix HVI3C function-group in pinctrl dtsi
  ACK: pavel
  - 6ca0b4089166 libbpf: Fix the possible memory leak on error
  - f1673e85254d gve: fix the wrong AdminQ buffer overflow check
  ACK: pavel
  - 1568dbe8892d drm/of: free the iterator object on failure
  ACK: pavel
  - 389dfd114780 bpf: Fix potential memleak and UAF in the verifier.
  ACK: pavel
  - d4213b709316 bpf: Fix a typo of reuseport map in bpf.h.
  ACK: pavel -- just a comment fix
  - 56e5c527cc2e drm/of: free the right object
  ACK: pavel
  - 38235f195de9 media: cxd2880-spi: Fix an error handling path
  - 25fbfc31ceec soc: rockchip: ROCKCHIP_GRF should not default to y, unconditionally
  ACK: pavel -- just a Kconfig tweak
  - c391728c9b25 leds: is31fl32xx: Fix missing error code in is31fl32xx_parse_dt()
  ACK: pavel
  - d4abb6e14105 media: TDA1997x: enable EDID support
  ACK: pavel
  - 8ce22f85381f ASoC: mediatek: mt8183: Fix Unbalanced pm_runtime_enable in mt8183_afe_pcm_dev_probe
  UR: pavel
  - 3d58f5e83f97 drm/gma500: Fix end of loop tests for list_for_each_entry
  ACK: pavel -- code & interface could be improved
  - 54912723f16b drm/panfrost: Fix missing clk_disable_unprepare() on error in panfrost_clk_init()
  UR: pavel
  - 1e1423449d1c EDAC/i10nm: Fix NVDIMM detection
  ACK: pavel
  - a20e6868cbfc spi: spi-zynq-qspi: use wait_for_completion_timeout to make zynq_qspi_exec_mem_op not interruptible
  ACK: pavel
  - e2cb04c61bcf spi: sprd: Fix the wrong WDG_LOAD_VAL
  ACK: pavel
  - cd8cca7268a2 regulator: vctrl: Avoid lockdep warning in enable/disable ops
  UR: pavel
  - 8665e30317c8 regulator: vctrl: Use locked regulator_get_voltage in probe path
  ACK: pavel
  - 80b1a70b0450 blk-crypto: fix check for too-large dun_bytes
  ACK: pavel
  - ba6e5af621ab spi: davinci: invoke chipselect callback
  ACK: pavel
  - c0aec70a256c x86/mce: Defer processing of early errors
  ACK: pavel -- just an interface tweak for MCE
  - 6627be8b36dc tpm: ibmvtpm: Avoid error message when process gets signal while waiting
  ACK: pavel
  - bd2028e9e27c certs: Trigger creation of RSA module signing key if it's not an RSA key
  ACK: pavel -- just a build system fix for unusual situation
  - fddf3a72abe1 crypto: qat - use proper type for vf_mask
  ACK: pavel
  - e7273d57d2b7 irqchip/gic-v3: Fix priority comparison when non-secure priorities are used
  ACK: pavel
  - f1f6d3d2ada8 spi: coldfire-qspi: Use clk_disable_unprepare in the remove function
  ACK: pavel
  - 4b21d4e820bb block: nbd: add sanity check for first_minor
  UR: pavel --! wrong label used for cleanup?
  - 31fc50cd93cd clocksource/drivers/sh_cmt: Fix wrong setting if don't request IRQ for clock source channel
  ACK: pavel -- two fixes in one
  - dde7ff1c1977 lib/mpi: use kcalloc in mpi_resize
  ACK: pavel
  - 20d84fc59e85 irqchip/loongson-pch-pic: Improve edge triggered interrupt support
  ACK: pavel
  - e9a902f88207 genirq/timings: Fix error return code in irq_timings_test_irqs()
  ACK: pavel
  - 10d3bdd2d578 spi: spi-pic32: Fix issue with uninitialized dma_slave_config
  ACK: pavel -- probably does not cause visible problems
  - d4ec971bfa88 spi: spi-fsl-dspi: Fix issue with uninitialized dma_slave_config
  ACK: pavel -- probably does not cause visible problems
  - 87aa69aa10b4 block: return ELEVATOR_DISCARD_MERGE if possible
  ACK: pavel
  - 386850718153 m68k: Fix invalid RMW_INSNS on CPUs that lack CAS
  ACK: pavel
  - 497f3d9c3f58 rcu: Fix stall-warning deadlock due to non-release of rcu_node ->lock
  ACK: pavel
  - ea5e5bc881a4 rcu: Add lockdep_assert_irqs_disabled() to rcu_sched_clock_irq() and callees
  ACK: pavel -- just a robustness
  - 527b56d7856f rcu: Fix to include first blocked task in stall warning
  ACK: pavel
  - e6778e1b22d0 sched: Fix UCLAMP_FLAG_IDLE setting
  ACK: pavel
  - 718180c24675 sched/numa: Fix is_core_idle()
  ACK: pavel
  - bf4b0fa3a2e2 m68k: emu: Fix invalid free in nfeth_cleanup()
  ACK: pavel
  - 246c771b8562 power: supply: cw2015: use dev_err_probe to allow deferred probe
  ACK: pavel -- just a printk tweak
  - a758b1d4ca20 s390/ap: fix state machine hang after failure to enable irq
  - 86f9980909f3 s390/debug: fix debug area life cycle
  - 0980d2b21f4d s390/debug: keep debug data on resize
  - 0404bf4a660c s390/pci: fix misleading rc in clp_set_pci_fn()
  - 8b471e72b51e s390/kasan: fix large PMD pages address alignment check
  - 9d999957cb39 udf_get_extendedattr() had no boundary checks.
  ACK: pavel
  - db2f238d8d12 fcntl: fix potential deadlock for &fasync_struct.fa_lock
  ACK: pavel
  - 349633ed311c crypto: qat - do not export adf_iov_putmsg()
  UR: pavel --!
  - 205cfad5c0ca crypto: qat - fix naming for init/shutdown VF to PF notifications
  UR: pavel --! just a cleanup? don't need
  - c29cc43e30ba crypto: qat - fix reuse of completion variable
  ACK: pavel
  - e53575ea28d9 crypto: qat - handle both source of interrupt in VF ISR
  ACK: pavel
  - 9819975c636c crypto: qat - do not ignore errors from enable_vf2pf_comms()
  ACK: pavel -- just a robustness
  - 6f3c58bd62f2 crypto: omap - Fix inconsistent locking of device lists
  ACK: pavel
  - fc4073df2968 libata: fix ata_host_start()
  ACK: pavel
  - cf619a528e01 s390/zcrypt: fix wrong offset index for APKA master key valid state
  IGN: pavel
  - b4aa00bf8a4d s390/cio: add dev_busid sysfs entry for each subchannel
  IGN: pavel
  - d0831db736bb power: supply: max17042_battery: fix typo in MAx17042_TOFF
  ACK: pavel -- just a cleanup
  - 5d59f38c6ba5 power: supply: smb347-charger: Add missing pin control activation
  ACK: pavel
  - 10e759e350d7 nvmet: pass back cntlid on successful completion
  ACK: pavel
  - ea4a353c0ef4 nvme-rdma: don't update queue count when failing to set io queues
  ACK: pavel
  - 5d0f0c3bbe99 nvme-tcp: don't update queue count when failing to set io queues
  ACK: pavel
  - 591f69d7c415 blk-throtl: optimize IOPS throttle for large IO scenarios
  ACK: pavel -- just a performance tweak
  - cf13537be54c bcache: add proper error unwinding in bcache_device_init
  ACK: pavel
  - 48aa6e4e28c4 isofs: joliet: Fix iocharset=utf8 mount option
  ACK: pavel -- not a minimum fix
  - 940ac461323e udf: Fix iocharset=utf8 mount option
  UR: pavel --a not nearly a minimum fix, is it suitable for stable?
  - 4cf1551af31d udf: Check LVID earlier
  ACK: pavel -- not a minimum fix
  - 3d12ccecfa31 hrtimer: Ensure timerfd notification for HIGHRES=n
  ACK: pavel
  - aadfa1d6ca5f hrtimer: Avoid double reprogramming in __hrtimer_start_range_ns()
  ACK: pavel -- just a performance tweak
  - 13ccaef77ee8 posix-cpu-timers: Force next expiration recalc after itimer reset
  ACK: pavel -- just a performance tweak
  - 8a6c5eec811c EDAC/mce_amd: Do not load edac_mce_amd module on guests
  ACK: pavel -- just a warning workaround
  - 4b680b3fc6f3 rcu/tree: Handle VM stoppage in stall detection
  ACK: pavel
  - 1cc05d71f04d sched/deadline: Fix missing clock update in migrate_task_rq_dl()
  ACK: pavel -- just a WARN fix
  - 104adbffbe4c crypto: omap-sham - clear dma flags only after omap_sham_update_dma_stop()
  ACK: pavel -- just a cleanup
  - ce7f2b516c77 power: supply: axp288_fuel_gauge: Report register-address on readb / writeb errors
  ACK: pavel -- just a printk tweak
  - 3ebd7b38415e sched/deadline: Fix reset_on_fork reporting of DL tasks
  ACK: pavel
  - 8c4d94db5acd crypto: mxs-dcp - Check for DMA mapping errors
  ACK: pavel
  - 7bb6302e9d09 regulator: tps65910: Silence deferred probe error
  ACK: pavel -- just a printk tweak
  - a85985099644 regmap: fix the offset of register error log
  ACK: pavel -- just a printk fix
  - 97bc540bfb61 locking/mutex: Fix HANDOFF condition
  ACK: pavel -- not a minimum fix
