# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v6.1.82
  - d7543167affd3 Linux 6.1.82
  - cf4b8c39b9a0b fs/proc: do_task_stat: use sig->stats_lock to gather the threads/children stats
  ACK: pavel -- just a performance fix (in critical section)
  - d95ef75162f47 fs/proc: do_task_stat: use __for_each_thread()
  ACK: pavel -- just a preparation
  - 9793a3bb531c5 getrusage: use sig->stats_lock rather than lock_task_sighand()
  ACK: pavel -- just a lockup warning fix
  - 2a304d8c922f2 getrusage: use __for_each_thread()
  ACK: pavel
  - d9fe6ef245766 getrusage: move thread_group_cputime_adjusted() outside of lock_task_sighand()
  ACK: pavel
  - eba76e4808c9a getrusage: add the "signal_struct *sig" local variable
  ACK: pavel -- just a preparation
  - 01d992088dce3 drm/amd/display: Fix MST Null Ptr for RV
  ACK: pavel
  - f0b6dc034e179 drm/amd/display: Wrong colorimetry workaround
  ACK: pavel -- just an interface tweak
  - 92cdc9d71ab08 selftests: mptcp: decrease BW in simult flows
  ACK: pavel -- just a test fix
  - b2e92ab17e440 KVM/x86: Export RFDS_NO and RFDS_CLEAR to guests
  ACK: pavel
  - d405b9c03f06b x86/rfds: Mitigate Register File Data Sampling (RFDS)
  ACK: pavel -- workaround for new hw bug
  - 29476fac750dd Documentation/hw-vuln: Add documentation for RFDS
  ACK: pavel -- just a documentation addition
  - 8b5760939db9c x86/mmio: Disable KVM mitigation when X86_FEATURE_CLEAR_CPU_BUF is set
  ACK: pavel
  - a28f4d1e0bed8 drm/amdgpu: Reset IH OVERFLOW_CLEAR bit
  ACK: pavel
  - 2e3ec80ea7ba5 xhci: handle isoc Babble and Buffer Overrun events properly
  ACK: pavel
  - 9158ea9395c12 xhci: process isoc TD properly when there was a transaction error mid TD.
  UR: pavel -- c/e
  - a584c7734a4dd selftests: mm: fix map_hugetlb failure on 64K page size systems
  ACK: pavel -- just a test fix
  - 02e16a41e5439 selftests/mm: switch to bash from sh
  ACK: pavel -- just a compatibility tweak
  - f0c349708290f readahead: avoid multiple marked readahead pages
  ACK: pavel
  - b0b89b470a863 nfp: flower: add hardware offload check for post ct entry
  ACK: pavel
  - 4e2f0cae0bfe6 nfp: flower: add goto_chain_index for ct entry
  ACK: pavel -- just a preparation
  - 66d663da86540 drm/amd/display: Fix uninitialized variable usage in core_link_ 'read_dpcd() & write_dpcd()' functions
  ACK: pavel -- just a warning fix
  - 35a0d43cee095 ASoC: codecs: wcd938x: fix headphones volume controls
  ACK: pavel -- just an API fix
  - 5df3b81a567eb KVM: s390: vsie: fix race during shadow creation
  IGN: pavel
  - 51c4435688ebe KVM: s390: add stat counter for shadow gmap events
  IGN: pavel
  - 43464808669ba netrom: Fix data-races around sysctl_net_busy_read
  ACK: pavel -- just a READ_ONCE annotation
  - cfedde3058bf9 netrom: Fix a data-race around sysctl_netrom_link_fails_count
  ACK: pavel -- just a READ_ONCE annotation
  - 4c02b9ccbb118 netrom: Fix a data-race around sysctl_netrom_routing_control
  ACK: pavel -- just a READ_ONCE annotation
  - 498f1d6da11ed netrom: Fix a data-race around sysctl_netrom_transport_no_activity_timeout
  ACK: pavel -- just a READ_ONCE annotation
  - 46803b776d869 netrom: Fix a data-race around sysctl_netrom_transport_requested_window_size
  ACK: pavel -- just a READ_ONCE annotation
  - 5ac337138272d netrom: Fix a data-race around sysctl_netrom_transport_busy_delay
  ACK: pavel -- just a READ_ONCE annotation
  - 5deaef2bf5645 netrom: Fix a data-race around sysctl_netrom_transport_acknowledge_delay
  ACK: pavel -- just a READ_ONCE annotation
  - d28fa5f0e6c15 netrom: Fix a data-race around sysctl_netrom_transport_maximum_tries
  ACK: pavel -- just a READ_ONCE annotation
  - fed835d415766 netrom: Fix a data-race around sysctl_netrom_transport_timeout
  ACK: pavel -- just a READ_ONCE annotation
  - a47d68d777b41 netrom: Fix data-races around sysctl_netrom_network_ttl_initialiser
  ACK: pavel -- just a READ_ONCE annotation
  - e439607291c08 netrom: Fix a data-race around sysctl_netrom_obsolescence_count_initialiser
  ACK: pavel -- just a READ_ONCE annotation
  - dec82a8fc45c6 netrom: Fix a data-race around sysctl_netrom_default_path_quality
  ACK: pavel -- just a READ_ONCE annotation
  - 6e49f3ac43e29 erofs: apply proper VMA alignment for memory mapped files on THP
  ACK: pavel
  - 39001e3c42000 netfilter: nf_conntrack_h323: Add protection for bmp length out of range
  ACK: pavel
  - bce83144ba7ec netfilter: nft_ct: fix l3num expectations with inet pseudo family
  ACK: pavel -- just an API tweak
  - 998fd719e6d64 net/rds: fix WARNING in rds_conn_connect_if_down
  ACK: pavel
  - 7faff12e828d9 net: dsa: microchip: fix register write order in ksz8_ind_write8()
  ACK: pavel
  - 3420b3ff1ff48 cpumap: Zero-initialise xdp_rxq_info struct before running XDP program
  ACK: pavel
  - 394334fe2ae3b net/ipv6: avoid possible UAF in ip6_route_mpath_notify()
  ACK: pavel
  - 63a3c1f3c9ecc igc: avoid returning frame twice in XDP_REDIRECT
  ACK: pavel -- not a minimum fix
  - afdd29726a6de net: ice: Fix potential NULL pointer dereference in ice_bridge_setlink()
  ACK: pavel
  - 6293ff942e9ce ice: virtchnl: stop pretending to support RSS over AQ or registers
  ACK: pavel -- mostly theoretical bug
  - 0de693d68b0a1 net: sparx5: Fix use after free inside sparx5_del_mact_entry
  ACK: pavel
  - c0b22568a9d83 geneve: make sure to pull inner header in geneve_rx()
  ACK: pavel
  - 44faf8a48294f tracing/net_sched: Fix tracepoints that save qdisc_dev() as a string
  ACK: pavel
  - 5822c02707dac ice: reorder disabling IRQ and NAPI in ice_qp_dis
  ACK: pavel
  - 8e23edc54a5cb i40e: disable NAPI right after disabling irqs when handling xsk_pool
  ACK: pavel
  - 6632e19acbdcf ixgbe: {dis, en}able irqs in ixgbe_txrx_ring_{dis, en}able
  ACK: pavel
  - ea2a1052f23c5 net: lan78xx: fix runtime PM count underflow on link stop
  ACK: pavel
  - 850bb481890fa ceph: switch to corrected encoding of max_xattr_size in mdsmap
  ACK: pavel
