#! /usr/bin/make -f
# -*- makefile -*-

# It is useful to do "git remote update" in the repository before running this
REPO:=../kernel
LATEST_4_9=$(shell cd $(REPO); git tag -l v4.9.* --sort=version:refname | grep -v -- - | tail -1 | sed -e 's/v4.9.//g')
LATEST_4_19=$(shell cd $(REPO); git tag -l v4.19.*  --sort=version:refname | grep -v -- - | tail -1 | sed -e 's/v4.19.//g')
LATEST_5_10=$(shell cd $(REPO); git tag -l v5.10.*  --sort=version:refname | grep -v -- - | tail -1 | sed -e 's/v5.10.//g')
LATEST_6_1=$(shell cd $(REPO); git tag -l v6.1.*  --sort=version:refname | grep -v -- - | tail -1 | sed -e 's/v6.1.//g')
TARGETVER:=v4.9 v4.19 v5.10 v6.1

define create_commit_list
	ver=$2 ; \
	while [ $$ver -lt $3 ] ; do \
		ver_s=$$ver ; \
		ver=$$((ver+1)) ; \
		if [ -e $(PWD)/linux-kernel-$1.$$ver-commit.list ] ; then \
			continue ; \
		fi ; \
		cat  $(PWD)/header > $(PWD)/linux-kernel-$1.$$ver-commit.list; \
		echo "## $1.$$ver" >> $(PWD)/linux-kernel-$1.$$ver-commit.list; \
		if [ $$ver_s -eq 0 ] ; then \
			git log --oneline $1...$1.$$ver | sed -e "s/^/  - /g" >> $(PWD)/linux-kernel-$1.$$ver-commit.list; \
		else \
			git log --oneline $1.$$ver_s...$1.$$ver | sed -e "s/^/  - /g" >> $(PWD)/linux-kernel-$1.$$ver-commit.list; \
		git add linux-kernel-$1.$$ver-commit.list; \
		fi \
	done
endef

all: $(TARGETVER)
v4.9: ${REPO}
	cd ${REPO}; \
	$(call create_commit_list,$@,154,${LATEST_4_9})

v4.19: ${REPO}
	cd ${REPO}; \
	$(call create_commit_list,$@,0,${LATEST_4_19})

v5.10: ${REPO}
	cd ${REPO}; \
	$(call create_commit_list,$@,0,${LATEST_5_10})

v6.1: ${REPO}
	cd ${REPO}; \
	$(call create_commit_list,$@,0,${LATEST_6_1})
