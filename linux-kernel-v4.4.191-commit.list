# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v4.4.191
  - efbc4a3 Linux 4.4.191
  - 61263fb x86/ptrace: fix up botched merge of spectrev1 fix
  - 8c0932c mac80211: fix possible sta leak
  - 3161dea Revert "cfg80211: fix processing world regdomain when non modular"
  - 16ab568 VMCI: Release resource if the work is already queued
  - c25b7d7 stm class: Fix a double free of stm_source_device
  - e2f1509 mmc: core: Fix init of SD cards reporting an invalid VDD range
  - 12a897d mmc: sdhci-of-at91: add quirk for broken HS200
  - 4d1a416 uprobes/x86: Fix detection of 32-bit user mode
  - d6029fe ptrace,x86: Make user_64bit_mode() available to 32-bit builds
  - 4761474 USB: storage: ums-realtek: Whitelist auto-delink support
  - 00087c6 USB: storage: ums-realtek: Update module parameter description for auto_delink_en
  - eb1ff50 usb: host: ohci: fix a race condition between shutdown and irq
  - 0d743d8 USB: cdc-wdm: fix race between write and disconnect due to flag abuse
  - 6f575b5 usb-storage: Add new JMS567 revision to unusual_devs
  - 5bd0d83 x86/apic: Include the LDR when clearing out APIC registers
  - 2628503 x86/apic: Do not initialize LDR and DFR for bigsmp
  - 0fff074 KVM: x86: Don't update RIP or do single-step on faulting emulation
  - e3abf92 ALSA: seq: Fix potential concurrent access to the deleted pool
  - ec19335 tcp: make sure EPOLLOUT wont be missed
  - a485888 ALSA: usb-audio: Fix an OOB bug in parse_audio_mixer_unit
  - 735a16d ALSA: usb-audio: Fix a stack buffer overflow bug in check_input_term
  - 1a48b39 tcp: fix tcp_rtx_queue_tail in case of empty retransmit queue
  - 59326db watchdog: bcm2835_wdt: Fix module autoload
  - 348e556 tools: hv: fix KVP and VSS daemons exit code
  - 6cf3f1c usb: host: fotg2: restart hcd after port reset
  - c66c6ac usb: gadget: composite: Clear "suspended" on reset/disconnect
  - d4d31cf dmaengine: ste_dma40: fix unneeded variable warning
  - ffb7396 scsi: ufs: Fix NULL pointer dereference in ufshcd_config_vreg_hpm()
  - 721a2f8 x86/CPU/AMD: Clear RDRAND CPUID bit on AMD family 15h/16h
  - f0395f6 x86/pm: Introduce quirk framework to save/restore extra MSR registers around suspend/resume
  - f5a3392 Revert "perf test 6: Fix missing kvm module load for s390"
  - b7a27ca netfilter: conntrack: Use consistent ct id hash calculation
  - 36bbd86 netfilter: ctnetlink: don't use conntrack/expect object addresses as id
  - 66f8c5f inet: switch IP ID generator to siphash
  - 71b951c siphash: implement HalfSipHash1-3 for hash tables
  - 994fcca siphash: add cryptographically secure PRF
  - 6ca2436 vhost: scsi: add weight support
  - bb85b4c vhost_net: fix possible infinite loop
  - 9e0b340 vhost: introduce vhost_exceeds_weight()
  - 9429104 vhost_net: introduce vhost_exceeds_weight()
  - 7f3cfe5 vhost_net: use packet weight for rx handler, too
  - e44915d vhost-net: set packet weight of tx polling to 2 * vq size
  - b31c993 net: arc_emac: fix koops caused by sk_buff free
  - d61e517 GFS2: don't set rgrp gl_object until it's inserted into rgrp tree
  - d60df1c cgroup: Disable IRQs while holding css_set_lock
  - 1f1d46f dm table: fix invalid memory accesses with too high sector number
  - 77529c2 dm space map metadata: fix missing store of apply_bops() return value
  - 063c4d1 dm btree: fix order of block initialization in btree_split_beneath
  - 24a5ed6 x86/boot: Fix boot regression caused by bootparam sanitizing
  - 41664b9 x86/boot: Save fields explicitly, zero out everything else
  - a0a0e3b x86/apic: Handle missing global clockevent gracefully
  - 35b67f4 x86/retpoline: Don't clobber RFLAGS during CALL_NOSPEC on i386
  - 858cfbe userfaultfd_release: always remove uffd flags and clear vm_userfaultfd_ctx
  - 74c6926 Revert "dm bufio: fix deadlock with loop device"
  - a19535e HID: wacom: correct misreported EKR ring values
  - a43b6e0 selftests: kvm: Adding config fragments
  - d2b2a7d libata: add SG safety checks in SFF pio transfers
  - 114bf65 net: hisilicon: Fix dma_map_single failed on arm64
  - e3ae5d3 net: hisilicon: fix hip04-xmit never return TX_BUSY
  - 47cadfd net: hisilicon: make hip04_tx_reclaim non-reentrant
  - acd6061 net: cxgb3_main: Fix a resource leak in a error path in 'init_one()'
  - d72eb71 NFSv4: Fix a potential sleep while atomic in nfs4_do_reclaim()
  - 1f46dbe can: peak_usb: force the string buffer NULL-terminated
  - 5833540 can: sja1000: force the string buffer NULL-terminated
  - 2931cc0 perf bench numa: Fix cpu0 binding
  - d1f1bad isdn: hfcsusb: Fix mISDN driver crash caused by transfer buffer on the stack
  - 9530580 isdn: mISDN: hfcsusb: Fix possible null-pointer dereferences in start_isoc_chain()
  - cbfc456 net: usb: qmi_wwan: Add the BroadMobi BM818 card
  - 25772b1 ASoC: ti: davinci-mcasp: Correct slot_width posed constraint
  - 7cdc112 st_nci_hci_connectivity_event_received: null check the allocation
  - c697bfc st21nfca_connectivity_event_received: null check the allocation
  - dc84388 can: dev: call netif_carrier_off() in register_candev()
  - 5927e91 bonding: Force slave speed check after link state recovery for 802.3ad
  - 2fd7fdc netfilter: ebtables: fix a memory leak bug in compat
  - afbd69c MIPS: kernel: only use i8253 clocksource with periodic clockevent
  - 4322f94 HID: Add 044f:b320 ThrustMaster, Inc. 2 in 1 DT
