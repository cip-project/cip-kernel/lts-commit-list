# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v5.10.73
  - 0268aa579b1f Linux 5.10.73
  - 825c00c2ee14 x86/hpet: Use another crystalball to evaluate HPET usability
  UR: pavel -- not a minimum fix, did not have enough testing
  - f2447f6587b8 x86/entry: Clear X86_FEATURE_SMAP when CONFIG_X86_SMAP=n
  ACK: pavel
  - 6bfe1f6fc876 x86/entry: Correct reference to intended CONFIG_64_BIT
  ACK: pavel -- not sure it fixes user-visible bug
  - 5d637bc6f98a x86/sev: Return an error on a returned non-zero SW_EXITINFO1[31:0]
  ACK: pavel
  - df121cf55003 x86/Kconfig: Correct reference to MWINCHIP3D
  ACK: pavel
  - d7c36115fb81 x86/platform/olpc: Correct ifdef symbol to intended CONFIG_OLPC_XO15_SCI
  ACK: pavel
  - f73ca4961d51 pseries/eeh: Fix the kdump kernel crash during eeh_pseries_init
  ACK: pavel
  - 411b38fe68ba powerpc/64s: fix program check interrupt emergency stack path
  ACK: pavel -- just a cleanup
  - 18a2a2cafcf9 powerpc/bpf: Fix BPF_SUB when imm == 0x80000000
  ACK: pavel
  - a4037dded56b RISC-V: Include clone3() on rv32
  ACK: pavel -- just an API tweak
  - 29fdb11ca88d bpf, s390: Fix potential memory leak about jit_data
  - 2c152d9da8fe riscv/vdso: make arch_setup_additional_pages wait for mmap_sem for write killable
  UR: pavel -- c/e
  - de834e12b96d i2c: mediatek: Add OFFSET_EXT_CONF setting back
  ACK: pavel
  - f86de018fd7a i2c: acpi: fix resource leak in reconfiguration device addition
  ACK: pavel
  - 87990a60b45f powerpc/iommu: Report the correct most efficient DMA mask for PCI devices
  - 985cca1ad11e net: prefer socket bound to interface when not in VRF
  ACK: pavel
  - 97aeed72af4f i40e: Fix freeing of uninitialized misc IRQ vector
  ACK: pavel
  - 2dc768a98c9b i40e: fix endless loop under rtnl
  ACK: pavel -- just a robustness
  - d3a07ca78ace gve: report 64bit tx_bytes counter from gve_handle_report_stats()
  ACK: pavel -- just a statistics fix
  - 35f6ddd934e6 gve: fix gve_get_stats()
  ACK: pavel
  - 9a043022522e rtnetlink: fix if_nlmsg_stats_size() under estimation
  ACK: pavel -- just a bug that doesn't manifest in current config
  - 72c2a68f1d83 gve: Avoid freeing NULL pointer
  UR: pavel -- check -- no need to free !msix_vectors case?; not a minimum fix
  - 5d903a694b08 gve: Correct available tx qpl check
  ACK: pavel
  - f69556a42043 drm/nouveau/debugfs: fix file release memory leak
  ACK: pavel
  - 65fff0a8efcd drm/nouveau/kms/nv50-: fix file release memory leak
  ACK: pavel
  - f86e19d918a8 drm/nouveau: avoid a use-after-free when BO init fails
  ACK: pavel
  - 008224cdc126 video: fbdev: gbefb: Only instantiate device when built for IP32
  ACK: pavel
  - d2ccbaaa6615 drm/sun4i: dw-hdmi: Fix HDMI PHY clock setup
  UR: pavel
  - 18d2568cc7ff bus: ti-sysc: Use CLKDM_NOAUTO for dra7 dcan1 for errata i893
  ACK: pavel -- just an improvement for bisection
  - 40a84fcae2bf perf jevents: Tidy error handling
  - 628b31d96711 netlink: annotate data races around nlk->bound
  ACK: pavel -- just a KCSAN annotation
  - 144715fbab1b net: sfp: Fix typo in state machine debug string
  ACK: pavel -- just an interface tweak
  - 3ec73ffeef54 net/sched: sch_taprio: properly cancel timer from taprio_destroy()
  ACK: pavel
  - 60955b65bd6a net: bridge: fix under estimation in br_get_linkxstats_size()
  ACK: pavel
  - c480d15190eb net: bridge: use nla_total_size_64bit() in br_get_linkxstats_size()
  ACK: pavel
  - cb8880680bdf ARM: imx6: disable the GIC CPU interface before calling stby-poweroff sequence
  ACK: pavel
  - 2b0035d1058a dt-bindings: drm/bridge: ti-sn65dsi86: Fix reg value
  ACK: pavel -- just a warning fix
  - 10afd1597263 arm64: dts: ls1028a: add missing CAN nodes
  ACK: pavel -- just adds disabled controllers
  - 95ba03fb4cb1 ptp_pch: Load module automatically if ID matches
  ACK: pavel
  - 442ea65d0ccb powerpc/fsl/dts: Fix phy-connection-type for fm1mac3
  UR: pavel -- grep
  - acff2d182c07 net_sched: fix NULL deref in fifo_set_limit()
  ACK: pavel
  - 0d2dd40a7be6 phy: mdio: fix memory leak
  ACK: pavel
  - 6e6f79e39830 net/mlx5: E-Switch, Fix double allocation of acl flow counter
  ACK: pavel -- not a minimum fix
  - d70cb6c77ad9 net/mlx5e: IPSEC RX, enable checksum complete
  ACK: pavel -- just a performance tweak
  - 064faa8e8a9b bpf: Fix integer overflow in prealloc_elems_and_freelist()
  ACK: pavel
  - d5f4b27c3cfc soc: ti: omap-prm: Fix external abort for am335x pruss
  ACK: pavel -- not a minimum fix, the error only happens in 5.15
  - 1d8f4447e8c4 bpf, arm: Fix register clobbering in div/mod implementation
  ACK: pavel
  - 29a19eaeb29d iwlwifi: pcie: add configuration of a Wi-Fi adapter on Dell XPS 15
  ACK: pavel
  - 6b0132f73094 xtensa: call irqchip_init only when CONFIG_USE_OF is selected
  ACK: pavel
  - 3d288ed98314 xtensa: use CONFIG_USE_OF instead of CONFIG_OF
  ACK: pavel -- exposes latent bug, fixed by next patch
  - 997bec509a83 arm64: dts: qcom: pm8150: use qcom,pm8998-pon binding
  ACK: pavel
  - fbca14abc111 ath5k: fix building with LEDS=m
  UR: pavel --a cc me...
  - 8aef3824e946 PCI: hv: Fix sleep while in non-sleep context when removing child devices from the bus
  ACK: pavel
  - d9b838ae390e ARM: dts: imx6qdl-pico: Fix Ethernet support
  ACK: pavel
  - 9e99ad4194a5 ARM: dts: imx: Fix USB host power regulator polarity on M53Menlo
  UR: pavel
  - 2ba34cf0c16c ARM: dts: imx: Add missing pinctrl-names for panel on M53Menlo
  ACK: pavel
  - 8f977e97b2b9 soc: qcom: mdt_loader: Drop PT_LOAD check on hash segment
  ACK: pavel
  - 14f52004bda5 ARM: at91: pm: do not panic if ram controllers are not enabled
  UR: pavel -- just a robustness, c/e
  - d89a313a5739 ARM: dts: qcom: apq8064: Use 27MHz PXO clock as DSI PLL reference
  ACK: pavel
  - 25ac88e601eb soc: qcom: socinfo: Fixed argument passed to platform_set_data()
  ACK: pavel
  - ab8073794be3 bus: ti-sysc: Add break in switch statement in sysc_init_soc()
  ACK: pavel -- just a warning fix
  - 427faa29e06f riscv: Flush current cpu icache before other cpus
  ACK: pavel -- interesting workaround, maybe in wrong place
  - 05287407dedf ARM: dts: qcom: apq8064: use compatible which contains chipid
  ACK: pavel -- just a cleanup
  - ac06fe40e889 ARM: dts: imx6dl-yapp4: Fix lp5562 LED driver probe
  UR: pavel --a might be better to fix C code
  - 71d3ce62ac88 ARM: dts: omap3430-sdp: Fix NAND device node
  ACK: pavel
  - f9a855d1bcb2 xen/balloon: fix cancelled balloon action
  ACK: pavel
  - 9aac782ab0ab SUNRPC: fix sign error causing rpcsec_gss drops
  ACK: pavel
  - 8f174a208c4c nfsd4: Handle the NFSv4 READDIR 'dircount' hint being zero
  ACK: pavel
  - 12d4b179022a nfsd: fix error handling of register_pernet_subsys() in init_nfsd()
  ACK: pavel
  - 1bc2f315a215 ovl: fix IOCB_DIRECT if underlying fs doesn't support direct IO
  UR: pavel -- marked for 4.19 but not there? c/e
  - 9763ffd4da21 ovl: fix missing negative dentry check in ovl_rename()
  ACK: pavel
  - 1500f0c83670 mmc: sdhci-of-at91: replace while loop with read_poll_timeout
  ACK: pavel -- cleanup, not a bugfix
  - 3a0feae5f642 mmc: sdhci-of-at91: wait for calibration done before proceed
  pavel -- version of this patch for older release reviewed
  - e5cb3680b958 mmc: meson-gx: do not use memcpy_to/fromio for dram-access-quirk
  ACK: pavel
  - 13d17cc717d5 xen/privcmd: fix error handling in mmap-resource processing
  ACK: pavel
  - de1e8bd36ab4 drm/nouveau/kms/tu102-: delay enabling cursor until after assign_windows
  ACK: pavel
  - 1d4e9f27d20d usb: typec: tcpm: handle SRC_STARTUP state if cc changes
  ACK: pavel
  - feb3fe702a58 USB: cdc-acm: fix break reporting
  ACK: pavel
  - fc8b3e838bdf USB: cdc-acm: fix racy tty buffer accesses
  ACK: pavel
  - b3265b88e83b usb: chipidea: ci_hdrc_imx: Also search for 'phys' phandle
  ACK: pavel -- just a dt compatibility
  - 16d728110bd7 Partially revert "usb: Kconfig: using select for USB_COMMON dependency"
  ACK: pavel -- just a Kconfig tweak
