====== Stable Kernel Patches Review Status ======
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)


===== v4.4.175 =====
  - 332deb1f5ce9 Linux 4.4.175
  
    ACK: pavel, iwamatsu

  - cb4f43bf87eb uapi/if_ether.h: move __UAPI_DEF_ETHHDR libc define
 
    ACK: pavel, iwamatsu

  - d5800cff4fb8 pinctrl: msm: fix gpio-hog related boot issues

    ACK: pavel,iwamatsu

  - a4af2252e5e2 usb: dwc2: Remove unnecessary kfree

    ACK: pavel, iwamatsu

  - 49cf68d70f47 kaweth: use skb_cow_head() to deal with cloned skbs

    pavel -- performance optimalization only, AFAICT.

  - 4307a871ce57 ch9200: use skb_cow_head() to deal with cloned skbs

    ACK: pavel, iwamatsu

  - f8e3d1b10140 smsc95xx: Use skb_cow_head to deal with cloned skbs

    ACK: pavel, iwamatsu

  - 27a70770c37d dm thin: fix bug where bio that overwrites thin block ignores FUA

    ACK: pavel, iwamatsu

  - 5079b1d1aac9 x86/a.out: Clear the dump structure initially

    ACK: pavel, iwamatsu

  - 492647b22f99 signal: Restore the stop PTRACE_EVENT_EXIT

    ACK: pavel, iwamatsu

  - 7212e37cbdf9 x86/platform/UV: Use efi_runtime_lock to serialise BIOS calls

    ACK: pavel -- strange, patch is marked as v4.9+ but was applied to 4.4

  - 137f4db172af tracing/uprobes: Fix output for multiple string arguments

    ACK: pavel, iwamatsu

  - 5fc9518605a9 alpha: Fix Eiger NR_IRQS to 128

    ACK: pavel, iwamatsu

  - 1d8b20304de1 alpha: fix page fault handling for r16-r18 targets

    ACK: pavel, iwamatsu

  - fd08513bf56e Input: elantech - enable 3rd button support on Fujitsu CELSIUS H780

    ACK: pavel, iwamatsu

  - 6d6d6255b7a8 Input: bma150 - register input device after setting private data

    ACK: pavel, iwamatstu

  - 05268b5cca5f ALSA: usb-audio: Fix implicit fb endpoint setup by quirk

    ACK: pavel, iwmaatsu

  - 422e1adf1714 ALSA: hda - Add quirk for HP EliteBook 840 G5

    ACK: pavel, iwamatsu

  - 222b22e1f32d perf/core: Fix impossible ring-buffer sizes warning

    ACK: pavel, iwamatsu
    ACK: pavel

  - b7febf3b2909 Input: elan_i2c - add ACPI ID for touchpad in Lenovo V330-15ISK

    ACK: pavel, iwamatsu

  - d347a8948c92 Revert "Input: elan_i2c - add ACPI ID for touchpad in ASUS Aspire F5-573G"

    ACK: pavel, iwamatsu

  - 29c84aa9f2a2 Documentation/network: reword kernel version reference

    ACK: pavel -- this is kind of interesting. I guess mainline should include this, then, too...

  - 1f39e518cf91 cifs: Limit memory used by lock request calls to a page

    pavel -- the description says it could be an problem in unusual case, basically. Not sure it is good idea.

  - 834767012751 gpio: pl061: handle failed allocations

    ACK: pavel, iwamatsu

  - 32f047108851 ARM: dts: kirkwood: Fix polarity of GPIO fan lines

    pavel -- dunno. Looks like bugfix, probably will not break anything, but may not be neccessary.

  - 20ad50464b74 ARM: dts: da850-evm: Correct the sound card name

    pavel -- fixing code to allow /s in the name would be prefferable. This may change someone's sound config, AFAICT.

  - 28b5d0be0ab6 uapi/if_ether.h: prevent redefinition of struct ethhdr

    pavel -- adds support for musl. Not strictly a bugfix.
    iwamatsu -- Note: da360299b6734135a5f66d7db458dcc7801c826a is also necessary. 

  - 7cbbbf750ef6 Revert "exec: load_script: don't blindly truncate shebang string"

    ACK: pavel, iwamatsu

  - b2942d59a3cd batman-adv: Force mac header to start of data on xmit

    ACK: pavel, iwamatsu

  - 8fe161469f18 batman-adv: Avoid WARN on net_device without parent in netns

    ACK: pavel, iwamatsu

  - ca6fd8df6f73 xfrm: refine validation of template and selector families

    ACK: pavel, iwamatsu

  - 2e9e4e1590d2 libceph: avoid KEEPALIVE_PENDING races in ceph_con_keepalive()

    ACK: pavel, iwamatsu

  - 1793dc65ee85 Revert "cifs: In Kconfig CONFIG_CIFS_POSIX needs depends on legacy (insecure cifs)"

    ACK: pavel, iwamatsu

  - 2a41ed30abe7 NFC: nxp-nci: Include unaligned.h instead of access_ok.h

    ACK: pavel, iwamatsu

  - b661fff5f8a0 HID: debug: fix the ring buffer implementation

    pavel -- patch is bigger than rules allow, not "trivial". Not checked. This is not really easy to check.

  - 697c6f72c493 drm/vmwgfx: Return error code from vmw_execbuf_copy_fence_user

    ACK: pavel, iwamatsu

  - 6bcca0bc4765 drm/vmwgfx: Fix setting of dma masks

    ACK: pavel, iwamatsu

  - f1cd557ecbbf drm/modes: Prevent division by zero htotal

    ACK: pavel, iwamatsu

  - 06288a8e3707 mac80211: ensure that mgmt tx skbs have tailroom for encryption

    ACK: pavel, iwamatsu

  - fd9d0553fba3 ARM: iop32x/n2100: fix PCI IRQ mapping

    ACK: pavel, iwamatsu

  - dfb3536268fc MIPS: VDSO: Include $(ccflags-vdso) in o32,n32 .lds builds

    ACK: pavel, iwamatsu

  - 91aaa0dd778b MIPS: OCTEON: don't set octeon_dma_bar_type if PCI is disabled

    pavel -- saves a bit of memory. Not sure if this is worth being in -stable.

  - d93fdf4464a0 mips: cm: reprime error cause

    pavel -- not really good description. What bug does it fix?

  - 2b46cd1ae687 debugfs: fix debugfs_rename parameter checking

    ACK: pavel, iwamatsu

  - 99b23b0d5fd5 misc: vexpress: Off by one in vexpress_syscfg_exec()

    ACK: pavel, iwamatsu

  - 60de9fffbe5d signal: Better detection of synchronous signals

    pavel -- quite complex. Does not fix a clear bug, just "arguably incorrect behavior".

  - 381fc50960aa signal: Always notice exiting tasks

    ACK: pavel, iwamatsu

  - 6f17dfe5bbd5 mtd: rawnand: gpmi: fix MX28 bus master lockup problem

    ACK: pavel, iwamatsu

  - cb7c96ee91df perf tests evsel-tp-sched: Fix bitwise operator

    ACK: pavel -- perf tests, not sure testsuite is worth fixing.
    iwamatsu -- 03d309711d687460d1345de8a0363f45b1c8cd11 is also necessary

  - 06bbc4838a38 perf/core: Don't WARN() for impossible ring-buffer sizes

    ACK: pavel, iwamatsu

  - 122c0149ad74 x86/MCE: Initialize mce.bank in the case of a fatal error in mce_no_way_out()

    ACK: pavel, iwamatsu

  - 00025153e202 perf/x86/intel/uncore: Add Node ID mask

    ACK: pavel, iwamatsu

  - 9872ddae1949 KVM: nVMX: unconditionally cancel preemption timer in free_nested (CVE-2019-7221)

    ACK: pavel, iwamatsu

  - 1b5fd913a4eb KVM: x86: work around leak of uninitialized stack contents (CVE-2019-7222)

    ACK: pavel, iwamatsu

  - b0f59c3d1fc6 usb: gadget: udc: net2272: Fix bitwise and boolean operations

    pavel -- this patch apparently received no testing. Do we still want it?

  - ff8c1826eec1 usb: phy: am335x: fix race condition in _probe

    ACK: pavel, iwamatsu

  - b191f1953846 dmaengine: imx-dma: fix wrong callback invoke

    ACK: pavel, iwamatsu

  - c5cf17c81df2 fuse: handle zero sized retrieve correctly

    ACK: pavel, iwamatsu

  - bade8e5f2686 fuse: decrement NR_WRITEB  ACK_TEMP on the right page

    ACK: pavel, iwamatsu

  - a7dfde0f5bc0 fuse: call pipe_buf_release() under pipe lock

    ACK: pavel, iwamatsu

  - 71ce2e8957ff ALSA: hda - Serialize codec registrations

    ACK: pavel, iwamatsu

  - d7204d3860ee ALSA: compress: Fix stop handling on compressed capture streams

    ACK: pavel, iwamatsu

  - b1746f9f05d3 net: dsa: slave: Don't propagate flag changes on down slave interfaces

    ACK: pavel -- "In the other -> On the other"

  - 8d8aafcdd74f net: systemport: Fix WoL with password after deep sleep

    ACK: pavel, iwamatsu

  - 5296ebc355b2 skge: potential memory corruption in skge_get_regs()

    ACK: pavel, iwamatsu

  - b1a5e14594b3 net: dp83640: expire old TX-skb

    ACK: pavel -- unneccessary changes mixed in.

  - 16570a4ab84d enic: fix checksum validation for IPv6

    ACK: pavel -- performance only AFAICT.

  - ff0a4fa3e70f dccp: fool proof ccid_hc_[rt]x_parse_options()

    ACK: pavel, iwamatsu

  - a7ea4de36645 string: drop __must_check from strscpy() and restore strscpy() usages in cgroup

    NAK: pavel -- changelog no longer matches the patch; it only removes __must_check to avoid warnings, but there is no explanation if there are such warnings in 4.4 at all. (but it will not break anything).

  - ae46de2430b7 tipc: use destination length for copy string

    ACK: pavel -- better changelog would be welcome -- this probably fixes buffer overrun.

  - 5ef0ebd78509 test_hexdump: use memcpy instead of strncpy

    ACK: pavel, iwamatsu

  - 93af75d0aba0 thermal: hwmon: inline helpers when CONFIG_THERMAL_HWMON is not set

    ACK: pavel, iwamatsu

  - 9a376109f0c3 exec: load_script: don't blindly truncate shebang string

    pavel -- this is reverted in the same series. Why are we doing it like this?!

  - e1d575b52c32 fs/epoll: drop ovflist branch prediction

    pavel -- 3% performance boost. Would not call this "bad bug". (But this will not break anything).

  - 25768bb65482 kernel/hung_task.c: break RCU locks based on jiffies

    ACK: pavel, iwamatsu

  - 3f516da858df HID: lenovo: Add checks to fix of_led_classdev_register

    pavel -- this does not really fix a bad bug. We might get small behaviour change...

  - a711dcb28339 block/swim3: Fix -EBUSY error when re-opening device after unmount

    ACK: pavel, iwamatsu

  - 4c549499c4c9 gdrom: fix a memory leak bug

    pavel -- memory leak once per rmmod. This is not serious.

  - cab4f01c9cd2 isdn: hisax: hfc_pci: Fix a possible concurrency use-after-free bug in HFCPCI_l1hw()

    ACK: pavel, iwamatsu

  - 80f8149151d8 ocfs2: don't clear bh uptodate for block read

    ACK: pavel, iwamatsu
    ACK: pavel

  - bc5abb80f129 scripts/decode_stacktrace: only strip base path when a prefix of the path

    ACK: pavel, iwamatsu

  - 1f8aea084cb3 niu: fix missing checks of niu_pci_eeprom_read

    NAK: pavel -- this is incorrect. It fails to initialize memory in error case.

  - ccc9ed24493b um: Avoid marking pages with "changed protection"

    pavel -- performance optimalization.

  - 7d1cfc10d42b cifs: check ntwrk_buf_start for NULL before dereferencing it

    ACK: pavel -- just a workaround, not really a fix.

  - b49344a2dc4b crypto: ux500 - Use proper enum in hash_set_dma_transfer

    NAK: pavel -- just a warning fix, for compiler noone uses and not listed in Documentation/Changes

  - 846aa256e610 crypto: ux500 - Use proper enum in cryp_set_dma_transfer

    NAK: pavel -- just a warning fix, for compiler noone uses and not listed in Documentation/Changes

  - cc980857718f seq_buf: Make seq_buf_puts() null-terminate the buffer

    pavel -- the bug would be in caller; yes, this is reasonable, but there's no record it fixes actual bug. 

  - 90beb6bbed67 hwmon: (lm80) fix a missing check of bus read in lm80 probe

    ACK: pavel

  - 259c502d0a0f hwmon: (lm80) fix a missing check of the status of SMBus read

    ACK: pavel -- altrough it contains unrelated change.

  - 8ba3a4eec0a5 NFS: nfs_compare_mount_options always compare auth flavors.

    ACK: pavel, iwamatsu

  - a8014f2741b4 KVM: x86: svm: report MSR_IA32_MCG_EXT_CTL as unsupported

    ACK: pavel -- coding style leaves something to be desired.

  - 2e7c1f0dedb0 fbdev: fbcon: Fix unregister crash when more than one framebuffer

    ACK: pavel, iwamatsu

  - 03c5ed7dc9b9 igb: Fix an issue that PME is not enabled during runtime suspend

    ACK: pavel, iwamatsu

  - 472f3f067cb9 fbdev: fbmem: behave better with small rotated displays and many CPUs

    ACK: pavel, iwamatsu

  - da079f87f933 video: clps711x-fb: release disp device node in probe()

    ACK: pavel, iwamatsu

  - 1fbbe0ccd56a drbd: Avoid Clang warning about pointless switch statment

    NAK: pavel -- fix for warning, only, in compiler noone should use.

  - eb15351d9c12 drbd: skip spurious timeout (ping-timeo) when failing promote
  - 5c857f014b05 drbd: disconnect, if the wrong UUIDs are attached on a connected peer
  - 3b5d41253b54 drbd: narrow rcu_read_lock in drbd_sync_handshake
  - 8b79471d2352 cw1200: Fix concurrency use-after-free bugs in cw1200_hw_scan()

    NAK: pavel -- FIXME: this is wrong. It returns without unlocking the lock in error case. Mainline already has a fix: 51c8d24101c79ffce3e79137e2cee5dfeb956dd7

  - a66490925b70 Bluetooth: Fix unnecessary error message for HCI request completion
 
    ACK: pavel -- this fixes just debug output. Neccessary?

  - f74e04acc2d7 xfrm6_tunnel: Fix spi check in __xfrm6_tunnel_alloc_spi
   -- english in changelog could use some improvements. ... and obfuscated code remains. 
  - b489ed3fbc3b mac80211: fix radiotap vendor presence bitmap handling

    ACK: pavel

  - 3df04b50ef3f powerpc/uaccess: fix warning/error with access_ok()

    pavel -- warning fix, not a serious bug.

  - fac39ee2e52b arm64: KVM: Skip MMIO insn after emulation

    pavel -- not sure what bug it fixes. Sounds like preparation for another patch...

  - 9f01d0d19c73 tty: serial: samsung: Properly set flags in autoCTS mode

    ACK: pavel

  - 98920f4a5d5b memstick: Prevent memstick host from getting runtime suspended during card detection

    ACK: pavel

  - fff375ea867e ASoC: fsl: Fix SND_SOC_EUKREA_TLV320 build error on i.MX8M

    pavel -- in v4.4 ARCH_MXC is 32-bit only, so I don't think this is needed. It will not break anything.

  - b9037406e151 ARM: pxa: avoid section mismatch warning

    pavel -- warning only.

  - ea59fcf67d78 udf: Fix BUG on corrupted inode

    ACK: pavel -- but probably should use different error code and do udf_err()

  - 0d997e1635f7 i2c-axxia: check for error conditions first

    pavel -- bug probably can not happen in our config

  - c4d10ceb9534 cpuidle: big.LITTLE: fix refcount leak

    ACK: pavel -- tiny tiny memory leak. Not sure this is serious bug.

  - e21e67ef6ec8 clk: imx6sl: ensure MMDC CH0 handshake is bypassed

    ACK: pavel, iwamatsu

  - c90636fda3d6 sata_rcar: fix deferred probing

    ACK: pavel -- wrong error code in internal routine, not sure it causes any problems outside

  - 771727c059d9 iommu/arm-smmu-v3: Use explicit mb() when moving cons pointer

    ACK: pavel -- but I'd not do writel()->writel_relaxed() conversion.

  - cab345f0e9b4 mips: bpf: fix encoding bug for mm_srlv32_op

    ACK: pavel, iwamatsu

  - 54b53c599452 ARM: dts: Fix OMAP4430 SDP Ethernet startup

    pavel -- changelog lists 28msec as suitable delay.

  - eb9c64e728f2 timekeeping: Use proper seqcount initializer

    ACK: pavel, iwamatsu

  - 235f1e5e124d usb: hub: delay hub autosuspend if USB3 port is still link training

    ACK: pavel, iwamatsu

  - 7518fa7dc10a smack: fix access permissions for keyring

    ACK: pavel, iwamatsu

  - da327232f52b media: DaVinci-VPBE: fix error handling in vpbe_initialize()

    ACK: pavel, iwamatsu

  - 028f17e00e1a x86/fpu: Add might_fault() to user_insn()

    NAK: pavel -- this is not fixing any serious bug. This is adding diagnostics.

  - 75086100ab60 ARM: dts: mmp2: fix TWSI2

    NAK: pavel -- this fixes olpc-1.75 machine, which will not boot 4.4, anyway. It did not receive any testing outside of that, and is not known to fix any problems outside olpc-1.75.

  - 2d6f9a86cd9a arm64: ftrace: don't adjust the LR value

    ACK: pavel, iwamatsu

  - ef9cc1ec63de nfsd4: fix crash on writing v4_end_grace before nfsd startup

    ACK: pavel

  - 627e656239c6 sunvdc: Do not spin in an infinite loop when vio_ldc_send() returns EAGAIN

    ACK: pavel

  - d67dec906b7a f2fs: fix wrong return value of f2fs_acl_create

    ACK: pavel

  - 262871e63399 f2fs: move dir data flush to write checkpoint process

    pavel -- performance optimalization only, not sure what bug it fixes.

  - f936083094d8 soc/tegra: Don't leak device tree node reference

    ACK: pavel -- tiny memory leak, not sure if it counts as serious.

  - 1e94c7b4db9a perf tools: Add Hygon Dhyana support

    ACK: pavel

  - b02bfd8416d3 modpost: validate symbol names also in find_elf_symbol

    ACK: pavel

  - c34330e1eccd ARM: OMAP2+: hwmod: Fix some section annotations

    pavel -- warning only.

  - eba6d94b54e7 staging: iio: ad7780: update voltage on read

    ACK: pavel

  - 524b2be52946 staging:iio:ad2s90: Make probe handle spi_setup failure

    ACK: pavel

  - caf6a81cd703 ptp: check gettime64 return code in PTP_SYS_OFFSET ioctl

    ACK: pavel

  - 7c6340dfba35 serial: fsl_lpuart: clear parity enable bit when disable parity

    ACK: pavel

  - c9ddfda641c4 powerpc/pseries: add of_node_put() in dlpar_detach_node()

    pavel -- this is revert of 68baf692c435, without being marked as such. Not sure which version is correct.

  - 2f85daf84873 x86/PCI: Fix Broadcom CNB20LE unintended sign extension (redux)

    ACK: pavel, iwamatsu

  - dd307a8d156b dlm: Don't swamp the CPU with callbacks queued during recovery

    pavel -- just realtime behaviour, not serious bug.

  - aeef84f38288 ARM: 8808/1: kexec:offline panic_smp_self_stop CPU

    pavel -- Comment style is wrong. I don't see why this problem is ARM-specific.

  - 871243fc4903 scsi: lpfc: Correct LCB RJT handling

    ACK: pavel, iwamatsu

  - f9d6798d26b9 ASoC: Intel: mrfld: fix uninitialized variable access

    ACK: pavel, iwamatsu

  - 6a7e6587a0c4 staging: iio: adc: ad7280a: handle error from __ad7280_read32()

    ACK: pavel, iwamatsu

  - 3f951709eb24 drm/bufs: Fix Spectre v1 vulnerability

    ACK: pavel, iwamatsu
