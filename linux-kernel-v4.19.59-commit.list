# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v4.19.59
  - 3bd837b Linux 4.19.59
  - 70bae38 staging: rtl8712: reduce stack usage, again
  IGN: pavel
  - b46475e staging: bcm2835-camera: Handle empty EOS buffers whilst streaming
  IGN: pavel
  - 0ee144e staging: bcm2835-camera: Remove check of the number of buffers supplied
  IGN: pavel
  - fcbc6dd staging: bcm2835-camera: Ensure all buffers are returned on disable
  IGN: pavel
  - 4502c43 staging: bcm2835-camera: Replace spinlock protecting context_map with mutex
  IGN: pavel
  - 22a20b9 staging: fsl-dpaa2/ethsw: fix memory leak of switchdev_work
  IGN: pavel
  - cc396af MIPS: Remove superfluous check for __linux__
  ACK: pavel -- just clang fix
  - d202b5a VMCI: Fix integer overflow in VMCI handle arrays
  ACK: pavel -- bigger than limit
  - 486c323 carl9170: fix misuse of device driver API
  ACK: pavel -- locking rewrite to fix firmware loading problem, unlikely to happen in production
  - 524ad00 binder: fix memory leak in error path
  ACK: pavel
  - 294b893 lkdtm: support llvm-objcopy
  ACK: pavel -- llvm fix
  - 5c90a2e HID: Add another Primax PIXART OEM mouse quirk
  ACK: pavel
  - c04c751 staging: comedi: amplc_pci230: fix null pointer deref on interrupt
  IGN: pavel
  - 4e49c6c staging: comedi: dt282x: fix a null pointer deref on interrupt
  IGN: pavel
  - 8419fd5 drivers/usb/typec/tps6598x.c: fix 4CC cmd write
  ACK: pavel -- ok, but .. how could this ever work?
  - 63b3028 drivers/usb/typec/tps6598x.c: fix portinfo width
  ACK: pavel
  - 57e16e0 usb: renesas_usbhs: add a workaround for a race condition of workqueue
  ACK: pavel -- strange workaround, probably more work left there
  - aa9a803 usb: dwc2: use a longer AHB idle timeout in dwc2_core_reset()
  ACK: pavel
  - cac4a04 usb: gadget: ether: Fix race between gether_disconnect and rx_submit
  ACK: pavel -- introduces bad codingstyle
  - 449a8d0 p54usb: Fix race between disconnect and firmware loading
  ACK: pavel -- locking rewrite to fix firmware loading problem, unlikely to happen in production
  - 135d9ba Revert "serial: 8250: Don't service RX FIFO if interrupts are disabled"
  ACK: pavel
  - 0891268 USB: serial: option: add support for GosunCn ME3630 RNDIS mode
  ACK: pavel
  - 0a1c811 USB: serial: ftdi_sio: add ID for isodebug v1
  ACK: pavel
  - bb902b6 mwifiex: Don't abort on small, spec-compliant vendor IEs
  ACK: pavel
  - ffbbd626e mwifiex: Abort at too short BSS descriptor element
  ACK: pavel -- this apparently needs to be fixed by next commit 
  - a2a24b5 Documentation/admin: Remove the vsyscall=native documentation
  ACK: pavel
  - 8a81500 Documentation: Add section about CPU vulnerabilities for Spectre
  ACK: pavel -- long, just a docs update
  - bd96040 x86/tls: Fix possible spectre-v1 in do_get_thread_area()
  ACK: pavel
  - 68ff282 x86/ptrace: Fix possible spectre-v1 in ptrace_get_debugreg()
  ACK: pavel
  - d8e2665 perf pmu: Fix uncore PMU alias list for ARM64
  ACK: pavel -- not a minimal fix.
  - 018524b7 block, bfq: NULL out the bic when it's no longer valid
  ACK: pavel
  - ff75e5f ALSA: hda/realtek - Headphone Mic can't record after S3
  ACK: pavel -- but changelog could use some work
  - 87c3262 ALSA: usb-audio: Fix parse of UAC2 Extension Units
  ACK: pavel
  - ef374f5 media: stv0297: fix frequency range limit
  ACK: pavel
  - 5db079e udf: Fix incorrect final NOT_ALLOCATED (hole) extent length
  ACK: pavel -- too big, but..
  - 0fc3e9b fscrypt: don't set policy for a dead directory
  ACK: pavel
  - e9f76b9 net :sunrpc :clnt :Fix xps refcount imbalance on the error path
  ACK: pavel
  - 810cfc3 NFS4: Only set creation opendata if O_CREAT
  ACK: pavel -- not a minimal fix
  - 7075654 net: dsa: mv88e6xxx: fix shift of FID bits in mv88e6185_g1_vtu_loadpurge()
  ACK: pavel
  - 606561e quota: fix a problem about transfer quota
  ACK: pavel
  - 5ad566a scsi: qedi: Check targetname while finding boot target information
  ACK: pavel
  - 37232ab net: lio_core: fix potential sign-extension overflow on large shift
  ACK: pavel
  - 740b2ac ip6_tunnel: allow not to count pkts on tstats by passing dev as NULL
  ACK: pavel
  - a02ac12 drm: return -EFAULT if copy_to_user() fails
  ACK: pavel
  - 4c938a6 bnx2x: Check if transceiver implements DDM before access
  ACK: pavel
  - 270ae00 md: fix for divide error in status_resync
  ACK: pavel
  - 5533d9e mmc: core: complete HS400 before checking status
  ACK: pavel
  - 2da8053 qmi_wwan: extend permitted QMAP mux_id value range
  ACK: pavel -- interface change, not a serious bug
  - dc84e98 qmi_wwan: avoid RCU stalls on device disconnect when in QMAP mode
  ACK: pavel
  - dbc6a83 qmi_wwan: add support for QMAP padding in the RX path
  ACK: pavel
  - 292ba5b bpf, x64: fix stack layout of JITed bpf code
  ACK: pavel
  - 4c2ce7a bpf, devmap: Add missing RCU read lock on flush
  ACK: pavel
  - ab44f8b bpf, devmap: Add missing bulk queue free
  ACK: pavel
  - 8d09e86 bpf, devmap: Fix premature entry free on destroying map
  ACK: pavel
  - ba0afe5 mac80211: do not start any work during reconfigure flow
  ACK: pavel
  - de8cf2c mac80211: only warn once on chanctx_conf being NULL
  ACK: pavel -- just changes frequency of warning
  - 9c2dd6d ARM: davinci: da8xx: specify dma_coherent_mask for lcdc
  ACK: pavel -- just a WARN_ON fix
  - 3bbcc8b ARM: davinci: da850-evm: call regulator_has_full_constraints()
  ACK: pavel
  - 4432506 mlxsw: spectrum: Disallow prio-tagged packets when PVID is removed
  ACK: pavel -- behaviour change to make api consistent
  - 512bbb1 KVM: arm/arm64: vgic: Fix kvm_device leak in vgic_its_destroy
  ACK: pavel
  - 41420ac Input: imx_keypad - make sure keyboard can always wake up system
  ACK: pavel
  - b71f312 riscv: Fix udelay in RV32.
  ACK: pavel
  - 122c6a7 drm/vmwgfx: fix a warning due to missing dma_parms
  ACK: pavel -- just a WARN_ON fix
  - d3861d4 drm/vmwgfx: Honor the sg list segment size limitation
  ACK: pavel
  - c0b12ab s390/boot: disable address-of-packed-member warning
  IGN: pavel
  - e71daed ARM: dts: am335x phytec boards: Fix cd-gpios active level
  ACK: pavel
  - 822c2ee ibmvnic: Fix unchecked return codes of memory allocations
  ACK: pavel
  - 0f06004 ibmvnic: Refresh device multicast list after reset
  ACK: pavel
  - e65dd52 ibmvnic: Do not close unopened driver during reset
  ACK: pavel
  - 374180b net: phy: rename Asix Electronics PHY driver
  ACK: pavel -- renames a module. Serious enough?
  - 473a75c can: af_can: Fix error path of can_init()
  ACK: pavel
  - 4869542 can: m_can: implement errata "Needless activation of MRAF irq"
  ACK: pavel
  - 270149f can: mcp251x: add support for mcp25625
  pavel -- contains whitespace changes & code cleanups
  - 33672c7 dt-bindings: can: mcp251x: add mcp25625 support
  pavel -- this just updates documentation
  - 07c96e8 soundwire: intel: set dai min and max channels correctly
  ACK: pavel -- not sure what impact is
  - c7e427e mwifiex: Fix heap overflow in mwifiex_uap_parse_tail_ies()
  ACK: pavel
  - 8e115a0 iwlwifi: Fix double-free problems in iwl_req_fw_callback()
  ACK: pavel
  - d4c0f75 mwifiex: Fix possible buffer overflows at parsing bss descriptor
  ACK: pavel
  - b8588a0 mac80211: free peer keys before vif down in mesh
  ACK: pavel
  - acc42e5 mac80211: mesh: fix RCU warning
  ACK: pavel -- just a warning fix
  - e3868c1 staging:iio:ad7150: fix threshold mode config bit
  IGN: pavel
  - 6b1ce39 soundwire: stream: fix out of boundary access on port properties
  ACK: pavel
  - 6be8570 bpf: sockmap, fix use after free from sleep in psock backlog workqueue
  ACK: pavel
  - bc84982 mac80211: fix rate reporting inside cfg80211_calculate_bitrate_he()
  ACK: pavel -- fix interface
  - 3c24a93 samples, bpf: suppress compiler warning
  ACK: pavel -- just a warning fix
  - e777911 samples, bpf: fix to change the buffer size for read()
  ACK: pavel
  - fe01e93 Input: elantech - enable middle button support on 2 ThinkPads
  ACK: pavel
  - 2883fc1 soc: bcm: brcmstb: biuctrl: Register writes require a barrier
  ACK: pavel
  - 2f1c962 soc: brcmstb: Fix error path for unsupported CPUs
  ACK: pavel
  - e8250f7 crypto: talitos - rename alternative AEAD algos.
  ACK: pavel -- interface change
