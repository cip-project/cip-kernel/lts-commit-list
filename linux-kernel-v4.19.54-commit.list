# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v4.19.54
  - 63bbbcd8ed53 Linux 4.19.54
  - e8e448b08450 Abort file_remove_privs() for non-reg. files
  pavel -- seems like performance optimalization?
  - 465ce9a50f8a coredump: fix race condition between collapse_huge_page() and core dumping
  ACK: pavel
  - c7fb6b75def2 ocfs2: fix error path kobject memory leak
  ACK: pavel
  - fedb1b9c9191 mlxsw: spectrum: Prevent force of 56G
  ACK: pavel -- better error message, not really a serious bug
  - 114e8135ae00 scsi: libsas: delete sas port if expander discover failed
  ACK: pavel
  - 89ede9d8b5b8 scsi: scsi_dh_alua: Fix possible null-ptr-deref
  ACK: pavel
  - cb7c6c33d3bb scsi: smartpqi: properly set both the DMA mask and the coherent DMA mask
  ACK: pavel
  - 214c5933ffcf scsi: libcxgbi: add a check for NULL pointer in cxgbi_check_route()
  ACK: pavel
  - 7b9e10944f0d net: phy: dp83867: Set up RGMII TX delay
  ACK: pavel
  - 7698ad8c14c7 net: phylink: ensure consistent phy interface mode
  ACK: pavel
  - 8fb2c7969009 net: sh_eth: fix mdio access in sh_eth_close() for R-Car Gen2 and RZ/A1 SoCs
  ACK: pavel
  - 467f902643f5 arm64: use the correct function type for __arm64_sys_ni_syscall
  pavel -- suitable iff control flow is checked in 4.19.
  - 98fd62e0a157 arm64: use the correct function type in SYSCALL_DEFINE0
  pavel -- suitable iff control flow is checked in 4.19.
  - c5fdfaedecc2 arm64: fix syscall_fn_t type
  pavel -- suitable iff control flow is checked in 4.19.
  - df6384e0f42e KVM: PPC: Book3S HV: Don't take kvm->lock around kvm_for_each_vcpu
  - b376683f6ab1 KVM: PPC: Book3S: Use new mutex to synchronize access to rtas token list
  - 4acce744284c xenbus: Avoid deadlock during suspend due to open transactions
  - 66f33b2bd2d8 xen/pvcalls: Remove set but not used variable
  - d92ebe0c1d26 ia64: fix build errors by exporting paddr_to_nid()
  ACK: pavel
  - 60a3e3b9e5ec perf record: Fix s390 missing module symbol and warning for non-root users
  - be0e62666da1 perf namespace: Protect reading thread's namespace
  - 7d523e33f4b6 perf data: Fix 'strncat may truncate' build failure with recent gcc
  - e9fcebe01822 configfs: Fix use-after-free when accessing sd->s_dentry
  ACK: pavel
  - ab7a3d9accae ALSA: hda - Force polling mode on CNL for fixing codec communication
  ACK: pavel
  - 7bea5618eaf9 i2c: dev: fix potential memory leak in i2cdev_ioctl_rdwr
  ACK: pavel
  - 197501af7ff3 net: aquantia: fix LRO with FCS error
  ACK: pavel
  - 388534d45f04 net: aquantia: tx clean budget logic error
  ACK: pavel
  - b7ca3f331d57 drm/etnaviv: lock MMU while dumping core
  ACK: pavel
  - ee61fb4de955 ACPI/PCI: PM: Add missing wakeup.flags.valid checks
  ACK: pavel
  - bc19b50b80ca net: tulip: de4x5: Drop redundant MODULE_DEVICE_TABLE()
  ACK: pavel -- just a warning fix
  - 9a3208b66cc1 net: stmmac: update rx tail pointer register to fix rx dma hang issue.
  ACK: pavel -- sounds like a performance fix
  - 3fbcef3350ab gpio: fix gpio-adp5588 build errors
  ACK: pavel
  - 991ea848a5c9 perf/ring-buffer: Always use {READ,WRITE}_ONCE() for rb->user_page data
  - c133c9db233d perf/ring_buffer: Add ordering to rb->nest increment
  - cca19ab29a1a perf/ring_buffer: Fix exposing a temporarily decreased data_head
  - a35e78220a9f x86/CPU/AMD: Don't force the CPB cap when running under a hypervisor
  pavel -- interesting; it reintroduces erratum 1076 under a hypervisor. That will mean loosing CPB feature, which might be ok?
  - 8e5666cdb36b mISDN: make sure device name is NUL terminated
  IGN: pavel
  - f3885eecd253 usb: xhci: Fix a potential null pointer dereference in xhci_debugfs_create_endpoint()
  ACK: pavel -- but GFP_KERNEL never fails...
  - 930d31a6f344 powerpc/powernv: Return for invalid IMC domain
  pavel -- this just fixes confusing printk()s...
  - 00ed897d618e clk: ti: clkctrl: Fix clkdm_clk handling
  ACK: pavel
  - ef4ffa0f0b67 selftests: netfilter: missing error check when setting up veth interface
  IGN: pavel
  - 61c83de6e622 ipvs: Fix use-after-free in ip_vs_in
  ACK: pavel
  - 883ce78cded5 netfilter: nf_queue: fix reinject verdict handling
  ACK: pavel
  - 5a9c29cc2140 perf/x86/intel/ds: Fix EVENT vs. UEVENT PEBS constraints
  - dd9b6de79b67 Staging: vc04_services: Fix a couple error codes
  IGN: pavel
  - 97605ba68790 net: mvpp2: prs: Use the correct helpers when removing all VID filters
  ACK: pavel
  - b6a1eabf72a0 net: mvpp2: prs: Fix parser range for VID filtering
  - 4642a659ab96 net/mlx5: Avoid reloading already removed devices
  ACK: pavel
  - 1b201b63b647 vsock/virtio: set SOCK_DONE on peer shutdown
  ACK: pavel
  - b86a5ccda5c3 tipc: purge deferredq list for each grp member in tipc_group_delete
  ACK: pavel
  - e1b0c311b790 sunhv: Fix device naming inconsistency between sunhv_console and sunhv_reg
  ACK: pavel
  - d7fcb54ed2a9 sctp: Free cookie before we memdup a new one
  ACK: pavel -- kfree() handles NULL just fine, tests are not neccessary
  - 4bb4ba362cc1 nfc: Ensure presence of required attributes in the deactivate_target handler
  ACK: pavel
  - 7530c3f3d5b9 net: openvswitch: do not free vport if register_netdevice() is failed.
  ACK: pavel -- but not a minimal fix
  - fc762c999768 net: dsa: rtl8366: Fix up VLAN filtering
  ACK: pavel
  - 103835df6821 neigh: fix use-after-free read in pneigh_get_next
  ACK: pavel
  - 2980196db6c1 lapb: fixed leak of control-blocks.
  ACK: pavel
  - 7eadfacd2be2 ipv6: flowlabel: fl6_sock_lookup() must use atomic_inc_not_zero
  ACK: pavel
  - a5ae5920426e hv_netvsc: Set probe mode to sync
  ACK: pavel -- not sure if bug is serious enough for stable
  - 674dc77bd3ec be2net: Fix number of Rx queues used for flow hashing
  ACK: pavel
  - 10faaa359b41 ax25: fix inconsistent lock state in ax25_destroy_timer
  ACK: pavel
