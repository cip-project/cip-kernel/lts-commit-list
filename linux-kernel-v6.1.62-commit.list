# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v6.1.62
  - fb2635ac69aba Linux 6.1.62
  - 585da49ad62c9 ASoC: SOF: sof-pci-dev: Fix community key quirk detection
  - c1c15b09f456a ALSA: hda: intel-dsp-config: Fix JSL Chromebook quirk detection
  - 5c59879031eae misc: pci_endpoint_test: Add deviceID for J721S2 PCIe EP device support
  ACK: pavel
  - 6bebd303ad7ea tty: 8250: Add Brainboxes Oxford Semiconductor-based quirks
  ACK: pavel
  - 3017a17ad6b98 tty: 8250: Add support for Intashield IX cards
  ACK: pavel
  - c3444894e8553 tty: 8250: Add support for additional Brainboxes PX cards
  ACK: pavel
  - 31ebf431ed020 tty: 8250: Fix up PX-803/PX-857
  ACK: pavel
  - bfe9bde05fcd2 tty: 8250: Fix port count of PX-257
  ACK: pavel
  - b75ee2d9d792f tty: 8250: Add support for Intashield IS-100
  ACK: pavel
  - cdd260b220d96 tty: 8250: Add support for Brainboxes UP cards
  ACK: pavel
  - 8af676c69815b tty: 8250: Add support for additional Brainboxes UC cards
  ACK: pavel
  - abcb12f3192d6 tty: 8250: Remove UC-257 and UC-431
  ACK: pavel
  - df6cfab66ff2a tty: n_gsm: fix race condition in status line change on dead connections
  ACK: pavel
  - 23107989be8e2 usb: raw-gadget: properly handle interrupted requests
  - e7a802447c491 usb: typec: tcpm: Fix NULL pointer dereference in tcpm_pd_svdm()
  - 6f17be2700039 usb: storage: set 1.50 as the lower bcdDevice for older "Super Top" compatibility
  ACK: pavel
  - b25a2f2470833 PCI: Prevent xHCI driver from claiming AMD VanGogh USB3 DRD device
  ACK: pavel
  - 977ae4dbe2313 ALSA: usb-audio: add quirk flag to enable native DSD for McIntosh devices
  ACK: pavel
  - 9411dbe2c66c3 mmap: fix error paths with dup_anon_vma()
  ACK: pavel
  - 21ca008c53a50 mm/mempolicy: fix set_mempolicy_home_node() previous VMA pointer
  ACK: pavel -- just a WARN fix
  - 7ab62e3415fb5 x86: KVM: SVM: always update the x2avic msr interception
  ACK: pavel
  - e833591265672 perf evlist: Avoid frequency mode for the dummy event
  ACK: pavel -- just a performance fix
  - b3eed11110486 power: supply: core: Use blocking_notifier_call_chain to avoid RCU complaint
  ACK: pavel -- just a WARN fix
  - 803cc77a3acc3 ceph_wait_on_conflict_unlink(): grab reference before dropping ->d_lock
  ACK: pavel
  - 9eab5008db6c9 io_uring: kiocb_done() should *not* trust ->ki_pos if ->{read,write}_iter() failed
  ACK: pavel
  - ed0ba37e7b9b2 powerpc/mm: Fix boot crash with FLATMEM
  ACK: pavel -- likely just a cleanup
  - 31ae7876da36e r8152: Check for unplug in r8153b_ups_en() / r8153c_ups_en()
  ACK: pavel -- just a softlockup fix
  - f90656fbf6182 r8152: Check for unplug in rtl_phy_patch_request()
  ACK: pavel -- just a softlockup fix
  - 98567c9d849b8 net: chelsio: cxgb4: add an error code check in t4_load_phy_fw
  ACK: pavel
  - ff86d69b2e500 drm/amdgpu: Reserve fences for VM update
  ACK: pavel
  - 8e4a77ba25ec7 platform/mellanox: mlxbf-tmfifo: Fix a warning message
  ACK: pavel -- just a warning fix
  - 0f2840dabfea8 netfilter: nf_tables: audit log object reset once per table
  ACK: pavel
  - ec80ad4585d71 LoongArch: Replace kmap_atomic() with kmap_local_page() in copy_user_highpage()
  ACK: pavel
  - afe80b58eea31 LoongArch: Export symbol invalid_pud_table for modules building
  ACK: pavel
  - 9f9b2ec53aca6 gpu/drm: Eliminate DRM_SCHED_PRIORITY_UNSET
  ACK: pavel -- just a cleanup
  - cafa191b27dd3 drm/amdgpu: Unset context priority is now invalid
  UR: pavel
  - 0eb733b53ebfe scsi: mpt3sas: Fix in error path
  ACK: pavel
  - 4e000daf394a1 fbdev: uvesafb: Call cn_del_callback() at the end of uvesafb_exit()
  ACK: pavel
  - 50736464a75a4 fbdev: omapfb: fix some error codes
  ACK: pavel -- just a robustness
  - 6a87b333ba478 drm/ttm: Reorder sys manager cleanup step
  ACK: pavel
  - 9951b2309ea77 ASoC: codecs: tas2780: Fix log of failed reset via I2C.
  ACK: pavel
  - b7ed4aa0c2e64 ASoC: rt5650: fix the wrong result of key button
  ACK: pavel
  - 8e1a6594d7828 efi: fix memory leak in krealloc failure handling
  UR: pavel -- do we need to free name in all cases?
  - 678edd2dfd550 netfilter: nfnetlink_log: silence bogus compiler warning
  ACK: pavel -- just a warning workaround
  - c6f6a505277fb spi: npcm-fiu: Fix UMA reads when dummy.nbytes == 0
  ACK: pavel
  - 6a7a2d5a08642 fs/ntfs3: Avoid possible memory leak
  UR: pavel
  - 84aabd18c8d7c fs/ntfs3: Fix directory element type detection
  ACK: pavel
  - 3bff4bb7f9c7d fs/ntfs3: Fix NULL pointer dereference on error in attr_allocate_frame()
  ACK: pavel -- just a robustness against corrupted fs
  - c8cbae3cbbc4b fs/ntfs3: Fix possible NULL-ptr-deref in ni_readpage_cmpr()
  ACK: pavel
  - 6fe32f79abea8 fs/ntfs3: Use kvmalloc instead of kmalloc(... __GFP_NOWARN)
  UR: pavel -- NOFS->KERNEL
  - 92f9c7c7ddbf7 fs/ntfs3: Write immediately updated ntfs state
  UR: pavel -- no changelog
  - fc91bb3e1b2bc fs/ntfs3: Add ckeck in ni_update_parent()
  ACK: pavel
  - 768e857ac3e0a fbdev: atyfb: only use ioremap_uc() on i386 and ia64
  ACK: pavel
  - dd6d75eb00eea Input: synaptics-rmi4 - handle reset delay when using SMBus trsnsport
  ACK: pavel
  - c64c237275b46 powerpc/85xx: Fix math emulation exception
  ACK: pavel
  - 96c7aac8d8049 dmaengine: ste_dma40: Fix PM disable depth imbalance in d40_probe
  ACK: pavel
  - 9ef4697548c21 irqchip/stm32-exti: add missing DT IRQ flag translation
  ACK: pavel
  - 7378415da0484 irqchip/riscv-intc: Mark all INTC nodes as initialized
  ACK: pavel
  - d3204c0fdd33a net: sched: cls_u32: Fix allocation size in u32_init()
  ACK: pavel
  - f15dbcda2ce81 ASoC: tlv320adc3xxx: BUG: Correct micbias setting
  ACK: pavel
  - 30ed998604372 coresight: tmc-etr: Disable warnings for allocation failures
  ACK: pavel
  - 6618e7a740ded ASoC: simple-card: fixup asoc_simple_probe() error handling
  ACK: pavel -- just a cleanup/warning workaround (not in 5.10, reported)
