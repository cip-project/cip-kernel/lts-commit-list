# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v5.10.54
  - 08277b9dde63 Linux 5.10.54
  - c9f8e17990e0 skbuff: Fix build with SKB extensions disabled
  ACK: pavel
  - ba28765d338a xhci: add xhci_get_virt_ep() helper
  ACK: pavel
  - 624290f368af sfc: ensure correct number of XDP queues
  ACK: pavel -- just a robustness
  - 1df4fe5a8871 drm/i915/gvt: Clear d3_entered on elsp cmd submission.
  ACK: pavel
  - c938e65768e0 usb: ehci: Prevent missed ehci interrupts with edge-triggered MSI
  ACK: pavel
  - 25af91a806d2 perf inject: Close inject.output on exit
  - fb35426d123e Documentation: Fix intiramfs script name
  ACK: pavel -- just a documentation fix
  - 570341f10ecc skbuff: Release nfct refcount on napi stolen or re-used skbs
  ACK: pavel -- buggy, fixed below
  - 31828ffdab19 bonding: fix build issue
  ACK: pavel
  - c9d97b7bb897 PCI: Mark AMD Navi14 GPU ATS as broken
  ACK: pavel -- just disables feature
  - f7ee361182e0 net: dsa: mv88e6xxx: enable SerDes PCS register dump via ethtool -d on Topaz
  ACK: pavel
  - 30f1d4d03641 net: dsa: mv88e6xxx: enable SerDes RX stats for Topaz
  ACK: pavel
  - fc31b5be1383 drm/amdgpu: update golden setting for sienna_cichlid
  ACK: pavel
  - 69a603aa170e drm: Return -ENOTTY for non-drm ioctls
  ACK: pavel -- just a (very reasonable) API change
  - 2831eeb7bc3d driver core: Prevent warning when removing a device link from unregistered consumer
  ACK: pavel -- just an WARN fix
  - 0e759383236a nds32: fix up stack guard gap
  ACK: pavel
  - 7497f4c91da3 misc: eeprom: at24: Always append device id even if label property is set.
  ACK: pavel -- may change API/naming
  - 8571daace5a6 rbd: always kick acquire on "acquired" and "released" notifications
  ACK: pavel
  - 2f3731de5e69 rbd: don't hold lock_rwsem while running_list is being drained
  ACK: pavel
  - 92291fa2d144 hugetlbfs: fix mount mode command line processing
  ACK: pavel -- wow. reverts back to old API
  - 1a25c5738d0c memblock: make for_each_mem_range() traverse MEMBLOCK_HOTPLUG regions
  ACK: pavel
  - 0b591c020d28 userfaultfd: do not untag user pointers
  ACK: pavel -- just an API tweak
  - fca5343b4892 io_uring: remove double poll entry on arm failure
  ACK: pavel
  - 9eef9029151c io_uring: explicitly count entries for poll reqs
  ACK: pavel
  - 1077e2b15283 selftest: use mmap instead of posix_memalign to allocate memory
  - 6e81e2c38a38 posix-cpu-timers: Fix rearm racing against process tick
  ACK: pavel
  - 3efec3b4b16f bus: mhi: core: Validate channel ID when processing command completions
  UR: pavel -- max_chan -> should be <= ?
  - b3f3a58a86c4 ixgbe: Fix packet corruption due to missing DMA sync
  ACK: pavel
  - e991457afdcb media: ngene: Fix out-of-bounds bug in ngene_command_config_free_buf()
  ACK: pavel
  - 755971dc7ee8 btrfs: check for missing device in btrfs_trim_fs
  ACK: pavel
  - 552b053f1a53 tracing: Synthetic event field_pos is an index not a boolean
  ACK: pavel
  - 757bdba8026b tracing: Fix bug in rb_per_cpu_empty() that might cause deadloop.
  ACK: pavel
  - a5e1aff58943 tracing/histogram: Rename "cpu" to "common_cpu"
  ACK: pavel -- just an API tweak
  - 0edad8b9f65d tracepoints: Update static_call before tp_funcs when adding a tracepoint
  ACK: pavel
  - 4ed4074c6c6c firmware/efi: Tell memblock about EFI iomem reservations
  ACK: pavel
  - 647e26b03ee9 usb: typec: stusb160x: register role switch before interrupt registration
  ACK: pavel
  - a206167bd638 usb: dwc2: gadget: Fix sending zero length packet in DDMA mode.
  ACK: pavel
  - f2c04f6b21ef usb: dwc2: gadget: Fix GOUTNAK flow for Slave mode.
  ACK: pavel
  - 7073acb51a3b usb: gadget: Fix Unbalanced pm_runtime_enable in tegra_xudc_probe
  ACK: pavel
  - 1bf7371b9004 USB: serial: cp210x: add ID for CEL EM3588 USB ZigBee stick
  ACK: pavel
  - 45c87a94336f USB: serial: cp210x: fix comments for GE CS1000
  ACK: pavel -- just a comment fix
  - f528521c1574 USB: serial: option: add support for u-blox LARA-R6 family
  ACK: pavel
  - 311fd7f7f186 usb: renesas_usbhs: Fix superfluous irqs happen after usb_pkt_pop()
  ACK: pavel
  - 7af54a4e221e usb: max-3421: Prevent corruption of freed memory
  ACK: pavel
  - 69da81a96442 USB: usb-storage: Add LaCie Rugged USB3-FW to IGNORE_UAS
  ACK: pavel
  - e6343aab3ee7 usb: hub: Fix link power management max exit latency (MEL) calculations
  ACK: pavel
  - 8f087b4cf1a3 usb: hub: Disable USB 3 device initiated lpm if exit latency is too high
  ACK: pavel
  - 709137c85327 KVM: PPC: Book3S HV Nested: Sanitise H_ENTER_NESTED TM state
  ACK: pavel
  - c1fbdf0f3c26 KVM: PPC: Book3S: Fix H_RTAS rets buffer overflow
  UR: pavel -- c/e
  - e3eb672c169d xhci: Fix lost USB 2 remote wake
  ACK: pavel
  - 02e2e96ba56c usb: xhci: avoid renesas_usb_fw.mem when it's unusable
  UR: pavel --a that is not exactly nice; we should not make config break other configurations like that
  - 9e9cf23b77d4 Revert "usb: renesas-xhci: Fix handling of unknown ROM state"
  ACK: pavel
  - ebaa67086fae ALSA: pcm: Fix mmap capability check
  ACK: pavel
  - 431e31105579 ALSA: pcm: Call substream ack() method upon compat mmap commit
  ACK: pavel
  - 3c9afa23f3fc ALSA: hdmi: Expose all pins on MSI MS-7C94 board
  ACK: pavel
  - 253759df8082 ALSA: hda/realtek: Fix pop noise and 2 Front Mic issues on a machine
  ACK: pavel
  - 2b3cdf581993 ALSA: sb: Fix potential ABBA deadlock in CSP driver
  ACK: pavel
  - 5858c8a46421 ALSA: usb-audio: Add registration quirk for JBL Quantum headsets
  ACK: pavel
  - 2de518548de1 ALSA: usb-audio: Add missing proc text entry for BESPOKEN type
  ACK: pavel -- just a robustness
  - 37a88b41dc29 s390/boot: fix use of expolines in the DMA code
  IGN: pavel
  - d1ab96288061 s390/ftrace: fix ftrace_update_ftrace_func implementation
  IGN: pavel
  - 3b4009b49634 mmc: core: Don't allocate IDA for OF aliases
  ACK: pavel -- just a better support for debug configurations
  - fc6ac92cfcab proc: Avoid mixing integer types in mem_rw()
  ACK: pavel
  - 76f7eae7ec80 cifs: fix fallocate when trying to allocate a hole.
  ACK: pavel
  - c26372b8a8c3 cifs: only write 64kb at a time when fallocating a small region of a file
  pavel -- introduces problems with len == 0
  - b91e5b63470d drm/panel: raspberrypi-touchscreen: Prevent double-free
  ACK: pavel
  - 9e0373945ed6 net: sched: cls_api: Fix the the wrong parameter
  ACK: pavel
  - c8ebf135c199 net: dsa: sja1105: make VID 4095 a bridge VLAN too
  ACK: pavel
  - 164294d09c47 tcp: disable TFO blackhole logic by default
  pavel -- breaks some, fixes some; should it be in stable?
  - 8eb225873246 sctp: update active_key for asoc when old key is being replaced
  ACK: pavel
  - ef799bd8ff5a nvme: set the PRACT bit when using Write Zeroes with T10 PI
  ACK: pavel
  - 7850f03ed814 r8169: Avoid duplicate sysfs entry creation error
  - 0f5dc3971473 afs: Fix tracepoint string placement with built-in AFS
  ACK: pavel
  - 711057846aa7 Revert "USB: quirks: ignore remote wake-up on Fibocom L850-GL LTE modem"
  ACK: pavel -- hmm. just replaces bug with different bug
  - 8985dc2cabd6 nvme-pci: don't WARN_ON in nvme_reset_work if ctrl.state is not RESETTING
  ACK: pavel -- just a warning tweak
  - fb28b1592098 ceph: don't WARN if we're still opening a session to an MDS
  ACK: pavel -- just a WARN fix
  - ce8fafb68051 ipv6: fix another slab-out-of-bounds in fib6_nh_flush_exceptions
  ACK: pavel
  - 071729150be9 net/sched: act_skbmod: Skip non-Ethernet packets
  ACK: pavel
  - ee36bb471389 spi: spi-bcm2835: Fix deadlock
  ACK: pavel
  - 432738c9740c net: hns3: fix rx VLAN offload state inconsistent issue
  ACK: pavel
  - 3e903e0b578b net: hns3: fix possible mismatches resp of mailbox
  ACK: pavel
  - f4305375f031 ALSA: hda: intel-dsp-cfg: add missing ElkhartLake PCI ID
  ACK: pavel
  - 41a839437a07 net/tcp_fastopen: fix data races around tfo_active_disable_stamp
  ACK: pavel
  - ba3336397677 net: hisilicon: rename CACHE_LINE_MASK to avoid redefinition
  ACK: pavel
  - 320dcbdec4c6 bnxt_en: Check abort error state in bnxt_half_open_nic()
  ACK: pavel
  - 134a0536f0a4 bnxt_en: Validate vlan protocol ID on RX packets
  ACK: pavel
  - 4f7da0f97beb bnxt_en: Add missing check for BNXT_STATE_ABORT_ERR in bnxt_fw_rset_task()
  ACK: pavel
  - 927370485e98 bnxt_en: Refresh RoCE capabilities in bnxt_ulp_probe()
  ACK: pavel
  - ab830c3bae19 bnxt_en: don't disable an already disabled PCI device
  ACK: pavel
  - 26463689445d ACPI: Kconfig: Fix table override from built-in initrd
  ACK: pavel
  - 113ce8c5043a spi: cadence: Correct initialisation of runtime PM again
  ACK: pavel
  - 3ea448b62b49 scsi: target: Fix protect handling in WRITE SAME(32)
  ACK: pavel -- not nearly a minimum fix
  - b82a1a26aaee scsi: iscsi: Fix iface sysfs attr detection
  ACK: pavel
  - 6811744bd0ef netrom: Decrease sock refcount when sock timers expire
  ACK: pavel
  - 096a8dca8ca5 sctp: trim optlen when it's a huge value in sctp_setsockopt
  ACK: pavel
  - 8e9662fde6d6 net: sched: fix memory leak in tcindex_partial_destroy_work
  ACK: pavel
  - e14ef1095387 KVM: PPC: Fix kvm_arch_vcpu_ioctl vcpu_load leak
  ACK: pavel
  - fcbad8e18d31 KVM: PPC: Book3S: Fix CONFIG_TRANSACTIONAL_MEM=n crash
  ACK: pavel
  - 30b830215158 net: decnet: Fix sleeping inside in af_decnet
  ACK: pavel
  - d402c60da0fd efi/tpm: Differentiate missing and invalid final event log table.
  ACK: pavel
  - 898376690310 dma-mapping: handle vmalloc addresses in dma_common_{mmap,get_sgtable}
  ACK: pavel
  - 115e4f5b64ae usb: hso: fix error handling code of hso_create_net_device
  ACK: pavel
  - 1582a02fecff net: fix uninit-value in caif_seqpkt_sendmsg
  ACK: pavel
  - 2fc8048265ce bpftool: Check malloc return value in mount_bpffs_for_pin
  - 3b5b0afd8d97 bpf, sockmap, udp: sk_prot needs inuse_idx set for proc stats
  ACK: pavel
  - c260442431b4 bpf, sockmap, tcp: sk_prot needs inuse_idx set for proc stats
  ACK: pavel
  - 715f378f4290 bpf, sockmap: Fix potential memory leak on unlikely error case
  ACK: pavel
  - e3a9548ae538 s390/bpf: Perform r1 range checking before accessing jit->seen_reg[r1]
  IGN: pavel
  - 9264bebe9ef9 liquidio: Fix unintentional sign extension issue on left shift of u16
  ACK: pavel
  - 0ff2ea9d8fa3 timers: Fix get_next_timer_interrupt() with no timers pending
  ACK: pavel -- just a performance fix
  - ca9ba1de8f09 xdp, net: Fix use-after-free in bpf_xdp_link_release
  ACK: pavel
  - 39f1735c8107 bpf: Fix tail_call_reachable rejection for interpreter when jit failed
  ACK: pavel
  - 2b4046e64f7d bpf, test: fix NULL pointer dereference on invalid expected_attach_type
  ACK: pavel
  - 3dba72d1fc01 ASoC: rt5631: Fix regcache sync errors on resume
  ACK: pavel
  - 2435dcfd16ac spi: mediatek: fix fifo rx mode
  ACK: pavel
  - a9a85bfedd83 regulator: hi6421: Fix getting wrong drvdata
  ACK: pavel
  - 5cdc986aad95 regulator: hi6421: Use correct variable type for regmap api val argument
  ACK: pavel
  - 23811b75fdb8 spi: stm32: fixes pm_runtime calls in probe/remove
  ACK: pavel
  - 844ab04b62a5 spi: imx: add a check for speed_hz before calculating the clock
  ACK: pavel -- not sure it fixes user-visible bug
  - 3b6c430d1248 ASoC: wm_adsp: Correct wm_coeff_tlv_get handling
  ACK: pavel
  - 57efe4f82a76 perf sched: Fix record failure when CONFIG_SCHEDSTATS is not set
  - 61f2e1e79578 perf data: Close all files in close_dir()
  - 7c91e0ce2601 perf probe-file: Delete namelist in del_events() on the error path
  - a6c32317cd3d perf lzma: Close lzma stream on exit
  - 2ae8f40a8fdf perf script: Fix memory 'threads' and 'cpus' leaks on exit
  - 51077d315a46 perf report: Free generated help strings for sort option
  - 2bfa3c53ea8a perf env: Fix memory leak of cpu_pmu_caps
  - a2f0da3af614 perf test maps__merge_in: Fix memory leak of maps
  - b7bfd8aeb956 perf dso: Fix memory leak in dso__new_map()
  - c9c101da3e83 perf test event_update: Fix memory leak of evlist
  - b768db7f8070 perf test session_topology: Delete session->evlist
  - b8892d16a928 perf env: Fix sibling_dies memory leak
  - 306411a8bf75 perf probe: Fix dso->nsinfo refcounting
  - f21987d7bb58 perf map: Fix dso->nsinfo refcounting
  - 7337ff2093e0 perf inject: Fix dso->nsinfo refcounting
  - a87d42ae7f5d KVM: x86/pmu: Clear anythread deprecated bit when 0xa leaf is unsupported on the SVM
  ACK: pavel
  - b990585f9b7a nvme-pci: do not call nvme_dev_remove_admin from nvme_remove
  ACK: pavel
  - 0fa11e1a20c7 mptcp: fix warning in __skb_flow_dissect() when do syn cookie for subflow join
  ACK: pavel
  - 3714e0bb0dcf cxgb4: fix IRQ free race during driver unload
  ACK: pavel
  - d92337bf54f2 pwm: sprd: Ensure configuring period and duty_cycle isn't wrongly skipped
  ACK: pavel
  - f1edbcc47f46 selftests: icmp_redirect: IPv6 PMTU info should be cleared after redirect
  - 906bbb18db78 selftests: icmp_redirect: remove from checking for IPv6 route get
  - bb737eceb9a4 stmmac: platform: Fix signedness bug in stmmac_probe_config_dt()
  ACK: pavel
  - 79ec7b5b2f4f ipv6: fix 'disable_policy' for fwd packets
  ACK: pavel -- just an interface tweak
  - 35eaefb44ed7 bonding: fix incorrect return value of bond_ipsec_offload_ok()
  ACK: pavel
  - 13626bad63e8 bonding: fix suspicious RCU usage in bond_ipsec_offload_ok()
  ACK: pavel
  - 56ccdf868ab6 bonding: Add struct bond_ipesc to manage SA
  ACK: pavel
  - b3bd1f5e5037 bonding: disallow setting nested bonding + ipsec offload
  ACK: pavel
  - 43511a6a164a bonding: fix suspicious RCU usage in bond_ipsec_del_sa()
  ACK: pavel
  - 6ca0e55a1310 ixgbevf: use xso.real_dev instead of xso.dev in callback functions of struct xfrmdev_ops
  ACK: pavel -- not a minimum fix
  - ba7bfcdff1ad bonding: fix null dereference in bond_ipsec_add_sa()
  ACK: pavel
  - 3ae639af3626 bonding: fix suspicious RCU usage in bond_ipsec_add_sa()
  ACK: pavel
  - 4a31baf55f6a net: add kcov handle to skb extensions
  ACK: pavel
  - 78e4baff950d gve: Fix an error handling path in 'gve_probe()'
  ACK: pavel
  - 813449fb85f6 igb: Fix position of assignment to *ring
  ACK: pavel -- just a robustness
  - 44171801d39c igb: Check if num of q_vectors is smaller than max before array access
  NAK: pavel -- probably incorrect; array is overflew elsewhere
  - cb9292445d23 iavf: Fix an error handling path in 'iavf_probe()'
  ACK: pavel
  - a6756d637b40 e1000e: Fix an error handling path in 'e1000_probe()'
  ACK: pavel
  - dea695a2ee23 fm10k: Fix an error handling path in 'fm10k_probe()'
  ACK: pavel
  - a099192fe7e1 igb: Fix an error handling path in 'igb_probe()'
  ACK: pavel
  - db4c32c1b926 igc: Fix an error handling path in 'igc_probe()'
  ACK: pavel
  - 7bc9fb1f8019 ixgbe: Fix an error handling path in 'ixgbe_probe()'
  ACK: pavel
  - 02d1af0bee65 igc: change default return of igc_read_phy_reg()
  ACK: pavel
  - f153664d8e70 igb: Fix use-after-free error during reset
  ACK: pavel
  - e15f629036ba igc: Fix use-after-free error during reset
  ACK: pavel
