# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v5.10.5
  - f5247949c0a930 Linux 5.10.5
  - 12d377b93eef28 device-dax: Fix range release
  - aceb8ae8e3b105 ext4: avoid s_mb_prefetch to be zero in individual scenarios
  - aff18aa806fd14 dm verity: skip verity work if I/O error when system is shutting down
  - 610d2fa0ec76ad ALSA: pcm: Clear the full allocated memory at hw_params
  - c7b04d27c9107f io_uring: remove racy overflow list fast checks
  - 13f9eec229734b s390: always clear kernel stack backchain before calling functions
  - 330c1ee7d59373 tick/sched: Remove bogus boot "safety" check
  - 9b22bc0f1663be drm/amd/display: updated wm table for Renoir
  - 86be0f2a0ef9d8 ceph: fix inode refcount leak when ceph_fill_inode on non-I_NEW inode fails
  - 8bcfa178f92a1f NFSv4.2: Don't error when exiting early on a READ_PLUS buffer overflow
  - ef3b9ad967d0bd um: ubd: Submit all data segments atomically
  - a8b49c4bdf8770 um: random: Register random as hwrng-core device
  - 0aa2eecf853417 watchdog: rti-wdt: fix reference leak in rti_wdt_probe
  - eae1fb3bc565ea fs/namespace.c: WARN if mnt_count has become negative
  - b1e155ccc882cd powerpc/64: irq replay remove decrementer overflow check
  - 8b5b2b76834487 module: delay kobject uevent until after module init call
  - db6129f6ad88da f2fs: fix race of pending_pages in decompression
  - ee3f8aefd0373f f2fs: avoid race condition for shrinker count
  - 3c0f0f5f58a785 NFSv4: Fix a pNFS layout related use-after-free race when freeing the inode
  - 06ac2ca0989d6b i3c master: fix missing destroy_workqueue() on error in i3c_master_register
  - 498d90690f24d1 powerpc: sysdev: add missing iounmap() on error in mpic_msgr_probe()
  - acc3c8cc27a80a rtc: pl031: fix resource leak in pl031_probe
  - 26058c397b9f67 quota: Don't overflow quota file offsets
  - bb2ab902f6f0ff module: set MODULE_STATE_GOING state when a module fails to load
  - 0ad9a6e6139dab rtc: sun6i: Fix memleak in sun6i_rtc_clk_init
  - b5a2f093b6b16d io_uring: check kthread stopped flag when sq thread is unparked
  - 90803050177255 fcntl: Fix potential deadlock in send_sig{io, urg}()
  - 721972b8665f78 ext4: check for invalid block size early when mounting a file system
  - 8ed894f1117e5e bfs: don't use WARNING: string when it's just info.
  - fb05e983eaf71f ALSA: rawmidi: Access runtime->avail always in spinlock
  - cf7fe671cd7eba ALSA: seq: Use bool for snd_seq_queue internal flags
  - 1c5a034710da75 f2fs: fix shift-out-of-bounds in sanity_check_raw_super()
  - 2b56f16e348789 media: gp8psk: initialize stats at power control logic
  - f290cffdf761a5 misc: vmw_vmci: fix kernel info-leak by initializing dbells in vmci_ctx_get_chkpt_doorbells()
  - a021b669613248 reiserfs: add check for an invalid ih_entry_count
  - 397971e1d891f3 fbcon: Disable accelerated scrolling
  - df83b9b674495f Bluetooth: hci_h5: close serdev device and free hu in h5_close
  - 9d4053cfb3f340 scsi: cxgb4i: Fix TLS dependency
  - fdac87be009f1d zlib: move EXPORT_SYMBOL() and MODULE_LICENSE() out of dfltcc_syms.c
  - bf81221a40fa6b cgroup: Fix memory leak when parsing multiple source parameters
  - 9154d2eeb4f5b3 tools headers UAPI: Sync linux/const.h with the kernel headers
  - e8afbbac2f687e uapi: move constants from <linux/kernel.h> to <linux/const.h>
  - ce00a7d0d95231 io_uring: fix io_sqe_files_unregister() hangs
  - b25b86936a8dcc io_uring: add a helper for setting a ref node
  - 25a2de679b5d55 io_uring: use bottom half safe lock for fixed file data
  - 7247bc60e8e145 io_uring: don't assume mm is constant across submits
  - a5184f3cc284e5 lib/zlib: fix inflating zlib streams on s390
  - 98b57685c26d8f mm: memmap defer init doesn't work as expected
  - df73c80338ef39 mm/hugetlb: fix deadlock in hugetlb_cow error path
  - 092898b070e0fa scsi: block: Fix a race in the runtime power management code
  - 1a58c171a523d2 opp: Call the missing clk_put() on error
  - e8322837a2e56d opp: fix memory leak in _allocate_opp_table
  - c6dd62c14b3228 spi: dw-bt1: Fix undefined devm_mux_control_get symbol
  - 6d63cc42bb8f42 jffs2: Fix NULL pointer dereference in rp_size fs option parsing
  - 58dc34446c5280 jffs2: Allow setting rp_size to zero during remounting
  - 52504a61ab9992 io_uring: close a small race gap for files cancel
  - 8b8a688260b4ad drm/amd/display: Add get_dig_frontend implementation for DCEx
  - 5ef98378eff84b md/raid10: initialize r10_bio->read_slot before use.
  - 62162b322364f0 ethtool: fix string set id check
  - 95fcb69c491e97 ethtool: fix error paths in ethnl_set_channels()
  - aeab3d7a04f812 mptcp: fix security context on server socket
  - a969a632cbe716 net/sched: sch_taprio: reset child qdiscs before freeing them
