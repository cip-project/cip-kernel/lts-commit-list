# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v4.19.242
  - 89ef890678b1 Linux 4.19.242
  - 98c790eabed7 mmc: rtsx: add 74 Clocks in power on flow
  - 8dfd365c37f5 PCI: aardvark: Fix reading MSI interrupt number
  - 119016ee90fe PCI: aardvark: Clear all MSIs at setup
  - b18fdfb00838 dm: interlock pending dm_io and dm_wait_for_bios_completion
  - 9e07272cca2e dm: fix mempool NULL pointer race when completing IO
  - 6c2176f5ad48 tcp: make sure treq->af_specific is initialized
  - f86d55cf6161 mm: fix unexpected zeroed page mapping with zram swap
  - dac10d64c77a kvm: x86/cpuid: Only provide CPUID leaf 0xA if host has architectural PMU
  - 2fc50abb0b3c net: igmp: respect RCU rules in ip_mc_source() and ip_mc_msfilter()
  - a435e43f253c btrfs: always log symlinks in full mode
  - 4da83759f973 smsc911x: allow using IRQ0
  - 3e20f6b5f270 selftests: mirror_gre_bridge_1q: Avoid changing PVID while interface is operational
  - 18b44e366b92 net: emaclite: Add error handling for of_address_to_resource()
  - 2b738fe6ff45 net: stmmac: dwmac-sun8i: add missing of_node_put() in sun8i_dwmac_register_mdio_mux()
  - d43055721d35 ASoC: dmaengine: Restore NULL prepare_slave_config() callback
  - 4483090917f4 hwmon: (adt7470) Fix warning on module removal
  - d360fc8df363 NFC: netlink: fix sleep in atomic bug when firmware download timeout
  - b266f492b2af nfc: nfcmrvl: main: reorder destructive operations in nfcmrvl_nci_unregister_dev to avoid bugs
  - 7deebb94a311 nfc: replace improper check device_is_registered() in netlink related functions
  - b2c3091d4da6 can: grcan: use ofdev->dev when allocating DMA memory
  - 52795b567bff can: grcan: grcan_close(): fix deadlock
  - 143059bd2022 ASoC: wm8958: Fix change notifications for DSP controls
  - 99bc5b7098fc genirq: Synchronize interrupt thread startup
  - 914c59ddab3d firewire: core: extend card->lock in fw_core_handle_bus_reset
  - 4faa185f3548 firewire: remove check of list iterator against head past the loop body
  - 34380b5647f1 firewire: fix potential uaf in outbound_phy_packet_callback()
  - c969344fa935 Revert "SUNRPC: attempt AF_LOCAL connect on setup"
  - 09965f3aa1b5 gpiolib: of: fix bounds check for 'gpio-reserved-ranges'
  - 0e03be062685 ALSA: fireworks: fix wrong return count shorter than expected by 4 bytes
  - b8b7cc4b5a87 parisc: Merge model and model name into one line in /proc/cpuinfo
  - 47f1b5665827 MIPS: Fix CP0 counter erratum detection for R4k CPUs
  - df2c1f38939a drm/vgem: Close use-after-free race in vgem_gem_create
  - 0917ac8005c2 tty: n_gsm: fix incorrect UA handling
  - 143a5364574c tty: n_gsm: fix wrong command frame length field encoding
  - 87d56b773119 tty: n_gsm: fix wrong command retry handling
  - a6e2bca9c2df tty: n_gsm: fix missing explicit ldisc flush
  - 0401adeaac56 tty: n_gsm: fix insufficient txframe size
  - 5bc01fce4867 netfilter: nft_socket: only do sk lookups when indev is available
  - 567ac05123a0 tty: n_gsm: fix malformed counter for out of frame data
  - 0114092582cf tty: n_gsm: fix wrong signal octet encoding in convergence layer type 2
  - dc53866d5ca9 x86/cpu: Load microcode during restore_processor_state()
  - 5db5001dca67 drivers: net: hippi: Fix deadlock in rr_close()
  - f2e8af110984 cifs: destage any unwritten data to the server before calling copychunk_write
  - 47b97d5b3a3c x86: __memcpy_flushcache: fix wrong alignment if size > 2^32
  - b099b29dd1cf ip6_gre: Avoid updating tunnel->tun_hlen in __gre6_xmit()
  - f8755e6cd4f0 ASoC: wm8731: Disable the regulator when probing fails
  - af46047a10c4 bnx2x: fix napi API usage sequence
  - 55da1d7b9070 net: bcmgenet: hide status block before TX timestamping
  - f495a90ccfa2 clk: sunxi: sun9i-mmc: check return value after calling platform_get_resource()
  - c95d4bf1c779 bus: sunxi-rsb: Fix the return value of sunxi_rsb_device_create()
  - cc639aa3c2f5 tcp: fix potential xmit stalls caused by TCP_NOTSENT_LOWAT
  - fc9266636352 ip_gre: Make o_seqno start from 0 in native mode
  - 9a0b45fd9543 net: hns3: add validity check for message data length
  - aa7d6ea2d167 pinctrl: pistachio: fix use of irq_of_parse_and_map()
  - 9bc9179d9316 ARM: dts: imx6ull-colibri: fix vqmmc regulator
  - ea76e51b2b2e sctp: check asoc strreset_chunk in sctp_generate_reconf_event
  - aad94f14c263 tcp: md5: incorrect tcp_header_len for incoming connections
  - abe55c894249 mtd: rawnand: Fix return value check of wait_for_completion_timeout
  - 3fc2c30a6a63 ipvs: correctly print the memory size of ip_vs_conn_tab
  - 554021caf393 ARM: dts: logicpd-som-lv: Fix wrong pinmuxing on OMAP35
  - 3c6995b71d2a ARM: dts: Fix mmc order for omap3-gta04
  - b5d92ad96f27 ARM: OMAP2+: Fix refcount leak in omap_gic_of_init
  - 2edeba4d5d41 phy: samsung: exynos5250-sata: fix missing device put in probe error paths
  - b5f5b404c005 phy: samsung: Fix missing of_node_put() in exynos_sata_phy_probe
  - 904585504a56 ARM: dts: imx6qdl-apalis: Fix sgtl5000 detection issue
  - cb7a795a4751 USB: Fix xhci event ring dequeue pointer ERDP update issue
  - bf9fe6ecb19a mtd: rawnand: fix ecc parameters for mt7622
  - a4981bed962f hex2bin: fix access beyond string end
  - 0f509c442883 hex2bin: make the function hex_to_bin constant-time
  - 5c3fef73bf1d serial: 8250: Correct the clock for EndRun PTP/1588 PCIe device
  - f014e39e38fb serial: 8250: Also set sticky MCR bits in console restoration
  - 0e6a40bd462a serial: imx: fix overrun interrupts in DMA mode
  - ca30aeb6e731 usb: dwc3: gadget: Return proper request status
  - 8366a4d19816 usb: dwc3: core: Fix tx/rx threshold settings
  - cda86f15f5b3 usb: gadget: configfs: clear deactivation flag in configfs_composite_unbind()
  - fc5624f24015 usb: gadget: uvc: Fix crash when encoding data for usb request
  - 09af8ccf725c usb: misc: fix improper handling of refcount in uss720_probe()
  - 63cee8b63d96 iio: magnetometer: ak8975: Fix the error handling in ak8975_power_on()
  - b8ed00a6a610 iio: dac: ad5446: Fix read_raw not returning set value
  - c3bf64171384 iio: dac: ad5592r: Fix the missing return value.
  - 318f86306de1 xhci: stop polling roothubs after shutdown
  - e0b513c30826 USB: serial: option: add Telit 0x1057, 0x1058, 0x1075 compositions
  - 690e7754ee18 USB: serial: option: add support for Cinterion MV32-WA/MV32-WB
  - 79ea6827952e USB: serial: cp210x: add PIDs for Kamstrup USB Meter Reader
  - adc12b0b1471 USB: serial: whiteheat: fix heap overflow in WHITEHEAT_GET_DTR_RTS
  - e440f4953c3b USB: quirks: add STRING quirk for VCOM device
  - a15ee0724db0 USB: quirks: add a Realtek card reader
  - fb39a75c3450 usb: mtu3: fix USB 3.0 dual-role-switch from device to host
