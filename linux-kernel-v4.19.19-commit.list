# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)


## v4.19.19
  - dffbba4348e9 Linux 4.19.19
  - 3a3b6a6b15db Input: input_event - fix the CONFIG_SPARC64 mixup
  - d4a6ac28d44a ide: fix a typo in the settings proc file name
  - 25ad17d692ad usb: dwc3: gadget: Clear req->needs_extra_trb flag on cleanup
  - 6bab957396ce Revert "mm, memory_hotplug: initialize struct pages for the full memory section"
  - 7dbf12973d53 nvmet-rdma: fix null dereference under heavy load
  - fa9184be67a6 nvmet-rdma: Add unlikely for response allocated check
  - 48046a0177e6 s390/smp: Fix calling smp_call_ipl_cpu() from ipl CPU
  ACK: pavel -- already reviewed in 4.4.173
  - 37c9e3ee4239 bpf: fix inner map masking to prevent oob under speculation
  - eed84f94ff8d bpf: fix sanitation of alu op with pointer / scalar type from different paths
  - f92a819b4cbe bpf: prevent out of bounds speculation on pointer arithmetic
  - 4f7f708d0e6c bpf: fix check_map_access smin_value test when pointer contains offset
  - 44f8fc649980 bpf: restrict unknown scalars of mixed signed bounds for unprivileged
  - 5332dda94f63 bpf: restrict stack pointer arithmetic for unprivileged
  - 9e57b2969d4a bpf: restrict map value pointer arithmetic for unprivileged
  - 232ac70dd38b bpf: enable access to ax register also from verifier rewrite
  - b855e3103740 bpf: move tmp variable into ax register in interpreter
  - 333a31c89ae2 bpf: move {prev_,}insn_idx into verifier env
  - 437112946263 bpf: add per-insn complexity limit
  - 7da6cd690c43 bpf: improve verifier branch analysis
  - ce8d0581ae33 drm/meson: Fix atomic mode switching regression
  - 8b4dffe8261a vt: invoke notifier on screen size change
  ACK: pavel -- already reviewed in 4.4.173
  - 18ef43def81c vt: always call notifier with the console lock held
  - 855f7e64169f vt: make vt_console_print() compatible with the unicode screen buffer
  - 6f4f2a443d87 can: flexcan: fix NULL pointer exception during bringup
  - 576f474fb2d3 can: bcm: check timer values before ktime conversion
  ACK: pavel -- already reviewed in 4.4.173
  - 8d85aa96c54b can: dev: __can_get_echo_skb(): fix bogous check for non-existing skb by removing it
  ACK: pavel -- already reviewed in 4.4.173
  - bdcf74e735b1 irqchip/gic-v3-its: Align PCI Multi-MSI allocation on their size
  ACK: pavel -- already reviewed in 4.4.173
  - 6f4db68ab5ce net: sun: cassini: Cleanup license conflict
  - 21c0d1621b8d posix-cpu-timers: Unbreak timer rearming
  - dd085f9b1dc1 x86/entry/64/compat: Fix stack switching for XEN PV
  - ed334be9c2ed x86/kaslr: Fix incorrect i8254 outb() parameters
  ACK: pavel -- already reviewed in 4.4.173
  - 334c0e1b3cdd x86/selftests/pkeys: Fork() to check for state being preserved
  - db01b8d40feb x86/pkeys: Properly copy pkey state at fork()
  - f9203cd03125 KVM/nVMX: Do not validate that posted_intr_desc_addr is page aligned
  - d58f5e638b18 kvm: x86/vmx: Use kzalloc for cached_vmcs12
  - bbb8c5c75f6e KVM: x86: WARN_ONCE if sending a PV IPI returns a fatal error
  - b2598858ac21 KVM: x86: Fix PV IPIs for 32-bit KVM host
  - 6d3dabbdf46e KVM: x86: Fix single-step debugging
  ACK: pavel -- already reviewed in 4.4.173
  - c1bfae340367 drm/amdgpu: Add APTX quirk for Lenovo laptop
  - b911f1dcb60d dm crypt: fix parsing of extended IV arguments
  - 5b779f841773 dm thin: fix passdown_double_checking_shared_status()
  - eba68bd45672 scsi: ufs: Use explicit access size in ufshcd_dump_regs
  - b18931c5fe0d acpi/nfit: Fix command-supported detection
  - 3cb00cfa3d37 acpi/nfit: Block function zero DSMs
  - 92fbac528fd0 Input: uinput - fix undefined behavior in uinput_validate_absinfo()
  - 71b1af87749b Input: input_event - provide override for sparc64
  - 865a07956db5 Input: xpad - add support for SteelSeries Stratus Duo
  ACK: pavel -- already reviewed in 4.4.173
  - 06d9f987201f smb3: add credits we receive from oplock/break PDUs
  - 779c65bb7739 CIFS: Do not reconnect TCP session in add_credits()
  - 2ae6fedbd5cb CIFS: Fix credit calculation for encrypted reads with errors
  - 0380ed9b1cd3 CIFS: Fix credits calculations for reads with errors
  - 07b9e5e35e8f CIFS: Fix possible hang during async MTU reads and writes
  ACK: pavel -- already reviewed in 4.4.173
  - f4abbb16ed9a vgacon: unconfuse vc_origin when using soft scrollback
  - a912e16faeda Drivers: hv: vmbus: Check for ring when getting debug info
  - bfe482b9b299 hv_balloon: avoid touching uninitialized struct page during tail onlining
  - 71d1a74f36a7 tty/n_hdlc: fix __might_sleep warning
  - 6d15ef2c912a uart: Fix crash in uart_write and uart_put_char
  - 80250b48803a tty: Handle problem if line discipline does not have receive_buf
  - 3209eeded863 staging: rtl8188eu: Add device code for D-Link DWA-121 rev B1
  - 75a08b9a0fcb mmc: meson-gx: Free irq in release() callback
  - 4a559dfe6603 mmc: dw_mmc-bluefield: : Fix the license information
  - d3faea2d152e char/mwave: fix potential Spectre v1 vulnerability
  - 0479bdbf55d8 misc: ibmvsm: Fix potential NULL pointer dereference
  - 049c7b068dd1 s390/smp: fix CPU hotplug deadlock with CPU rescan
  - e0d573a08f23 s390/early: improve machine detection
  - b563764443a3 s390/mm: always force a load of the primary ASCE on context switch
  - 8cbca17381ac ARC: perf: map generic branches to correct hardware condition
  - 2f0d2f3ace1f ARC: adjust memblock_reserve of kernel memory
  - 7bb78e62f712 ARCv2: lib: memeset: fix doing prefetchw outside of buffer
  - cf662d989425 ALSA: hda - Add mute LED support for HP ProBook 470 G5
  - 2173f5a1b0d7 ALSA: hda/realtek - Fix typo for ALC225 model
  - a719cbe07847 inotify: Fix fd refcount leak in inotify_add_watch().
  - afb4a7ca7818 clk: socfpga: stratix10: fix naming convention for the fixed-clocks
  - cf8ea8d536a4 clk: socfpga: stratix10: fix rate calculation for pll clocks
  - 0af64fda917d ASoC: tlv320aic32x4: Kernel OOPS while entering DAPM standby mode
  - 4fedd516d554 ASoC: rt5514-spi: Fix potential NULL pointer dereference
  - d6847f539bd6 ASoC: atom: fix a missing check of snd_pcm_lib_malloc_pages
  - 3e05ceedf143 ceph: clear inode pointer when snap realm gets dropped by its inode
  - 8e7320b9f551 USB: serial: pl2303: add new PID to support PL2303TB
  - 4d984aab54a6 USB: serial: simple: add Motorola Tetra TPG2200 device id
  - a70e5cd09361 USB: leds: fix regression in usbport led trigger
  - f8982204cbea mei: me: add denverton innovation engine device IDs
  - adfda26bdf47 mei: me: mark LBG devices as having dma support
  - 2cade15d58ca tcp: allow MSG_ZEROCOPY transmission also in CLOSE_WAIT state
  - 6c4d069aec0f ip6_gre: update version related info when changing link
  - c9fe9d194d45 net: phy: marvell: Fix deadlock from wrong locking
  - 552cd931b483 erspan: build the header with the right proto according to erspan_ver
  - 0449da6fc203 ip6_gre: fix tunnel list corruption for x-netns
  - e3fa624ee7af udp: with udp_segment release on error path
  - 84bf74307c88 net/sched: cls_flower: allocate mask dynamically in fl_change()
  - bdafc159ac8c mlxsw: pci: Ring CQ's doorbell before RDQ's
  - c82f4684d330 mlxsw: spectrum_fid: Update dummy FID index
  - adbf7e580994 net: ipv4: Fix memory leak in network namespace dismantle
  - bc4e2300e44a mlxsw: pci: Increase PCI SW reset timeout
  - 1688e75cae7d vhost: log dirty page correctly
  - 3d997bf0074e openvswitch: Avoid OOB read when parsing flow nlattrs
  - 916c27c8cf88 net_sched: refetch skb protocol for each filter
  - 02239e797ac7 net/sched: act_tunnel_key: fix memory leak in case of action replace
  - 3e4cd0677715 net: phy: mdio_bus: add missing device_del() in mdiobus_register() error handling
  - 1a864e38b39e net: phy: marvell: Errata for mv88e6390 internal PHYs
  - 40f2f08030fa net: Fix usage of pskb_trim_rcsum
  - e287968a3837 net: bridge: Fix ethernet header pointer before check skb forwardable
  - 779a5077d9cf amd-xgbe: Fix mdio access for non-zero ports and clause 45 PHYs
