# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v5.10.71
  - 5cd40b137cba Linux 5.10.71
  - 96f439a7eda6 netfilter: nf_tables: Fix oversized kvmalloc() calls
  ACK: pavel
  ACK: uli -- required because of 57a269a1b ("mm: don't allow oversized kvmalloc() calls")
  - e2d192301a0d netfilter: conntrack: serialize hash resizes and cleanups
  UR: pavel -- changes locking to avoid performance issue
  ACK: uli -- comes with (not strictly required) optimization
  - deb294941767 KVM: x86: Handle SRCU initialization failure during page track init
  ACK: pavel -- just a robustness
  ACK: uli
  - f7ac4d24e161 HID: usbhid: free raw_report buffers in usbhid_stop
  ACK: pavel -- not a minimum fix
  ACK: uli
  - 57a269a1b12a mm: don't allow oversized kvmalloc() calls
  ACK: pavel -- just a robustness, I'd put check earlier
  ACK: uli -- necessitates "*Fix oversized kvmalloc() calls* patches
  - da5b8b9319f0 netfilter: ipset: Fix oversized kvmalloc() calls
  ACK: pavel -- just an API tweak as we don't have Linus' commit
  ACK: uli -- required because of 57a269a1b ("mm: don't allow oversized kvmalloc() calls")
  - dedfc35a2de2 HID: betop: fix slab-out-of-bounds Write in betop_probe
  ACK: pavel -- just a robustness against bad hardware
  ACK: uli -- joystick driver, secfix?
  - 17ccc64e4fa5 crypto: ccp - fix resource leaks in ccp_run_aes_gcm_cmd()
  UR: pavel
  ACK: uli -- 3-in-1 fix
  - 28f0fdbac0f5 usb: hso: remove the bailout parameter
  UR: pavel --! just a cleanup/preparation -- but we don't have corresponding patch in stable
  ACK: uli -- cleanup, no functional changes
  - 4ad4852b9adf ASoC: dapm: use component prefix when checking widget names
  ACK: pavel
  ACK: uli
  - 5c3a90b6ff75 net: udp: annotate data race around udp_sk(sk)->corkflag
  ACK: pavel -- just a warning fix
  ACK: uli -- static code analyzer appeasement, no functional changes
  - a7f4c633ae12 HID: u2fzero: ignore incomplete packets without data
  ACK: pavel
  ACK: uli
  - 3770e21f60fc ext4: fix potential infinite loop in ext4_dx_readdir()
  ACK: pavel -- just a robustness against disk errors
  ACK: uli -- real-life issue?
  - a63474dbf692 ext4: add error checking to ext4_ext_replay_set_iblocks()
  ACK: pavel -- just a robustness
  ACK: uli -- incomplete; fixes system lock-up caused by FS corruption, but not the bug that corrupts the FS in the first place
  - 9ccf35492b08 ext4: fix reserved space counter leakage
  ACK: pavel -- not a minimum fix
  ACK: uli -- 2-in-1 fix (1st fixes resource leak, 2nd adds error message)
  - dc0942168ab3 ext4: limit the number of blocks in one ADD_RANGE TLV
  ACK: pavel
  ACK: uli
  - d11502fa2691 ext4: fix loff_t overflow in ext4_max_bitmap_size()
  ACK: pavel -- includes whitespace change
  ACK: uli -- OMG...
  - 7cea84867847 ipack: ipoctal: fix module reference leak
  ACK: pavel
  ACK: uli
  - 843efca98e6a ipack: ipoctal: fix missing allocation-failure check
  ACK: pavel -- printk when we are unable to allocate a channel would not be bad
  ACK: uli
  - 67d1df661088 ipack: ipoctal: fix tty-registration error handling
  ACK: pavel
  ACK: uli
  - f46e5db92fa2 ipack: ipoctal: fix tty registration race
  ACK: pavel
  ACK: uli
  - 5f6a309a6996 ipack: ipoctal: fix stack information leak
  ACK: pavel
  ACK: uli -- secfix
  - 3bef1b7242e0 debugfs: debugfs_create_file_size(): use IS_ERR to check for error
  ACK: pavel
  - 15fd3954bca7 elf: don't use MAP_FIXED_NOREPLACE for elf interpreter mappings
  ACK: pavel
  - 011b4de950d8 nvme: add command id quirk for apple controllers
  ACK: pavel -- just a support for broken hw
  - 44c600a57d57 hwmon: (pmbus/mp2975) Add missed POUT attribute for page 1 mp2975 controller
  ACK: pavel -- just a feature addition
  - 7fc5f60a01bb perf/x86/intel: Update event constraints for ICX
  ACK: pavel
  - 3db53827a0e9 af_unix: fix races in sk_peer_pid and sk_peer_cred accesses
  UR: pavel -- buggy? needs "cred takes null" patches?
  - d0d520c19e7e net: sched: flower: protect fl_walk() with rcu
  ACK: pavel
  - e63f6d8fe74a net: phy: bcm7xxx: Fixed indirect MMD operations
  ACK: pavel
  - 071febc37e06 net: hns3: fix always enable rx vlan filter problem after selftest
  ACK: pavel
  - 85e4f5d28d25 net: hns3: reconstruct function hns3_self_test
  UR: pavel -- just a refactoring, not a trivial one
  - 8e89876c84b2 net: hns3: fix prototype warning
  ACK: pavel -- just a kerneldoc tweak
  - d4a14faf7919 net: hns3: fix show wrong state when add existing uc mac address
  ACK: pavel
  - 64dae9551f8a net: hns3: fix mixed flag HCLGE_FLAG_MQPRIO_ENABLE and HCLGE_FLAG_DCB_ENABLE
  ACK: pavel -- just enables additional configurations
  - 8d3d27664ef4 net: hns3: keep MAC pause mode when multiple TCs are enabled
  ACK: pavel -- just a better hw support
  - f8ba689cb695 net: hns3: do not allow call hns3_nic_net_open repeatedly
  ACK: pavel
  - 20f6c4a31a52 ixgbe: Fix NULL pointer dereference in ixgbe_xdp_setup
  ACK: pavel
  - 16138cf938dc scsi: csiostor: Add module softdep on cxgb4
  ACK: pavel
  - 0306a2c7df7e Revert "block, bfq: honor already-setup queue merges"
  ACK: pavel
  - 1f2ca30fbde6 net: ks8851: fix link error
  ACK: pavel -- not a minimum fix
  - f1dd6e10f077 selftests, bpf: test_lwt_ip_encap: Really disable rp_filter
  ACK: pavel -- just a test tweak
  - 4967ae9ab44b selftests, bpf: Fix makefile dependencies on libbpf
  ACK: pavel -- just a buildsystem tweak
  - 59efda5073ab bpf: Exempt CAP_BPF from checks against bpf_jit_limit
  ACK: pavel
  - f908072391a6 RDMA/hns: Fix inaccurate prints
  IGN: pavel
  - 7e3eda32b881 e100: fix buffer overrun in e100_get_regs
  ACK: pavel -- not nearly a minimum fix
  - f2edf80cdd03 e100: fix length calculation in e100_get_regs_len
  ACK: pavel
  - c20a0ad7b6a0 dsa: mv88e6xxx: Include tagger overhead when setting MTU for DSA and CPU ports
  ACK: pavel
  - 7b771b12229e dsa: mv88e6xxx: Fix MTU definition
  ACK: pavel
  - ee4d0495a65e dsa: mv88e6xxx: 6161: Use chip wide MAX MTU
  ACK: pavel
  - d35d95e8b9da drm/i915/request: fix early tracepoints
  UR: pavel --! check we have prerequisites; we don't have "Do not share hwsp across contexts any more,". , just a tracing fix
  - 8321738c6e5a smsc95xx: fix stalled rx after link change
  ACK: pavel
  - 8de12ad9162c net: ipv4: Fix rtnexthop len when RTA_FLOW is present
  ACK: pavel
  - b22c5e2c8e03 net: enetc: fix the incorrect clearing of IF_MODE bits
  ACK: pavel -- probably does not fix a real problem
  - 5ee40530b0a6 hwmon: (tmp421) fix rounding for negative values
  ACK: pavel
  - 89d96f147d82 hwmon: (tmp421) report /PVLD condition as fault
  ACK: pavel
  - 560271d09f78 mptcp: don't return sockets in foreign netns
  ACK: pavel -- not sure it fixes user-visible bug
  - 9c6591ae8e63 sctp: break out if skb_header_pointer returns NULL in sctp_rcv_ootb
  ACK: pavel
  - 2c204cf594df mac80211-hwsim: fix late beacon hrtimer handling
  ACK: pavel
  - 8576e72ac5d6 mac80211: mesh: fix potentially unaligned access
  ACK: pavel -- not a minimum fix
  - 1282bb00835f mac80211: limit injected vht mcs/nss in ieee80211_parse_tx_radiotap
  ACK: pavel
  - 3748871e1215 mac80211: Fix ieee80211_amsdu_aggregate frag_tail bug
  ACK: pavel
  - 76bbb482d33b hwmon: (mlxreg-fan) Return non-zero value when fan current state is enforced from sysfs
  ACK: pavel
  - c61736a994fe bpf, mips: Validate conditional branch offsets
  IGN: pavel
  - 3f4e68902d2e RDMA/cma: Fix listener leak in rdma_cma_listen_on_all() failure
  IGN: pavel
  - 62ba3c50104b IB/cma: Do not send IGMP leaves for sendonly Multicast groups
  IGN: pavel
  - d93f65586c59 bpf: Handle return value of BPF_PROG_TYPE_STRUCT_OPS prog
  ACK: pavel -- not a minimum fix
  - 12cbdaeeb5d4 ipvs: check that ip_vs_conn_tab_bits is between 8 and 20
  ACK: pavel -- just a robustness
  - 9f382e1edf90 drm/amdgpu: correct initial cp_hqd_quantum for gfx9
  ACK: pavel
  - c331fad63b6d drm/amd/display: Pass PCI deviceid into DC
  ACK: pavel
  - 0a16c9751e0f RDMA/cma: Do not change route.addr.src_addr.ss_family
  IGN: pavel
  - 31a13f039e15 media: ir_toy: prevent device from hanging during transmit
  ACK: pavel -- just a workaround for buggy hw, not a minimum fix
  - 249e5e5a501e KVM: rseq: Update rseq when processing NOTIFY_RESUME on xfer to KVM guest
  ACK: pavel
  - 3778511dfc59 KVM: nVMX: Filter out all unsupported controls when eVMCS was activated
  ACK: pavel -- just a API tweak
  - 4ed671e6bc62 KVM: x86: nSVM: don't copy virt_ext from vmcb12
  ACK: pavel -- just a robustness
  - bebabb76ad9a KVM: x86: Fix stack-out-of-bounds memory access from ioapic_write_indirect()
  ACK: pavel
  - 782122ae7db0 x86/kvmclock: Move this_cpu_pvti into kvmclock.h
  UR: pavel --! why? hv_clock_per_cpu is unused elsewhere on 5.10
  - 57de2dcb1874 mac80211: fix use-after-free in CCMP/GCMP RX
  ACK: pavel
  - 201ba843fef5 scsi: ufs: Fix illegal offset in UPIU event trace
  ACK: pavel
  - bd4e446a6947 gpio: pca953x: do not ignore i2c errors
  ACK: pavel -- not sure this fixes user-visible bug
  - 516d90550390 hwmon: (w83791d) Fix NULL pointer dereference by removing unnecessary structure field
  ACK: pavel -- not a minimum fix
  - 1499bb2c3a87 hwmon: (w83792d) Fix NULL pointer dereference by removing unnecessary structure field
  ACK: pavel -- not a minimum fix
  - 7c4fd5de39f2 hwmon: (w83793) Fix NULL pointer dereference by removing unnecessary structure field
  ACK: pavel -- not a minimum fix
  - 196dabd96bbf hwmon: (tmp421) handle I2C errors
  ACK: pavel -- not a minimum fix
  - 23a6dfa10f03 fs-verity: fix signed integer overflow with i_size near S64_MAX
  ACK: pavel
  - d1d0016e4a7d ACPI: NFIT: Use fallback node id when numa info in NFIT table is incorrect
  ACK: pavel -- just a arm64 support
  - e9edc7bc611a ALSA: hda/realtek: Quirks to enable speaker output for Lenovo Legion 7i 15IMHG05, Yoga 7i 14ITL5/15ITL5, and 13s Gen2 laptops.
  ACK: pavel -- just a improved hw support
  - 23115ca7d227 usb: cdns3: fix race condition before setting doorbell
  ACK: pavel
  - 3945c481360c cpufreq: schedutil: Destroy mutex before kobject_put() frees the memory
  ACK: pavel
  - 2193cf76f43a scsi: qla2xxx: Changes to support kdump kernel for NVMe BFS
  ACK: pavel
  - a7d4fc84404d cpufreq: schedutil: Use kobject release() method to free sugov_tunables
  NAK: pavel -- Rafael said this needs delay
  - d570c48dd37d tty: Fix out-of-bound vmalloc access in imageblit
  ACK: pavel
