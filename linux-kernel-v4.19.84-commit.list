# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v4.19.84
  - c555efaf1402 Linux 4.19.84
  - 46a4a014c48e kvm: x86: mmu: Recovery of shattered NX large pages
  - 6082f2e28887 kvm: Add helper function for creating VM worker threads
  - 5219505fcbb6 kvm: mmu: ITLB_MULTIHIT mitigation
  - db77548b1652 KVM: vmx, svm: always run with EFER.NXE=1 when shadow paging is active
  - 37dfbc8ba763 KVM: x86: add tracepoints around __direct_map and FNAME(fetch)
  - 9ef1fae24d58 KVM: x86: change kvm_mmu_page_get_gfn BUG_ON to WARN_ON
  - b182093d1c70 KVM: x86: remove now unneeded hugepage gfn adjustment
  - e79234ce5765 KVM: x86: make FNAME(fetch) and __direct_map more similar
  - 8aaac3068623 kvm: mmu: Do not release the page inside mmu_set_spte()
  - 30d8d8d6cd92 kvm: Convert kvm_lock to a mutex
  - a991063ce576 kvm: x86, powerpc: do not allow clearing largepages debugfs entry
  - 580c79e7e3e5 Documentation: Add ITLB_MULTIHIT documentation
  - db5ae6596ae2 cpu/speculation: Uninline and export CPU mitigations helpers
  - 955607466ace x86/cpu: Add Tremont to the cpu vulnerability whitelist
  - f9aa6b73a407 x86/bugs: Add ITLB_MULTIHIT bug infrastructure
  - 415bb221a070 x86/speculation/taa: Fix printing of TAA_MSG_SMT on IBRS_ALL CPUs
  - 4ad7466ddf2d x86/tsx: Add config options to set tsx=on|off|auto
  - e3bf6b3ff55a x86/speculation/taa: Add documentation for TSX Async Abort
  - 2402432d5557 x86/tsx: Add "auto" option to the tsx= cmdline parameter
  - a0808f06dfa1 kvm/x86: Export MDS_NO=0 to guests when TSX is enabled
  - 15dfa5d706df x86/speculation/taa: Add sysfs reporting for TSX Async Abort
  - 6c58ea8525bf x86/speculation/taa: Add mitigation for TSX Async Abort
  - b8eb348ae408 x86/cpu: Add a "tsx=" cmdline option with TSX disabled by default
  - 37cf9ef900cc x86/cpu: Add a helper function x86_read_arch_cap_msr()
  - 4002d16a2ae1 x86/msr: Add the IA32_TSX_CTRL MSR
  - dbf38b17a892 KVM: x86: use Intel speculation bugs and features as derived in generic x86 code
  - fee619bb8136 drm/i915/cmdparser: Fix jump whitelist clearing
  - 255ed51599de drm/i915/gen8+: Add RC6 CTX corruption WA
  - 011b7173cbdb drm/i915: Lower RM timeout to avoid DSI hard hangs
  - a7bda639a17f drm/i915/cmdparser: Ignore Length operands during command matching
  - 6e53c71a6913 drm/i915/cmdparser: Add support for backward jumps
  - f27bc2b5950d drm/i915/cmdparser: Use explicit goto for error paths
  - cdd77c6b4be4 drm/i915: Add gen9 BCS cmdparsing
  - fea688c5dd81 drm/i915: Allow parsing of unsized batches
  - 7ce726b61c57 drm/i915: Support ro ppgtt mapped cmdparser shadow buffers
  - fc3510fe6f6b drm/i915: Add support for mandatory cmdparsing
  - fba4207cf15e drm/i915: Remove Master tables from cmdparser
  - f1ff77080fa1 drm/i915: Disable Secure Batches for gen6+
  - b4b1abdc6b18 drm/i915: Rename gen7 cmdparser tables
  - e238e05ec2dc vsock/virtio: fix sock refcnt holding during the shutdown
  - 2e7e3f16901d iio: imu: mpu6050: Fix FIFO layout for ICM20602
  - 99ea48af7bd9 net: prevent load/store tearing on sk->sk_stamp
  - d32629dcd1e5 netfilter: ipset: Copy the right MAC address in hash:ip,mac IPv6 sets
  - 5833560d54fd usbip: Fix free of unallocated memory in vhci tx
  - 6890b4bc3d2b cgroup,writeback: don't switch wbs immediately on dead wbs if the memcg is dead
  ACK: pavel
  - d3b3c0a14615 mm/filemap.c: don't initiate writeback if mapping has no dirty pages
  ACK: pavel -- just a performance improvement
  - 285eb6af4351 iio: imu: inv_mpu6050: fix no data on MPU6050
  ACK: pavel
  - d888a80727ab iio: imu: mpu6050: Add support for the ICM 20602 IMU
  UR: pavel -- too long, adds new hardware support, known buggy
  - 522128128dec blkcg: make blkcg_print_stat() print stats only for online blkgs
  ACK: pavel
  - 30b969392cf2 pinctrl: cherryview: Fix irq_valid_mask calculation
  ACK: pavel
  - ca79bb7e1168 ocfs2: protect extent tree in ocfs2_prepare_inode_for_write()
  UR: pavel -- long
  - 2c655a111968 pinctrl: intel: Avoid potential glitches if pin is in GPIO mode
  ACK: pavel -- behaviour change; it changes things
  - 713adf6dd327 e1000: fix memory leaks
  ACK: pavel
  - 4a05571772cc igb: Fix constant media auto sense switching when no cable is connected
  ACK: pavel
  - 1baab8352d80 net: ethernet: arc: add the missed clk_disable_unprepare
  ACK: pavel
  - 24523745ed41 NFSv4: Don't allow a cached open with a revoked delegation
  ACK: pavel
  - 440a748ed6df usb: dwc3: gadget: fix race when disabling ep with cancelled xfers
  ACK: pavel -- same code three times -- create helper?
  - e66f52eb3f29 hv_netvsc: Fix error handling in netvsc_attach()
  ACK: pavel
  - 99d5f18cebbf drm/amd/display: Passive DP->HDMI dongle detection fix
  ACK: pavel -- fix for dodgy hardware, not a serious bug
  - e5edbf9c45ce drm/amdgpu: If amdgpu_ib_schedule fails return back the error.
  ACK: pavel
  - b651ddc15e7a iommu/amd: Apply the same IVRS IOAPIC workaround to Acer Aspire A315-41
  ACK: pavel
  - 214e4f0ecdd1 net: mscc: ocelot: refuse to overwrite the port's native vlan
  ACK: pavel
  - 5aedcc8aa8be net: mscc: ocelot: fix vlan_filtering when enslaving to bridge before link is up
  ACK: pavel
  - 3b956e63e2f3 net: hisilicon: Fix "Trying to free already-free IRQ"
  ACK: pavel
  - f09b99c883e8 fjes: Handle workqueue allocation failure
  ACK: pavel
  - 6376736d016f nvme-multipath: fix possible io hang after ctrl reconnect
  ACK: pavel
  - 1372527e6876 scsi: qla2xxx: stop timer in shutdown path
  ACK: pavel
  - f2bab3ed456c RDMA/hns: Prevent memory leaks of eq->buf_list
  ACK: pavel
  - 55ca08347487 RDMA/iw_cxgb4: Avoid freeing skb twice in arp failure case
  ACK: pavel
  - e36be7959326 usbip: tools: Fix read_usb_vudc_device() error path handling
  ACK: pavel
  - cd9561a53d26 USB: ldusb: use unsigned size format specifiers
  ACK: pavel -- just a printk fix
  - c753113ae714 USB: Skip endpoints with 0 maxpacket length
  ACK: pavel -- catches unusual situation, not a bugfix
  - ef38f4d123d0 perf/x86/uncore: Fix event group support
  ACK: pavel -- not a minimum bugfix, contains impossible tests
  - f14751658a01 perf/x86/amd/ibs: Handle erratum #420 only on the affected CPU family (10h)
  ACK: pavel -- performance improvement, not a bugfix
  - 5b99e97b275a perf/x86/amd/ibs: Fix reading of the IBS OpData register and thus precise RIP validity
  ACK: pavel
  - 45944c4a7743 usb: dwc3: remove the call trace of USBx_GFLADJ
  ACK: pavel
  - dff38149cec4 usb: gadget: configfs: fix concurrent issue between composite APIs
  ACK: pavel -- code is seriously strange and could use some cleanup: bool should be set to true, false; why get_data twice?
  - 10eb9abd21ba usb: dwc3: pci: prevent memory leak in dwc3_pci_probe
  ACK: pavel
  - c73ccf65e169 usb: gadget: composite: Fix possible double free memory bug
  ACK: pavel
  - 26d31e1c3ab2 usb: gadget: udc: atmel: Fix interrupt storm in FIFO mode.
  ACK: pavel
  - 88912019b49c usb: fsl: Check memory resource before releasing it
  ACK: pavel
  - 3a2675a2d97a macsec: fix refcnt leak in module exit routine
  ACK: pavel
  - 0d0ca85ad424 bonding: fix unexpected IFF_BONDING bit unset
  ACK: pavel
  - 50e31318b525 ipvs: move old_secure_tcp into struct netns_ipvs
  ACK: pavel
  - 102f4078fbdd ipvs: don't ignore errors in case refcounting ip_vs module fails
  ACK: pavel
  - 81de0b500baa netfilter: nf_flow_table: set timeout before insertion into hashes
  ACK: pavel
  - d45fc2ed472b scsi: qla2xxx: Initialized mailbox to prevent driver load failure
  ACK: pavel
  - b6612a3dbad8 scsi: lpfc: Honor module parameter lpfc_use_adisc
  ACK: pavel
  - 4e80e5614770 net: openvswitch: free vport unless register_netdevice() succeeds
  ACK: pavel
  - 027253315d70 RDMA/uverbs: Prevent potential underflow
  ACK: pavel
  - d582769add68 scsi: qla2xxx: fixup incorrect usage of host_byte
  ACK: pavel
  - 42de3a902443 net/mlx5: prevent memory leak in mlx5_fpga_conn_create_cq
  ACK: pavel
  - 7dfdcd9407f3 net/mlx5e: TX, Fix consumer index of error cqe dump
  ACK: pavel
  - 48dd71289ca3 RDMA/qedr: Fix reported firmware version
  ACK: pavel -- interface change, not really a bugfix
  - 6208c2bfe224 iw_cxgb4: fix ECN check on the passive accept
  ACK: pavel
  - 89aa9e2626f4 RDMA/mlx5: Clear old rate limit when closing QP
  ACK: pavel
  - d6706b2ec108 HID: intel-ish-hid: fix wrong error handling in ishtp_cl_alloc_tx_ring()
  ACK: pavel
  - 113a154ef2f2 dmaengine: sprd: Fix the possible memory leak issue
  ACK: pavel
  - 6040f96d5147 dmaengine: xilinx_dma: Fix control reg update in vdma_channel_set_config
  ACK: pavel
  - 78e7e0248eb8 HID: google: add magnemite/masterball USB ids
  ACK: pavel
  - 8181146cd7de PCI: tegra: Enable Relaxed Ordering only for Tegra20 & Tegra30
  ACK: pavel
  - e2dd254bde5c usbip: Implement SG support to vhci-hcd and stub driver
  UR: pavel -- way over limit, long long buffer sizes...
  - f865ae473c16 usbip: Fix vhci_urb_enqueue() URB null transfer buffer error path
  ACK: pavel -- just reduces severity of debug check
  - e9c0fc4a7ccd sched/fair: Fix -Wunused-but-set-variable warnings
  ACK: pavel -- not a minimal fix
  - 502bd151448c sched/fair: Fix low cpu usage with high throttling by removing expiration of cpu-local slices
  pavel -- performance fix, has been like this for 5 years; long; causes warning, fixed by next patch...
  - 4ebee4875eab ALSA: usb-audio: Fix copy&paste error in the validator
  ACK: pavel
  - e0051889243d ALSA: usb-audio: remove some dead code
  ACK: pavel -- cleanup
  - 4f6c52002699 ALSA: usb-audio: Fix possible NULL dereference at create_yamaha_midi_quirk()
  NAK: pavel -- wrong
  - 3a0cdf210b94 ALSA: usb-audio: Clean up check_input_term()
  UR: pavel -- long
  - 9feeaa50e5b4 ALSA: usb-audio: Remove superfluous bLength checks
  ACK: pavel -- cleanup enabled by previous code, not a bugfix
  - f0e164f66e75 ALSA: usb-audio: Unify the release of usb_mixer_elem_info objects
  ACK: pavel -- cleanup, not a bugfix
  - dae4d839e549 ALSA: usb-audio: Simplify parse_audio_unit()
  ACK: pavel -- cleanup, not a bugfix
  - 17821e2fb167 ALSA: usb-audio: More validations of descriptor units
  UR: pavel -- big
  - 5e36cf8edb58 configfs: fix a deadlock in configfs_symlink()
  UR: pavel
  - 0dfc45be875a configfs: provide exclusion between IO and removals
  UR: pavel
  - 25c118d8d158 configfs: new object reprsenting tree fragments
  pavel -- preparation for next patches
  - 65524d647e9d configfs_register_group() shouldn't be (and isn't) called in rmdirable parts
  ACK: pavel -- cleanup
  - 2bd63490c1dd configfs: stash the data we need into configfs_buffer at open time
  UR: pavel -- big, cleanup, not a bugfix
  - a7be2debb769 can: peak_usb: fix slab info leak
  ACK: pavel
  - ce9b94da0e04 can: mcba_usb: fix use-after-free on disconnect
  ACK: pavel
  - 5a9e37f2029f can: dev: add missing of_node_put() after calling of_get_child_by_name()
  ACK: pavel
  - 9289226f6982 can: gs_usb: gs_can_open(): prevent memory leak
  ACK: pavel
  - 9f5c59428843 can: rx-offload: can_rx_offload_queue_sorted(): fix error handling, avoid skb mem leak
  ACK: pavel
  - ef502d5a84d6 can: peak_usb: fix a potential out-of-sync while decoding packets
  ACK: pavel
  - 7ae08111ca70 can: c_can: c_can_poll(): only read status register after status IRQ
  ACK: pavel
  - 0327c7818da2 can: flexcan: disable completely the ECC mechanism
  ACK: pavel
  - 46265660e5ba can: usb_8dev: fix use-after-free on disconnect
  ACK: pavel
  - d8a76e300e37 SMB3: Fix persistent handles reconnect
  ACK: pavel
  - caddaf43b024 x86/apic/32: Avoid bogus LDR warnings
  ACK: pavel
  - dc1a91dc4917 intel_th: pci: Add Jasper Lake PCH support
  ACK: pavel
  - f9d3aea1dca2 intel_th: pci: Add Comet Lake PCH support
  ACK: pavel
  - 64997ee49c8c netfilter: ipset: Fix an error code in ip_set_sockfn_get()
  ACK: pavel
  - 1b0e60f6a48b netfilter: nf_tables: Align nft_expr private data to 64-bit
  ACK: pavel
  - 2dae80b5b666 ARM: sunxi: Fix CPU powerdown on A83T
  ACK: pavel
  - 20b9e094dcd3 iio: srf04: fix wrong limitation in distance measuring
  ACK: pavel -- raise distance limit, not a serious bug
  - bee45b44b13e iio: imu: adis16480: make sure provided frequency is positive
  ACK: pavel
  - a428996147e2 iio: adc: stm32-adc: fix stopping dma
  ACK: pavel
  - 78a1d6cdd302 ceph: add missing check in d_revalidate snapdir handling
  ACK: pavel
  - 6f9657793a6e ceph: fix use-after-free in __ceph_remove_cap()
  ACK: pavel
  - 3840610d60b2 arm64: Do not mask out PTE_RDONLY in pte_same()
  ACK: pavel
  - 56f270a1d72c soundwire: bus: set initial value to port_status
  ACK: pavel
  - 9a06efc745c3 soundwire: depend on ACPI
  ACK: pavel -- just a warning fix
  - a81a4637456b HID: wacom: generic: Treat serial number and related fields as unsigned
  ACK: pavel -- code could be way simpler
  - e3fdd0c1a3d0 drm/radeon: fix si_enable_smc_cac() failed issue
  ACK: pavel
  - f39fbd05f921 perf tools: Fix time sorting
  ACK: pavel
  - 66d53cd683a8 tools: gpio: Use !building_out_of_srctree to determine srctree
  ACK: pavel
  - 8e358a027611 dump_stack: avoid the livelock of the dump_lock
  ACK: pavel
  - 6c944fc51f0a mm, vmstat: hide /proc/pagetypeinfo from normal users
  ACK: pavel -- workaround, not a bugfix
  - 2686f71fdcc5 mm: thp: handle page cache THP correctly in PageTransCompoundMap
  ACK: pavel -- improves performance, not fix for serious bug
  - 7dfa51beacac mm, meminit: recalculate pcpu batch and high limits after init completes
  UR: pavel -- caused problems in 4.4?
  - 8e6bf4bc3a88 mm: memcontrol: fix network errors from failing __GFP_ATOMIC charges
  ACK: pavel -- one liner, but... tricky
  - 6ecc16351a84 ALSA: hda/ca0132 - Fix possible workqueue stall
  ACK: pavel
  - 6921b1609912 ALSA: bebob: fix to detect configured source of sampling clock for Focusrite Saffire Pro i/o series
  ACK: pavel
  - b85472244b70 ALSA: timer: Fix incorrectly assigned timer instance
  ACK: pavel
  - 107451b87ea5 net: hns: Fix the stray netpoll locks causing deadlock in NAPI path
  ACK: pavel
  - 26e398dcb3f1 ipv6: fixes rt6_probe() and fib6_nh->last_probe init
  ACK: pavel
  - 05b761423d67 net: mscc: ocelot: fix NULL pointer on LAG slave removal
  ACK: pavel
  - 1cfc967ef584 net: mscc: ocelot: don't handle netdev events for other netdevs
  ACK: pavel
  - a6fdbaeef1f2 qede: fix NULL pointer deref in __qede_remove()
  ACK: pavel
  - 956b38853517 NFC: st21nfca: fix double free
  ACK: pavel
  - 1143496c9632 nfc: netlink: fix double device reference drop
  ACK: pavel
  - 760a1f7f22ee NFC: fdp: fix incorrect free object
  ACK: pavel
  - 5580091ce7d9 net: usb: qmi_wwan: add support for DW5821e with eSIM support
  ACK: pavel
  - 4fd218071f21 net: qualcomm: rmnet: Fix potential UAF when unregistering
  ACK: pavel
  - b9bda52f8f3e net: fix data-race in neigh_event_send()
  ACK: pavel -- theoretical race
  - 2fbfdb2de4a1 net: ethernet: octeon_mgmt: Account for second possible VLAN header
  ACK: pavel
  - 88f8c39912bc ipv4: Fix table id reference in fib_sync_down_addr
  ACK: pavel
  - 0ddabef89067 CDC-NCM: handle incomplete transfer of MTU
  ACK: pavel
  - 27b5f4bf5ba9 bonding: fix state transition issue in link monitoring
  ACK: pavel
