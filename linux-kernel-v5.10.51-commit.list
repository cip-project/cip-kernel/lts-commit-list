# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v5.10.51
  - f68261346518 Linux 5.10.51
  - 86786603014e f2fs: fix to avoid racing on fsync_entry_slab by multi filesystem instances
  ACK: pavel
  - 5e4f5138bd85 ext4: fix memory leak in ext4_fill_super
  UR: pavel
  - 3780348c1a0e smackfs: restrict bytes count in smk_set_cipso()
  ACK: pavel
  - 801893695036 jfs: fix GPF in diFree
  ACK: pavel
  - fcb041ca5c77 drm/ast: Remove reference to struct drm_device.pdev
  ACK: pavel -- just a cleanup
  - 3785f3c1e3c7 pinctrl: mcp23s08: Fix missing unlock on error in mcp23s08_irq()
  ACK: pavel
  - b716ccffbc8d dm writecache: write at least 4k when committing
  ACK: pavel -- just a performance tweak
  - 090588059c30 io_uring: fix clear IORING_SETUP_R_DISABLED in wrong function
  ACK: pavel
  - aa57b2d6b37e media: uvcvideo: Fix pixel format change for Elgato Cam Link 4K
  UR: pavel --! commented out code
  - 31874b6b63dd media: gspca/sunplus: fix zero-length control requests
  ACK: pavel
  - de95c0bd797a media: gspca/sq905: fix control-request direction
  ACK: pavel
  - c57bfd8000d7 media: zr364xx: fix memory leak in zr364xx_start_readpipe
  ACK: pavel
  - dbd58d397844 media: dtv5100: fix control-request directions
  ACK: pavel
  - db317a37229b media: subdev: disallow ioctl for saa6588/davinci
  ACK: pavel
  - e2c1218ddc5f PCI: aardvark: Implement workaround for the readback value of VEND_ID
  ACK: pavel -- just an ID change for hw bug workaround
  - 130919708990 PCI: aardvark: Fix checking for PIO Non-posted Request
  ACK: pavel
  - f147115018aa PCI: Leave Apple Thunderbolt controllers on for s2idle or standby
  ACK: pavel
  - ba47e65a5de3 dm btree remove: assign new_root only when removal succeeds
  ACK: pavel
  - 1b5918b087b1 dm writecache: flush origin device when writing and cache is full
  UR: pavel -- c/e
  - cbc03ffec260 dm zoned: check zone capacity
  ACK: pavel
  - 35c1c4bd2d59 coresight: tmc-etf: Fix global-out-of-bounds in tmc_update_etf_buffer()
  UR: pavel -- no check for null?
  - 048624ad564c coresight: Propagate symlink failure
  ACK: pavel
  - 0c2bc1489104 ipack/carriers/tpci200: Fix a double free in tpci200_pci_probe
  ACK: pavel -- not the nicest code
  - eb81b5a37dc5 tracing: Resize tgid_map to pid_max, not PID_MAX_DEFAULT
  UR: pavel -- better tracing feature, not a bugfix
  - 3cda5b7f4e29 tracing: Simplify & fix saved_tgids logic
  UR: pavel
  - 8cc58a6e2c39 rq-qos: fix missed wake-ups in rq_qos_throttle try two
  ACK: pavel
  - f9fb4986f4d8 seq_buf: Fix overflow in seq_buf_putmem_hex()
  ACK: pavel
  - 418b333afbd5 extcon: intel-mrfld: Sync hardware and software state on init
  ACK: pavel
  - af092ec16e06 selftests/lkdtm: Fix expected text for CR4 pinning
  - 0af643fa7e74 lkdtm/bugs: XFAIL UNALIGNED_LOAD_STORE_WRITE
  ACK: pavel
  - baedb1f5a08c nvmem: core: add a missing of_node_put
  ACK: pavel -- not a minimum fix
  - f0a079c0ba87 mfd: syscon: Free the allocated name field of struct regmap_config
  ACK: pavel
  - a8a2e506ea2f power: supply: ab8500: Fix an old bug
  ACK: pavel
  - 38dde03eb239 ubifs: Fix races between xattr_{set|get} and listxattr operations
  UR: pavel -- c/e, typo in comment
  - 690a11fb4e9f thermal/drivers/int340x/processor_thermal: Fix tcc setting
  ACK: pavel
  - ef5066f95c15 ipmi/watchdog: Stop watchdog timer when the current action is 'none'
  ACK: pavel
  - 7ade84f8df8f qemu_fw_cfg: Make fw_cfg_rev_attr a proper kobj_attribute
  ACK: pavel -- just a warning fix
  - 02671eda9ab9 i40e: fix PTP on 5Gb links
  ACK: pavel
  - ab9d7c5fc9c6 ASoC: tegra: Set driver_name=tegra for all machine drivers
  ACK: pavel -- just an API tweak
  - e0d9beb44abd fpga: stratix10-soc: Add missing fpga_mgr_free() call
  ACK: pavel
  - 5a5ebf5d4822 clocksource/arm_arch_timer: Improve Allwinner A64 timer workaround
  ACK: pavel
  - b5e26be407e6 cpu/hotplug: Cure the cpusets trainwreck
  ACK: pavel -- "interesting"
  - a11a457820fb arm64: tlb: fix the TTL value of tlb_get_level
  ACK: pavel
  - 0afa6ad0c49a ata: ahci_sunxi: Disable DIPM
  ACK: pavel
  - 5543f61e2e0c mmc: core: Allow UHS-I voltage switch for SDSC cards if supported
  UR: pavel --a interesting; intentionaly deviates from SD spec
  - b53b0ca4a4ec mmc: core: clear flags before allowing to retune
  ACK: pavel
  - 658f58189a4f mmc: sdhci: Fix warning message when accessing RPMB in HS400 mode
  ACK: pavel -- just a warning removal
  - 5ced01c0e855 mmc: sdhci-acpi: Disable write protect detection on Toshiba Encore 2 WT8-B
  ACK: pavel
  - 3f9c2a058e61 drm/i915/display: Do not zero past infoframes.vsc
  ACK: pavel
  - 8abf5eec0ebd drm/nouveau: Don't set allow_fb_modifiers explicitly
  ACK: pavel -- just a cleanup
  - 42a333ea4b4f drm/arm/malidp: Always list modifiers
  ACK: pavel -- API change, does not fix a bug
  - 0bcc074f90d2 drm/msm/mdp4: Fix modifier support enabling
  ACK: pavel -- API change, does not fix a bug
  - 4d61ddd74041 drm/tegra: Don't set allow_fb_modifiers explicitly
  ACK: pavel -- API change, does not fix a bug
  - c6016936171a drm/amd/display: Reject non-zero src_y and src_x for video planes
  ACK: pavel -- disables configuration to work around a bug
  - 7d3053889400 pinctrl/amd: Add device HID for new AMD GPIO controller
  ACK: pavel
  - b13574fa83ac drm/amd/display: fix incorrrect valid irq check
  ACK: pavel
  - 3c8216b3503a drm/rockchip: dsi: remove extra component_del() call
  ACK: pavel
  - 2998599fb16c drm/dp: Handle zeroed port counts in drm_dp_read_downstream_info()
  ACK: pavel -- wrongly formatted comment
  - 98bd09d928b3 drm/vc4: hdmi: Prevent clock unbalance
  ACK: pavel -- misses error checking
  - a2b8835cb4d1 drm/vc4: crtc: Skip the TXP
  ACK: pavel
  - 293e520d2043 drm/vc4: txp: Properly set the possible_crtcs mask
  ACK: pavel
  - 0d50d93d05d6 drm/radeon: Call radeon_suspend_kms() in radeon_pci_shutdown() for Loongson64
  ACK: pavel -- quite a hack
  - 7aa28f2f6742 drm/radeon: Add the missed drm_gem_object_put() in radeon_user_framebuffer_create()
  ACK: pavel
  - 2674ffcad0ae drm/amdgpu: enable sdma0 tmz for Raven/Renoir(V2)
  ACK: pavel
  - 8f933b27cbf1 drm/amdgpu: Update NV SIMD-per-CU to 2
  ACK: pavel
  - 97ebbfe445cd powerpc/powernv/vas: Release reference to tgid during window close
  ACK: pavel
  - a024e88f8ab7 powerpc/barrier: Avoid collision with clang's __lwsync macro
  ACK: pavel -- just a clang workaround
  - d2e52d466409 powerpc/mm: Fix lockup on kernel exec fault
  ACK: pavel
  - 4ad382bc4abc scsi: iscsi: Fix iSCSI cls conn state
  ACK: pavel
  - 221b7e1e76fb scsi: iscsi: Fix race condition between login and sync thread
  UR: pavel -- c/e; buggy, fixed by next patch
  - 907318883508 io_uring: convert io_buffer_idr to XArray
  ACK: pavel
  - c5a50a220a41 io_uring: Convert personality_idr to XArray
  UR: pavel -- replaces data structure
  - cb2985feb118 io_uring: simplify io_remove_personalities()
  UR: pavel -- cleanup, not a bugfix
  - 7d4f96158852 mm,hwpoison: return -EBUSY when migration fails
  UR: pavel --! not a minimum fix, creative marking of upstream...
  - fd6625a1ec40 loop: fix I/O error on fsync() in detached loop devices
  ACK: pavel
  - 88f0bc830c52 arm64: dts: rockchip: Enable USB3 for rk3328 Rock64
  ACK: pavel
  - 421aff50af5e arm64: dts: rockchip: add rk3328 dwc3 usb controller node
  ACK: pavel
  - 8eb12fa96bc5 ath11k: unlock on error path in ath11k_mac_op_add_interface()
  ACK: pavel
  - 9706c5343346 MIPS: MT extensions are not available on MIPS32r1
  - 6cf2e905b1a0 selftests/resctrl: Fix incorrect parsing of option "-t"
  - 10f8fca6761b MIPS: set mips32r5 for virt extensions
  - ff4762bcb95e MIPS: loongsoon64: Reserve memory below starting pfn to prevent Oops
  - 6ef81a5c0e22 sctp: add size validation when walking chunks
  ACK: pavel
  - d4dbef7046e2 sctp: validate from_addr_param return
  UR: pavel
  - e83f312114a0 flow_offload: action should not be NULL when it is referenced
  ACK: pavel
  - a61af0114118 bpf: Fix false positive kmemleak report in bpf_ringbuf_area_alloc()
  ACK: pavel -- just a warning workaround
  - 20285dc2711c sched/fair: Ensure _sum and _avg values stay consistent
  ACK: pavel
  - e2296a4365f2 Bluetooth: btusb: fix bt fiwmare downloading failure issue for qca btsoc.
  UR: pavel -- strange place for delay?
  - 8d7a3989c14d Bluetooth: mgmt: Fix the command returns garbage parameter value
  ACK: pavel
  - 05298f1733c6 Bluetooth: btusb: Add support USB ALT 3 for WBS
  ACK: pavel
  - cc49ab24ec37 Bluetooth: L2CAP: Fix invalid access on ECRED Connection response
  ACK: pavel
  - 79a313086426 Bluetooth: L2CAP: Fix invalid access if ECRED Reconfigure fails
  ACK: pavel
  - c4a9967e4d09 Bluetooth: btusb: Add a new QCA_ROME device (0cf3:e500)
  ACK: pavel
  - 60789afc02f5 Bluetooth: Shutdown controller after workqueues are flushed or cancelled
  UR: pavel -- what about cmd_work?
  - 5147d86c4a5b Bluetooth: Fix alt settings for incoming SCO with transparent coding format
  ACK: pavel
  - 8f939b4c2563 Bluetooth: Fix the HCI to MGMT status conversion table
  ACK: pavel
  - 5f5f8022c1aa Bluetooth: btusb: Fixed too many in-token issue for Mediatek Chip.
  UR: pavel -- c/e
  - 3d08b5917984 RDMA/cma: Fix rdma_resolve_route() memory leak
  ACK: pavel
  - a8585fdf42b5 net: ip: avoid OOM kills with large UDP sends over loopback
  UR: pavel
  - 04177aa99a93 media, bpf: Do not copy more entries than user space requested
  ACK: pavel
  - d8bb134d808c IB/isert: Align target max I/O size to initiator size
  - d330f5f8dff7 mac80211_hwsim: add concurrent channels scanning support over virtio
  UR: pavel
  - 97f067722669 mac80211: consider per-CPU statistics if present
  ACK: pavel -- not a minimum fix
  - 1b728869a134 cfg80211: fix default HE tx bitrate mask in 2G band
  ACK: pavel
  - 0a7ba5d373f1 wireless: wext-spy: Fix out-of-bounds warning
  ACK: pavel -- not a minimum fix, just a warning workaround
  - c1ad55b6a1f4 sfc: error code if SRIOV cannot be disabled
  ACK: pavel -- not a minimum fix
  - 1013dc896d99 sfc: avoid double pci_remove of VFs
  ACK: pavel
  - 7cd6986f2de5 iwlwifi: pcie: fix context info freeing
  ACK: pavel
  - b98ec6d8b34d iwlwifi: pcie: free IML DMA memory allocation
  ACK: pavel
  - 78eadadff3d1 iwlwifi: mvm: fix error print when session protection ends
  ACK: pavel -- just a printk tweak
  - 1e1bb1efd60e iwlwifi: mvm: don't change band on bound PHY contexts
  ACK: pavel
  - 1df36030393a RDMA/rxe: Don't overwrite errno from ib_umem_get()
  - ee33c042f492 vsock: notify server to shutdown when client has pending signal
  ACK: pavel
  - 38bc2ebf344c atm: nicstar: register the interrupt handler in the right place
  UR: pavel -- correct? will it correctly handle interrupts at that point?
  - 90efb7f1006a atm: nicstar: use 'dma_free_coherent' instead of 'kfree'
  ACK: pavel
  - 1d304c7ddd36 net: fec: add ndo_select_queue to fix TX bandwidth fluctuations
  UR: pavel
  - c7a31ae63e2c MIPS: add PMD table accounting into MIPS'pmd_alloc_one
  - 50ce920fe113 rtl8xxxu: Fix device info for RTL8192EU devices
  ACK: pavel -- just printk tweaks
  - a10e871b73b4 mt76: mt7915: fix IEEE80211_HE_PHY_CAP7_MAX_NC for station mode
  ACK: pavel
  - 4cd713e48c27 drm/amdkfd: Walk through list with dqm lock hold
  ACK: pavel
  - a2122e079204 drm/amdgpu: fix bad address translation for sienna_cichlid
  ACK: pavel
  - 932be4cf2ba2 io_uring: fix false WARN_ONCE
  ACK: pavel
  - 92a9fb51e5ec net: sched: fix error return code in tcf_del_walker()
  ACK: pavel
  - d2801d111829 net: ipa: Add missing of_node_put() in ipa_firmware_load()
  ACK: pavel
  - 5cc0cf735f13 net: fix mistake path for netdev_features_strings
  NAK: pavel -- incorrect comment change
  - 891db094a0aa mt76: mt7615: fix fixed-rate tx status reporting
  ACK: pavel
  - 090b06b25afe ice: mark PTYPE 2 as reserved
  ACK: pavel
  - b88a90783043 ice: fix incorrect payload indicator on PTYPE
  ACK: pavel
  - 2e66c36f1308 bpf: Fix up register-based shifts in interpreter to silence KUBSAN
  ACK: pavel -- just a warning workaround
  - 0e72b151e394 drm/amdkfd: Fix circular lock in nocpsch path
  ACK: pavel
  - cd29db48bb65 drm/amdkfd: fix circular locking on get_wave_state
  ACK: pavel
  - 9d21abc8fd20 cw1200: add missing MODULE_DEVICE_TABLE
  ACK: pavel
  - c5e4a10d7bd5 wl1251: Fix possible buffer overflow in wl1251_cmd_scan
  UR: pavel -- is ssid_len always initialized?
  - 5a3d373c4a33 wlcore/wl12xx: Fix wl12xx get_mac error if device is in ELP
  ACK: pavel -- should do msleep; 200msec is not reasonable slack
  - ad7083a95d8a dm writecache: commit just one block, not a full page
  ACK: pavel
  - 57f7ed25bd16 xfrm: Fix error reporting in xfrm_state_construct.
  UR: pavel -- c/e
  - a5f8862967c4 drm/amd/display: Verify Gamma & Degamma LUT sizes in amdgpu_dm_atomic_check
  UR: pavel -- c/e, just a robustness
  - db3c3643d55e r8169: avoid link-up interrupt issue on RTL8106e if user enables ASPM
  ACK: pavel -- fixes delay at expense of power
  - f38371821c25 selinux: use __GFP_NOWARN with GFP_NOWAIT in the AVC
  ACK: pavel -- just a WARN workaround
  - 0a244be95bca fjes: check return value after calling platform_get_resource()
  UR: pavel -- c/e
  - 378c156f9dd0 drm/amdkfd: use allowed domain for vmbo validation
  UR: pavel --! what is going on with introducing _unused parameter?
  - fb3b4bcdd3bc net: sgi: ioc3-eth: check return value after calling platform_get_resource()
  ACK: pavel -- just a robustness
  - e613f67f1b51 selftests: Clean forgotten resources as part of cleanup()
  ACK: pavel
  - 8a4318c14ace net: phy: realtek: add delay to fix RXC generation issue
  ACK: pavel
  - c71de31b2e0f drm/amd/display: Fix off-by-one error in DML
  ACK: pavel -- that coding style/line lengths are really crazy
  - afa06442d23d drm/amd/display: Set DISPCLK_MAX_ERRDET_CYCLES to 7
  ACK: pavel
  - 02f444321b3a drm/amd/display: Release MST resources on switch from MST to SST
  ACK: pavel
  - 01d6a6931965 drm/amd/display: Update scaling settings on modeset
  ACK: pavel
  - 57c63b47d6f1 drm/amd/display: Fix DCN 3.01 DSCCLK validation
  ACK: pavel
  - 8e4da401425b net: moxa: Use devm_platform_get_and_ioremap_resource()
  ACK: pavel
  - 278dc34b7112 net: micrel: check return value after calling platform_get_resource()
  UR: pavel -- c/e, likely wrong?
  - ce1307ec621b net: mvpp2: check return value after calling platform_get_resource()
  ACK: pavel
  - 49b3a7f38a9b net: bcmgenet: check return value after calling platform_get_resource()
  UR: pavel -- c/e
  - 92820a12823e net: mscc: ocelot: check return value after calling platform_get_resource()
  UR: pavel -- c/e
  - f3b96f4b6b2d virtio_net: Remove BUG() to avoid machine dead
  UR: pavel -- c/e
  - 87c39048ec7f ice: fix clang warning regarding deadcode.DeadStores
  ACK: pavel -- just a warning workaround
  - e352556acef9 ice: set the value of global config lock timeout longer
  ACK: pavel
  - b5f2982e0609 pinctrl: mcp23s08: fix race condition in irq handler
  ACK: pavel -- buggy, fixed by following patch
  - a4a86400c68c net: bridge: mrp: Update ring transitions.
  ACK: pavel
  - cc4f0a9d5aa1 dm: Fix dm_accept_partial_bio() relative to zone management commands
  ACK: pavel -- just a robustness
  - 939f750215b8 dm writecache: don't split bios when overwriting contiguous cache content
  ACK: pavel -- just a performance tweak
  - 65e780667cf3 dm space maps: don't reset space map allocation cursor when committing
  UR: pavel --! just a robustness
  - 313d9f25804c RDMA/cxgb4: Fix missing error code in create_qp()
  ACK: pavel
  - f9c67c179e3b net: tcp better handling of reordering then loss cases
  UR: pavel
  - 8fa6473a61ec drm/amdgpu: remove unsafe optimization to drop preamble ib
  ACK: pavel
  - c5b518f4b98d drm/amd/display: Avoid HDCP over-read and corruption
  ACK: pavel
  - 3c172f6e444b MIPS: ingenic: Select CPU_SUPPORTS_CPUFREQ && MIPS_EXTERNAL_TIMER
  ACK: pavel
  - 0903ac8f09c6 MIPS: cpu-probe: Fix FPU detection on Ingenic JZ4760(B)
  ACK: pavel
  - 8f939b795797 ipv6: use prandom_u32() for ID generation
  UR: pavel --! not for stable?
  - c92298d228f6 virtio-net: Add validation for used length
  UR: pavel
  - 5e039a80a76b drm: bridge: cdns-mhdp8546: Fix PM reference leak in
  ACK: pavel
  - d1eaf4cb4408 clk: tegra: Ensure that PLLU configuration is applied properly
  ACK: pavel -- does not fix known problem
  - dc5bacea9462 clk: tegra: Fix refcounting of gate clocks
  UR: pavel -- not known to cause any real problems
  - 315988817aa7 RDMA/rtrs: Change MAX_SESS_QUEUE_DEPTH
  IGN: pavel
  - 4f6a0f31c627 net: stmmac: the XPCS obscures a potential "PHY not found" error
  UR: pavel
  - a7d608bb786c drm: rockchip: add missing registers for RK3066
  ACK: pavel -- three fixes in one
  - d89ea206e99c drm: rockchip: add missing registers for RK3188
  ACK: pavel
  - e54b4a534845 net/mlx5: Fix lag port remapping logic
  ACK: pavel
  - 62137d1ae5f8 net/mlx5e: IPsec/rep_tc: Fix rep_tc_update_skb drops IPsec packet
  ACK: pavel
  - 219150485d73 clk: renesas: r8a77995: Add ZA2 clock
  UR: pavel --a just adds unused clock?
  - 0680344d7131 drm/bridge: cdns: Fix PM reference leak in cdns_dsi_transfer()
  ACK: pavel
  - 95f8ce9f18cb igb: fix assignment on big endian machines
  ACK: pavel
  - 66d593aa3aea igb: handle vlan types with checker enabled
  ACK: pavel -- just a sparse warning fixes
  - ffb865715a0f e100: handle eeprom as little endian
  ACK: pavel
  - f06ea024c176 drm/vc4: hdmi: Fix PM reference leak in vc4_hdmi_encoder_pre_crtc_co()
  ACK: pavel
  - 48c96d5bacc0 drm/vc4: Fix clock source for VEC PixelValve on BCM2711
  ACK: pavel
  - 21bf1414580c udf: Fix NULL pointer dereference in udf_symlink function
  UR: pavel -- c/e
  - 0687411e2a88 drm/sched: Avoid data corruptions
  ACK: pavel
  - 5ed8c298b2e1 drm/scheduler: Fix hang when sched_entity released
  ACK: pavel
  - 73ac001f060b pinctrl: equilibrium: Add missing MODULE_DEVICE_TABLE
  ACK: pavel
  - 1b832bd77799 net/sched: cls_api: increase max_reclassify_loop
  ACK: pavel
  - 6ceb0182b087 net: mdio: provide shim implementation of devm_of_mdiobus_register
  ACK: pavel
  - d2d17ca924f4 drm/virtio: Fix double free on probe failure
  ACK: pavel
  - 69a71b59b18c reiserfs: add check for invalid 1st journal block
  UR: pavel -- c/e
  - c5073100dc4f drm/bridge: lt9611: Add missing MODULE_DEVICE_TABLE
  ACK: pavel
  - b5713dac1916 net: mdio: ipq8064: add regmap config to disable REGCACHE
  ACK: pavel -- not sure what bug it fixes
  - c0dd36bcb67f drm/mediatek: Fix PM reference leak in mtk_crtc_ddp_hw_init()
  ACK: pavel
  - 3393405257ed net: Treat __napi_schedule_irqoff() as __napi_schedule() on PREEMPT_RT
  NAK: pavel -- PREEMPT_RT is not working in older kernels
  - a7f7c42e3115 atm: nicstar: Fix possible use-after-free in nicstar_cleanup()
  ACK: pavel
  - b7ee9ae1e0cf mISDN: fix possible use-after-free in HFC_cleanup()
  ACK: pavel
  - e759ff76ebbb atm: iphase: fix possible use-after-free in ia_module_exit()
  ACK: pavel
  - 2292d9691ce9 hugetlb: clear huge pte during flush function on mips platform
  ACK: pavel
  - a74872106e78 clk: renesas: rcar-usb2-clock-sel: Fix error handling in .probe()
  UR: pavel
  - 3ca86d44b902 drm/amd/display: fix use_max_lb flag for 420 pixel formats
  ACK: pavel
  - 5953b984c3e4 net: pch_gbe: Use proper accessors to BE data in pch_ptp_match()
  ACK: pavel
  - fb960728f8f1 drm/bridge: nwl-dsi: Force a full modeset when crtc_state->active is changed to be true
  UR: pavel -- preparation for what?
  - 796554d3d68f drm/vc4: fix argument ordering in vc4_crtc_get_margins()
  ACK: pavel -- just a cleanup
  - b025bc07c947 drm/amd/amdgpu/sriov disable all ip hw status by default
  UR: pavel -- patch and changelog don't match?
  - fb7479d64d77 drm/amd/display: fix HDCP reset sequence on reinitialize
  ACK: pavel
  - d055669e669a drm/ast: Fixed CVE for DP501
  UR: pavel
  - 95c3133bc8eb drm/zte: Don't select DRM_KMS_FB_HELPER
  ACK: pavel -- just a config tweak
  - b60ae0fab550 drm/mxsfb: Don't select DRM_KMS_FB_HELPER
  ACK: pavel -- just a config tweak
