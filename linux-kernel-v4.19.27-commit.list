# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)


## v4.19.27
  - adc2a008ae56 Linux 4.19.27
  - 7371994d6cfa x86/uaccess: Don't leak the AC flag into __put_user() value evaluation
  - 9f77e4cb12d4 MIPS: eBPF: Fix icache flush end address
  - 4a418a3d94f2 MIPS: BCM63XX: provide DMA masks for ethernet devices
  - 3bfa6413b03a MIPS: fix truncation in __cmpxchg_small for short values
  - 527cabfffbc5 hugetlbfs: fix races and page leaks during migration
  - f0233ca89ce2 drm: Block fb changes for async plane updates
  - de04d2973a62 mm: enforce min addr even if capable() in expand_downwards()
  - ff86bb4dc656 mmc: sdhci-esdhc-imx: correct the fix of ERR004536
  - d612d7b4ff4d mmc: cqhci: Fix a tiny potential memory leak on error condition
  - e446ae40dc33 mmc: cqhci: fix space allocated for transfer descriptor
  - 17bf96122472 mmc: core: Fix NULL ptr crash from mmc_should_fail_request
  - 85d9ad404676 mmc: tmio: fix access width of Block Count Register
  - 5b716bc54e47 mmc: tmio_mmc_core: don't claim spurious interrupts
  ACK: pavel -- already reviewed in 4.4.178, but slightly different patch
  - c69e07a84d8f mmc: spi: Fix card detection during probe
  - b246986a1fc0 kvm: selftests: Fix region overlap check in kvm_util
  - 60a4b3f7c867 KVM: nSVM: clear events pending from svm_complete_interrupts() when exiting to L1
  - 0149b03ec269 svm: Fix AVIC incomplete IPI emulation
  - 99b1dbe6ba17 cfg80211: extend range deviation for DMG
  - 7a27cb609f03 mac80211: Add attribute aligned(2) to struct 'action'
  - 0a7c92826f79 mac80211: don't initiate TDLS connection if station is not associated to AP
  - e91cbe1de3b3 ibmveth: Do not process frames after calling napi_reschedule
  - 61fe1005f334 net: dev_is_mac_header_xmit() true for ARPHRD_RAWIP
  - f7901f15d6f3 net: usb: asix: ax88772_bind return error when hw_reset fail
  - 156a43cc8986 drm/msm: Fix A6XX support for opp-level
  - 9f260d76b474 nvme-multipath: drop optimization for static ANA group IDs
  - 550e0ea7e7e4 nvme-rdma: fix timeout handler
  - bbbb9874a9fe hv_netvsc: Fix hash key value reset after other ops
  - d2ce8e1bd338 hv_netvsc: Refactor assignments of struct netvsc_device_info
  - 51b547f2a445 hv_netvsc: Fix ethtool change hash key error
  - ad74456b500c net: altera_tse: fix connect_local_phy error path
  - 9de388112c63 scsi: csiostor: fix NULL pointer dereference in csio_vport_set_state()
  - ee2a02a66821 scsi: lpfc: nvmet: avoid hang / use-after-free when destroying targetport
  - 30b62656690d scsi: lpfc: nvme: avoid hang / use-after-free when destroying localport
  - edca54b897bb writeback: synchronize sync(2) against cgroup writeback membership switches
  - c5a1dc256cc2 direct-io: allow direct writes to empty inodes
  - bcb8e0a23698 staging: android: ion: Support cpu access during dma_buf_detach
  - f73577353256 drm/sun4i: hdmi: Fix usage of TMDS clock
  - 89d9a53346b2 serial: fsl_lpuart: fix maximum acceptable baud rate with over-sampling
  - 1ed436cd1690 tty: serial: qcom_geni_serial: Allow mctrl when flow control is disabled
  - 8c5571b9df10 drm/amd/powerplay: OD setting fix on Vega10
  - 9ad6216e8c3c locking/rwsem: Fix (possible) missed wakeup
  - 2368e6d3bcf4 futex: Fix (possible) missed wakeup
  - 653a1dbcb011 sched/wake_q: Fix wakeup ordering for wake_q
  - 5024f0a29a8f sched/wait: Fix rcuwait_wake_up() ordering
  - a2887f6fab98 mac80211: fix miscounting of ttl-dropped frames
  - bbc300c8c75e staging: rtl8723bs: Fix build error with Clang when inlining is disabled
  - a99e0377cca4 drivers: thermal: int340x_thermal: Fix sysfs race condition
  - 4749ffdfbb09 ARC: show_regs: lockdep: avoid page allocator...
  - 4e34dd37943d ARC: fix __ffs return value to avoid build warnings
  - 0655618dd92d irqchip/gic-v3-mbi: Fix uninitialized mbi_lock
  - f352e84e6e3c selftests: gpio-mockup-chardev: Check asprintf() for error
  - 357d9c7a01c6 selftests: seccomp: use LDLIBS instead of LDFLAGS
  - eecde0a099af phy: ath79-usb: Fix the main reset name to match the DT binding
  - e55af638c4f4 phy: ath79-usb: Fix the power on error path
  - fc8176da28b5 selftests/vm/gup_benchmark.c: match gup struct to kernel
  - 7bba7aff51ea ASoC: imx-audmux: change snprintf to scnprintf for possible overflow
  - 9500ecb9ad01 ASoC: dapm: change snprintf to scnprintf for possible overflow
  - 375a9673264c ASoC: rt5682: Fix PLL source register definitions
  - 7ff778648947 x86/mm/mem_encrypt: Fix erroneous sizeof()
  - 17fab8914f86 genirq: Make sure the initial affinity is not empty
  - 7746dd64c2c9 selftests: rtc: rtctest: add alarm test on minute boundary
  - 2409a869dad3 selftests: rtc: rtctest: fix alarm tests
  - 4670e8391710 usb: gadget: Potential NULL dereference on allocation error
  - 08c937f9a3db usb: dwc3: gadget: Fix the uninitialized link_state when udc starts
  - 03a5d4d55335 usb: dwc3: gadget: synchronize_irq dwc irq in suspend
  - f29024c0e9a7 thermal: int340x_thermal: Fix a NULL vs IS_ERR() check
  - fc1073dfc4e0 clk: vc5: Abort clock configuration without upstream clock
  - 71943c38620d clk: sysfs: fix invalid JSON in clk_dump
  - acc934f57691 clk: tegra: dfll: Fix a potential Oop in remove()
  - 651023ed93ab ASoC: Variable "val" in function rt274_i2c_probe() could be uninitialized
  - e7b2f9f2bce2 ALSA: compress: prevent potential divide by zero bugs
  - a4964959ee83 ASoC: Intel: Haswell/Broadwell: fix setting for .dynamic field
  - 5a7005337c2a drm/msm: Unblock writer if reader closes file
  - 0f978ec3ed3e scsi: libsas: Fix rphy phy_identifier for PHYs with end devices attached
  - a7c6cf3bdf33 mac80211: Change default tx_sk_pacing_shift to 7
  - 765c30b31849 genirq/matrix: Improve target CPU selection for managed interrupts.
  - 8cae7757e862 irq/matrix: Spread managed interrupts on allocation
  - 2948b8875df4 irq/matrix: Split out the CPU selection code into a helper
