# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v4.4.259
  - 93af63b25443 Linux 4.4.259
  - 684f2dacfd3c dm era: Update in-core bitset after committing the metadata
  - e68489bc827d futex: Fix OWNER_DEAD fixup
  - 66c48b3a9a6a dm era: only resize metadata in preresume
  - 8c6a513f3241 dm era: Reinitialize bitset cache before digesting a new writeset
  - 1f1558879117 dm era: Use correct value size in equality function of writeset tree
  - 1527b4472cbd dm era: Fix bitset memory leaks
  - 06a7ca6fa81b dm era: Verify the data block size hasn't changed
  - de3de7196c5b dm era: Recover committed writeset after crash
  - 2656919fb89c gfs2: Don't skip dlm unlock if glock has an lvb
  - cfa0ac72b4bd sparc32: fix a user-triggerable oops in clear_user()
  - 35e0957ee2f9 f2fs: fix out-of-repair __setattr_copy()
  - 20c8e0e48542 gpio: pcf857x: Fix missing first interrupt
  - 250704fae6c5 module: Ignore _GLOBAL_OFFSET_TABLE_ when warning for undefined symbols
  - 7a77d0ce6ccd libnvdimm/dimm: Avoid race between probe and available_slots_show()
  - cac3b5627ac1 usb: renesas_usbhs: Clear pipe running flag in usbhs_pkt_pop()
  - ea279e9f078e mm: hugetlb: fix a race between freeing and dissolving the page
  - f502ef682ee1 floppy: reintroduce O_NDELAY fix
  - a6d4fc8c4ff0 x86/reboot: Force all cpus to exit VMX root if VMX is supported
  - 4a7bf4f5a55e staging: rtl8188eu: Add Edimax EW-7811UN V2 to device table
  - 6aefe0969806 drivers/misc/vmw_vmci: restrict too big queue size in qp_host_alloc_queue
  - 523d8c7a9954 btrfs: fix reloc root leak with 0 ref reloc roots on recovery
  - 2657d0b6ed9c KEYS: trusted: Fix migratable=1 failing
  - b3252dbe2e10 usb: dwc3: gadget: Fix dep->interval for fullspeed interrupt
  - b2fe72a1bca8 usb: dwc3: gadget: Fix setting of DEPCFG.bInterval_m1
  - 92964aa4cc67 USB: serial: mos7720: fix error code in mos7720_write()
  - 9a80a0d4cf6a USB: serial: mos7840: fix error code in mos7840_write()
  - aa7273ffb25f USB: serial: option: update interface mapping for ZTE P685M
  - e480ccf433be Input: i8042 - add ASUS Zenbook Flip to noselftest list
  - ade5180681d7 Input: joydev - prevent potential read overflow in ioctl
  - 8a313c4dd4e7 Input: xpad - add support for PowerA Enhanced Wired Controller for Xbox Series X|S
  - b807c76a5838 blk-settings: align max_sectors on "logical_block_size" boundary
  - d094b3d83040 block: Move SECTOR_SIZE and SECTOR_SHIFT definitions into <linux/blkdev.h>
  - 856f42d9f702 scsi: bnx2fc: Fix Kconfig warning & CNIC build errors
  - a6ec0c75b00c i2c: brcmstb: Fix brcmstd_send_i2c_cmd condition
  - 6fd892537ab4 mm/hugetlb: fix potential double free in hugetlb_register_node() error path
  - 78d7a8be2ae5 mm/memory.c: fix potential pte_unmap_unlock pte error
  - 2035e8e36c48 PCI: Align checking of syscall user config accessors
  - ec35ff32f368 VMCI: Use set_page_dirty_lock() when unregistering guest memory
  - a4886be552bf misc: eeprom_93xx46: Add module alias to avoid breaking support for non device tree users
  - e0ea43f4aa15 misc: eeprom_93xx46: Fix module alias to enable module autoprobe
  - fc28c122a90f sparc64: only select COMPAT_BINFMT_ELF if BINFMT_ELF is set
  - fb176d32f8af Input: elo - fix an error code in elo_connect()
  - 25e2012955b2 perf test: Fix unaligned access in sample parsing test
  - bb3641953083 perf intel-pt: Fix missing CYC processing in PSB
  - 9e1641e3fcda powerpc/pseries/dlpar: handle ibm, configure-connector delay status
  - f73b389cb9ae mfd: wm831x-auxadc: Prevent use after free in wm831x_auxadc_read_irq()
  - 7a77bf015ede tracepoint: Do not fail unregistering a probe due to memory failure
  - 3d80c3619052 amba: Fix resource leak for drivers without .remove
  - 0767520a2faf ARM: 9046/1: decompressor: Do not clear SCTLR.nTLSMD for ARMv7+ cores
  - e5f80e8244fa mmc: usdhi6rol0: Fix a resource leak in the error handling path of the probe
  - 07bb4c93f39f powerpc/47x: Disable 256k page size
  - 22f4b139cb02 IB/umad: Return EIO in case of when device disassociated
  - d07c5076074a isofs: release buffer head before return
  - 0213f338da80 regulator: axp20x: Fix reference cout leak
  - da3f0e7b3898 clocksource/drivers/mxs_timer: Add missing semicolon when DEBUG is defined
  - a4ecf0cd0654 dmaengine: fsldma: Fix a resource leak in an error handling path of the probe function
  - 1e280c8d6106 dmaengine: fsldma: Fix a resource leak in the remove function
  - 8ef1894d9118 HID: core: detect and skip invalid inputs to snto32()
  - d41f9b55b44a clk: meson: clk-pll: fix initializing the old rate (fallback) for a PLL
  - 340ac6924e92 jffs2: fix use after free in jffs2_sum_write_data()
  - 81b5611d5188 fs/jfs: fix potential integer overflow on shift of a int
  - 506ea1d5a72d btrfs: clarify error returns values in __load_free_space_cache
  - b995464010fd media: uvcvideo: Accept invalid bFormatIndex and bFrameIndex values
  - b9df8a3fca61 media: cx25821: Fix a bug when reallocating some dma memory
  - e601efd031d8 media: lmedm04: Fix misuse of comma
  - 0443fcfd72ee ASoC: cs42l56: fix up error handling in probe
  - 19d542790b35 media: tm6000: Fix memleak in tm6000_start_stream
  - 667b1de3bcc8 media: media/pci: Fix memleak in empress_init
  - 70738fc740c6 MIPS: lantiq: Explicitly compare LTQ_EBU_PCC_ISTAT against 0
  - f70cd6d47cac MIPS: c-r4k: Fix section mismatch for loongson2_sc_init
  - 8b32bed6fd8c gma500: clean up error handling in init
  - 2cd24552d10b drm/gma500: Fix error return code in psb_driver_load()
  - 695c5332855f fbdev: aty: SPARC64 requires FB_ATY_CT
  - c298a92e2990 b43: N-PHY: Fix the update of coef for the PHY revision >= 3case
  - 4e72502187c3 xen/netback: fix spurious event detection for common event case
  - 87cbfb03c9f8 bnxt_en: reverse order of TX disable and carrier off
  - 10a0c6746644 ARM: s3c: fix fiq for clang IAS
  - bd99c503d7c3 usb: dwc2: Make "trimming xfer length" a debug message
  - 48dd0f513e12 usb: dwc2: Abort transaction after errors with unknown reason
  - 9c241b9a671f Bluetooth: Put HCI device if inquiry procedure interrupts
  - 9729dcb127c8 Bluetooth: drop HCI device reference before return
  - 45dd079a78b6 ARM: dts: exynos: correct PMIC interrupt trigger level on Arndale Octa
  - 04148d2d8abe ARM: dts: exynos: correct PMIC interrupt trigger level on Spring
  - 8bfe9113f38a Bluetooth: Fix initializing response id after clearing struct
  - 7a88fadc980c MIPS: vmlinux.lds.S: add missing PAGE_ALIGNED_DATA() section
  - f1c9225ad3a0 kdb: Make memory allocations more robust
  - 8a5c9045f582 scripts/recordmcount.pl: support big endian for ARCH sh
  - ae8e2742b9b6 igb: Remove incorrect "unexpected SYS WRAP" log message
  - 2e7ca50aef89 ntfs: check for valid standard information attribute
  - 32906b272499 xen-netback: delete NAPI instance when queue fails to initialize
  - b03e8f1e26fe usb: quirks: add quirk to start video capture on ELMO L-12F document camera reliable
  - 795a8c31cabf HID: make arrays usage and value to be the same
