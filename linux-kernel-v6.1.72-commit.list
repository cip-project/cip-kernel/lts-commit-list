# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v6.1.72
  - 7c58bfa711cb5 Linux 6.1.72
  - 2dbe25ae06e65 Revert "interconnect: qcom: sm8250: Enable sync_state"
  - f73a374c1969e smb3: Replace smb2pdu 1-element arrays with flex-arrays
  - ec162546a7338 media: qcom: camss: Comment CSID dt_id field
  - a5c3f2b4cee7a bpf: syzkaller found null ptr deref in unix_bpf proto add
  ACK: pavel
  - 15db682980fc0 bpf: Fix a verifier bug due to incorrect branch offset comparison with cpu=v4
  ACK: pavel
  - 7cbdf36eabf3d net/sched: act_ct: Always fill offloading tuple iifidx
  ACK: pavel
  - 2be4e8ac2d167 net/sched: act_ct: additional checks for outdated flows
  - 87318b7e374cb f2fs: compress: fix to assign compress_level for lz4 correctly
  ACK: pavel
  - 397f719037c2f genirq/affinity: Only build SMP-only helper functions on SMP kernels
  - 28c9222e29e5b mmc: sdhci-sprd: Fix eMMC init failure after hw reset
  ACK: pavel
  - 2813a434d461f mmc: core: Cancel delayed work before releasing host
  ACK: pavel
  - 575e127041f21 mmc: rpmb: fixes pause retune on all RPMB partitions.
  ACK: pavel -- includes stray whitespace change
  - 9c5efaa09b31d mmc: meson-mx-sdhc: Fix initialization frozen issue
  ACK: pavel
  - 48e1d426f452c drm/amd/display: add nv12 bounding box
  - 11c3510d1d4d5 drm/amdgpu: skip gpu_info fw loading on navi12
  - dafdeb7b91f15 mm: fix unmap_mapping_range high bits shift bug
  ACK: pavel
  - 08038069c2379 i2c: core: Fix atomic xfer check for non-preempt config
  ACK: pavel -- bandaid on top of bandaid
  - 53b42cb33fb1b x86/kprobes: fix incorrect return address calculation in kprobe_emulate_call_indirect
  ACK: pavel
  - d1db1ef5e6330 firewire: ohci: suppress unexpected system reboot in AMD Ryzen machines and ASM108x/VT630x PCIe cards
  UR: pavel -- wrong comments, static test for cpu type?
  - 09a44d994bfe9 ring-buffer: Fix 32-bit rb_time_read() race with rb_time_cmpxchg()
  ACK: pavel
  - 820a7802f25ac btrfs: mark the len field in struct btrfs_ordered_sum as unsigned
  ACK: pavel -- just a preparation
  - ab220f4f5c704 btrfs: fix qgroup_free_reserved_data int overflow
  ACK: pavel
  - 0f74dde5be2c3 octeontx2-af: Support variable number of lmacs
  - 7d3912613d5b0 octeontx2-af: Fix pause frame configuration
  - a29b15cc68a66 net/sched: act_ct: Take per-cb reference to tcf_ct_flow_table
  ACK: pavel
  - 2bb4ecb3349c1 netfilter: flowtable: GC pushes back packets to classic path
  ACK: pavel
  - df01de08b4118 net/sched: act_ct: Fix promotion of offloaded unreplied tuple
  UR: pavel -- c/e
  - 87466a374571f net/sched: act_ct: offload UDP NEW connections
  - 8b160f2fba777 netfilter: flowtable: cache info of last offload
  ACK: pavel -- just a preparation
  - c29a7656f8a2a netfilter: flowtable: allow unidirectional rules
  ACK: pavel -- just a preparation
  - e681f711e9e8e net: sched: call tcf_ct_params_free to free params in tcf_ct_init
  - d49bf9c1ceb3b mm/memory_hotplug: fix error handling in add_memory_resource()
  ACK: pavel
  - 4666f003afffb mm/memory_hotplug: add missing mem_hotplug_lock
  UR: pavel -- c/e
  - a576780a2a66b lib/group_cpus.c: avoid acquiring cpu hotplug lock in group_cpus_evenly
  ACK: pavel
  - f33b27f5c3de5 genirq/affinity: Move group_cpus_evenly() into lib/
  UR: pavel -- big move, as a preparation
  - 617ba3735d3b4 genirq/affinity: Rename irq_build_affinity_masks as group_cpus_evenly
  ACK: pavel -- just a preparation, long
  - aeeb4e4e49f81 genirq/affinity: Don't pass irq_affinity_desc array to irq_build_affinity_masks
  ACK: pavel -- just a preparation
  - 9e84d7bb15053 genirq/affinity: Pass affinity managed mask array to irq_build_affinity_masks
  ACK: pavel -- just a preparation
  - a1dcd1794730b genirq/affinity: Remove the 'firstvec' parameter from irq_build_affinity_masks
  ACK: pavel -- just a preparation
  - f4fe76467e7bd ALSA: hda/realtek: Add quirk for Lenovo Yoga Pro 7
  - aee609302d65a firmware: arm_scmi: Fix frequency truncation by promoting multiplier type
  ACK: pavel -- just a support for > 4GHz machines
  - 90d1f74c3cf68 bpf, sockmap: af_unix stream sockets need to hold ref for pair sock
  ACK: pavel -- buggy, fixed below
  - 5ff1682fec185 ethtool: don't propagate EOPNOTSUPP from dumps
  - e570b15087532 dpaa2-eth: recycle the RX buffer only after all processing done
  - 5b8938fc7d00a net: dpaa2-eth: rearrange variable in dpaa2_eth_get_ethtool_stats
  - e88275ce7e7ba smb: client: fix missing mode bits for SMB symlinks
  - bf223fd4d914f block: update the stable_writes flag in bdev_add
  ACK: pavel
  - a8e4300ae58da filemap: add a per-mapping stable writes flag
  ACK: pavel -- just a preparation
  - d0eafc7631355 mm, netfs, fscache: stop read optimisation when folio removed from pagecache
  ACK: pavel -- just a preparation
  - bceff380f361c mm: merge folio_has_private()/filemap_release_folio() call pairs
  ACK: pavel -- just a preparation
  - 8b6b3ecf0c139 memory-failure: convert truncate_error_page() to use folio
  ACK: pavel -- just a preparation
  - a6f440f3b9569 khugepage: replace try_to_release_page() with filemap_release_folio()
  ACK: pavel -- just a preparation
  - 4c78612e5fbc6 ext4: convert move_extent_per_page() to use folios
  ACK: pavel -- just a preparation
  - b92a8f591ca8b media: qcom: camss: Fix set CSI2_RX_CFG1_VC_MODE when VC is greater than 3
  - 710f70555d5b6 media: camss: sm8250: Virtual channels for CSID
  - c96a4f936008f selftests: mptcp: set FAILING_LINKS in run_tests
  - 4b85e920afc80 selftests: mptcp: fix fastclose with csum failure
  - 336d1ee07efb8 f2fs: set the default compress_level on ioctl
  ACK: pavel
  - 1ff3f5ef284b3 f2fs: assign default compression level
  ACK: pavel -- buggy, fixed below
  - 55d3f41e55839 f2fs: convert to use bitmap API
  ACK: pavel -- just a preparation, long
  - 84a8d913fb532 f2fs: clean up i_compress_flag and i_compress_level usage
  UR: pavel --a just a preparation
  - 2c14f4991610f s390/cpumf: support user space events for counting
  IGN: pavel
  - a1a1e5ce88a7a s390/mm: add missing arch_set_page_dat() call to vmem_crst_alloc()
  IGN: pavel
  - 31051f722db23 net/mlx5: Increase size of irq name buffer
  ACK: pavel
  - b5c8e0ff76d10 blk-mq: make sure active queue usage is held for bio_integrity_prep()
  ACK: pavel
  - 803fb6109fcfa bpf: fix precision backtracking instruction iteration
  ACK: pavel
  - b08acd5c46023 bpf: handle ldimm64 properly in check_cfg()
  ACK: pavel
  - 2c795ce09042c bpf: Support new 32bit offset jmp instruction
  ACK: pavel -- just a preparation
  - b1c780ed3c220 bpf: clean up visit_insn()'s instruction processing
  ACK: pavel -- just a preparation
  - 97bb6dab01728 bpf: Remove unused insn_cnt argument from visit_[func_call_]insn()
  ACK: pavel -- just a preparation
  - 8266c47d04b2c bpf: remove unnecessary prune and jump points
  ACK: pavel -- just a preparation
  - 743f3548d3018 bpf: decouple prune and jump points
  ACK: pavel -- just a preparation
  - eb4f2e17886ad fbdev: imsttfb: fix double free in probe()
  ACK: pavel
  - f2a79f3651a54 fbdev: imsttfb: Release framebuffer and dealloc cmap on error path
  ACK: pavel
  - 51a1b943022fe arm64: dts: qcom: sdm845: Fix PSCI power domain names
  ACK: pavel
  - 5db8b93cbe2d2 arm64: dts: qcom: sdm845: align RPMh regulator nodes with bindings
  ACK: pavel -- just a dts cleanup
  - 343bb27e31528 wifi: iwlwifi: yoyo: swap cdb and jacket bits values
  ACK: pavel
  - 158b71f3a9fa4 udp: annotate data-races around udp->encap_type
  ACK: pavel -- just a READ_ONCE annotation
  - 8d929b6c11141 udp: lockless UDP_ENCAP_L2TPINUDP / UDP_GRO
  ACK: pavel -- just a preparation
  - b680a907d17ca udp: move udp->accept_udp_{l4|fraglist} to udp->udp_flags
  ACK: pavel -- probably does not cause problems in practice
  - 753886c0b994f udp: move udp->gro_enabled to udp->udp_flags
  ACK: pavel -- just a sysbot warning fix
  - a01cff15ccdc3 udp: move udp->no_check6_rx to udp->udp_flags
  ACK: pavel -- just a sysbot warning fix
  - 50e41aa9ea0d5 udp: move udp->no_check6_tx to udp->udp_flags
  UR: pavel -- mostly just a cleanup
  - e2a4392b61f6d udp: introduce udp->udp_flags
  ACK: pavel -- just a preparation.. for sysbot warning fix
  - 2489502fb1f5e ipv4, ipv6: Use splice_eof() to flush
  ACK: pavel
  - 4713b7c7568ba splice, net: Add a splice_eof op to file-ops and socket-ops
  ACK: pavel -- just a preparation
  - ac8c69e448f7e udp: Convert udp_sendpage() to use MSG_SPLICE_PAGES
  ACK: pavel -- just a cleanup/preparation
  - 6bcc79a4e7607 net: Declare MSG_SPLICE_PAGES internal sendmsg() flag
  UR: pavel -- just a preparation
  - 89b51e70e5e33 bpf, x86: save/restore regs with BPF_DW size
  ACK: pavel -- just a preparation
  - 4ee461c5dc994 bpf, x86: Simplify the parsing logic of structure parameters
  UR: pavel
  - 605c8d8f9966f bpf, x64: Fix tailcall infinite loop
  ACK: pavel
  - 5573fdbc34234 srcu: Fix callbacks acceleration mishandling
  ACK: pavel
  - abc3e3fb71a55 cpu/SMT: Make SMT control more robust against enumeration failures
  ACK: pavel
  - 482fa21635c88 cpu/SMT: Create topology_smt_thread_allowed()
  ACK: pavel -- just a feature/preparation
  - a364c18553d0f selftests: secretmem: floor the memory size to the multiple of page_size
  ACK: pavel -- just a test fix
  - c38c5cfd3ed7c net: Implement missing SO_TIMESTAMPING_NEW cmsg support
  ACK: pavel
  - 14937f47a48f4 bnxt_en: Remove mis-applied code from bnxt_cfg_ntp_filters()
  ACK: pavel
  - 55fbcd83aacac net: ravb: Wait for operating mode to be applied
  UR: pavel -- c/e
  - 8a09b0f01c404 asix: Add check for usbnet_get_endpoints
  ACK: pavel
  - db9c4a1f37ee9 octeontx2-af: Re-enable MAC TX in otx2_stop processing
  ACK: pavel
  - b67e7d78e48a4 octeontx2-af: Always configure NIX TX link credits based on max frame size
  ACK: pavel -- just drops broken feature
  - 84c3833a93bb5 net/smc: fix invalid link access in dumping SMC-R connections
  ACK: pavel
  - 0af75845ff5e6 net/qla3xxx: fix potential memleak in ql_alloc_buffer_queues
  ACK: pavel
  - 9b05042922373 igc: Fix hicredit calculation
  ACK: pavel
  - 7663226274af1 i40e: Restore VF MSI-X state during PCI reset
  ACK: pavel
  - 5735f529e318d ASoC: meson: g12a-tohdmitx: Fix event generation for S/PDIF mux
  ACK: pavel
  - 8719838c126ac ASoC: meson: g12a-toacodec: Fix event generation
  ACK: pavel
  - 5de3c8496e770 ASoC: meson: g12a-tohdmitx: Validate written enum values
  ACK: pavel -- mostly an API fix
  - 95b4d4093ac0c ASoC: meson: g12a-toacodec: Validate written enum values
  ACK: pavel -- mostly an API fix
  - 2f3b6e8600c9a i40e: fix use-after-free in i40e_aqc_add_filters()
  ACK: pavel
  - 72fa66177859d net: Save and restore msg_namelen in sock_sendmsg
  ACK: pavel -- probably jsut a robustness
  - 81f8a995ebc8f netfilter: nft_immediate: drop chain reference counter on error
  UR: pavel -- c/e
  - bb1bf97fa1877 net: bcmgenet: Fix FCS generation for fragmented skbuffs
  ACK: pavel
  - e75715e1c2e5a sfc: fix a double-free bug in efx_probe_filters
  ACK: pavel
  - 725d44e49fb5e ARM: sun9i: smp: Fix array-index-out-of-bounds read in sunxi_mc_smp_init
  ACK: pavel -- probably does not fix user-visible bug
  - 85f6fae44bba4 selftests: bonding: do not set port down when adding to bond
  ACK: pavel
  - 3edd66bd4e422 net: Implement missing getsockopt(SO_TIMESTAMPING_NEW)
  ACK: pavel -- just an interface fix
  - ac5fde92b5103 net: annotate data-races around sk->sk_bind_phc
  ACK: pavel -- just a READ_ONCE annotation
  - c48fcb4f49061 net: annotate data-races around sk->sk_tsflags
  ACK: pavel -- probably just a theoretical bug
  - 5d586f7ca0fc8 net-timestamp: extend SOF_TIMESTAMPING_OPT_ID to HW timestamps
  ACK: pavel -- just a new feature
  - b2130366a952b can: raw: add support for SO_MARK
  ACK: pavel -- just a new feature
  - 633a49e34b32b r8169: Fix PCI error on system resume
  ACK: pavel
  - 565460e180d9d net: sched: em_text: fix possible memory leak in em_text_destroy()
  ACK: pavel
  - ac5cbe931c435 mlxbf_gige: fix receive packet race condition
  ACK: pavel
  - 6d7f45492706b ASoC: mediatek: mt8186: fix AUD_PAD_TOP register and offset
  ACK: pavel
  - 811604fb02c44 ASoC: fsl_rpmsg: Fix error handler with pm_runtime_enable
  ACK: pavel
  - c3a37dc156858 igc: Check VLAN EtherType mask
  ACK: pavel -- just a interface fix
  - 6edff0b8381c9 igc: Check VLAN TCI mask
  ACK: pavel -- just a interface fix
  - d27b98f4aeaeb igc: Report VLAN EtherType matching back to user
  ACK: pavel -- just a interface fix
  - e76d1913f6a8a i40e: Fix filter input checks to prevent config with invalid values
  ACK: pavel
  - 188c9970d05e7 ice: Shut down VSI with "link-down-on-close" enabled
  ACK: pavel
  - 83b80170b7fa2 ice: Fix link_down_on_close message
  ACK: pavel -- just a printk tweak / preparation
  - f3f6a23e054c7 drm/i915/dp: Fix passing the correct DPCD_REV for drm_dp_set_phy_test_pattern
  ACK: pavel
  - 6cf7235bc1fb6 octeontx2-af: Fix marking couple of structure as __packed
  ACK: pavel -- not sure if it fixes user-visible bug
  - a4b0a9b80a963 nfc: llcp_core: Hold a ref to llcp_local->dev when holding a ref to llcp_local
  UR: pavel -- c/e
  - 282e3fb612852 netfilter: nf_tables: set transport offset from mac header for netdev/egress
  ACK: pavel
  - 9487cc4c90fbb netfilter: use skb_ip_totlen and iph_totlen
  ACK: pavel
  - 5f523f1beb465 drm/bridge: ti-sn65dsi86: Never store more than msg->size bytes in AUX xfer
  ACK: pavel
  - 3da4868907dad wifi: iwlwifi: pcie: don't synchronize IRQs from IRQ
  ACK: pavel
  - 493d556278a32 KVM: x86/pmu: fix masking logic for MSR_CORE_PERF_GLOBAL_CTRL
  ACK: pavel
  - 3152a7d361c67 cifs: do not depend on release_iface for maintaining iface_list
  ACK: pavel
  - 5982a625fc0d5 cifs: cifs_chan_is_iface_active should be called with chan_lock held
  ACK: pavel
  - 4afcb82518b9b drm/mgag200: Fix gamma lut not initialized for G200ER, G200EV, G200SE
  ACK: pavel
  - b9c370b61d735 Revert "PCI/ASPM: Remove pcie_aspm_pm_state_change()"
  ACK: pavel
  - af9a5307656d1 mptcp: prevent tcp diag from closing listener subflows
  ACK: pavel
  - 105063f7f4419 ALSA: hda/realtek: Fix mute and mic-mute LEDs for HP ProBook 440 G6
  ACK: pavel
  - 0fa3cf2d151e6 ALSA: hda/realtek: fix mute/micmute LEDs for a HP ZBook
  ACK: pavel
  - beda900d3aaf0 ALSA: hda/realtek: enable SND_PCI_QUIRK for hp pavilion 14-ec1xxx series
  ACK: pavel
  - 9539e3b56e0d8 block: Don't invalidate pagecache for invalid falloc modes
  UR: pavel -- Fixes: ?? just an interface tweak
  - 079eefaecfd7b keys, dns: Fix missing size check of V1 server-list header
  ACK: pavel
