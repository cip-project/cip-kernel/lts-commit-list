# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v5.10.164
  - 3a9f1b907bc4 Linux 5.10.164
  - 74985c575767 Revert "usb: ulpi: defer ulpi_register on ulpi_read_id timeout"
  - a88a0d16e18f io_uring/io-wq: only free worker if it was allocated for creation
  ACK: uli -- fixup for "io_uring/io-wq: free worker if task_work creation is canceled"
  - b912ed1363b3 io_uring/io-wq: free worker if task_work creation is canceled
  ACK: uli -- overridden by fixup "io_uring/io-wq: only free worker if it was allocated for creation"
  - 68bcd0638570 drm/virtio: Fix GEM handle creation UAF
  - 4ca71bc0e199 efi: fix NULL-deref in init error path
  - 057f5ddfbc13 arm64: cmpxchg_double*: hazard against entire exchange variable
  - 9a5fd0844e7f arm64: atomics: remove LL/SC trampolines
  - 28840e46eaaf arm64: atomics: format whitespace consistently
  - 5dac4c7212a7 x86/resctrl: Fix task CLOSID/RMID update race
  ACK: uli
  - 446c7251f007 x86/resctrl: Use task_curr() instead of task_struct->on_cpu to prevent unnecessary IPI
  ACK: uli
  - 196c6f0c3e21 KVM: x86: Do not return host topology information from KVM_GET_SUPPORTED_CPUID
  ACK: uli
  - 0027164b24f2 Documentation: KVM: add API issues section
  ACK: uli -- docs only
  - caaea2ab6b6d iommu/mediatek-v1: Fix an error handling path in mtk_iommu_v1_probe()
  ACK: uli
  - cf38e7624179 iommu/mediatek-v1: Add error handle for mtk_iommu_probe
  ACK: uli
  - 60806adc9be0 mm: Always release pages to the buddy allocator in memblock_free_late().
  - 092f0c2d1f83 net/mlx5e: Don't support encap rules with gbp option
  ACK: uli
  - b3d47227f06f net/mlx5: Fix ptp max frequency adjustment range
  ACK: uli
  - 453277feb41c net/sched: act_mpls: Fix warning during failed attribute validation
  ACK: uli
  - 0ca78c99656f nfc: pn533: Wait for out_urb's completion in pn533_usb_send_frame()
  ACK: uli
  - 92b30a27e4fa hvc/xen: lock console list traversal
  - 14e72a56e16c octeontx2-af: Fix LMAC config in cgx_lmac_rx_tx_enable
  ACK: uli
  - 8e2bfcfaabc3 octeontx2-af: Map NIX block from CGX connection
  UR: uli -- loosens a seemingly unrelated firmware version check without any explanation
  - d9be5b57ab2c octeontx2-af: Update get/set resource count functions
  ACK: uli
  - 0d0675bc33e6 tipc: fix unexpected link reset due to discovery messages
  ACK: uli
  - d83cac6c00b8 ASoC: wm8904: fix wrong outputs volume after power reactivation
  ACK: uli
  - d4aa749e0464 regulator: da9211: Use irq handler when ready
  ACK: uli
  - 3ca8ef4d91ef EDAC/device: Fix period calculation in edac_device_reset_delay_period()
  - 28b9a0e216db x86/boot: Avoid using Intel mnemonics in AT&T syntax asm
  - 8cbeb60320ac powerpc/imc-pmu: Fix use of mutex in IRQs disabled section
  ACK: uli
  - 4e6a70fd8404 netfilter: ipset: Fix overflow before widen in the bitmap_ip_create() function.
  - a3a1114aa615 xfrm: fix rcu lock in xfrm_notify_userpolicy()
  - 091f85db4c3f ext4: fix uninititialized value in 'ext4_evict_inode'
  - 98407a4ae34b usb: ulpi: defer ulpi_register on ulpi_read_id timeout
  - 3d13818a9995 xhci: Prevent infinite loop in transaction errors recovery for streams
  - 2f90fcedc5d6 xhci: move and rename xhci_cleanup_halted_endpoint()
  ACK: uli -- not a bug
  - cad965cedbc4 xhci: store TD status in the td struct instead of passing it along
  ACK: uli -- not a bug
  - 9b63a80c45e9 xhci: move xhci_td_cleanup so it can be called by more functions
  ACK: uli -- not a bug
  - 44c635c60f78 xhci: Add xhci_reset_halted_ep() helper function
  ACK: uli -- not a bug
  - 10287d18f524 xhci: adjust parameters passed to cleanup_halted_endpoint()
  ACK: uli -- not a bug
  - aaaa7cc4aba1 xhci: get isochronous ring directly from endpoint structure
  - a81ace065694 xhci: Avoid parsing transfer events several times
  - ba20d6056b6b clk: imx: imx8mp: add shared clk gate for usb suspend clk
  ACK: uli
  - 2b331d2137cc dt-bindings: clocks: imx8mp: Add ID for usb suspend clock
  ACK: uli
  - cb769960ef48 clk: imx8mp: add clkout1/2 support
  ACK: uli
  - 85eaaa17c0a9 clk: imx8mp: Add DISP2 pixel clock
  ACK: uli
  - 6b21077146c5 iommu/amd: Fix ill-formed ivrs_ioapic, ivrs_hpet and ivrs_acpihid options
  ACK: uli
  - 5badda810f69 iommu/amd: Add PCI segment support for ivrs_[ioapic/hpet/acpihid] commands
  ACK: uli
  - ab9bb65b8519 bus: mhi: host: Fix race between channel preparation and M0 event
  ACK: uli
  - 6c9e2c11c33c ipv6: raw: Deduct extension header length in rawv6_push_pending_frames
  - 112df4cd2b09 ixgbe: fix pci device refcount leak
  - f401062d8dbd platform/x86: sony-laptop: Don't turn off 0x153 keyboard backlight during probe
  ACK: uli
  - 785607e5e6fb drm/msm/dp: do not complete dp_aux_cmd_fifo_tx() if irq is not for aux transfer
  ACK: uli
  - 8c71777b6a17 drm/msm/adreno: Make adreno quirks not overwrite each other
  ACK: uli
  - afb6063aa89f cifs: Fix uninitialized memory read for smb311 posix symlink create
  - 51dbedee2ff3 s390/percpu: add READ_ONCE() to arch_this_cpu_to_op_simple()
  IGN: uli
  - bddb35526784 s390/cpum_sf: add READ_ONCE() semantics to compare and swap loops
  IGN: uli
  - 2adc64f3e669 ASoC: qcom: lpass-cpu: Fix fallback SD line index handling
  ACK: uli
  - 5ee3083307ef s390/kexec: fix ipl report address for kdump
  IGN: uli
  - d1725dbf2310 perf auxtrace: Fix address filter duplicate symbol selection
  ACK: uli -- userspace
  - eaabceae1b70 docs: Fix the docs build with Sphinx 6.0
  ACK: uli -- docs only
  - 38c4a17c6b32 efi: tpm: Avoid READ_ONCE() for accessing the event log
  - c47883105cff KVM: arm64: Fix S1PTW handling on RO memslots
  - 443b390f2cc9 ALSA: hda/realtek: Enable mute/micmute LEDs on HP Spectre x360 13-aw0xxx
  ACK: uli
  - 550efeff989b netfilter: nft_payload: incorrect arithmetics when fetching VLAN header bits
