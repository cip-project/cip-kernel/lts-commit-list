# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v5.10.15
  - 2d18b3ee633e Linux 5.10.15
  - 0414bde77968 net: sched: replaced invalid qdisc tree flush helper in qdisc_replace
  ACK: pavel
  - 836f791aba58 net: dsa: mv88e6xxx: override existent unicast portvec in port_fdb_add
  ACK: pavel
  - 3d6df63a5cbe udp: ipv4: manipulate network header of NATed UDP GRO fraglist
  ACK: pavel
  - f2b30f9f0889 net: ip_tunnel: fix mtu calculation
  ACK: pavel -- just enables bigger mtu
  - 6e4583ad6df0 neighbour: Prevent a dead entry from updating gc_list
  ACK: pavel
  - 0a8a25d56a63 igc: Report speed and duplex as unknown when device is runtime suspended
  ACK: pavel
  - fe272570d003 md: Set prev_flush_start and flush_bio in an atomic way
  ACK: pavel
  - 3a492e4403ee Input: ili210x - implement pressure reporting for ILI251x
  ACK: pavel -- new feature, not a bugfix
  - 1841be8d0bc6 Input: xpad - sync supported devices with fork on GitHub
  ACK: pavel
  - b442912f678a Input: goodix - add support for Goodix GT9286 chip
  ACK: pavel
  - 2ce5be67d134 x86/apic: Add extra serialization for non-serializing MSRs
  ACK: pavel -- not known to cause problems in practice
  - 3dcf233b5845 x86/debug: Prevent data breakpoints on cpu_dr7
  ACK: pavel
  - b796770c6db3 x86/debug: Prevent data breakpoints on __per_cpu_offset
  ACK: pavel
  - c000dcfb3aed x86/debug: Fix DR6 handling
  ACK: pavel
  - 2a2dfe6a319a x86/build: Disable CET instrumentation in the kernel
  UR: pavel -- sounds like weird idea; what if other archs need the option?
  - 032f8e04c035 mm/filemap: add missing mem_cgroup_uncharge() to __add_to_page_cache_locked()
  ACK: pavel
  - 0a249ac189fc mm: thp: fix MADV_REMOVE deadlock on shmem THP
  ACK: pavel
  - 9abdd2c05b59 mm/vmalloc: separate put pages and flush VM flags
  ACK: pavel -- probably just a performance fix
  - 76303d3fab9f mm, compaction: move high_pfn to the for loop scope
  ACK: pavel
  - eca84ebef17f mm: hugetlb: remove VM_BUG_ON_PAGE from page_huge_active
  ACK: pavel
  - 5b9631cb6f34 mm: hugetlb: fix a race between isolating and freeing page
  ACK: pavel
  - e334b1fec6f4 mm: hugetlb: fix a race between freeing and dissolving the page
  ACK: pavel
  - afe6c31b84f6 mm: hugetlbfs: fix cannot migrate the fallocated HugeTLB page
  ACK: pavel
  - 2de0745463e3 ARM: 9043/1: tegra: Fix misplaced tegra_uart_config in decompressor
  ACK: pavel
  - 384cddbee46f ARM: footbridge: fix dc21285 PCI configuration accessors
  ACK: pavel
  - cc7b2fc90916 ARM: dts; gta04: SPI panel chip select is active low
  ACK: pavel
  - 160237c192c4 DTS: ARM: gta04: remove legacy spi-cs-high to make display work again
  ACK: pavel
  - 7159239d2de1 KVM: x86: Set so called 'reserved CR3 bits in LM mask' at vCPU reset
  ACK: pavel
  - d73af5ae22d4 KVM: x86: Update emulator context mode if SYSENTER xfers to 64-bit mode
  ACK: pavel
  - 46add0349ba3 KVM: x86: fix CPUID entries returned by KVM_GET_CPUID2 ioctl
  ACK: pavel
  - 6c0e069ac6e8 KVM: x86: Allow guests to see MSR_IA32_TSX_CTRL even if tsx=off
  ACK: pavel
  - dd7f10523b19 KVM: x86/mmu: Fix TDP MMU zap collapsible SPTEs
  - ff0c437a0e02 KVM: SVM: Treat SVM as unsupported when running as an SEV guest
  - 720639ef01f5 nvme-pci: avoid the deepest sleep state on Kingston A2000 SSDs
  ACK: pavel
  - 4f25d448d947 io_uring: don't modify identity's files uncess identity is cowed
  ACK: pavel
  - 2fd938741a79 drm/amd/display: Revert "Fix EDID parsing after resume from suspend"
  UR: pavel -- reintroduces the old bug?
  - 09c6d51b16ef drm/i915: Power up combo PHY lanes for for HDMI as well
  ACK: pavel -- just to match specs
  - 24946da51ce7 drm/i915: Extract intel_ddi_power_up_lanes()
  ACK: pavel -- just a cleanup, preparation for next patch
  - 1f27c7362e2b drm/i915/display: Prevent double YUV range correction on HDR planes
  UR: pavel
  - 2545b18b9834 drm/i915/gt: Close race between enable_breadcrumbs and cancel_breadcrumbs
  ACK: pavel
  - 1cd8e3ef7f68 drm/i915/gem: Drop lru bumping on display unpinning
  pavel -- not a bugfix, complex
  - 0fe98e455784 drm/i915: Fix the MST PBN divider calculation
  ACK: pavel
  - 8ef4cf6abaa7 drm/dp/mst: Export drm_dp_get_vc_payload_bw()
  NAK: pavel -- the patch is not neccesary in 5.10, as followup is not applied
  - 4f627ecde732 Fix unsynchronized access to sev members through svm_register_enc_region
  ACK: pavel
  - a03a8693b1a2 mmc: core: Limit retries when analyse of SDIO tuples fails
  ACK: pavel
  - 57b452c5ab1e mmc: sdhci-pltfm: Fix linking err for sdhci-brcmstb
  UR: pavel --! we should have macros for this; check, do we need working suspend/resume callbacks outside of sleep?
  - 2502610927ee smb3: fix crediting for compounding when only one request in flight
  ACK: pavel
  - b793e9fca633 smb3: Fix out-of-bounds bug in SMB2_negotiate()
  ACK: pavel
  - e2bb221a16ac iommu: Check dev->iommu in dev_iommu_priv_get() before dereferencing it
  ACK: pavel
  - 7a3361e5ecf1 cifs: report error instead of invalid when revalidating a dentry fails
  ACK: pavel
  - c026844c6156 RISC-V: Define MAXPHYSMEM_1GB only for RV32
  ACK: pavel
  - 57ea7b257a1a xhci: fix bounce buffer usage for non-sg list case
  ACK: pavel
  - ee23b9329ec2 scripts: use pkg-config to locate libcrypto
  ACK: pavel -- just a build improvement
  - 0fe48a40ac63 genirq/msi: Activate Multi-MSI early when MSI_FLAG_ACTIVATE_EARLY is set
  ACK: pavel
  - d2415fde8cad genirq: Prevent [devm_]irq_alloc_desc from returning irq 0
  ACK: pavel -- just a warning workaround
  - a80e9eee5003 libnvdimm/dimm: Avoid race between probe and available_slots_show()
  UR: pavel -- the code is a little crazy converting dev->ndd->dev
  - a2560f88e1c3 libnvdimm/namespace: Fix visibility of namespace resource attribute
  ACK: pavel
  - 059e68da31b0 tracepoint: Fix race between tracing and removing tracepoint
  ACK: pavel
  - 9e4a668f4f0a tracing: Use pause-on-trace with the latency tracers
  - 8ce84b8e8eb3 kretprobe: Avoid re-registration of the same kretprobe earlier
  ACK: pavel
  - fb03f14cc148 tracing/kprobe: Fix to support kretprobe events on unloaded modules
  ACK: pavel
  - 43b5bdbf9644 fgraph: Initialize tracing_graph_pause at task creation
  ACK: pavel
  - 8847a756e1df gpiolib: free device name on error path to fix kmemleak
  ACK: pavel
  - 2ca1ddc32b88 mac80211: fix station rate table updates on assoc
  ACK: pavel
  - 8ccf963c6227 ovl: implement volatile-specific fsync error behaviour
  ACK: pavel -- long, adds new error reporting mode, not a bugfix
  - a66f82a1de02 ovl: avoid deadlock on directory ioctl
  ACK: pavel
  - fb8caef7c020 ovl: fix dentry leak in ovl_get_redirect
  ACK: pavel
  - 0e5cb872fbbb thunderbolt: Fix possible NULL pointer dereference in tb_acpi_add_link()
  ACK: pavel
  - 19155473f3ba kbuild: fix duplicated flags in DEBUG_CFLAGS
  ACK: pavel -- not sure if causes real problems
  - 1897a8f0ef20 memblock: do not start bottom-up allocations with kernel_end
  ACK: pavel -- big hammer for a powerpc bug, not a minimal patch
  - 346ea7cc27b7 vdpa/mlx5: Restore the hardware used index after change map
  - c1debbaf158d nvmet-tcp: fix out-of-bounds access when receiving multiple h2cdata PDUs
  - b9464c5f4663 ARM: dts: sun7i: a20: bananapro: Fix ethernet phy-mode
  ACK: pavel
  - 38b83bcec904 net: ipa: pass correct dma_handle to dma_free_coherent()
  ACK: pavel
  - 714c19bc1315 r8169: fix WoL on shutdown if CONFIG_DEBUG_SHIRQ is set
  ACK: pavel
  - 397ae1a24502 net: mvpp2: TCAM entry enable should be written after SRAM data
  ACK: pavel
  - dec629e97261 net: lapb: Copy the skb before sending a packet
  ACK: pavel
  - 6a5c3bac8054 net/mlx5e: Release skb in case of failure in tc update skb
  - c2b2c4d24b40 net/mlx5e: Update max_opened_tc also when channels are closed
  - 11c2c8fb889d net/mlx5: Fix leak upon failure of rule creation
  - ada342012b2d net/mlx5: Fix function calculation for page trees
  - b5802b747596 ibmvnic: device remove has higher precedence over reset
  - cd77dccc122f i40e: Revert "i40e: don't report link up for a VF who hasn't enabled queues"
  UR: pavel --! suspect it might re-introduce old problem
  - 1ac8bec2205e igc: check return value of ret_val in igc_config_fc_after_link_up
  ACK: pavel
  - 0cda16041858 igc: set the default return value to -IGC_ERR_NVM in igc_write_nvm_srwr
  ACK: pavel
  - 8e081627f3a7 SUNRPC: Fix NFS READs that start at non-page-aligned offsets
  ACK: pavel
  - ceca8baed5d8 arm64: dts: ls1046a: fix dcfg address range
  ACK: pavel
  - e5ed4e08d850 rxrpc: Fix deadlock around release of dst cached on udp tunnel
  ACK: pavel
  - 7fc1a5a50e6e r8169: work around RTL8125 UDP hw bug
  - ee1709a311cd arm64: dts: meson: switch TFLASH_VDD_EN pin to open drain on Odroid-C4
  ACK: pavel
  - 6f5ee57a68c7 bpf, preload: Fix build when $(O) points to a relative path
  - 72c8389fc7ff um: virtio: free vu_dev only with the contained struct device
  - 571fe1ba22c2 bpf, inode_storage: Put file handler if no storage was found
  - 9447d0f8a621 bpf, cgroup: Fix problematic bounds check
  - ee3844e61706 bpf, cgroup: Fix optlen WARN_ON_ONCE toctou
  - 28ad17a5e936 vdpa/mlx5: Fix memory key MTT population
  ACK: pavel -- coding style leaves something to be desired
  - 636ef657eedf ARM: dts: stm32: Fix GPIO hog flags on DHCOM DRC02
  ACK: pavel -- not a bugfix
  - 6ec543da64e1 ARM: dts: stm32: Disable optional TSC2004 on DRC02 board
  ACK: pavel -- just a warning workaround
  - 43019f6f8884 ARM: dts: stm32: Disable WP on DHCOM uSD slot
  ACK: pavel
  - f7a74822c6eb ARM: dts: stm32: Connect card-detect signal on DHCOM
  ACK: pavel
  - 29aebc79169c ARM: dts: stm32: Fix polarity of the DH DRC02 uSD card detect
  ACK: pavel
  - 25af99f88d3e arm64: dts: rockchip: Use only supported PCIe link speed on Pinebook Pro
  ACK: pavel
  - c2947904fbba arm64: dts: rockchip: fix vopl iommu irq on px30
  ACK: pavel
  - 9b1996ae3a27 arm64: dts: amlogic: meson-g12: Set FL-adj property value
  ACK: pavel
  - 4fcaf04963e2 Input: i8042 - unbreak Pegatron C15B
  ACK: pavel
  - bd508a509c2a arm64: dts: qcom: c630: keep both touchpad devices enabled
  ACK: pavel -- quite interesting hack
  - 4bcb395a7f67 ARM: OMAP1: OSK: fix ohci-omap breakage
  ACK: pavel
  - f808da6bc6e4 usb: xhci-mtk: break loop when find the endpoint to drop
  ACK: pavel -- just a tiny optimalization
  - 85f0409e9ce3 usb: xhci-mtk: skip dropping bandwidth of unchecked endpoints
  ACK: pavel -- not a minimum fix
  - 5139bf6a3455 usb: xhci-mtk: fix unreleased bandwidth data
  UR: pavel -- long
  - b6609c0a537b usb: dwc3: fix clock issue during resume in OTG mode
  ACK: pavel
  - 750829e1931a usb: dwc2: Fix endpoint direction check in ep_from_windex
  UR: pavel --! I don't see described checks in index_to_ep()?
  - 039656997da3 usb: renesas_usbhs: Clear pipe running flag in usbhs_pkt_pop()
  ACK: pavel
  - 75582ceb723e USB: usblp: don't call usb_set_interface if there's a single alt
  ACK: pavel -- comment in the code would be good
  - 4025244544b8 usb: gadget: aspeed: add missing of_node_put
  - c8e1dabc1e05 USB: gadget: legacy: fix an error code in eth_bind()
  ACK: pavel
  - d56e0ac9a1fc usb: host: xhci: mvebu: make USB 3.0 PHY optional for Armada 3720
  ACK: pavel
  - 73b1de6b5ea3 USB: serial: option: Adding support for Cinterion MV31
  ACK: pavel
  - c43cb08791a2 USB: serial: cp210x: add new VID/PID for supporting Teraoka AD2000
  ACK: pavel
  - 17fb12b4a756 USB: serial: cp210x: add pid/vid for WSDA-200-USB
  ACK: pavel
