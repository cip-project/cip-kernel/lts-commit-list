# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v4.19.205
  - e23d55af0e1f Linux 4.19.205
  - 991158d68077 netfilter: nft_exthdr: fix endianness of tcp option cast
  - 6ce8ad137e3a fs: warn about impending deprecation of mandatory locks
  ACK: pavel -- just adds a warning to force deprecation
  - 3cab951cf059 locks: print a warning when mount fails due to lack of "mand" support
  - c764cf4c8f93 ASoC: intel: atom: Fix breakage for PCM buffer address setup
  ACK: pavel
  - aad377827b65 PCI: Increase D3 delay for AMD Renoir/Cezanne XHCI
  ACK: pavel
  - 9732f81ed648 btrfs: prevent rename2 from exchanging a subvol with a directory from different parents
  ACK: pavel
  - f4f2872d6641 ipack: tpci200: fix memory leak in the tpci200_register
  ACK: pavel
  - 19c09f4cc967 ipack: tpci200: fix many double free issues in tpci200_pci_probe
  ACK: pavel
  - 299400448c78 slimbus: ngd: reset dma setup during runtime pm
  ACK: pavel
  - ac8d2c61359c slimbus: messaging: check for valid transaction id
  ACK: pavel
  - ea99a7fae21b slimbus: messaging: start transaction ids from 1 instead of zero
  ACK: pavel
  - 2d349b0a69ed tracing / histogram: Fix NULL pointer dereference on strcmp() on NULL event name
  ACK: pavel
  - 6675b20518ad ALSA: hda - fix the 'Capture Switch' value change notifications
  ACK: pavel
  - e0eb0f65e681 mmc: dw_mmc: Fix hang on data CRC error
  ACK: pavel -- code cleanup would be possible
  - fed44f82ffa8 net: mdio-mux: Handle -EPROBE_DEFER correctly
  ACK: pavel -- quite subtle
  - 2f87a13eb37f net: mdio-mux: Don't ignore memory allocation errors
  ACK: pavel
  - 260ad8a2daea net: qlcnic: add missed unlock in qlcnic_83xx_flash_read32
  ACK: pavel
  - 1458ae977ae0 ptp_pch: Restore dependency on PCI
  ACK: pavel
  - 4e370cc081a7 net: 6pack: fix slab-out-of-bounds in decode_data
  ACK: pavel
  - b80bc6fba1cb bnxt: disable napi before canceling DIM
  ACK: pavel
  - fa0a75c4f0a5 bnxt: don't lock the tx queue from napi poll
  ACK: pavel
  - 7cfaec657d41 vhost: Fix the calculation in vhost_overflow()
  ACK: pavel
  - 53764b451221 dccp: add do-while-0 stubs for dccp_pr_debug macros
  ACK: pavel -- just a warning fix
  - 16a4777a05bc cpufreq: armada-37xx: forbid cpufreq for 1.2 GHz variant
  ACK: pavel
  - 5e2d55bcebe0 Bluetooth: hidp: use correct wait queue when removing ctrl_wait
  ACK: pavel
  - edf5cef7be1a net: usb: lan78xx: don't modify phy_device state concurrently
  ACK: pavel
  - 2e9659ee1e33 ARM: dts: nomadik: Fix up interrupt controller node names
  ACK: pavel -- just a dts warning fix
  - 460add310494 scsi: core: Avoid printing an error if target_alloc() returns -ENXIO
  ACK: pavel -- just a printk tweak
  - e25e7495d726 scsi: scsi_dh_rdac: Avoid crash during rdac_bus_attach()
  ACK: pavel
  - 119f2748df9d scsi: megaraid_mm: Fix end of loop tests for list_for_each_entry()
  ACK: pavel -- not a minimum fix
  - c863d9535d0b dmaengine: of-dma: router_xlate to return -EPROBE_DEFER if controller is not yet available
  ACK: pavel -- route_allocate has strange calling convention
  - 32e6ea21d636 ARM: dts: am43x-epos-evm: Reduce i2c0 bus speed for tps65218
  ACK: pavel
  - 2a29364ca274 dmaengine: usb-dmac: Fix PM reference leak in usb_dmac_probe()
  ACK: pavel
  - 8b1868d2cc4b dmaengine: xilinx_dma: Fix read-after-free bug when terminating transfers
  pavel -- is locking needed in xilinx_dma_terminate_all?
  - 08c613a2cb06 ath9k: Postpone key cache entry deletion for TXQ frames reference it
  ACK: pavel
  - 7c5a966edd3c ath: Modify ath_key_delete() to not need full key entry
  ACK: pavel -- just a preparation
  - fb924bfcecc9 ath: Export ath_hw_keysetmac()
  ACK: pavel -- just a preparation
  - d2fd9d34210f ath9k: Clear key cache explicitly on disabling hardware
  ACK: pavel
  - dd5815f023b8 ath: Use safer key clearing with key cache entries
  ACK: pavel
  - e829367f4721 x86/fpu: Make init_fpstate correct with optimized XSAVE
  ACK: pavel
  - 42f4312c0e8a KVM: nSVM: avoid picking up unsupported bits from L2 in int_ctl (CVE-2021-3653)
  pavel -- version of this patch for older release reviewed
  - 119d547cbf7c KVM: nSVM: always intercept VMLOAD/VMSAVE when nested (CVE-2021-3656)
  ACK: pavel
  - 11cad2a46103 mac80211: drop data frames without key on encrypted links
  ACK: pavel
  - 6a9449e95688 iommu/vt-d: Fix agaw for a supported 48 bit guest address width
  ACK: pavel
  - c47f8a185747 vmlinux.lds.h: Handle clang's module.{c,d}tor sections
  pavel -- version of this patch for older release reviewed
  - 153cc7c9dfef PCI/MSI: Enforce MSI[X] entry updates to be visible
  - b590b85fc919 PCI/MSI: Enforce that MSI-X table entry is masked for update
  - 3b570884c868 PCI/MSI: Mask all unused MSI-X entries
  - 3c9534778d4c PCI/MSI: Protect msi_desc::masked for multi-MSI
  ACK: pavel
  - 1b36c30a9335 PCI/MSI: Use msi_mask_irq() in pci_msi_shutdown()
  ACK: pavel -- just a preparation
  - c5b223cd0470 PCI/MSI: Correct misleading comments
  ACK: pavel -- just a cleanup
  - 22f4a36d086d PCI/MSI: Do not set invalid bits in MSI mask
  ACK: pavel -- probably does not fix an user-visible bug
  - 6aea847496c8 PCI/MSI: Enable and mask MSI-X early
  ACK: pavel
  - 504a4c105715 genirq/msi: Ensure deactivation on teardown
  ACK: pavel
  - cc656023d169 x86/resctrl: Fix default monitoring groups reporting
  UR: pavel -- we now go past with m uninitialized?
  - 697658a61db4 x86/ioapic: Force affinity setup before startup
  ACK: pavel -- not sure it fixes user-visible bug
  - 354b210062b1 x86/msi: Force affinity setup before startup
  ACK: pavel
  - cab824f67d7e genirq: Provide IRQCHIP_AFFINITY_PRE_STARTUP
  ACK: pavel -- just a preparation
  - 0b926fdfca71 x86/tools: Fix objdump version check again
  ACK: pavel -- just a robustness against strange version strings
  - 04283ebd7622 powerpc/kprobes: Fix kprobe Oops happens in booke
  ACK: pavel
  - a6013d42d256 vsock/virtio: avoid potential deadlock when vsock device remove
  ACK: pavel
  - 387635925cd0 xen/events: Fix race in set_evtchn_to_irq
  ACK: pavel
  - ec75ebd1645e net: igmp: increase size of mr_ifc_count
  ACK: pavel
  - 32b6627fec71 tcp_bbr: fix u32 wrap bug in round logic if bbr_init() called after 2B packets
  ACK: pavel -- really quite theoretical
  - f41237f60cb0 net: bridge: fix memleak in br_add_if()
  ACK: pavel
  - 782e2706b091 net: dsa: lan9303: fix broken backpressure in .port_fdb_dump
  ACK: pavel
  - fb5db3106036 net: igmp: fix data-race in igmp_ifc_timer_expire()
  UR: pavel -- crazy; should use atomic_t? Buggy, fixed below
  - 7da72e2db1b3 net: Fix memory leak in ieee802154_raw_deliver
  ACK: pavel
  - 5518a26ef281 psample: Add a fwd declaration for skbuff
  ACK: pavel -- just a warning fix for unusual use
  - 0bc8d39791e6 ppp: Fix generating ifname when empty IFLA_IFNAME is specified
  ACK: pavel
  - f08a3b83463c net: dsa: mt7530: add the missing RxUnicast MIB counter
  ACK: pavel
  - 39edeccf57fe ASoC: cs42l42: Fix LRCLK frame start edge
  ACK: pavel
  - 6f0e1374e192 ASoC: cs42l42: Remove duplicate control for WNF filter frequency
  ACK: pavel -- just an API tweak
  - 8a203103eef3 ASoC: cs42l42: Fix inversion of ADC Notch Switch control
  ACK: pavel
  - b19d07068b25 ASoC: cs42l42: Don't allow SND_SOC_DAIFMT_LEFT_J
  ACK: pavel
  - 49f49cd9a389 ASoC: cs42l42: Correct definition of ADC Volume control
  ACK: pavel
  - 202e294bdf7d ieee802154: hwsim: fix GPF in hwsim_new_edge_nl
  ACK: pavel
  - 5442be288efc ieee802154: hwsim: fix GPF in hwsim_set_edge_lqi
  ACK: pavel
  - c39e22fd3f7c ACPI: NFIT: Fix support for virtual SPA ranges
  ACK: pavel
  - 888ae2b85c6d i2c: dev: zero out array used for i2c reads from userspace
  ACK: pavel -- two fixes in one
  - 9703440e681c ASoC: intel: atom: Fix reference to PCM buffer address
  ACK: pavel
  - 2870da9189dd iio: adc: Fix incorrect exit of for-loop
  ACK: pavel
  - 13ca1daf27fd iio: humidity: hdc100x: Add margin to the conversion time
  ACK: pavel -- changelog does not match code
