# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)


## v4.19.18
  - 34ae65724615 Linux 4.19.18
  - b40aec33eeb5 ipmi: Don't initialize anything in the core until something uses it
  - 031a94ff8ac4 ipmi:ssif: Fix handling of multi-part return messages
  - 821a003d8e81 ipmi: Prevent use-after-free in deliver_response
  - 753abe2a6339 ipmi: msghandler: Fix potential Spectre v1 vulnerabilities
  - 1c393ca118cf ipmi: fix use-after-free of user->release_barrier.rda
  - 7557895b3dda Bluetooth: Fix unnecessary error message for HCI request completion
  - d9bcbcb7d77c iwlwifi: mvm: Send LQ command as async when necessary
  - 0d73e773edc9 mm, proc: be more verbose about unstable VMA flags in /proc/<pid>/smaps
  - 2011eb741803 userfaultfd: clear flag if remap event not enabled
  - b0cd52e644ef mm/swap: use nr_node_ids for avail_lists in swap_info_struct
  - dc15e3fd3fbe mm/page-writeback.c: don't break integrity writeback on ->writepage() error
  - 5a404f39f8fa ocfs2: fix panic due to unrecovered local alloc
  - c9dcb871b1a9 iomap: don't search past page end in iomap_is_partially_uptodate
  - 00886cebcce5 scsi: megaraid: fix out-of-bound array accesses
  - d640fb10cab4 scsi: smartpqi: call pqi_free_interrupts() in pqi_shutdown()
  - dd619b90dd71 ath10k: fix peer stats null pointer dereference
  - ca8ad9bcbebd scsi: smartpqi: correct lun reset issues
  - 868152e4ca97 scsi: mpt3sas: fix memory ordering on 64bit writes
  - 6fa75685aa3a IB/usnic: Fix potential deadlock
  - a13daf038b49 sysfs: Disable lockdep for driver bind/unbind files
  - 959bf5c190ea ALSA: bebob: fix model-id of unit for Apogee Ensemble
  - c5e68453be0c Bluetooth: btusb: Add support for Intel bluetooth device 8087:0029
  - 887b1c9a7d17 dm: Check for device sector overflow if CONFIG_LBDAF is not set
  - decca9bc2116 clocksource/drivers/integrator-ap: Add missing of_node_put()
  - 876b79b973f2 quota: Lock s_umount in exclusive mode for Q_XQUOTA{ON,OFF} quotactls.
  - 77f14a495530 perf tools: Add missing open_memstream() prototype for systems lacking it
  - e2a1f8d695c2 perf tools: Add missing sigqueue() prototype for systems lacking it
  - 4bc4b575131f perf cs-etm: Correct packets swapping in cs_etm__flush()
  - 9e5be33b1061 dm snapshot: Fix excessive memory usage and workqueue stalls
  - d9513fdbeb62 tools lib subcmd: Don't add the kernel sources to the include path
  - 8603cac28a78 perf stat: Avoid segfaults caused by negated options
  - cbd257f3bbc9 dm kcopyd: Fix bug causing workqueue stalls
  - 4e26ee3149e4 dm crypt: use u64 instead of sector_t to store iv_offset
  - a4772e8b3e60 x86/topology: Use total_cpus for max logical packages calculation
  - 9d51378a6893 netfilter: ipt_CLUSTERIP: fix deadlock in netns exit routine
  - bb7b6c49cc34 netfilter: ipt_CLUSTERIP: remove wrong WARN_ON_ONCE in netns exit routine
  - 744383c88e2e netfilter: ipt_CLUSTERIP: check MAC address when duplicate config is set
  - bd1040e646d6 perf vendor events intel: Fix Load_Miss_Real_Latency on SKL/SKX
  - 58c67a0b06a7 perf parse-events: Fix unchecked usage of strncpy()
  - b332b4cd25e7 perf svghelper: Fix unchecked usage of strncpy()
  - f54fc4c23eea perf tests ARM: Disable breakpoint tests 32-bit
  - c3e8c335e788 perf intel-pt: Fix error with config term "pt=0"
  - f74fc96e32ab tty/serial: do not free trasnmit buffer page under port lock
  - 310f8296d630 btrfs: improve error handling of btrfs_add_link
  - 38b17eee7074 btrfs: fix use-after-free due to race between replace start and cancel
  - 720b86a53a10 btrfs: alloc_chunk: fix more DUP stripe size handling
  - bb5717a4a165 btrfs: volumes: Make sure there is no overlap of dev extents at mount time
  - c21991ed17e9 mmc: atmel-mci: do not assume idle after atmci_request_end
  - 461991104dcb kconfig: fix memory leak when EOF is encountered in quotation
  - ba8efcdc5779 kconfig: fix file name and line number of warn_ignored_character()
  - 344b51e7ce13 bpf: relax verifier restriction on BPF_MOV | BPF_ALU
  - dfbf8c981f83 arm64: Fix minor issues with the dcache_by_line_op macro
  - 73f0b2e36e40 clk: imx6q: reset exclusive gates on init
  - 8f183b332398 arm64: kasan: Increase stack size for KASAN_EXTRA
  - 656257cf1cbd selftests: do not macro-expand failed assertion expressions
  - 3ad8148ce042 scsi: target/core: Make sure that target_wait_for_sess_cmds() waits long enough
  - 25d3546acd60 scsi: target: use consistent left-aligned ASCII INQUIRY data
  - 50deccdceb59 net: call sk_dst_reset when set SO_DONTROUTE
  - fd4c7fe100fc staging: erofs: fix use-after-free of on-stack `z_erofs_vle_unzip_io'
  - 38be2cbae300 media: venus: core: Set dma maximum segment size
  - 9df6861a35d1 ASoC: use dma_ops of parent device for acp_audio_dma
  - 597a09e0ce2c media: firewire: Fix app_info parameter type in avc_ca{,_app}_info
  - 3049cdc28409 powerpc/pseries/cpuidle: Fix preempt warning
  - 115a0d668793 powerpc/xmon: Fix invocation inside lock region
  - 819e2e0760f3 media: uvcvideo: Refactor teardown of uvc on USB disconnect
  - 265242d82a3c pstore/ram: Do not treat empty buffers as valid
  - ed99d79a158c clk: imx: make mux parent strings const
  - c356972f27cc jffs2: Fix use of uninitialized delayed_work, lockdep breakage
  - 50063ba9dd51 efi/libstub: Disable some warnings for x86{,_64}
  - fded1b0e0c89 rxe: IB_WR_REG_MR does not capture MR's iova field
  - e34e54f925e2 drm/amdgpu: Reorder uvd ring init before uvd resume
  - 4614fe394c55 scsi: qedi: Check for session online before getting iSCSI TLV data.
  - 22e568af3989 ASoC: pcm3168a: Don't disable pcm3168a when CONFIG_PM defined
  - 34ea589d97d7 selinux: always allow mounting submounts
  - e4c04fd44157 fpga: altera-cvp: fix probing for multiple FPGAs on the bus
  - 7cae65dbc7b4 usb: gadget: udc: renesas_usb3: add a safety connection way for forced_b_device
  - 7187ac3e3c4d samples: bpf: fix: error handling regarding kprobe_events
  - 5e1be9c72310 clk: meson: meson8b: fix incorrect divider mapping in cpu_scale_table
  - 6ebffc54843b drm/atomic-helper: Complete fake_commit->flip_done potentially earlier
  - 6f88ff119205 arm64: perf: set suppress_bind_attrs flag to true
  - dbb97f7663c0 crypto: ecc - regularize scalar for scalar multiplication
  - 6e5be6e3f56a MIPS: SiByte: Enable swiotlb for SWARM, LittleSur and BigSur
  - 1d839c72fc13 x86/mce: Fix -Wmissing-prototypes warnings
  - 3945c33a608e ALSA: oxfw: add support for APOGEE duet FireWire
  - 464b01e4406e bpf: Allow narrow loads with offset > 0
  - 4c7c36a7f348 serial: set suppress_bind_attrs flag only if builtin
  - e7a5f0073533 writeback: don't decrement wb->refcnt if !wb->bdi
  - b6d75422462a of: overlay: add missing of_node_put() after add new node to changeset
  - d216d503a0b2 selftests/bpf: enable (uncomment) all tests in test_libbpf.sh
  - 579f3fc1f46f usb: typec: tcpm: Do not disconnect link for self powered devices
  - 88f3beae5836 e1000e: allow non-monotonic SYSTIM readings
  - e5090418fbb0 platform/x86: asus-wmi: Tell the EC the OS will handle the display off hotkey
  - 3dc1bc575e75 ixgbe: allow IPsec Tx offload in VEPA mode
  - bd240b1182da drm/amdkfd: fix interrupt spin lock
  - 07f4cf9547d9 drm/amd/display: Guard against null stream_state in set_crc_source
  - 414dbd6c00b9 gpio: pl061: Move irq_chip definition inside struct pl061
  - ad7013cd6d6a netfilter: ipset: Allow matching on destination MAC address for mac and ipmac sets
  - 183144815e34 net: clear skb->tstamp in bridge forwarding path
  - c1fa98a5748f ipv6: Take rcu_read_lock in __inet6_bind for mapped addresses
  - 19ad57e220a1 r8169: Add support for new Realtek Ethernet
  - 8bb38336b721 qmi_wwan: add MTU default to qmap network interface
  - 598e57e02929 net, skbuff: do not prefer skb allocation fails early
  - e10411389248 net: dsa: mv88x6xxx: mv88e6390 errata
  - 07348a7ebb58 mlxsw: spectrum_switchdev: Set PVID correctly during VLAN deletion
  - cac1bffc77e9 mlxsw: spectrum: Disable lag port TX before removing it
  - 9b25d7b69d61 ipv6: Consider sk_bound_dev_if when binding a socket to a v4 mapped address
