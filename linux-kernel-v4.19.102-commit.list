# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v4.19.102
  - b499cf4b3a90 Linux 4.19.102
  - b6606cc13491 mm/migrate.c: also overwrite error when it is bigger than zero
  ACK: pavel
  - 0649c61de462 perf report: Fix no libunwind compiled warning break s390 issue
  - 0f7a33432aa1 btrfs: do not zero f_bavail if we have available space
  ACK: pavel -- just an accounting fix
  - 7e4d3a8c4885 net: Fix skb->csum update in inet_proto_csum_replace16().
  ACK: pavel
  - 3239d4b4c222 l2t_seq_next should increase position index
  ACK: pavel
  - 6f2c1c20ddce seq_tab_next() should increase position index
  ACK: pavel
  - fd3f8ebd7d68 net: fsl/fman: rename IF_MODE_XGMII to IF_MODE_10G
  - c37d91475be0 net/fsl: treat fsl,erratum-a011043
  - 567d6f8beb2f powerpc/fsl/dts: add fsl,erratum-a011043
  ACK: pavel
  - 7c5d75c9e05c qlcnic: Fix CPU soft lockup while collecting firmware dump
  ACK: pavel
  - c32492c3d685 ARM: dts: am43x-epos-evm: set data pin directions for spi0 and spi1
  ACK: pavel
  - 97f0fcaf0da0 r8152: get default setting of WOL before initializing
  ACK: pavel
  - c82866b251d7 airo: Add missing CAP_NET_ADMIN check in AIROOLDIOCTL/SIOCDEVPRIVATE
  - eb935b34400c airo: Fix possible info leak in AIROOLDIOCTL/SIOCDEVPRIVATE
  - 3ac901fc064d tee: optee: Fix compilation issue with nommu
  ACK: pavel -- just adds Kconfig dependency 
  - 8071075d6e9f ARM: 8955/1: virt: Relax arch timer version check during early boot
  ACK: pavel -- help backports, not a bugfix
  - 17176e58b037 scsi: fnic: do not queue commands during fwreset
  ACK: pavel
  - 33bafd311856 xfrm: interface: do not confirm neighbor when do pmtu update
  - 9ea046c4df42 xfrm interface: fix packet tx through bpf_redirect()
  - 011e94777d90 vti[6]: fix packet tx through bpf_redirect()
  ACK: pavel
  - 7a2c1d387f59 ARM: dts: am335x-boneblack-common: fix memory size
  ACK: pavel
  - 2c00f819a721 iwlwifi: Don't ignore the cap field upon mcc update
  - 313abce0a149 riscv: delete temporary files
  - 23b8f9d7aa90 bnxt_en: Fix ipv6 RFS filter matching logic.
  - 7f9681a11b18 net: dsa: bcm_sf2: Configure IMP port for 2Gb/sec
  ACK: pavel -- just a performance tweak
  - 9f19727f16fa netfilter: nft_tunnel: ERSPAN_VERSION must not be null
  ACK: pavel
  - ffd89a6c2daa wireless: wext: avoid gcc -O3 warning
  ACK: pavel -- just a warning fix
  - d3f51f28574a mac80211: Fix TKIP replay protection immediately after key setup
  ACK: pavel
  - a4f85674e469 cfg80211: Fix radar event during another phy CAC
  UR: pavel
  - dc9a80e48e85 wireless: fix enabling channel 12 for custom regulatory domain
  ACK: pavel
  - a618848babc3 parisc: Use proper printk format for resource_size_t
  - 78c15b2405f5 qmi_wwan: Add support for Quectel RM500Q
  ACK: pavel
  - a7642b2c2913 ASoC: sti: fix possible sleep-in-atomic
  ACK: pavel
  - 678ad8eb4250 platform/x86: GPD pocket fan: Allow somewhat lower/higher temperature limits
  pavel -- just a configuration tweak, printk is now wong
  - adbaaac0a35b igb: Fix SGMII SFP module discovery for 100FX/LX.
  ACK: pavel
  - 4600709706f1 ixgbe: Fix calculation of queue with VFs and flow director on interface flap
  ACK: pavel
  - fd12a4ffd665 ixgbevf: Remove limit of 10 entries for unicast filter list
  ACK: pavel -- driver improvement, not a bugfix
  - 38c78f918e8d ASoC: rt5640: Fix NULL dereference on module unload
  - 68229946d8f2 clk: mmp2: Fix the order of timer mux parents
  - 6b81007aecba mac80211: mesh: restrict airtime metric to peered established plinks
  ACK: pavel
  - e37ee4b177a6 clk: sunxi-ng: h6-r: Fix AR100/R_APB2 parent order
  - 6fb761dbbabc rseq: Unregister rseq for clone CLONE_VM
  ACK: pavel -- fix is to change API
  - 6cb939e8d47d tools lib traceevent: Fix memory leakage in filter_event
  - 17d87b3e0b2e soc: ti: wkup_m3_ipc: Fix race condition with rproc_boot
  ACK: pavel
  - 8d22af64f828 ARM: dts: beagle-x15-common: Model 5V0 regulator
  pavel -- I don't think this fixes anything
  - 8c2c6cebdd0e ARM: dts: am57xx-beagle-x15/am57xx-idk: Remove "gpios" for endpoint dt nodes
  ACK: pavel
  - 645d72fb040b ARM: dts: sun8i: a83t: Correct USB3503 GPIOs polarity
  ACK: pavel -- changes value that is currently ignored
  - 0bf57f087e57 media: si470x-i2c: Move free() past last use of 'radio'
  ACK: pavel
  - 6d2663091292 cgroup: Prevent double killing of css when enabling threaded cgroup
  ACK: pavel -- just a WARN fix
  - 71729b05e727 Bluetooth: Fix race condition in hci_release_sock()
  ACK: pavel
  - fb56687038cf ttyprintk: fix a potential deadlock in interrupt context issue
  - 8f1c7fe1d57e tomoyo: Use atomic_t for statistics counter
  ACK: pavel -- just a syzbot warning fix
  - ddba92fa8338 media: dvb-usb/dvb-usb-urb.c: initialize actlen to 0
  - 3f43d55a2553 media: gspca: zero usb_buf
  ACK: pavel
  - 373403c65479 media: vp7045: do not read uninitialized values if usb transfer fails
  - bb3d4573bc77 media: af9005: uninitialized variable printked
  - 124669382012 media: digitv: don't continue if remote control state can't be read
  - 1764dc15a685 reiserfs: Fix memory leak of journal device string
  ACK: pavel
  - 732ecd4aad51 mm/mempolicy.c: fix out of bounds write in mpol_parse_str()
  ACK: pavel
  - cb1702c403ad ext4: validate the debug_want_extra_isize mount option at parse time
  ACK: pavel -- this changes 1 << (EXT4_EPOCH_BITS - 2) into 1, which should be ok, but is not mentioned in changelog
  - 1f3b1614c274 arm64: kbuild: remove compressed images on 'make ARCH=arm64 (dist)clean'
  ACK: pavel -- just a build system fix
  - 6d6c4c1bb569 tools lib: Fix builds when glibc contains strlcpy()
  ACK: pavel
  - 1635d4fc76a0 PM / devfreq: Add new name attribute for sysfs
  ACK: pavel -- new kernel interface, not really bugfix; is original commit problematic?
  - e292b266359d perf c2c: Fix return type for histogram sorting comparision functions
  - 94b4f57a9c9d rsi: fix use-after-free on failed probe and unbind
  - 28fc6259cff9 rsi: add hci detach for hibernation and poweroff
  - 47ef5cb87881 crypto: pcrypt - Fix user-after-free on module unload
  ACK: pavel
  - cc071b7c434f x86/resctrl: Fix a deadlock due to inaccurate reference
  - 95a41c7b7f14 x86/resctrl: Fix use-after-free due to inaccurate refcount of rdtgroup
  - 1b006f8cbde9 x86/resctrl: Fix use-after-free when deleting resource groups
  - 8d7a5100e29d vfs: fix do_last() regression
