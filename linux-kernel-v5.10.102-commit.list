# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v5.10.102
  - 47667effb7d2 Linux 5.10.102
  - 6062d1267ff3 lockdep: Correct lock_classes index mapping
  ACK: uli
  - f333c1916fd6 i2c: brcmstb: fix support for DSL and CM variants
  ACK: uli
  - 9fee985f9afa copy_process(): Move fd_install() out of sighand->siglock critical section
  ACK: uli
  - e3fdbc40b750 i2c: qcom-cci: don't put a device tree node before i2c_add_adapter()
  ACK: uli
  - b5b2a9211713 i2c: qcom-cci: don't delete an unregistered adapter
  ACK: uli -- questionable coding style
  - 3b6d25d1b6a2 dmaengine: sh: rcar-dmac: Check for error num after dma_set_max_seg_size
  ACK: uli
  - 2c35c95d3640 dmaengine: stm32-dmamux: Fix PM disable depth imbalance in stm32_dmamux_probe
  ACK: uli
  - 4f907b6eb701 dmaengine: sh: rcar-dmac: Check for error num after setting mask
  ACK: uli
  - 797b380f0756 net: sched: limit TC_ACT_REPEAT loops
  ACK: uli
  - 595c259f75ae EDAC: Fix calculation of returned address and next offset in edac_align_ptr()
  ACK: uli
  - f6ce4e328939 scsi: lpfc: Fix pt2pt NVMe PRLI reject LOGO loop
  ACK: uli
  - 3680b2b8104b kconfig: fix failing to generate auto.conf
  ACK: uli -- build fix
  - b6787e284d3d net: macb: Align the dma and coherent dma masks
  ACK: uli
  - 439171a2917c net: usb: qmi_wwan: Add support for Dell DW5829e
  ACK: uli -- hardware enablement
  - 15616ba17d02 tracing: Fix tp_printk option related with tp_printk_stop_on_boot
  ACK: uli
  - 5a253a23d9f1 drm/rockchip: dw_hdmi: Do not leave clock enabled in error case
  ACK: uli
  - 1e7433fb95cc xprtrdma: fix pointer derefs in error cases of rpcrdma_ep_create
  ACK: uli
  - a21f472fb5cc soc: aspeed: lpc-ctrl: Block error printing on probe defer cases
  ACK: uli
  - fecb05b1ce6b ata: libata-core: Disable TRIM on M88V29
  ACK: uli
  - b19ec7afa929 lib/iov_iter: initialize "flags" in new pipe_buffer
  - 30455322787a kconfig: let 'shell' return enough output for deep path names
  ACK: uli -- build fix
  - e05dde47f52a selftests: fixup build warnings in pidfd / clone3 tests
  ACK: uli -- warning fix, test only
  - 531a56c2e0bf pidfd: fix test failure due to stack overflow on some arches
  ACK: uli -- test only
  - 429ef36c4fc4 arm64: dts: meson-g12: drop BL32 region from SEI510/SEI610
  ACK: uli -- requires "arm64: dts: meson-g12: add ATF BL32 reserved-memory region"
  - 1415f22ee541 arm64: dts: meson-g12: add ATF BL32 reserved-memory region
  ACK: uli -- bug?
  - 605080f19eb7 arm64: dts: meson-gx: add ATF BL32 reserved-memory region
  pavel -- version of this patch for older release reviewed
  - eefb68794f94 netfilter: conntrack: don't refresh sctp entries in closed state
  ACK: pavel
  - 1ab48248573b irqchip/sifive-plic: Add missing thead,c900-plic match string
  - 98bc06c46d1f phy: usb: Leave some clocks running during suspend
  - 717f2fa85822 ARM: OMAP2+: adjust the location of put_device() call in omapdss_init_of
  ACK: pavel
  - 6932353af74c ARM: OMAP2+: hwmod: Add of_node_put() before break
  ACK: pavel
  - 521dcc107e39 NFS: Don't set NFS_INO_INVALID_XATTR if there is no xattr cache
  ACK: pavel
  - fb00319afb72 KVM: x86/pmu: Use AMD64_RAW_EVENT_MASK for PERF_TYPE_RAW
  NAK: pavel -- we don't have b8bfee in tree, so we should not have this?
  - 0ee4bb8ce8b8 KVM: x86/pmu: Don't truncate the PerfEvtSeln MSR when creating a perf event
  ACK: pavel
  - 99cd2a043760 KVM: x86/pmu: Refactoring find_arch_event() to pmc_perf_hw_id()
  UR: pavel -- why?
  - 91d8866ca552 Drivers: hv: vmbus: Fix memory leak in vmbus_add_channel_kobj
  - a176d559e826 mtd: rawnand: brcmnand: Fixed incorrect sub-page ECC status
  ACK: pavel
  - 1a49b1b0b0cb mtd: rawnand: qcom: Fix clock sequencing in qcom_nandc_probe()
  - 8c848744c11b tty: n_tty: do not look ahead for EOL character past the end of the buffer
  ACK: pavel
  - 8daa0436ce79 NFS: Do not report writeback errors in nfs_getattr()
  ACK: pavel
  - f9b7385c0f62 NFS: LOOKUP_DIRECTORY is also ok with symlinks
  ACK: pavel
  - 598dbaf74b64 block/wbt: fix negative inflight counter when remove scsi device
  ACK: pavel -- changelog is not accurate
  - dc6faa0ede4d ASoC: tas2770: Insert post reset delay
  - 9dcedbe943be KVM: SVM: Never reject emulation due to SMAP errata for !SEV guests
  ACK: pavel
  - a4eeeaca5019 mtd: rawnand: gpmi: don't leak PM reference in error path
  ACK: pavel
  - fb26219b4046 powerpc/lib/sstep: fix 'ptesync' build error
  ACK: pavel
  - 54f76366cd01 ASoC: ops: Fix stereo change notifications in snd_soc_put_volsw_range()
  ACK: pavel
  - 0df1badfdfcd ASoC: ops: Fix stereo change notifications in snd_soc_put_volsw()
  ACK: pavel
  - 1ef76832fef3 ALSA: hda: Fix missing codec probe on Shenker Dock 15
  ACK: pavel
  - c72c3b597a79 ALSA: hda: Fix regression on forced probe mask option
  ACK: pavel
  - 63b1602c2fd5 ALSA: hda/realtek: Fix deadlock by COEF mutex
  UR: pavel -- does it need "acquires" annotation?
  - b6a5e8f45f89 ALSA: hda/realtek: Add quirk for Legion Y9000X 2019
  ACK: pavel
  - 67de71b94331 selftests/exec: Add non-regular to TEST_GEN_PROGS
  ACK: pavel -- just a test fix
  - d3018a196221 perf bpf: Defer freeing string after possible strlen() on it
  ACK: pavel
  - 016e3ca9c588 dpaa2-eth: Initialize mutex used in one step timestamping path
  ACK: pavel
  - 50f3b00d4c7b libsubcmd: Fix use-after-free for realloc(..., 0)
  ACK: pavel
  - ffa8df4f0e8f bonding: fix data-races around agg_select_timer
  ACK: pavel -- should use atomic_dec_if_positive?
  - d9bd9d4c60c3 net_sched: add __rcu annotation to netdev->qdisc
  ACK: pavel
  - 877a05672f95 drop_monitor: fix data-race in dropmon_net_event / trace_napi_poll_hit
  ACK: pavel
  - a0e004e6206e bonding: force carrier update when releasing slave
  ACK: pavel
  - 8dec3c4e7350 ping: fix the dif and sdif check in ping_lookup
  ACK: pavel
  - 6793a9b028ce net: ieee802154: ca8210: Fix lifs/sifs periods
  ACK: pavel
  - f48bd3413771 net: dsa: lantiq_gswip: fix use after free in gswip_remove()
  ACK: pavel
  - d9b2203e5a30 net: dsa: lan9303: fix reset on probe
  ACK: pavel
  - 4f523f15e5d7 ipv6: per-netns exclusive flowlabel checks
  ACK: pavel -- just an interface tweak
  - 100344200a0c netfilter: nft_synproxy: unregister hooks on init error path
  ACK: pavel
  - 26931971db5f selftests: netfilter: fix exit value for nft_concat_range
  ACK: pavel
  - b26ea3f6b7b0 iwlwifi: pcie: gen2: fix locking when "HW not ready"
  UR: pavel -- c/e
  - 8867f993790d iwlwifi: pcie: fix locking when "HW not ready"
  UR: pavel -- c/e
  - f3c1910257c8 drm/i915/gvt: Make DRM_I915_GVT depend on X86
  ACK: pavel -- just a Kconfig tweak
  - 87cd1bbd6677 vsock: remove vsock from connected table when connect is interrupted by a signal
  ACK: pavel
  - eb7bf11e8ef1 drm/i915/opregion: check port number bounds for SWSCI display power state
  UR: pavel -- c/e
  - 5564d83ebc1b drm/radeon: Fix backlight control on iMac 12,1
  ACK: pavel
  - 008508c16af0 iwlwifi: fix use-after-free
  ACK: pavel
  - 44b81136e868 kbuild: lto: Merge module sections if and only if CONFIG_LTO_CLANG is enabled
  ACK: pavel
  - 8b53e5f737bc kbuild: lto: merge module sections
  ACK: pavel -- just a size optimalization; buggy, fixed by next patch
  - 45102b538a9e random: wake up /dev/random writers after zap
  ACK: pavel -- just an interface fix
  - 143aaf79bafa gcc-plugins/stackleak: Use noinstr in favor of notrace
  ACK: pavel -- just a objtool warning fix
  - de55891e162c Revert "module, async: async_synchronize_full() on module init iff async is used"
  ACK: pavel
  - 3c958dbcba18 x86/Xen: streamline (and fix) PV CPU enumeration
  ACK: pavel
  - e76d0a9692c5 drm/amdgpu: fix logic inversion in check
  ACK: pavel
  - 324f5bdc52ec nvme-rdma: fix possible use-after-free in transport error_recovery work
  ACK: pavel
  - e192184cf8bc nvme-tcp: fix possible use-after-free in transport error_recovery work
  ACK: pavel
  - 0ead57ceb21b nvme: fix a possible use-after-free in controller reset during load
  ACK: pavel
  - fe9ac3eaa2e3 scsi: pm8001: Fix use-after-free for aborted SSP/STP sas_task
  ACK: pavel
  - d872e7b5fe38 scsi: pm8001: Fix use-after-free for aborted TMF sas_task
  ACK: pavel
  - 1e73f5cfc160 quota: make dquot_quota_sync return errors from ->sync_fs
  UR: pavel -- c/e
  - c405640aad56 vfs: make freeze_super abort when sync_filesystem returns error
  UR: pavel -- c/e
  - b9a229fd48bf ax25: improve the incomplete fix to avoid UAF and NPD bugs
  ACK: pavel
  - 139fce2992ee selftests: skip mincore.check_file_mmap when fs lacks needed support
  UR: pavel -- c/e
  - 204a2390da42 selftests: openat2: Skip testcases that fail with EOPNOTSUPP
  UR: pavel -- c/e
  - 2be48bfac713 selftests: openat2: Add missing dependency in Makefile
  ACK: pavel -- just a test fix
  - 74a30666b4b5 selftests: openat2: Print also errno in failure messages
  ACK: pavel -- just a test print tweak
  - bfc84cfd909b selftests/zram: Adapt the situation that /dev/zram0 is being used
  ACK: pavel -- just a test tweak
  - f0eba714c11d selftests/zram01.sh: Fix compression ratio calculation
  ACK: pavel -- just a test fix
  - 7bb704b69fb1 selftests/zram: Skip max_comp_streams interface on newer kernel
  ACK: pavel -- simply disables a test
  - 0fd484644c68 net: ieee802154: at86rf230: Stop leaking skb's
  ACK: pavel
  - 0c18a751930c kselftest: signal all child processes
  ACK: pavel -- just a test fix
  - 1136141f19ab selftests: rtc: Increase test timeout so that all tests run
  ACK: pavel -- just a test fix
  - 79175b6ee658 platform/x86: ISST: Fix possible circular locking dependency detected
  ACK: pavel
  - 066c905ed06c platform/x86: touchscreen_dmi: Add info for the RWC NANOTE P8 AY07J 2-in-1
  ACK: pavel
  - 0b17d4b51c63 btrfs: send: in case of IO error log it
  ACK: pavel -- just a debugging tweak
  - 78a68bbebdcc parisc: Add ioread64_lo_hi() and iowrite64_lo_hi()
  ACK: pavel
  - ade1077c7fc0 PCI: hv: Fix NUMA node assignment when kernel boots with custom NUMA topology
  ACK: pavel
  - 254090925e16 mm: don't try to NUMA-migrate COW pages that have other uses
  ACK: pavel
  - ab2b4e65a130 mmc: block: fix read single on recovery logic
  ACK: pavel
  - 775671687299 parisc: Fix sglist access in ccio-dma.c
  IGN: pavel
  - f8f519d7df66 parisc: Fix data TLB miss in sba_unmap_sg
  IGN: pavel
  - 4d569b959e54 parisc: Drop __init from map_pages declaration
  ACK: pavel
  - 8e3f9a098eca serial: parisc: GSC: fix build when IOSAPIC is not set
  ACK: pavel
  - fe383750d40d Revert "svm: Add warning message for AVIC IPI invalid target"
  ACK: pavel -- just a printk tweak
  - 126382b5565f HID:Add support for UGTABLET WP5540
  ACK: pavel
  - f100e758cef5 scsi: lpfc: Fix mailbox command failure during driver initialization
  ACK: pavel -- not a minimum fix, keeps the bug if hardware is too slow
  - 4578b979ef61 can: isotp: add SF_BROADCAST support for functional addressing
  UR: pavel -- c/e, new feature, not a bugfix
  - 5d42865fc311 can: isotp: prevent race between isotp_bind() and isotp_setsockopt()
  ACK: pavel
  - db3f3636e4ae fs/proc: task_mmu.c: don't read mapcount for migration entry
  ACK: pavel
  - 0849f83e4782 fget: clarify and improve __fget_files() implementation
  ACK: pavel -- just a performance improvement... for a benchmark
  - 657991fb06a4 rcu: Do not report strict GPs for outgoing CPUs
  ACK: pavel
  - 8c8385972ea9 mm: memcg: synchronize objcg lists with a dedicated spinlock
  ACK: pavel
  - d0f4aa2d978f drm/nouveau/pmu/gm200-: use alternate falcon reset sequence
  ACK: pavel
