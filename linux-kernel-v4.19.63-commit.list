# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v4.19.63
  - 9a9de33 Linux 4.19.63
  - 408af82 access: avoid the RCU grace period for the temporary subjective credentials
  pavel -- rewritten RCU handling for performance gains...
  - 1a547d2 libnvdimm/bus: Stop holding nvdimm_bus_list_mutex over __nd_ioctl()
  NAK: pavel -- cleanup, not a bugfix, and long one, and it prepares for something not in 4.19
  - b993a66 powerpc/tm: Fix oops on sigreturn on systems without TM
  ACK: pavel
  - b9310c5 powerpc/xive: Fix loop exit-condition in xive_find_target_in_mask()
  ACK: pavel
  - c219444 ALSA: hda - Add a conexant codec entry to let mute led work
  ACK: pavel
  - 491483e ALSA: line6: Fix wrong altsetting for LINE6_PODHD500_1
  ACK: pavel
  - 6027440 ALSA: ac97: Fix double free of ac97_codec_device
  ACK: pavel
  - 9845fb5 hpet: Fix division by zero in hpet_time_div()
  ACK: pavel
  - e4c9158 mei: me: add mule creek canyon (EHL) device ids
  ACK: pavel
  - 3d0a692 fpga-manager: altera-ps-spi: Fix build error
  ACK: pavel -- just a build fix
  - e907b13 binder: prevent transactions to context manager from its own process.
  ACK: pavel
  - 7d20e3b x86/speculation/mds: Apply more accurate check on hypervisor platform
  ACK: pavel
  - 5e87e8b x86/sysfb_efi: Add quirks for some devices with swapped width and height
  ACK: pavel
  - e3dc9ea btrfs: inode: Don't compress if NODATASUM or NODATACOW set
  ACK: pavel -- code does check same condition twice...
  - 1f37bec usb: pci-quirks: Correct AMD PLL quirk detection
  ACK: pavel
  - 41d3dbb usb: wusbcore: fix unbalanced get/put cluster_id
  ACK: pavel
  - 148959c locking/lockdep: Hide unused 'class' variable
  ACK: pavel -- just a warning fix
  - b076872 mm: use down_read_killable for locking mmap_sem in access_remote_vm
  ACK: pavel
  - 4acb04e locking/lockdep: Fix lock used or unused stats error
  ACK: pavel -- not sure what it fixes
  - af0883f proc: use down_read_killable mmap_sem for /proc/pid/maps
  ACK: pavel -- debugging aid, not a bugfix
  - 0d72bb8 cxgb4: reduce kernel stack usage in cudbg_collect_mem_region()
  ACK: pavel -- typo "kilobytes", woodoo to affect clang code generation, can't see how its supposed to work
  - 6ecdcbc proc: use down_read_killable mmap_sem for /proc/pid/map_files
  ACK: pavel -- debugging aid, not a bugfix
  - 3d617da proc: use down_read_killable mmap_sem for /proc/pid/clear_refs
  ACK: pavel -- debugging aid, not a bugfix
  - 42beb7b proc: use down_read_killable mmap_sem for /proc/pid/pagemap
  ACK: pavel -- debugging aid, not a bugfix
  - 1b3042d proc: use down_read_killable mmap_sem for /proc/pid/smaps_rollup
  ACK: pavel -- debugging aid, not a bugfix
  - a8c568f mm/mmu_notifier: use hlist_add_head_rcu()
  ACK: pavel -- theoretical bug...
  - 3062448 memcg, fsnotify: no oom-kill for remote memcg charging
  ACK: pavel
  - 041b127 mm/gup.c: remove some BUG_ONs from get_gate_page()
  ACK: pavel -- fixes a bug that can not happen in current configurations
  - fa099d6 mm/gup.c: mark undo_dev_pagemap as __maybe_unused
  ACK: pavel -- just a warning fix
  - 8be4a30 9p: pass the correct prototype to read_cache_page
  ACK: pavel -- cleanup, not a bugfix
  - 071f213 mm/kmemleak.c: fix check for softirq context
  ACK: pavel
  - 7bd5902 sh: prevent warnings when using iounmap
  ACK: pavel -- just a warning fix
  - af50d6a block/bio-integrity: fix a memory leak bug
  ACK: pavel
  - 7f775a6 powerpc/eeh: Handle hugepages in ioremap space
  ACK: pavel
  - e7a41b2 dlm: check if workqueues are NULL before flushing/destroying
  ACK: pavel
  - 5d59e28 mailbox: handle failed named mailbox channel request
  ACK: pavel
  - 2140a6b f2fs: avoid out-of-range memory access
  ACK: pavel -- should be -EUCLEAN, should printk, should mark system as dirty
  - 8a1a3d3 block: init flush rq ref count to 1
  ACK: pavel
  - 4b9dc73a powerpc/boot: add {get, put}_unaligned_be32 to xz_config.h
  pavel -- preparation for commit that did not make it to stable
  - 549f726 PCI: dwc: pci-dra7xx: Fix compilation when !CONFIG_GPIOLIB
  ACK: pavel
  - 367cc37 RDMA/rxe: Fill in wc byte_len with IB_WC_RECV_RDMA_WITH_IMM
  ACK: pavel
  - 4fe7ea2 perf hists browser: Fix potential NULL pointer dereference found by the smatch tool
  IGN: pavel
  - 915945f perf annotate: Fix dereferencing freed memory found by the smatch tool
  IGN: pavel
  - b305dcf perf session: Fix potential NULL pointer dereference found by the smatch tool
  IGN: pavel
  - 19cf571 perf top: Fix potential NULL pointer dereference detected by the smatch tool
  IGN: pavel
  - 995527d perf stat: Fix use-after-freed pointer detected by the smatch tool
  IGN: pavel
  - 3b8c4ea perf test mmap-thread-lookup: Initialize variable to suppress memory sanitizer warning
  IGN: pavel
  - dd0a0c7 PCI: mobiveil: Use the 1st inbound window for MEM inbound transactions
  ACK: pavel -- not sure what it fixes?
  - 270972d PCI: mobiveil: Initialize Primary/Secondary/Subordinate bus numbers
  ACK: pavel
  - 9eb4f28 kallsyms: exclude kasan local symbols on s390
  ACK: pavel
  - 4613f46 PCI: mobiveil: Fix the Class Code field
  ACK: pavel
  - 51308ec PCI: mobiveil: Fix PCI base address in MEM/IO outbound windows
  ACK: pavel
  - 05959ed arm64: assembler: Switch ESB-instruction with a vanilla nop if !ARM64_HAS_RAS
  ACK: pavel
  - 007b01a IB/ipoib: Add child to parent list only if device initialized
  ACK: pavel
  - d48720b powerpc/mm: Handle page table allocation failures
  IGN: pavel
  - f14537b IB/mlx5: Fixed reporting counters on 2nd port for Dual port RoCE
  - d03aeb8 serial: sh-sci: Fix TX DMA buffer flushing and workqueue races
  ACK: pavel
  - 48c73b8 serial: sh-sci: Terminate TX DMA during buffer flushing
  ACK: pavel
  - ca730bf RDMA/i40iw: Set queue pair state when being queried
  ACK: pavel
  - 52373ab powerpc/4xx/uic: clear pending interrupt after irq type/pol change
  ACK: pavel
  - 7452014 um: Silence lockdep complaint about mmap_sem
  ACK: pavel
  - 30edc7c mm/swap: fix release_pages() when releasing devmap pages
  ACK: pavel
  - b4e7700 mfd: hi655x-pmic: Fix missing return value check for devm_regmap_init_mmio_clk
  ACK: pavel
  - 9b1691c mfd: arizona: Fix undefined behavior
  ACK: pavel -- just a warning fix
  - d9c7417 mfd: core: Set fwnode for created devices
  ACK: pavel
  - 7b24a4a mfd: madera: Add missing of table registration
  ACK: pavel
  - e00cf1d recordmcount: Fix spurious mcount entries on powerpc
  ACK: pavel
  - 9fac394 powerpc/xmon: Fix disabling tracing while in xmon
  IGN: pavel
  - a80f67d powerpc/cacheflush: fix variable set but not used
  ACK: pavel -- just a warning fix
  - b150423 iio: iio-utils: Fix possible incorrect mask calculation
  pavel -- it does not change the code
  - fc9c15c PCI: xilinx-nwl: Fix Multi MSI data programming
  ACK: pavel
  - e3e2bb1 genksyms: Teach parser about 128-bit built-in types
  ACK: pavel
  - 27f2335 kbuild: Add -Werror=unknown-warning-option to CLANG_FLAGS
  ACK: pavel
  - 1fa9438 i2c: stm32f7: fix the get_irq error cases
  ACK: pavel
  - f930727 PCI: sysfs: Ignore lockdep for remove attribute
  ACK: pavel
  - 9d45fbe serial: mctrl_gpio: Check if GPIO property exisits before requesting it
  ACK: pavel -- but system will boot in different config if allocatoin fails
  - e7f206f drm/msm: Depopulate platform on probe failure
  ACK: pavel
  - 216462f powerpc/pci/of: Fix OF flags parsing for 64bit BARs
  ACK: pavel
  - 5d3ad90 mmc: sdhci: sdhci-pci-o2micro: Check if controller supports 8-bit width
  pavel -- enables 8-bit mode, not a bugfix
  - bf7cf9f usb: gadget: Zero ffs_io_data
  ACK: pavel
  - ac380eb tty: serial_core: Set port active bit in uart_port_activate
  ACK: pavel
  - 785e11c serial: imx: fix locking in set_termios()
  ACK: pavel
  - 741f8b3 drm/rockchip: Properly adjust to a true clock in adjusted_mode
  ACK: pavel
  - fd0d171 powerpc/pseries/mobility: prevent cpu hotplug during DT update
  ACK: pavel
  - 6a70474 drm/amd/display: fix compilation error
  ACK: pavel
  - 709ca46 phy: renesas: rcar-gen2: Fix memory leak at error paths
  ACK: pavel -- tiny memory leak
  - 725c7b7 drm/virtio: Add memory barriers for capset cache.
  ACK: pavel -- maybe smp_wmb__before_atomic() would be enough
  - 11b4e9f drm/amd/display: Always allocate initial connector state state
  ACK: pavel
  - 1a2425b serial: 8250: Fix TX interrupt handling condition
  ACK: pavel
  - a0e7d6b tty: serial: msm_serial: avoid system lockup condition
  ACK: pavel -- but guess it could use some fixing; printk() + better delay loop
  - e40f5a8 tty/serial: digicolor: Fix digicolor-usart already registered warning
  ACK: pavel
  - 5c0e548 memstick: Fix error cleanup path of memstick_init
  ACK: pavel
  - 0a50a27 drm/crc-debugfs: Also sprinkle irqrestore over early exits
  ACK: pavel
  - 26a6645 drm/crc-debugfs: User irqsafe spinlock in drm_crtc_add_crc_entry
  ACK: pavel -- buggy, needs following patch to fix it up
  - 4d14323 gpu: host1x: Increase maximum DMA segment size
  ACK: pavel
  - f9bfd6b drm/bridge: sii902x: pixel clock unit is 10kHz instead of 1kHz
  ACK: pavel
  - 7af9abd drm/bridge: tc358767: read display_props in get_modes()
  ACK: pavel
  - 49c7230 PCI: Return error if cannot probe VF
  ACK: pavel
  - 2a18d76 drm/edid: Fix a missing-check bug in drm_load_edid_firmware()
  ACK: pavel -- gfp_kernel allocations do not fail
  - 210dfe63 drm/amdkfd: Fix sdma queue map issue
  pavel -- it also changes error behaviour in 2-engine case
  - db64bc1 drm/amdkfd: Fix a potential memory leak
  ACK: pavel
  - 6b1d287 drm/amd/display: Disable ABM before destroy ABM struct
  ACK: pavel
  - c242a53 drm/amdgpu/sriov: Need to initialize the HDP_NONSURFACE_BAStE
  ACK: pavel -- "should not hurt". Hmm. Hopefully someone tested it
  - 147137f drm/amd/display: Fill prescale_params->scale for RGB565
  ACK: pavel
  - 08b0bcc tty: serial: cpm_uart - fix init when SMC is relocated
  ACK: pavel -- poor changelog, probably fixes compatibility with some bootloaders, hard to tell
  - c901780 pinctrl: rockchip: fix leaked of_node references
  ACK: pavel
  - a9dfb6e tty: max310x: Fix invalid baudrate divisors calculator
  ACK: pavel -- rewritten baudrate computations
  - b0084c1 usb: core: hub: Disable hub-initiated U1/U2
  ACK: pavel -- only a performance fix
  - 19755a1 staging: vt6656: use meaningful error code during buffer allocation
  IGN: pavel
  - b59f765 iio: adc: stm32-dfsdm: missing error case during probe
  ACK: pavel
  - 302e4cd iio: adc: stm32-dfsdm: manage the get_irq error case
  ACK: pavel
  - 586946c drm/panel: simple: Fix panel_simple_dsi_probe
  ACK: pavel
  - 49fb03d hvsock: fix epollout hang from race condition
  IGN: pavel
