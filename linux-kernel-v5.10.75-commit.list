# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v5.10.75
  - 3a9842b42e42 Linux 5.10.75
  - 3e2873652163 net: dsa: mv88e6xxx: don't use PHY_DETECT on internal PHY's
  ACK: uli
  - 3593fa147c86 ionic: don't remove netdev->dev_addr when syncing uc list
  ACK: uli
  - f33890d9bb59 net: mscc: ocelot: warn when a PTP IRQ is raised for an unknown skb
  ACK: uli -- adds a warning for an error case, near-cosmetic
  - 9c546af181bc nfp: flow_offload: move flow_indr_dev_register from app init to app start
  ACK: uli
  - 6da9af2d2531 r8152: select CRC32 and CRYPTO/CRYPTO_HASH/CRYPTO_SHA256
  ACK: uli
  - ecfd4fa15b06 qed: Fix missing error code in qed_slowpath_start()
  ACK: uli
  - 51f6e72ca656 mqprio: Correct stats in mqprio_dump_class_stats().
  ACK: uli
  - fdaff7f9e806 platform/x86: intel_scu_ipc: Fix busy loop expiry time
  ACK: uli
  - 057ee6843bbb acpi/arm64: fix next_platform_timer() section mismatch error
  ACK: uli
  - c6b2400095ba drm/msm/dsi: fix off by one in dsi_bus_clk_enable error handling
  ACK: uli
  - 2c5658717428 drm/msm/dsi: Fix an error code in msm_dsi_modeset_init()
  ACK: uli
  - b28586fb04f3 drm/msm/a6xx: Track current ctx by seqno
  ACK: uli
  - abd11864159b drm/msm/mdp5: fix cursor-related warnings
  ACK: uli
  - 91a340768b01 drm/msm: Fix null pointer dereference on pointer edp
  ACK: uli
  - a7b45024f66f drm/edid: In connector_bad_edid() cap num_of_ext by num_blocks read
  ACK: uli
  - d0f0e1710397 drm/panel: olimex-lcd-olinuxino: select CRC32
  ACK: uli
  - a4a37e6516f8 spi: bcm-qspi: clear MSPI spifie interrupt during probe
  ACK: uli -- sneaks in a fix not mentioned in the commit message ("if (qspi->clk)")
  - d9428f08e1c3 platform/mellanox: mlxreg-io: Fix read access of n-bytes size attributes
  ACK: uli
  - c216cebdd245 platform/mellanox: mlxreg-io: Fix argument base in kstrtou32() call
  ACK: uli
  - e59d839743b5 mlxsw: thermal: Fix out-of-bounds memory accesses
  UR: uli -- thermal->cooling_levels[] is not fully initialized (must be "for (i = 0; i <= MLXSW_THERMAL_MAX_STATE; i++)")
  - 7eef482db728 ata: ahci_platform: fix null-ptr-deref in ahci_platform_enable_regulators()
  ACK: uli
  - 116932c0e45e pata_legacy: fix a couple uninitialized variable bugs
  ACK: uli
  - 50cb95487c26 NFC: digital: fix possible memory leak in digital_in_send_sdd_req()
  ACK: uli
  - 3f2960b39f22 NFC: digital: fix possible memory leak in digital_tg_listen_mdaa()
  ACK: uli
  - 2f21f06a5e7a nfc: fix error handling of nfc_proto_register()
  ACK: uli
  - ba39f55952a2 vhost-vdpa: Fix the wrong input in config_cb
  ACK: uli
  - 84e0f2fc662e ethernet: s2io: fix setting mac address during resume
  ACK: pavel
  - e19c10d6e07c net: encx24j600: check error in devm_regmap_init_encx24j600
  UR: pavel -- c/e
  - f2e1de075018 net: dsa: microchip: Added the condition for scheduling ksz_mib_read_work
  ACK: pavel
  - 9053c5b4594c net: stmmac: fix get_hw_feature() on old hardware
  ACK: pavel
  - 12da46cb6a90 net/mlx5e: Mutually exclude RX-FCS and RX-port-timestamp
  UR: pavel
  - 4f7bddf8c5c0 net/mlx5e: Fix memory leak in mlx5_core_destroy_cq() error path
  UR: pavel --a not sure this will not cause other problems
  - afb0c67dfdb5 net: korina: select CRC32
  ACK: pavel
  - 33ca85010511 net: arc: select CRC32
  ACK: pavel
  - 17a027aafd52 gpio: pca953x: Improve bias setting
  ACK: pavel -- two changes in one
  - d84a69ac410f sctp: account stream padding length for reconf chunk
  ACK: pavel
  - 6fecdb5b54a5 nvme-pci: Fix abort command id
  ACK: pavel
  - 2d937cc12c14 ARM: dts: bcm2711-rpi-4-b: Fix pcie0's unit address formatting
  ACK: pavel -- just a warning fix, two changes in one
  - 6e6082250b53 ARM: dts: bcm2711-rpi-4-b: fix sd_io_1v8_reg regulator states
  ACK: pavel
  - 48613e687e28 ARM: dts: bcm2711: fix MDIO #address- and #size-cells
  ACK: pavel
  - 6e6e3018d3ce ARM: dts: bcm2711-rpi-4-b: Fix usb's unit address
  ACK: pavel -- two changes in one, just a warning fix
  - 76644f94595b tee: optee: Fix missing devices unregister during optee_remove
  ACK: pavel -- interesting use of strncmp
  - 07f885682486 iio: dac: ti-dac5571: fix an error code in probe()
  ACK: pavel
  - 6c0024bcaadc iio: ssp_sensors: fix error code in ssp_print_mcu_debug()
  ACK: pavel -- just a cleanup
  - 0fbc3cf7dd9a iio: ssp_sensors: add more range checking in ssp_parse_dataframe()
  UR: pavel -- c/e
  - abe5b13dd959 iio: adc: max1027: Fix the number of max1X31 channels
  ACK: pavel
  - 41e84a4f25b6 iio: light: opt3001: Fixed timeout error when 0 lux
  ACK: pavel
  - e811506f609a iio: mtk-auxadc: fix case IIO_CHAN_INFO_PROCESSED
  ACK: pavel
  - 1671cfd31b66 iio: adc: max1027: Fix wrong shift with 12-bit devices
  ACK: pavel
  - f931076d32b6 iio: adc128s052: Fix the error handling path of 'adc128_probe()'
  ACK: pavel
  - 4425d059aa2e iio: adc: ad7793: Fix IRQ flag
  ACK: pavel
  - d078043a1775 iio: adc: ad7780: Fix IRQ flag
  ACK: pavel
  - a8177f0576fa iio: adc: ad7192: Add IRQ flag
  ACK: pavel
  - be8ef91d6166 driver core: Reject pointless SYNC_STATE_ONLY device links
  ACK: pavel -- just a perfromance tweak
  - d5f13bbb5104 drivers: bus: simple-pm-bus: Add support for probing simple bus only devices
  ACK: pavel
  - b45923f66eb6 iio: adc: aspeed: set driver data when adc probe.
  ACK: pavel
  - ea947267eb6f powerpc/xive: Discard disabled interrupts in get_irqchip_state()
  ACK: pavel
  - 9e46bdfb55a3 x86/Kconfig: Do not enable AMD_MEM_ENCRYPT_ACTIVE_BY_DEFAULT automatically
  ACK: pavel -- just a Kconfig tweak
  - 57e48886401b nvmem: Fix shift-out-of-bound (UBSAN) with byte size cells
  ACK: pavel
  - a7bd0dd3f2ed EDAC/armada-xp: Fix output of uncorrectable error counter
  ACK: pavel
  - 92e6e08ca2b0 virtio: write back F_VERSION_1 before validate
  ACK: pavel
  - 86e3ad8b759d misc: fastrpc: Add missing lock before accessing find_vma()
  ACK: pavel
  - 3f0ca245a834 USB: serial: option: add prod. id for Quectel EG91
  ACK: pavel
  - ecad614b0c68 USB: serial: option: add Telit LE910Cx composition 0x1204
  ACK: pavel
  - bf26bc72dc59 USB: serial: option: add Quectel EC200S-CN module support
  ACK: pavel
  - d4b77900cffe USB: serial: qcserial: add EM9191 QDL support
  ACK: pavel
  - 3147f5721588 Input: xpad - add support for another USB ID of Nacon GC-100
  ACK: pavel
  - 9d89e2871167 usb: musb: dsps: Fix the probe error path
  ACK: pavel
  - 3b4275140142 efi: Change down_interruptible() in virt_efi_reset_system() to down_trylock()
  UR: pavel --! are you sure?
  - 5100dc4489ab efi/cper: use stack buffer for error record decoding
  ACK: pavel -- just a theoretical bug
  - 2c5dd2a8af77 cb710: avoid NULL pointer subtraction
  ACK: pavel -- just a warning workaround
  - d40e193abd07 xhci: Enable trust tx length quirk for Fresco FL11 USB controller
  ACK: pavel
  - dec944bb7079 xhci: Fix command ring pointer corruption while aborting a command
  ACK: pavel
  - dc3e0a20dbb9 xhci: guard accesses to ep_state in xhci_endpoint_reset()
  ACK: pavel
  - 0ee66290f006 USB: xhci: dbc: fix tty registration race
  UR: pavel
  - 9f0d6c781cb5 mei: me: add Ice Lake-N device id.
  ACK: pavel
  - e4f7171c2395 x86/resctrl: Free the ctrlval arrays when domain_setup_mon_state() fails
  ACK: pavel
  - 0e32a2b85c7d btrfs: fix abort logic in btrfs_replace_file_extents
  ACK: pavel
  - 52924879ed45 btrfs: update refs for any root except tree log roots
  ACK: pavel
  - 352349aa4948 btrfs: check for error when looking up inode during dir entry replay
  ACK: pavel -- not a minimum fix
  - 4ed68471bc37 btrfs: deal with errors when adding inode reference during log replay
  ACK: pavel
  - 95d3aba5febe btrfs: deal with errors when replaying dir entry during log replay
  ACK: pavel
  - 206868a5b6c1 btrfs: unlock newly allocated extent buffer after error
  ACK: pavel
  - e7e3ed5c92b6 drm/msm: Avoid potential overflow in timeout_to_jiffies()
  ACK: pavel
  - a31c33aa80a5 arm64/hugetlb: fix CMA gigantic page order for non-4K PAGE_SIZE
  ACK: pavel -- just a fix for unusal configuration
  - 0c97008859ca csky: Fixup regs.sr broken in ptrace
  ACK: pavel
  - 5dab6e8f141a csky: don't let sigreturn play with priveleged bits of status register
  ACK: pavel
  - e3c37135c9ca clk: socfpga: agilex: fix duplicate s2f_user0_clk
  ACK: pavel -- not sure it has functional impact
  - faba7916cdc0 s390: fix strrchr() implementation
  IGN: pavel
  - 7ef43c0f68fb nds32/ftrace: Fix Error: invalid operands (*UND* and *UND* sections) for `^'
  ACK: pavel
  - c3bf276fd7c8 ALSA: hda/realtek: Fix the mic type detection issue for ASUS G551JW
  ACK: pavel
  - 1099953b32c6 ALSA: hda/realtek: Fix for quirk to enable speaker output on the Lenovo 13s Gen2
  ACK: pavel
  - 554a5027f536 ALSA: hda/realtek: Add quirk for TongFang PHxTxX1
  ACK: pavel
  - 0fa256509b9f ALSA: hda/realtek - ALC236 headset MIC recording issue
  ACK: pavel
  - 1e10c6bf15d2 ALSA: hda/realtek: Add quirk for Clevo X170KM-G
  ACK: pavel
  - 8a5f01f4b01c ALSA: hda/realtek: Complete partial device name to avoid ambiguity
  ACK: pavel
  - c6e5290e6cc1 ALSA: hda - Enable headphone mic on Dell Latitude laptops with ALC3254
  ACK: pavel
  - 9bb1659ac594 ALSA: hda/realtek: Enable 4-speaker output for Dell Precision 5560 laptop
  ACK: pavel
  - 7680631ac7ab ALSA: seq: Fix a potential UAF by wrong private_free call order
  ACK: pavel
  - 4aab156d302c ALSA: pcm: Workaround for a wrong offset in SYNC_PTR compat ioctl
  UR: pavel
  - f077d699c1d2 ALSA: usb-audio: Add quirk for VF0770
  ACK: pavel
