# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v4.19.100
  - 7cdefde351b6 Linux 4.19.100
  - 86834898d5a5 mm/memory_hotplug: shrink zones when offlining memory
  ACK: pavel
  - d98d053efa65 mm/memory_hotplug: fix try_offline_node()
  pavel -- rewrites node handling code
  - f291080659d2 mm/memunmap: don't access uninitialized memmap in memunmap_pages()
  ACK: pavel
  - d830a11c6279 drivers/base/node.c: simplify unregister_memory_block_under_nodes()
  ACK: pavel -- cleanup
  - b9cda6501a34 mm/hotplug: kill is_dev_zone() usage in __remove_pages()
  ACK: pavel -- cleanup
  - dc6be8597c8c mm/memory_hotplug: remove "zone" parameter from sparse_remove_one_section
  ACK: pavel -- refactoring
  - 030d045dc067 mm/memory_hotplug: make unregister_memory_block_under_nodes() never fail
  pavel -- removes error handling
  - d883abbc097e mm/memory_hotplug: remove memory block devices before arch_remove_memory()
  pavel -- refactoring
  - aa49b6abcefc mm/memory_hotplug: create memory block devices after arch_add_memory()
  pavel -- refactoring, not simple bugfix
  - 97c60869dbf3 drivers/base/memory: pass a block_id to init_memory_block()
  pavel -- preparation for next patches?
  - 000a1d59cfe9 mm/memory_hotplug: allow arch_remove_memory() without CONFIG_MEMORY_HOTREMOVE
  pavel -- makes CONFIG_MEMORY_HOTREMOVE option less useful
  - 817edd2bb385 s390x/mm: implement arch_remove_memory()
  - 5163b1ec3a0c mm/memory_hotplug: make __remove_pages() and arch_remove_memory() never fail
  pavel -- patch removes error handling
  - 58ddf0b0eff2 powerpc/mm: Fix section mismatch warning
  - efaa8fb877a0 mm/memory_hotplug: make __remove_section() never fail
  pavel -- error handling is hard, so it is removed
  - 36976713c4d7 mm/memory_hotplug: make unregister_memory_section() never fail
  pavel -- simply removes error handling
  - 8893b51a8960 mm, memory_hotplug: update a comment in unregister_memory()
  ACK: pavel -- just a comment fixup
  - 9e59baa2da6d drivers/base/memory.c: clean up relics in function parameters
  ACK: pavel -- just a preparation
  - 2ad264f68873 mm/memory_hotplug: release memory resource after arch_remove_memory()
  pavel -- it simply removes error handling
  - 5c1f8f5358e8 mm, memory_hotplug: add nid parameter to arch_remove_memory
  ACK: pavel -- preparation for next patches
  - 4149c8693a8c drivers/base/memory.c: remove an unnecessary check on NR_MEM_SECTIONS
  pavel -- cleanup, not a bugfix
  - aa2e8b68f244 mm, sparse: pass nid instead of pgdat to sparse_add_one_section()
  pavel -- preparation for next patches
  - b1dbaa191628 mm, sparse: drop pgdat_resize_lock in sparse_add/remove_one_section()
  ACK: pavel
  - a3cf10bf73fd mm/memory_hotplug: make remove_memory() take the device_hotplug_lock
  pavel -- big series that re-does locking in memory hotplug
  - 868f9e509e8f net/x25: fix nonblocking connect
  ACK: pavel
  - 1f7a1bcd27c3 netfilter: nf_tables: add __nft_chain_type_get()
  pavel -- may not catch all the cases
  - 5b0d87620bbe netfilter: ipset: use bitmap infrastructure completely
  ACK: pavel
  - a76e62517465 scsi: iscsi: Avoid potential deadlock in iscsi_if_rx func
  ACK: pavel -- "interesting" code
  - f008896751c6 media: v4l2-ioctl.c: zero reserved fields for S/TRY_FMT
  ACK: pavel
  - cbd56515be5a libertas: Fix two buffer overflows at parsing bss descriptor
  NAK: pavel -- it returns success in case of failure, widening variable scope is not welcome
  - cb75ab691932 coresight: tmc-etf: Do not call smp_processor_id from preemptible
  ACK: pavel
  - 63906caff4a8 coresight: etb10: Do not call smp_processor_id from preemptible
  ACK: pavel
  - 03e520dcdc0a crypto: geode-aes - switch to skcipher for cbc(aes) fallback
  ACK: pavel -- patch claims to be v4.20+, but should be ok
  - 8d9aa36cc7ac sd: Fix REQ_OP_ZONE_REPORT completion handling
  ACK: pavel
  - ce28d664054d tracing: Fix histogram code when expression has same var as value
  ACK: pavel -- fixes very ugly/strange semantics, not a serious bug
  - cbb042fd8794 tracing: Remove open-coding of hist trigger var_ref management
  ACK: pavel -- cleanup
  - 836717841a30 tracing: Use hist trigger's var_ref array to destroy var_refs
  ACK: pavel
  - 90042a53980b net/sonic: Prevent tx watchdog timeout
  - 85d1250227b0 net/sonic: Fix CAM initialization
  - 6382bb92be25 net/sonic: Fix command register usage
  - fc590dcb62e6 net/sonic: Quiesce SONIC before re-initializing descriptor memory
  - 7d8c24e07569 net/sonic: Fix receive buffer replenishment
  - 1de65d2cadb9 net/sonic: Improve receive descriptor status flag check
  - 6f1355914bfb net/sonic: Avoid needless receive descriptor EOL flag updates
  - 75f91ec93567 net/sonic: Fix receive buffer handling
  - 04b5473a21a4 net/sonic: Fix interface error stats collection
  - 5205e9b20840 net/sonic: Use MMIO accessors
  - b9ef3fe67d10 net/sonic: Clear interrupt flags immediately
  - 655fb2209092 net/sonic: Add mutual exclusion for accessing shared state
  - 752f72edea55 do_last(): fetch directory ->i_mode and ->i_uid before it's too late
  ACK: pavel
  - 05f010d2ff4b tracing: xen: Ordered comparison of function pointers
  ACK: pavel -- just a warning fix
  - 5ce5ebfa007b scsi: RDMA/isert: Fix a recently introduced regression related to logout
  ACK: pavel
  - 7bf1558012e0 hwmon: (nct7802) Fix voltage limits to wrong registers
  ACK: pavel
  - 666a530b2e02 netfilter: nft_osf: add missing check for DREG attribute
  ACK: pavel
  - f5cdfc16faa8 Input: sun4i-ts - add a check for devm_thermal_zone_of_sensor_register
  ACK: pavel -- not sure this really happens to people
  - f6d8ff752710 Input: pegasus_notetaker - fix endpoint sanity check
  ACK: pavel
  - d6ca8b03fd80 Input: aiptek - fix endpoint sanity check
  ACK: pavel
  - 20ae16280a6d Input: gtco - fix endpoint sanity check
  ACK: pavel
  - 0c022c4a2391 Input: sur40 - fix interface sanity checks
  ACK: pavel
  - c694050c9679 Input: pm8xxx-vib - fix handling of separate enable register
  ACK: pavel
  - a243850af3fb Documentation: Document arm64 kpti control
  ACK: pavel
  - 6491a9dd3cf9 mmc: sdhci: fix minimum clock rate for v3 controller
  ACK: pavel
  - 3018dc1af524 mmc: tegra: fix SDR50 tuning override
  ACK: pavel
  - ddb2f192d723 ARM: 8950/1: ftrace/recordmcount: filter relocation types
  ACK: pavel
  - 76ac84d52720 Revert "Input: synaptics-rmi4 - don't increment rmiaddr for SMBus transfers"
  pavel -- reverts in preparation of fixing problem different way, but that one does not land?
  - ef2f9f37c392 Input: keyspan-remote - fix control-message timeouts
  ACK: pavel
  - 47eb3574d0ab tracing: trigger: Replace unneeded RCU-list traversals
  ACK: pavel -- just a WARN fix
  - b48fea52b951 PCI: Mark AMD Navi14 GPU rev 0xc5 ATS as broken
  ACK: pavel
  - 4235c1e80285 hwmon: (core) Do not use device managed functions for memory allocations
  ACK: pavel -- complex fix for a leak
  - c84732496ce7 hwmon: (adt7475) Make volt2reg return same reg as reg2volt input
  ACK: pavel -- ugly interface, not a serious bug
  - 881c9706ebf6 afs: Fix characters allowed into cell names
  ACK: pavel
  - 8f50a05dd6fe tun: add mutex_unlock() call and napi.skb clearing in tun_get_user()
  ACK: pavel
  - 9bbde0825846 tcp: do not leave dangling pointers in tp->highest_sack
  ACK: pavel
  - 33dba56493e7 tcp_bbr: improve arithmetic division in bbr_update_bw()
  ACK: pavel
  - 4c1c35c01531 Revert "udp: do rmem bulk free even if the rx sk queue is empty"
  ACK: pavel
  - c74b3d128d57 net: usb: lan78xx: Add .ndo_features_check
  ACK: pavel
  - b4b0f1fc1946 net-sysfs: Fix reference count leak
  ACK: pavel
  - 33c540f663d1 net-sysfs: Call dev_hold always in rx_queue_add_kobject
  ACK: pavel
  - f8862bc44fad net-sysfs: Call dev_hold always in netdev_queue_add_kobject
  ACK: pavel
  - 7070695e6077 net-sysfs: fix netdev_queue_add_kobject() breakage
  ACK: pavel
  - 60e715466109 net-sysfs: Fix reference count leak in rx|netdev_queue_add_kobject
  ACK: pavel
  - 66ac8ee96faa net_sched: fix datalen for ematch
  ACK: pavel
  - be1a2be7a7b0 net: rtnetlink: validate IFLA_MTU attribute in rtnl_create_link()
  ACK: pavel
  - 1d3b53f716b5 net, ip_tunnel: fix namespaces move
  ACK: pavel
  - fddb6ea5143a net, ip6_tunnel: fix namespaces move
  ACK: pavel
  - d0201d2405da net: ip6_gre: fix moving ip6gre between namespaces
  ACK: pavel
  - 404d333fd361 net: cxgb3_main: Add CAP_NET_ADMIN check to CHELSIO_GET_MEM
  ACK: pavel
  - 0705c8d7aae5 net: bcmgenet: Use netif_tx_napi_add() for TX NAPI
  ACK: pavel
  - d3c0a8be8bbb ipv6: sr: remove SKB_GSO_IPXIP6 on End.D* actions
  ACK: pavel -- just a performance fix
  - d3b5ecceea7d gtp: make sure only SOCK_DGRAM UDP sockets are accepted
  ACK: pavel
  - 8e360d7c4278 firestream: fix memory leaks
  ACK: pavel
  - bd1448458c6a can, slip: Protect tty->disc_data in write_wakeup and close with RCU
  ACK: pavel -- code could be simpler
