# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v4.4.213
  - d6ccbff9be43 Linux 4.4.213
  - 6d929d2c49f7 btrfs: do not zero f_bavail if we have available space
    IGN: btrfs
  - 078210b5b0d6 btrfs: fix mixed block count of available space
    IGN: btrfs
  - 571277b6ff27 net: Fix skb->csum update in inet_proto_csum_replace16().
    ACK: iwamatsu
  - c10c5bb8a8b3 l2t_seq_next should increase position index
    ACK: iwamatsu
  - 6ffe7ec419b5 seq_tab_next() should increase position index
    ACK: iwamatsu
  - 6d82377af9ba net/sonic: Quiesce SONIC before re-initializing descriptor memory
    ACK: iwamatsu
  - ffcfac3ae24e net/sonic: Fix receive buffer handling
    ACK: iwamatsu
  - 1bd3de964977 net/sonic: Use MMIO accessors
    ACK: iwamatsu
  - 6425373e5397 net/sonic: Add mutual exclusion for accessing shared state
    ACK: iwamatsu
  - c2f59355e72a net/fsl: treat fsl,erratum-a011043
    ACK: iwamatsu
  - 16b53f6d116d qlcnic: Fix CPU soft lockup while collecting firmware dump
    ACK: iwamatsu
  - e38df4efddb5 r8152: get default setting of WOL before initializing
    ACK: iwamatsu
  - bd786d8d13d5 airo: Add missing CAP_NET_ADMIN check in AIROOLDIOCTL/SIOCDEVPRIVATE
    ACK: iwamatsu
  - 98fd2fc6f18c airo: Fix possible info leak in AIROOLDIOCTL/SIOCDEVPRIVATE
    ACK: iwamatsu
  - 337776ece904 scsi: fnic: do not queue commands during fwreset
    ACK: iwamatsu
  - d233bd93b6ac vti[6]: fix packet tx through bpf_redirect()
    ACK: iwamatsu
  - 18330f2ae8af wireless: wext: avoid gcc -O3 warning
    ACK: iwamatsu
  - 3864a2a1fbb6 ixgbe: Fix calculation of queue with VFs and flow director on interface flap
    ACK: iwamatsu
  - aa3971194e4a ixgbevf: Remove limit of 10 entries for unicast filter list
    ACK: iwamatsu
  - c69bc89b8dee clk: mmp2: Fix the order of timer mux parents
    ACK: iwamatsu
  - 0406d59c89c8 media: si470x-i2c: Move free() past last use of 'radio'
    ACK: iwamatsu
  - 1da25d34b29b usb: dwc3: turn off VBUS when leaving host mode
    ACK: iwamatsu
  - c39cef9673d1 ttyprintk: fix a potential deadlock in interrupt context issue
    ACK: iwamatsu
  - adcd161ae408 media: dvb-usb/dvb-usb-urb.c: initialize actlen to 0
    ACK: iwamatsu
  - f97f40aa9aca media: gspca: zero usb_buf
    ACK: iwamatsu
  - 299ec5f621f9 media: digitv: don't continue if remote control state can't be read
    ACK: iwamatsu
  - 37f739275ff4 reiserfs: Fix memory leak of journal device string
    ACK: iwamatsu
  - 364474e72393 mm/mempolicy.c: fix out of bounds write in mpol_parse_str()
    ACK: iwamatsu
  - ac4ba8055d59 arm64: kbuild: remove compressed images on 'make ARCH=arm64 (dist)clean'
    ACK: iwamatsu
  - a44a55430321 crypto: pcrypt - Fix user-after-free on module unload
    ACK: iwamatsu
  - a77a9c9125f3 vfs: fix do_last() regression
    ACK: iwamatsu
  - f6b09fe83659 crypto: af_alg - Use bh_lock_sock in sk_destruct
    ACK: iwamatsu
  - 391af3d825fb net_sched: ematch: reject invalid TCF_EM_SIMPLE
    ACK: iwamatsu
  - 980494f6d532 usb-storage: Disable UAS on JMicron SATA enclosure
    ACK: iwamatsu
  - 104c94d7a031 atm: eni: fix uninitialized variable warning
    ACK: iwamatsu
  - b6547a7db839 net: wan: sdla: Fix cast from pointer to integer of different size
    ACK: iwamatsu
  - 93e0aca969d9 drivers/net/b44: Change to non-atomic bit operations on pwol_mask
    ACK: iwamatsu
  - a40ee3d6d29b watchdog: rn5t618_wdt: fix module aliases
    ACK: iwamatsu
  - 16ebb24441be zd1211rw: fix storage endpoint lookup
    ACK: iwamatsu
  - cc62a25dba10 rtl8xxxu: fix interface sanity check
    ACK: iwamatsu
  - 8e0d0c678677 brcmfmac: fix interface sanity check
    ACK: iwamatsu
  - f1f36e7796a5 ath9k: fix storage endpoint lookup
    ACK: iwamatsu
  - b9e9c736dea9 staging: vt6656: Fix false Tx excessive retries reporting.
    ACK: iwamatsu
  - 4f92be3f8154 staging: vt6656: use NULLFUCTION stack on mac80211
    ACK: iwamatsu
  - 3c25bc664595 staging: vt6656: correct packet types for CTS protect, mode.
    ACK: iwamatsu
  - e5a7333b9140 staging: wlan-ng: ensure error return is actually returned
    ACK: iwamatsu
  - 917c8fe39f4d staging: most: net: fix buffer overflow
    ACK: iwamatsu
  - 32c4ed0c4ab8 USB: serial: ir-usb: fix IrLAP framing
    ACK: iwamatsu
  - 39fa352a77dd USB: serial: ir-usb: fix link-speed handling
    ACK: iwamatsu
  - fa2cee6f614a USB: serial: ir-usb: add missing endpoint sanity check
    ACK: iwamatsu
  - ba1814393f49 rsi_91x_usb: fix interface sanity check
    ACK: iwamatsu
  - 036951850f0e orinoco_usb: fix interface sanity check
    ACK: iwamatsu
  - 1f4ffc0fcdfa ALSA: pcm: Add missing copy ops check before clearing buffer
    ACK; iwamatsu / This commit is a fix that is not in the Linus tree, but depends only on 4.4 and 4.9.
