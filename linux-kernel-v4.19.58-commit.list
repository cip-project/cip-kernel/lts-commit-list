# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v4.19.58
  - 7a6bfa0 Linux 4.19.58
  - f37de75 dmaengine: imx-sdma: remove BD_INTR for channel0
  ACK: pavel
  - 018c968 dmaengine: qcom: bam_dma: Fix completed descriptors count
  ACK: pavel
  - 870de14 MIPS: have "plain" make calls build dtbs for selected platforms
  ACK: pavel -- makes build easier to use, not sure if serious enough 
  - 8957895 MIPS: Add missing EHB in mtc0 -> mfc0 sequence.
  ACK: pavel
  - 2b8f8a8 MIPS: Fix bounds check virt_addr_valid
  ACK: pavel
  - 80b2562 svcrdma: Ignore source port when computing DRC hash
  ACK: pavel
  - 8129a10 nfsd: Fix overflow causing non-working mounts on 1 TB machines
  ACK: pavel
  - f25c069 KVM: LAPIC: Fix pending interrupt in IRR blocked by software disable LAPIC
  ACK: pavel
  - f6472f5 KVM: x86: degrade WARN to pr_warn_ratelimited
  ACK: pavel -- just changes warning level
  - ac0024b netfilter: ipv6: nf_defrag: accept duplicate fragments again
  ACK: pavel
  - 54e8cf4 bpf: fix bpf_jit_limit knob for PAGE_SIZE >= 64K
  ACK: pavel
  - e6c288f net: hns: fix unsigned comparison to less than zero
  ACK: pavel
  - 4f24801 sc16is7xx: move label 'err_spi' to correct section
  ACK: pavel -- just a warning fix
  - 318244f netfilter: ipv6: nf_defrag: fix leakage of unqueued fragments
  ACK: pavel
  - a8891c5 ip6: fix skb leak in ip6frag_expire_frag_queue()
  ACK: pavel
  - 382bc84 rds: Fix warning.
  ACK: pavel -- not a minimal fix.
  - 7e6af1f ALSA: hda: Initialize power_state field properly
  ACK: pavel
  - c8c8829 net: hns: Fixes the missing put_device in positive leg for roce reset
  ACK: pavel
  - 6bf9677 x86/boot/compressed/64: Do not corrupt EDX on EFER.LME=1 setting
  ACK: pavel
  - b91ec6a selftests: fib_rule_tests: Fix icmp proto with ipv6
  pavel -- not sure, does it paper over ABI regression?
  - e2851c3 scsi: tcmu: fix use after free
  ACK: pavel -- better changelog would be nice.
  - 04096b3 mac80211: mesh: fix missing unlock on error in table_path_del()
  ACK: pavel
  - e2379b0 f2fs: don't access node/meta inode mapping after iput
  ACK: pavel
  - e9fde78 drm/fb-helper: generic: Don't take module ref for fbcon
  pavel -- not sure what bug it fixes
  - 7821bcc media: s5p-mfc: fix incorrect bus assignment in virtual child device
  ACK: pavel
  - 3ddc2a1 net/smc: move unhash before release of clcsock
  ACK: pavel
  - cd54dc4 mlxsw: spectrum: Handle VLAN device unlinking
  pavel -- cleanup? Not sure what bug it fixes.
  - a8a296a tty: rocket: fix incorrect forward declaration of 'rp_init()'
  ACK: pavel -- just a warning fix
  - fb814f2 btrfs: Ensure replaced device doesn't have pending chunk allocation
  ACK: pavel
  - 27ce6c2 mm/vmscan.c: prevent useless kswapd loops
  ACK: pavel
  - c854d9b ftrace/x86: Remove possible deadlock between register_kprobe() and ftrace_run_update_code()
  ACK: pavel
  - 2e716c3 drm/imx: only send event on crtc disable if kept disabled
  ACK: pavel
  - 8ec242f drm/imx: notify drm core before sending event during crtc disable
  ACK: pavel
  - d2d0613 drm/etnaviv: add missing failure path to destroy suballoc
  ACK: pavel
  - ec5d99e drm/amdgpu/gfx9: use reset default for PA_SC_FIFO_SIZE
  ACK: pavel -- not much of a changelog, not sure how serious problem is
  - ec6d8c9 drm/amd/powerplay: use hardware fan control if no powerplay fan table
  ACK: pavel
  - b6d56f4 arm64: kaslr: keep modules inside module region when KASAN is enabled
  ACK: pavel
  - 7cab3df ARM: dts: armada-xp-98dx3236: Switch to armada-38x-uart serial node
  ACK: pavel
  - c8790d7 tracing/snapshot: Resize spare buffer if size changed
  - 052b318 fs/userfaultfd.c: disable irqs for fault_pending and event locks
  ACK: pavel -- long patch, but obvious
  - ea38007 lib/mpi: Fix karactx leak in mpi_powm
  ACK: pavel
  - 7df1e2f ALSA: hda/realtek - Change front mic location for Lenovo M710q
  ACK: pavel
  - 899377c ALSA: hda/realtek: Add quirks for several Clevo notebook barebones
  ACK: pavel
  - d9b6936 ALSA: usb-audio: fix sign unintended sign extension on left shifts
  ACK: pavel
  - 7f52af5 ALSA: line6: Fix write on zero-sized buffer
  ACK: pavel
  - 3663bf2 ALSA: firewire-lib/fireworks: fix miss detection of received MIDI messages
  ACK: pavel
  - 9d2ac58 ALSA: seq: fix incorrect order of dest_client/dest_ports arguments
  ACK: pavel -- Does it fix real bug?
  - ae3fa28 crypto: cryptd - Fix skcipher instance memory leak
  ACK: pavel
  - 015c205 crypto: user - prevent operating on larval algorithms
  ACK: pavel
  - 54435b7 ptrace: Fix ->ptracer_cred handling for PTRACE_TRACEME
  ACK: pavel
  - 600d371 drm/i915/dmc: protect against reading random memory
  ACK: pavel
  - 2b39351 ftrace: Fix NULL pointer dereference in free_ftrace_func_mapper()
  ACK: pavel
  - 9380441 module: Fix livepatch/ftrace module text permissions race
  ACK: pavel -- needs 214c6fe9e869 to fix it up
  - 220adcc tracing: avoid build warning with HAVE_NOP_MCOUNT
  ACK: pavel -- just a warning fix
  - 79fccb9 mm/mlock.c: change count_mm_mlocked_page_nr return type
  ACK: pavel
  - 4fce0a7 scripts/decode_stacktrace.sh: prefix addr2line with $CROSS_COMPILE
  ACK: pavel
  - b7747ec cpuset: restore sanity to cpuset_cpus_allowed_fallback()
  pavel -- behaviour change. Original behaviour is strange, but ...
  - e33aeb9 i2c: pca-platform: Fix GPIO lookup code
  ACK: pavel -- no note about testing.. was original code totally broken?
  - 7cf431e platform/mellanox: mlxreg-hotplug: Add devm_free_irq call to remove flow
  ACK: pavel
  - c241f3f platform/x86: mlx-platform: Fix parent device in i2c-mux-reg device registration
  ACK: pavel
  - f853112 platform/x86: intel-vbtn: Report switch events when event wakes device
  ACK: pavel
  - 2ac9617 platform/x86: asus-wmi: Only Tell EC the OS will handle display hotkeys from asus_nb_wmi
  ACK: pavel
  - 027e043 drm: panel-orientation-quirks: Add quirk for GPD MicroPC
  ACK: pavel
  - 2446563d drm: panel-orientation-quirks: Add quirk for GPD pocket2
  ACK: pavel
  - 8be5629 scsi: hpsa: correct ioaccel2 chaining
  ACK: pavel
  - c1bef20 SoC: rt274: Fix internal jack assignment in set_jack callback
  ACK: pavel
  - 1023af0 ALSA: hdac: fix memory release for SST and SOF drivers
  ACK: pavel
  - 26a6acd usb: gadget: udc: lpc32xx: allocate descriptor with GFP_ATOMIC
  ACK: pavel -- Error checking for failing allocation?
  - 9be058f usb: gadget: fusb300_udc: Fix memory leak of fusb300->ep[i]
  ACK: pavel -- Common cleanup function would be welcome
  - 5284327 x86/CPU: Add more Icelake model numbers
  pavel -- This adds unused defines
  - 7492908 ASoC: sun4i-i2s: Add offset to RX channel select
  ACK: pavel
  - 3247563 ASoC: sun4i-i2s: Fix sun8i tx channel offset mask
  ACK: pavel -- theoretical bug
  - 7b74863 ASoC: max98090: remove 24-bit format support if RJ is 0
  ACK: pavel
  - 3b60f98 drm/mediatek: call mtk_dsi_stop() after mtk_drm_crtc_atomic_disable()
  ACK: pavel
  - 34e5e1c drm/mediatek: clear num_pipes when unbind driver
  ACK: pavel
  - a8a86e9 drm/mediatek: call drm_atomic_helper_shutdown() when unbinding driver
  ACK: pavel
  - 79e095d drm/mediatek: unbind components in mtk_drm_unbind()
  ACK: pavel
  - 319f469 drm/mediatek: fix unbind functions
  ACK: pavel
  - dbd94f4 spi: bitbang: Fix NULL pointer dereference in spi_unregister_master
  ACK: pavel
  - 3f8d3c9 ASoC: ak4458: rstn_control - return a non-zero on error only
  ACK: pavel -- this fixes up a9c00e1fa899 commit below
  - 3c3dd68 ASoC: soc-pcm: BE dai needs prepare when pause release after resume
  ACK: pavel
  - 4c31b4b ASoC: ak4458: add return value for ak4458_probe
  ACK: pavel
  - 0c19bcd ASoC : cs4265 : readable register too low
  ACK: pavel
  - c549680 netfilter: nft_flow_offload: IPCB is only valid for ipv4 family
  ACK: pavel
  - 041c181 netfilter: nft_flow_offload: don't offload when sequence numbers need adjustment
  ACK: pavel
  - 48f611e netfilter: nft_flow_offload: set liberal tracking mode for tcp
  ACK: pavel
  - 3b2734b netfilter: nf_flow_table: ignore DF bit setting
  ACK: pavel
  - 869eec8 md/raid0: Do not bypass blocking queue entered for raid0 bios
  ACK: pavel
  - c9d8d3e block: Fix a NULL pointer dereference in generic_make_request()
  ACK: pavel
  - 5dd6139 Bluetooth: Fix faulty expression for minimum encryption key size check
  ACK: pavel -- Hmm. This is recuring theme.
