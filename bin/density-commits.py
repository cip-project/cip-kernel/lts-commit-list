#!/usr/bin/python3

import os, time, numpy

# git log origin/linux-5.10.y --date=raw --pretty=fuller

class Stats:
    def bin(m, d):
        v = int((m.now - d) / (24*60*60))
        if v < 0:
            return m.limit
        if v > m.limit:
            return m.limit
        return v
        
    def read_commits(m, ver):
        a = numpy.zeros(m.limit+1)
        print("Version", ver)
        num = 0 
        for l in os.popen("cd ../krc; git log v%s..origin/linux-%s.y --date=raw --pretty=fuller | grep ^CommitDate:" % (ver, ver)).readlines():
            date = int(l.split(" ")[1])
            date = m.bin(date)
            a[date] += 1
            num += 1
        print(num, "commits")
        return a
        
    def run(m):
        m.now = time.time()
        m.limit = 1000
        #m.read_commits("6.0")
        c = m.read_commits("5.10")
        #m.read_commits("4.19")
        o = m.read_commits("4.9")
        #m.read_commits("4.4")
        for date in range(m.limit):
            print(date, '#'*int(c[date]), '.'*int(o[date]))
        

s = Stats()
s.run()
