#!/usr/bin/python3
#
# Check, if "problematic" git repository contains buggy commits.
# Buggy commits can be identified by some other commit having
# "Fixes: buggy hash".
#
# Usage: point ../next to suitable upstream (or better next) kernel
#
# Checking we don't have buggy commits in our -cip repository:
# cd 1
# git checkout origin/linux-6.1.y-cip-rebase
# ../cl/bin/checkfixes.py

import os
import commit

class CheckFixes:
    # regular expression, set to "." to process all the commits
    filter = "ex"
    
    def __init__(m, problematic, problematic_num, upstream, upstream_num):
        m.problematic_dir = problematic
        m.upstream_dir = upstream
        m.problematic_num = problematic_num
        m.upstream_num = upstream_num

    def load_problematic(m):
        l1 = commit.List()
        l1.git_path = m.problematic_dir
        for l in os.popen("cd %s; git log --pretty=oneline | head -%d | grep -i %s" % (l1.git_path, m.problematic_num, m.filter)).readlines():
            l1.list_from_pretty(l)
        print(len(l1.commits), "commits to check")
        l1.parse_all()
        #l1.print()
        l2 = commit.List()
        for c in l1.commits:
            if c.upstream:
                cu = commit.Commit(c.upstream)
                l2.commits += [ cu ]
        print(len(l2.commits), "corresponding upstream commmits")
        m.problematic_downstream = l1
        m.problematic_upstream = l2

    def load_fixes(m):
        l1 = commit.List()
        # I'm getting utf-8 errors if I set limit to 1000000
        # 500000 works okay, but takes 2 hours
        l1.git_path = m.upstream_dir
        for l in os.popen("cd %s; git log --pretty=oneline | head -%d | grep -i %s" % (l1.git_path, m.upstream_num, m.filter)).readlines():
            l1.list_from_pretty(l)
        print(len(l1.commits), "commits from next")
        l1.parse_all()

        l2 = commit.List()
        for c in l1.commits:
            if c.fixes:
                l2.commits += [ c ]
        print(len(l2.commits), "commits fix something")
        m.fixes = l2

    def search_for_problems(m):
        if False:
            for n in m.problematic_downstream.commits:
                print(n.summary)
                print(n.upstream)

            for n in m.fixes.commits:
                print(n.summary)
                print(n.fixes)
        
        for p in m.problematic_downstream.commits:
            l = 13
            for n in m.fixes.commits:
                if p.hash[:l] == n.fixes[:l]:
                    print("Stable commit ", p.hash," appears to need an upstream fix (strange?!) -- ", n.hash)
                if p.upstream and p.upstream[:l] == n.fixes[:l]:
                    fix_backport = "MISSING"
                    msg = "MISSING FIX"
                    for f in m.problematic_downstream.commits:
                        if f.upstream and f.upstream[:l] == n.hash[:l]:
                            msg = "buggy but.."
                            fix_backport = f.hash
                    print(msg, "|", p.hash[:l], p.upstream[:l], p.summary,"|", fix_backport[:l], n.hash[:l], n.summary)
    

c = CheckFixes(".", 30000, "../next", 800000)
c.load_problematic()
c.load_fixes()
print('Cross-checking')
c.search_for_problems()

