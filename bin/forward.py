#!/usr/bin/python3
#
# Various tools useful for working with and reviewing commits
#
# - can parse commit lists from git history or linux-kernel-*-commit.list files.
# - can parse commit to learn referenced upstream commit.
# - can search for already-reviewed commits based on upstream commit.
# - work in progress, more work needed :-).
# 
# GPLv2+
# -*- python -*-

# Script to tell what needs forward-porting.
# 5.10-cip branch was created, but we already have commits from 5.11 in 4.19-cip.

# Usage: ~/cip/k$  ../cl/bin/forward.py

import os
import re
import sys
import relevant
import commit

li = commit.List()
li.git_path = "./"

print("Collecting commits")

num = 750
for l in os.popen("cd ../10; git log --pretty=oneline linux-5.10.y-cip | head -%d" % num).readlines():
    li.list_from_pretty(l)

print("Parsing commits")
li.parse_all()

print("Checking commits not in v6.1")
i = 0
for c in li.commits:
    i += 1
    if not i % 100:
        print(i)
    #print(c.summary, "?")
    if os.system("cd ../1; git merge-base --is-ancestor %s v6.1" % c.upstream):
        print(i, c.upstream, c.hash, c.summary)

