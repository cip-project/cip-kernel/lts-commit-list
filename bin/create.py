#!/usr/bin/python3
#
# GPLv2+
# -*- python -*-

import os
import re
import sys
import relevant
import commit
import sys
import subprocess

class OrgParser:
    def run(m):
        skip = True
        for l in sys.stdin.readlines():
            l = l[:-1]
            if l[:3] != "** " and l[:2] != "* ":
                continue
            sp = l.split("|")
            hash = sp[1].split(" ")[1]
            if sp[0] == "* v ":
                # Just a notice which major version this corresponds to, skip
                continue
            if sp[0] == "* x ":
                # Don't go past this point, unprepared commits are there
                return
            if sp[0] == "* s ":
                # Start backporting here
                skip = False
                continue
            if skip:
                continue
            if sp[0][0:3] != "** ":
                continue
            if sp[0] == "**   ":
                print("todo: ", l)
                continue
            if sp[0] == "** e ":
                # Commit is already in 4.4, this could happen at start of 4.4 maintainance
                #print("i: test applying ", hash)
                #os.system("git show %s | git apply --check -R" % hash)
                continue
            if sp[0] == "** p " or sp[0] == "** a ":
                # Commit needs to be applied
                os.system("git cherry-pick %s" % hash)
                continue
            if sp[0] == "** f " or sp[0] == "** i " or sp[0] == "** 7 " or sp[0] == "** 8 " or sp[0] == "** 9 " or sp[0] == "** m ":
                continue

            print("ERR: could not parse", l)

def pprint(*args):
    print(*args)
    sys.stdout.flush()

def prepare():
    l = commit.List()
    l.list_from_branch("v4.9.325^", "origin/linux-4.9.y")
    os.chdir("../4")
    for c in reversed(l.commits):
        c.applicable = " "
        pprint("Trying to apply", c.hash)
        res = subprocess.run("git cherry-pick %s" % c.hash, shell=True)
        if res.returncode == 0:
            c.applicable = "a"
        else:
            res = subprocess.run("git cherry-pick --abort", shell=True)
            if res.returncode != 0:
                pprint("### Something went really wrong aborting a cherry-pick")

    pprint("Parsing all upstream commits")
    l.parse_all()
    pprint("Determining if patch touches files relevant to CIP project")
    l.relevant_all()
    pprint("Determining if we have neccessary fixes")
    l.fixes_all()
    pprint("Printing result")
    l.print_org()

def commandline(arg):
    if arg[1] == "-p":
        prepare()
        return
    if arg[1] == "-r":
        o = OrgParser()
        o.run()
        return
    print("usage: -p -r")

if __name__ == "__main__":
    commandline(sys.argv)

