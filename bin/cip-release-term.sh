#!/bin/bash

# set -x

CIP_SUPPORT="4.4 4.19 5.10 6.1"

SCRIPT_DIR=$(cd $(dirname $0);pwd)
KDIR=${SCRIPT_DIR}/../kernel

function get_and_print_limit () {
	local cip_branch=$1

	latest_tag=$(git describe --abbrev=0 --tags cip/${cip_branch})
	release_utime=$(git log --format="%ct" ${latest_tag} -1)
	interval=${release_interval["${cip_branch}"]}
	limit_utime=$(( $release_utime + ${interval} * 24 * 60 * 60))

	echo "${cip_branch}: interval ${interval} day"
	d=$(TZ=UTC date --date "@${release_utime}")
	echo "  latest version release date: $d"
	d=$(TZ=UTC date --date "@${limit_utime}")
	echo "  limit date: $d"

	if [ ${limit_utime} -gt ${today_utime} ]  ; then
		echo "  Status: On track"
	else
		echo "  Status: late"
	fi
}

today_utime=$(date +%s)
d=$(TZ=UTC date --date "@${today_utime}")
echo "Date the data is created: $d"

declare -A release_interval=()

release_interval["linux-6.1.y-cip"]=15
release_interval["linux-6.1.y-cip-rt"]=30
release_interval["linux-5.10.y-cip"]=30
release_interval["linux-5.10.y-cip-rt"]=60
release_interval["linux-4.19.y-cip"]=30
release_interval["linux-4.19.y-cip-rt"]=60
release_interval["linux-4.4.y-cip"]=30
release_interval["linux-4.4.y-cip-rt"]=60

cd ${KDIR}
# git remote update cip

for v in ${CIP_SUPPORT} ; do

	get_and_print_limit linux-${v}.y-cip
	get_and_print_limit linux-${v}.y-cip-rt

done

