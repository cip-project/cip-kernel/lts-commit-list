#!/usr/bin/python3
#
# This takes AUTOSEL subject lines, and removes duplicities, preparing them for review.

import re
import sys

class PatchProcessor:
    def __init__(self, versions):
        self.init()

    def init(self):
        self.versions = versions
        self.patch_dict = {version: [] for version in versions}
        self.passthrough_lines = []
        self.pattern = re.compile(r"^(?:.*PATCH AUTOSEL )*(\d+\.\d+) (\d+/\d+]) (.+)")
        self.header = ""

    def read_input(self):
        lines = sys.stdin.read().strip().split("\n")
        if lines:
            self.header = lines[0]
            patches = lines[1:]
        else:
            patches = []
        
        for line in patches:
            match = self.pattern.match(line.strip())
            if line == "":
                self.process_patches()
                self.init()
                print()
                continue
            if match:
                version, patch_num, desc = match.groups()
                desc = desc[:60]
                if version in self.patch_dict:
                    self.patch_dict[version].append((patch_num, desc))
                continue
            print(line)
            #print("parse err: #", line)
        self.process_patches()
    
    def process_patches(self):
        missing_patches = [desc for num, desc in self.patch_dict[self.versions[0]] if desc not in {d for _, d in self.patch_dict.get(self.versions[1], [])}]
        
        for patch in missing_patches:
            print(f"Warning: {self.versions[0]} patch '{patch}' has no corresponding {self.versions[1]} patch!")

        print(self.header)
        ordered_results = []
        
        for version in self.versions:
            for num, desc in self.patch_dict[version]:
                if version == self.versions[1] and desc in {d for _, d in self.patch_dict[self.versions[0]]}:
                    continue
                print(f"{version} {num} {desc}")

if __name__ == "__main__":
    versions = sys.argv[1:] if len(sys.argv) > 1 else ["5.10", "6.1"]
    processor = PatchProcessor(versions)
    processor.read_input()
