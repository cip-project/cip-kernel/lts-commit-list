#!/usr/bin/python3
#
# Various tools useful for working with and reviewing commits
#
# - can parse commit lists from git history or linux-kernel-*-commit.list files.
# - can parse commit to learn referenced upstream commit.
# - can search for already-reviewed commits based on upstream commit.
# - work in progress, more work needed :-).
#
# Main options are: -b for creating list of commits that needs reviewing
#                   -m for merging it into official format
# 
# GPLv2+
# -*- python -*-

import os
import re
import sys
import relevant

git_path = "../krc"
#git_path = "/data/git/linux-stable-rc"

class Commit:
    def __init__(m, hash):
        m.hash = hash
        m.upstream = None
        m.summary = None
        m.state = None
        m.release = None
        m.comment = None
        m.fixes = None
        m.fixes_status = ":"
        m.relevant = ":"
        m.branch = None
        m.full_commit = None
        m.git_path = git_path
        m.preserve = []

    def read_commit(m):
        return os.popen("cd %s; git cat-file -p %s" % (m.git_path, m.hash)).readlines()
    
    def parse_commit(m):
        part = 0
        #print("Parsing commit", m.hash)
        for l in m.read_commit():
            l = l[:-1]
            #print(part, l)
            if part == 0:
                if l == "":
                    part = 1
                    continue

            ma = re.match("Fixes: ([0-9a-f]*).*", l)
            if ma:
                m.fixes = ma.group(1)

            if part == 1:
                if not m.summary:
                    m.summary = l
                ma = re.match(".*[Uu]pstream commit ([0-9a-f]*) .*", l)
                if ma:
                    m.upstream = ma.group(1)
                ma = re.match(".*[Cc]ommit ([0-9a-f]*) upstream[.]*.*", l)
                if ma:
                    m.upstream = ma.group(1)
                ma = re.match("[Cc]ommit: ([0-9a-f]*)", l)
                if ma:
                    m.upstream = ma.group(1)

                if re.match("^Signed-off-by:", l) \
                   or re.match("^Reported-by:", l) \
                   or re.match("^Fixes:", l) \
                   or False:
                    part = 2
                    continue

        if part < 2:
            #print("Parsing of commit", m.hash, "failed")
            pass

    def read_full(m):
        part = 0
        #print("Parsing commit", m.hash)
        m.full_commit = []
        for l in m.read_commit():
            l = l[:-1]
            
            if re.match("^commit [0-9a-f]*$", l):
                l = "commit XXX"
            if re.match("^tree [0-9a-f]*$", l):
                l = "tree XXX"
            if re.match("^parent [0-9a-f]*$", l):
                l = "parent XXX"
            if re.match("^committer ", l):
                l = "committer XXX"
            if re.match("^@@ [+-0-9,]* [+-0-9,]* @@", l):
                l = "@@ XXX @@"

            m.full_commit += [ l ]

    def short_branch(m):
        return m.branch.lstrip("origin/queue/").lstrip("linux-").rstrip(".y")
                
    def print(m):
        upstream = None
        if m.upstream:
            upstream = m.upstream[:6]

# What each line means is that $COMMIT in $VERS stable tree, which is
# backport of mainline $MAINLI, was reviewed by me with result $R.

# If $R starts if "a", I determined patch is okay ("ACK: pavel"), "v"
# means that I reviewed older version and should run interdiff to check
# it is same, and anything else means that I need to look at the patch
# some more.

        # R COMMIT    MAINLI RE VERS  TITLE
        print(" |%s %s %s%s %s| %s" % (m.hash[:9], upstream, m.relevant, m.fixes_status, m.short_branch(), m.summary))

    def print_org(m):
        upstream = None
        if m.upstream:
            upstream = m.upstream[:6]

        print("** %s | %s %s %s%s | %s" % (m.applicable, m.hash[:9], upstream, m.relevant, m.fixes_status, m.summary))

        # upstream
        # m.branch

# Looks at two commits, and compares changelogs and patches for non-trivial differences
def commits_same(h1, h2):
    c1 = Commit(h1)
    c2 = Commit(h2)
    c1.read_full()
    c2.read_full()
    f1 = c1.full_commit
    f2 = c2.full_commit
    if len(f1) != len(f2):
        print("Commits have different lengths")
        return False

    for i in range(len(f1)):
        if f1[i] != f2[i]:
            print("Commits differ at line ", i, f1[i], f2[i])
            print(f1[i])
            #return False

    if c1.full_commit != c2.full_commit:
        print("Commits differ")
        return False
    return True

class List:
    def __init__(m):
        m.commits = []
        m.git_path = git_path

    def list_from_pretty(m, l):
        s = l.split(" ")
        hash = s[0]
        summary = l[41:-1]
        c = Commit(hash)
        c.git_path = m.git_path
        c.summary = summary
        m.commits += [ c ]
        return c
        
    def list_from_git(m, num):
        for l in os.popen("cd %s; git log --pretty=oneline | head -%d" % (m.git_path, num)).readlines():
            m.list_from_pretty(l)

    def list_from_branch(m, last, br):
        for l in os.popen("cd %s; git log --pretty=oneline %s..%s" % (m.git_path, last, br)).readlines():
            c = m.list_from_pretty(l)
            c.branch = br

    def print(m):
        for c in reversed(m.commits):
            c.print()
            
    def print_org(m):
        for c in reversed(m.commits):
            c.print_org()

    def list_from_list_file(m, f, release = None):
        hash = None
        m.cur = None
        for l in f.readlines():
            l = l[:-1]
            r = re.match("^  - ([0-9a-f]*) (.*)$", l)
            if r:
                hash = r.group(1)
                m.cur = Commit(hash)
                m.cur.release = release
                m.cur.summary = r.group(2)
                m.commits += [ m.cur ]
                #m.cur.parse_commit()
                continue
            if m.cur:
                m.cur.preserve += [ l ]
                print(hash, "--", m.cur.preserve)
                continue
            # FIXME: due to l[:12], this will not work with arbitrary user.
            if l[:12] == "  ACK: " + user:
                #print("Have ack for ", hash)
                m.cur.state = "a"
                m.cur.comment = l[15:]
                continue
            if l[:12] == "  NAK: " + user:
                #print("Have nak for ", hash)
                m.cur.state = "n"
                m.cur.comment = l[15:]
                continue
            if l[:12] == "  IGN: " + user:
                #print("Have ign for ", hash)
                m.cur.state = "i"
                m.cur.comment = l[15:]
                continue
            if l[:11] == "  UR: " + user:
                #print("Have nak for ", hash)
                m.cur.state = "!"
                m.cur.comment = l[14:]
                continue
            if l[:7] == "  " + user:
                #print("Have comment for ", hash)
                m.cur.state = "?"
                m.cur.comment = l[10:]
                continue
            print(l)

    def list_from_summary(m, f, ver):
        hash = None
        m.cur = None
        for l in f.readlines():
            l = l[:-1]
            s = l.split("|")
            if len(s) < 3:
                print("Could not parse: ", l)
                continue
            t = s[1].split(" ")
            if ver != "" and t[3] != ver:
                continue
            c = Commit("00000000")
            c.hash = t[0]
            c.upstream = t[1]
            if len(s[0]) > 0:
                c.state = s[0][0]
                c.comment = s[0][1:]
            c.summary = s[2][1:]
            m.commits += [ c ]

    def write_list_file(m, f, version = "v4.19.XXX"):
        f.write("""# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## %s
""" % version)
        for c in m.commits:
            f.write("  - "+c.hash[:13]+" "+c.summary+"\n")
            for l in c.preserve:
                f.write(l + "\n")
            if c.state:
                s = ""
                if c.state == "?":
                    s = "  " + user
                elif c.state == "a":
                    s = "  ACK: " + user
                elif c.state == "n":
                    s = "  NAK: " + user
                elif c.state == "i":
                    s = "  IGN: " + user
                elif c.state == "v":
                    s = "  " + user + " -- version of this patch for older release reviewed"
                elif c.state == " ":
                    continue
                else: s = "  UR: " + user
                if c.comment:
                    s = s + " --" + c.comment.rstrip()
                s += "\n"
                f.write(s)

    def print_count(m, count, total, s):
        print("  ", count[s], "(%.0f%%)" % (100*count[s]/total), s)

    def statistics(m):
        total = 0
        count = {}
        count["a"] = 0
        count["n"] = 0
        count["?"] = 0
        count["upstream"] = 0

        for c in m.commits:
            total += 1
            if c.state:
                count[c.state] += 1
            if c.upstream:
                count["upstream"] += 1
            else:
                print("Not upstream:", c.hash, c.summary)
        print(total, "commits total;")
        m.print_count(count, total, "a")
        m.print_count(count, total, "n")
        m.print_count(count, total, "?")
        m.print_count(count, total, "upstream")

    def list_from_all_files(m):
        for l in os.popen("ls -1 ../lts-commit-list/linux-kernel-v4.*-commit.list").readlines():
            l = l[:-1]
            print("File:", l)
            f = open(l, "r")
            m.list_from_list_file(f, l)

    def same_upstream(m, c, c2):
        if c.state == c2.state:
            return
        if c.state == None:
            d = c
            d2 = c2
        elif c2.state == None:
            d = c2
            d2 = c
        else:
            print("!!! Different state and neither is empty", c.hash, c2.hash)
            return
        print("*** Commit needs new state:", d.hash, d.release, d2.hash, d2.state, d2.release)

    def parse_all(m):
        for c in m.commits:
            c.parse_commit()

    def relevant_all(m):
        r = relevant.RelevantCommit()
        r.load_review()
        for c in m.commits:
            c.relevant = r.check_files(c.hash)

    def fixes_all(m):
        for c in m.commits:
            if not c.fixes:
                c.fixes_status = " "
                continue
            if not os.system("git merge-base --is-ancestor HEAD %s" % c.fixes):
                c.fixes_status = "X"
            else:
                c.fixes_status = "+"

    def upstream(m):
        common = 0
        upstream_map = {}
        for c in m.commits:
            c.parse_commit()
            if c.upstream:
                if c.upstream in upstream_map:
                    c2 = upstream_map[c.upstream]
                    #print("Commit", c.hash, "is common in", c.hash, c2.hash)
                    m.same_upstream(c, c2)
                    common += 1
                else:
                    upstream_map[c.upstream] = c

        print(common, "commits have common upstream commit")

    # In this list, find commit with matching summary, and add review there
    # (if there's just one)
    def add_review(m, summary, review):
        num = 0
        matching = None
        for c in m.commits:
            s = c.summary[:len(summary)]
            #print(s, summary)
            if s == summary:
                matching = c
                num = num+1
        if num == 0:
            print(summary, "### no match?")
            return
        if num > 1:
            print("?! ", num, "matching commits for ", summary)
            return
        if review[0] == " ":
            return
        matching.state = review[0]
        review = review[1:]
        if review != " ":
            matching.comment = review

    # Add reviews from matching list
    def add_reviews(m, lo):
        for c in lo.commits:
            if c.state:
                m.add_review(c.summary, c.state + c.comment)

    # Review of backport for older version can be useful to determine
    # if patch is suitable for newer version
    def add_reviews_from_older(m, lo):
        for cnew in m.commits:
            if cnew.state != "v":
                continue
            cold_ = None 
            for cold in lo.commits:
                #print(cnew.hash, cnew.upstream, cold.hash, cold.upstream)
                if cnew.upstream == cold.upstream:
                    if cold_:
                        print("Two old commits with same hash?")
                    cold_ = cold
            if not cold_:
                print("Annotation says 'take review from older version' but can't find relevant commit -- ", cnew.hash, cnew.upstream, cnew.summary)
                continue
            s = commits_same(cnew.hash, cold_.hash)
            print("Two corresponding commits ", cnew.hash, cold_.hash, s)
            if s:
                cnew.state = cold_.state
                cnew.comment = cold_.comment

    def add_adhoc_review(m, f):
        for l in f.readlines():
            l = l[:-1]
            #print(l)
            r = re.match("(.*)[0-9][0-9][0-9]/[0-9][0-9][0-9]\] (.*)", l)
            #print("review: ", r.group(1), "title: ", r.group(2))
            review  = r.group(1)
            summary = r.group(2)
            m.add_review(summary, review)

    # Linear work selecting commits, preserving order in both releases
    # Does not work well in practice.
    def merge_with_old(m, lo):
        l = []
        l1 = m.commits
        l2 = lo.commits
        while True:
            found = True            
            if len(l1):
                c1 = l1[0]
                found = False
                for c2 in l2:
                    if c1.upstream == c2.upstream:
                        found = True
            if found:
                if not len(l2):
                    break
                c = l2[0]
                l += [ c ]
                l2.remove(c)
                continue
            c = l1[0]
            l += [ c ]
            l1.remove(c)
        m.commits = l

    # m.. new version
    # lo..old version
    def merge_with(m, lo):
        l = []
        l1 = m.commits
        l2 = lo.commits
        while True:
            found = True            
            if not len(l1):
                break
            
            c1 = l1[0]
            found = False
            for c2 in l2:
                if c1.upstream == c2.upstream:
                    found = True
                    c2_ = c2
            if found:
                l += [ c2_ ]
                l2.remove(c2_)

            l += [ c1 ]
            l1.remove(c1)
        m.commits = l + l2
        

def rc_to_reviews(ver, nver, branch, check_older):
    l = List()
    l.list_from_list_file(open("linux-kernel-"+ver+"-commit.list", "r"))
    lo = List()
    lo.list_from_summary(open(nver+".", "r"), branch)
    if check_older != "":
        lold = List()
        lold.list_from_summary(open("rc-"+nver+".list", "r"), check_older)
        print("Have %d commits in release; %d commits are in rc review file for %s, and %d commits for older release, %s" % (len(l.commits), len(lo.commits), branch,
 		len(lold.commits), check_older))
        lo.add_reviews_from_older(lold)

    l.add_reviews(lo)
        
    l.write_list_file(open("delme.list", "w"), ver)
    os.system("mv delme.list linux-kernel-"+ver+"-commit.list")

def full_rc_to_reviews(ver):
    rc_to_reviews(ver, ver, "", "")
    #os.system("mv rc-"+ver+".list rc-"+ver+".list.done")

def git_to_rc(num):
    l = List()
    l.list_from_git(num)
    l.parse_all()
    l.relevant_all()
    #l.fixes_all()
    l.print()

parent_v6_1 = "v6.1.125" # v6.1.111
parent_v5_10 = "00486ad0857d212e3936851f998a1a1ecc2c15a1" #
parent_v4_19 = "3c7c806b3eafd94ae0f77305a174d63b69ec187c" #
parent_v4_14 = "4369a22556166a9ac51f4f1dc57a4445de973021" # dead

def get_branch(s):
    return "origin/linux-"+s+".y"
#    return "origin/queue/"+s

def branches_to_rc():
    l = List()
    l.list_from_branch(parent_v6_1, get_branch("6.1"))
    l.parse_all()

    l1 = List()
    l1.list_from_branch(parent_v5_10, get_branch("5.10"))
    l1.parse_all()
    l.merge_with(l1)

    if False:
        l1 = List()
        l1.list_from_branch(parent_v4_19, get_branch("4.19"))
        l1.parse_all()
        l.merge_with(l1)

        l1 = List()
        l1.list_from_branch(parent_v4_14, get_branch("4.14"))
        l1.parse_all()
        l.merge_with(l1)
    
    l.relevant_all()
    l.print()

def review_mbox():
    os.system("> ../cl/review.mbox")
    vers = [ (parent_v6_1, "6.1"), (parent_v5_10, "5.10"), (parent_v4_19, "4.19"), (parent_v4_14, "4.14") ]
    for v in vers:
        parent, vers = v
        br = get_branch(vers)
        os.system("cd %s; git format-patch --stdout %s..%s >> ../cl/review.mbox" % (git_path, parent, br))

def find_duplicities():
    l = List()
    l.list_from_all_files()
    l.upstream()
    #l.statistics()
    pass

def commandline(arg):
    if arg[1] == "-n":
        # Convert files from git to review template
        i = int(arg[2])
        git_to_rc(i)
        return

    if arg[1] == "-d":
        find_duplicities()
        return

    if arg[1] == "-m":
        # Add reviews from "rc" to final format.
        # bin/commit.py -m v5.10.20
        # Use "makefile" to prepare review templates.
        full_rc_to_reviews(arg[2])
        return

    if arg[1] == "-e":
        # git log origin/queue/5.10 -- Makefile
        # git log origin/queue/4.19 -- Makefile
        # -e v4.4.123 v5.0.10
        rc_to_reviews(arg[2], arg[3], "4.19", "4.9")
        return
    
    if arg[1] == "-b":
        branches_to_rc()
        return

    if arg[1] == "-t":
        print(commits_same("9cefdbaf6c19b2465bcd4111fc481ccc6bebb57a", "4dc6cbf2019dd010453c901af52280613f358912"))
        return

    if arg[1] == "-o":
        review_mbox()
        return
        
    print("usage: -n <#commits> -m v4.19.69 -b")

if __name__ == "__main__":
    try:
        user = os.popen("git config user.email").readlines()[0].split('@')[0].split('+')[0].strip()
    except IndexError:
        user = "unknown"

    commandline(sys.argv)

