#!/usr/bin/env python3
# require python3-gitlab
#
# How to use
#  1. prepare .python-gitlab.cfg
#  2. run bin/check_tree.py
#         bin/check_tree.py --project GITLAB-PROJECT-ID --kerneldir KERNEL-SOURCE-DIR_PATH

"""
# Format for .python-gitlab.cfg
[global]
default = wiki
ssl_verify = true
timeout = 5

[wiki]
url = https://gitlab.com/
private_token = YOUR-TOKEN
"""

import argparse
import gitlab
import yaml
import json
import sys
import os
import re
import time
import io
import subprocess

class CheckTree:
    def __init__(self, git_repo, python_gitlab_path, project_id):
        self.git_repo = git_repo
        self.python_gitlab_path = python_gitlab_path
        self.project_id = project_id

    def get_latest_kernel_tag(self, branch=None):
        if branch == None:
            command = 'git describe --tags --abbrev=0'
        else:
            command = 'git describe --tags --abbrev=0 origin/linux-%s' % branch
        __command = command.split(' ')

        proc = subprocess.Popen(__command, cwd = self.git_repo, stdout = subprocess.PIPE)
        result = proc.communicate()
        return result[0].decode('utf-8').strip().replace('"', '')

    def get_prev_kernel_tag(self, branch=None):
        if branch == None:
            command = 'git describe --tags --abbrev=0 HEAD~'
        else:
            command = 'git describe --tags --abbrev=0 origin/linux-%s~' % branch
        __command = command.split(' ')

        proc = subprocess.Popen(__command, cwd = self.git_repo, stdout = subprocess.PIPE)
        result = proc.communicate()
        return result[0].decode('utf-8').strip().replace('"', '')

    def get_commit_num(self, prev = False, branch = None):
        if branch == None:
            command = 'git describe --tags HEAD'
        else:
            command = 'git describe --tags origin/linux-%s' % branch

        if prev == True:
            command = command + '~'

        __command = command.split(' ')

        proc = subprocess.Popen(__command, cwd = self.git_repo, stdout = subprocess.PIPE)
        result = proc.communicate()
        ret_str = result[0].decode('utf-8').strip().replace('"', '')

        # Check appropriately. linux kernel development only
        if "-g" in ret_str:
            data = ret_str.split('-')
            return int(data[1])

        return 0

    def get_commit_rc_subject(self, branch=None):
        if branch == None:
            command = 'git log --pretty="%s" -1'
        else:
            command = 'git log --pretty=\"%%s\" -1 origin/linux-%s' % branch
        __command = command.split(' ')

        proc = subprocess.Popen(__command, cwd = self.git_repo, stdout = subprocess.PIPE)
        result = proc.communicate()
        ret_str = result[0].decode('utf-8').strip().replace('"', '')

        # return rc version
        return ret_str.replace('Linux ', '')

    def get_commit_message(self, commit_num, branch=None):
        command = 'git checkout origin/linux-%s' % (branch)
        __command = command.split(' ')

        # Change branch 
        proc = subprocess.Popen(__command, cwd = self.git_repo, stdout = subprocess.PIPE)
        result = proc.communicate()

        # Get commit list
        basepath = os.path.dirname(os.path.abspath(__file__))
        #command = '%s/commit.py -n %d' % (basepath, commit_num)
        command = '%s/commit.py -b | tac' % (basepath)
        __command = command.split(' ')
        proc = subprocess.Popen(__command, cwd = basepath + "/../", stdout = subprocess.PIPE)
        result = proc.communicate()

        return result[0].decode('utf-8').replace('"', '')

    def get_latest_hash(self, branch=None):
        if branch == None:
            command = 'git log --pretty="%h" -1'
        else:
            command = 'git log --pretty=\"%%h\" origin/linux-%s -1' % branch
        __command = command.split(' ')
        proc = subprocess.Popen(__command, cwd = self.git_repo, stdout = subprocess.PIPE)
        result = proc.communicate()

        return result[0].decode('utf-8').strip().replace('"', '')

    def get_base_kernel_version(self, kernel_tag):
        _v = kernel_tag.replace('v', '').split('.')
        return "%s.%s.y" % (_v[0], _v[1])

    def create_wiki_page(self, title, commit_list, debug=False):
        if debug == True:
            print (title)
            print (commit_list)
            return

        gl = gitlab.Gitlab.from_config('wiki', [self.python_gitlab_path])
        project = gl.projects.get(self.project_id)
    
        page = project.wikis.create({'title': title, 'content': commit_list})

    def create_email_text(self, version):
        wiki_url_base = "https://gitlab.com/cip-project/cip-kernel/linux-cip/-/wikis"
        version_str = ""
        info = ""
        for r in range(len(version)):
            version_str += version[r]
            __base = version[r].split('.')

            info += "%s:\n" % version[r]
            info += "    - %s/%s.%s.y/%s\n" % \
                (wiki_url_base, __base[0].replace('v', ''), __base[1], version[r])
            if len(version) == 1:
                break
            if len(version) == r + 1:
                break
            version_str += " and "

        from_email = "Nobuhiro Iwamtsu <iwamatsu@nigauri.org>"
        to_email = "nobuhiro1.iwamatsu@toshiba.co.jp, pavel@denx.de, uli@fpond.eu, jan.kiszka@siemens.com"
        #to_email = "nobuhiro1.iwamatsu@toshiba.co.jp"

        message = f"""\
Subject: [CIP][LTS kernel patch review] Add {version_str}
To: {to_email}
From: {from_email}

Hi all,

The LTS patch review page has been added.

{info}
Best regards,
  Nobuhiro
"""
        return message

def _main() -> None:
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument (
        "--project", nargs="?", type=int, metavar="PROJECT-ID",
        help=(
            "select a GitLab CI project by ID"
            " (default: the last pipeline of a git branch)"
        ),
    )
    parser.add_argument (
        "--kerneldir", nargs="?", type=str, metavar="KERNEL_DIR",
        help=(
            "kernel source directory"
        ),
    )

    python_gitlab_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), '.python-gitlab.cfg') 
    status_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'kernel_status.json') 
    args = parser.parse_args()

    if not os.path.exists(python_gitlab_path):
        print ("There is no .python-gitlab.cfg. Please create.")
        sys.exit()

    if os.path.exists(status_file_path):
        with open(status_file_path, mode='rt', encoding='utf-8') as file:
            kernel_status_data = json.load(file)
    else:
        kernel_status_data = dict()
        kernel_status_data['kernel'] = [
            {'version': '5.10.y', 'hash': 'BADBEEF'}
        ]

    ct = CheckTree(args.kerneldir, python_gitlab_path, args.project)

    create_kernel_list = []
    for k in kernel_status_data['kernel']:
        b = k['version']
        commit_hash = ct.get_latest_hash(branch = b)
        if k['hash'] != commit_hash:
            k['hash'] = commit_hash

            num = ct.get_commit_num(branch = b)

            # released kernel
            if num == 0:
                num = ct.get_commit_num(branch = b, prev = True) + 1
                kernel_tag = ct.get_latest_kernel_tag(branch = b)
                #prev_kernel_tag = ct.get_prev_kernel_tag(branch = b)
            else:
                # No tag, but get version from subject 
                kernel_tag = ct.get_commit_rc_subject(branch = b)
                kernel_tag = 'v' + kernel_tag

            base_kernel_version = ct.get_base_kernel_version(kernel_tag)
            title = "%s/%s" % (base_kernel_version, kernel_tag)

            commit_list = ct.get_commit_message(num, branch = b)

            # convert to markdown
            # commit_list = re.sub('^', '* ', commit_list, flags=re.MULTILINE)
            commit_list = re.sub('^', '````\n', commit_list)
            commit_list = commit_list + '````'

            ct.create_wiki_page(title, commit_list)
            create_kernel_list.append(kernel_tag)

    with open(status_file_path, mode='wt', encoding='utf-8') as file:
        json.dump(kernel_status_data, file, ensure_ascii=False, indent=2)

    if create_kernel_list:
        message = ct.create_email_text(create_kernel_list)
        # FIXME: Requires local smtp server support.
        send_email = 'echo "%s" | msmtp -t' % message
        result = subprocess.run(send_email , shell=True)

    return

def main() -> None:
    try:
        _main()
    except (KeyboardInterrupt, BrokenPipeError):
        sys.exit(0)

if __name__ == "__main__":
    main()

