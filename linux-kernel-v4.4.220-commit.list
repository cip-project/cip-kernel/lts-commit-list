# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v4.4.220
  - 5efe91c00c98c Linux 4.4.220a
  ACK: iwamatsu
  - c5931231c51a1 x86/vdso: Fix lsl operand order
  ACK: iwamatsu
  - 27d65e5fe3786 x86/microcode/intel: replace sync_core() with native_cpuid_reg(eax)
  ACK: iwamatsu
  - eb077831ed8af x86/CPU: Add native CPUID variants returning a single datum
  ACK: iwamatsu
  - 128e99c8c35fb mtd: phram: fix a double free issue in error path
  ACK: iwamatsu
  - ea5a52ce59b22 mtd: lpddr: Fix a double free in probe()
  ACK: iwamatsu
  - c3d5d9c42c2b7 locktorture: Print ratio of acquisitions, not failures
  ACK: iwamatsu
  - a3632aea94881 tty: evh_bytechan: Fix out of bounds accesses
  ACK: iwamatsu
  - e2816c8f7725f fbdev: potential information leak in do_fb_ioctl()
  ACK: iwamatsu
  - 28a0ee956e253 iommu/amd: Fix the configuration of GCR3 table root pointer
  ACK: iwamatsu
  - 96b52bba31e44 ext2: fix empty body warnings when -Wextra is used
  ACK: iwamatsu, fix warning
  - 3b624885da854 NFS: Fix memory leaks in nfs_pageio_stop_mirroring()
  ACK: iwamatsu
  - 3da83779ecf25 compiler.h: fix error in BUILD_BUG_ON() reporting
  ACK: iwamatsu
  - dee955ffe5eec percpu_counter: fix a data race at vm_committed_as
  ACK: iwamatsu
  - 7fa41cf85dc48 ext4: do not commit super on read-only bdev
  ACK: iwamatsu
  - 4d92b525f600e NFS: direct.c: Fix memory leak of dreq when nfs_get_lock_context fails
  ACK: iwamatsu
  - 4f42e1a430305 clk: tegra: Fix Tegra PMC clock out parents
  ACK: iwamatsu
  - a91ba793a685b clk: at91: usb: continue if clk_hw_round_rate() return zero
  ACK: iwamatsu
  - 8ecea3d27f725 of: unittest: kmemleak in of_unittest_platform_populate()
  ACK: iwamatsu
  - 942d3a7dfb212 of: fix missing kobject init for !SYSFS && OF_DYNAMIC config
  ACK: iwamatsu
  - bf22e616ec960 soc: qcom: smem: Use le32_to_cpu for comparison
  ACK: iwamatsu
  - 182ce80359f20 rtc: pm8xxx: Fix issue in RTC write path
  ACK: iwamatsu
  - 22aae67745b7c wil6210: rate limit wil_rx_refill error
  ACK: iwamatsu
  - 6b1421639f9ff scsi: ufs: ufs-qcom: remove broken hci version quirk
  ACK: iwamatsu
  - 684106533da36 wil6210: fix temperature debugfs
  ACK: iwamatsu
  - 433fcbafed987 wil6210: increase firmware ready timeout
  ACK: iwamatsu
  - 3680668a3dce5 drm: NULL pointer dereference [null-pointer-deref] (CWE 476) problem
  ACK: iwamatsu
  - 01b680735a397 video: fbdev: sis: Remove unnecessary parentheses and commented code
  ACK: iwamatsu, Fix Clang warning
  - 9dbc13abe77d3 ALSA: hda: Don't release card at firmware loading error
  ACK: iwamatsu
  - 82e798f39ee8e scsi: sg: add sg_remove_request in sg_common_write
  ACK: iwamatsu
  - 7c6feb347a4bb tracing: Fix the race between registering 'snapshot' event trigger and triggering 'snapshot' operation
  ACK: iwamatsu
  - 58fb3c3589820 x86/mitigations: Clear CPU buffers on the SYSCALL fast path
  ACK: iwamatsu
  - 5706d13e270a6 kvm: x86: Host feature SSBD doesn't imply guest feature SPEC_CTRL_SSBD
  ACK: iwamatsu
  - e7ed14dae7250 dm flakey: check for null arg_name in parse_features()
  ACK: iwamatsu
  - e5e62c5c9e7f0 ext4: do not zeroout extents beyond i_disksize
  ACK: iwamatsu
  - d92b442e10d6e mac80211_hwsim: Use kstrndup() in place of kasprintf()
  ACK: iwamatsu
  - a2956cc8b66e8 ALSA: usb-audio: Don't override ignore_ctl_error value from the map
  ACK: iwamatsu
  - bccd7ba419012 ASoC: Intel: mrfld: return error codes when an error occurs
  ACK: iwamatsu
  - 2d4a7a2a44a0f ASoC: Intel: mrfld: fix incorrect check on p->sink
  ACK: iwamatsu
  - bef46dbd5acbe ext4: fix incorrect inodes per group in error message
  ACK: iwamatsu
  - 2b981db2e3e05 ext4: fix incorrect group count in ext4_fill_super error message
  ACK: iwamatsu
  - dbd194e559d81 jbd2: improve comments about freeing data buffers whose page mapping is NUL
  iwamatsu: only update comment.
  - 844031491145e scsi: ufs: Fix ufshcd_hold() caused scheduling while atomic
  ACK: iwamatsu
  - 637e005d55dca net: ipv6: do not consider routes via gateways for anycast address check
  ACK: iwamatsu
  - 773b99a0872dc net: ipv4: devinet: Fix crash when add/del multicast IP with autojoin
  ACK: iwamatsu
  - 84cc49f75290d mfd: dln2: Fix sanity checking for endpoints
  ACK: iwamatsu
  - 64c4bf0242e8b misc: echo: Remove unnecessary parentheses and simplify check for zero
  ACK: iwamatsu, Fix Clang warning.
  - a493baf2f2a2f powerpc/fsl_booke: Avoid creating duplicate tlb1 entry
  ACK: iwamatsu
  - IGN: ppc
  ACK: iwamatsu
  - 5cbf92e3c75e3 ipmi: fix hung processes in __get_guid()
  ACK: iwamatsu
  - 1cb5ab4dbdb1b drm/dp_mst: Fix clearing payload state on topology disable
  ACK: iwamatsu
  - ffe582d4956d7 Btrfs: fix crash during unmount due to race with delayed inode workers
  IGN: btrfs
  - f443b7d13c72e powerpc/64/tm: Don't let userspace set regs->trap via sigreturn
  ACK: iwamatsu
  - IGN: ppc
  ACK: iwamatsu
  - b95523968c6e1 libata: Return correct status in sata_pmp_eh_recover_pm() when ATA_DFLAG_DETACH is set
  ACK: iwamatsu
  - 19c601b9ff8aa hfsplus: fix crash and filesystem corruption when deleting files
  IGN: hfsplus
  - bc478ccf9192e kmod: make request_module() return an error when autoloading is disabled
  ACK: iwamatsu
  - f8698f812a23c Input: i8042 - add Acer Aspire 5738z to nomux list
  ACK: iwamatsu
  - bcea6552d34e2 s390/diag: fix display of diagnose call statistics
  IGN: s390
  - 07cec25b11f0b ocfs2: no need try to truncate file beyond i_size
  ACK: iwamatsu
  - d42e1f7e697a9 ext4: fix a data race at inode->i_blocks
  ACK: iwamatsu
  - 8c7cb68fb9cd3 arm64: armv8_deprecated: Fix undef_hook mask for thumb setend
  ACK: iwamatsu
  - a186c053fae71 scsi: zfcp: fix missing erp_lock in port recovery trigger for point-to-point
  ACK: iwamatsu
  - 5e20be1e47884 IB/ipoib: Fix lockdep issue found on ipoib_ib_dev_heavy_flush
  ACK: iwamatsu
  - 8c0b2d8213f07 Btrfs: incremental send, fix invalid memory access
  IGN: btrfs
  - 09d09e451358f ALSA: hda: Initialize power_state field properly
  ACK: iwamatsu
  - 3999e00756a46 xen-netfront: Rework the fix for Rx stall during OOM and network stress
  ACK: iwamatsu
  - 71b595cd27c63 futex: futex_wake_op, do not fail on invalid op
  ACK: iwamatsu
  - 776f159b8500b crypto: mxs-dcp - fix scatterlist linearization for hash
  ACK: iwamatsu
  - 38e41980e08f7 KVM: x86: Allocate new rmap and large page tracking when moving memslot
  ACK: iwamatsu
  - 9e0ec6c426683 x86/entry/32: Add missing ASM_CLAC to general_protection entry
  ACK: iwamatsu
  - 6b9e27da8fc59 signal: Extend exec_id to 64bits
  ACK: iwamatsu
  - e032e390984ea ath9k: Handle txpower changes even when TPC is disabled
  ACK: iwamatsu
  - 67c44831dd48a MIPS: OCTEON: irq: Fix potential NULL pointer dereference
  ACK: iwamatsu
  - 06bb14725731f irqchip/versatile-fpga: Apply clear-mask earlier
  ACK: iwamatsu
  - bfab354c48db9 KEYS: reaching the keys quotas correctly
  ACK: iwamatsu
  - fc58605320d97 thermal: devfreq_cooling: inline all stubs for CONFIG_DEVFREQ_THERMAL=n
  ACK: iwamatsu
  - 260207b5a3aca acpi/x86: ignore unspecified bit positions in the ACPI global lock field
  ACK: iwamatsu
  - 8f99c6cda8360 ALSA: pcm: oss: Fix regression by buffer overflow fix
  ACK: iwamatsu
  - 847842b9f6ae8 ALSA: ice1724: Fix invalid access for enumerated ctl items
  ACK: iwamatsu
  - 45d6d1df5d144 ALSA: hda: Fix potential access overflow in beep helper
  ACK: iwamatsu
  - 1746894c0b22a ALSA: hda: Add driver blacklist
  ACK: iwamatsu
  - ce0b861fa2199 ALSA: usb-audio: Add mixer workaround for TRX40 and co
  ACK: iwamatsu
  - 927bfe33e202d usb: gadget: composite: Inform controller driver of self-powered
  ACK: iwamatsu
  - 0a5849f103e2f usb: gadget: f_fs: Fix use after free issue as part of queue failure
  ACK: iwamatsu
  - ef29f2138c4d3 ASoC: topology: use name_prefix for new kcontrol
  ACK: iwamatsu
  - 9b38e368fc77d ASoC: dpcm: allow start or stop during pause for backend
  ACK: iwamatsu
  - c87efe2ddb04d ASoC: dapm: connect virtual mux with default value
  ACK: iwamatsu
  - 66bfd533a4c27 ASoC: fix regwmask
  ACK: iwamatsu
  - cb94a150eb782 misc: rtsx: set correct pcr_ops for rts522A
  ACK: iwamatsu
  - 5a22f88ca38b1 btrfs: track reloc roots based on their commit root bytenr
  ACK: iwamatsu
  - 753baee612d30 btrfs: remove a BUG_ON() from merge_reloc_roots()
  ACK: iwamatsu
  - 1d9bd981e0fe8 locking/lockdep: Avoid recursion in lockdep_count_{for,back}ward_deps()
  ACK: iwamatsu
  - 62a03e8082100 x86/boot: Use unsigned comparison for addresses
  ACK: iwamatsu
  - b5f3351760f5e gfs2: Don't demote a glock until its revokes are written
  ACK: iwamatsu
  - 38e61773c9c69 libata: Remove extra scsi_host_put() in ata_scsi_add_hosts()
  ACK: iwamatsu
  - 0d800dd4019ce selftests/x86/ptrace_syscall_32: Fix no-vDSO segfault
  ACK: iwamatsu
  - af35b1733f941 irqchip/versatile-fpga: Handle chained IRQs properly
  ACK: iwamatsu
  - 2e1d4d8de7865 i2c: st: fix missing struct parameter description
  ACK: iwamatsu
  - a7ab9af95dc5a qlcnic: Fix bad kzalloc null test
  ACK: iwamatsu
  - 1cb15d3f76bf5 net: vxge: fix wrong __VA_ARGS__ usage
  ACK: iwamatsu
  - b60f6e168f469 bus: sunxi-rsb: Return correct data when mixing 16-bit and 8-bit reads
  ACK: iwamatsu
