# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v5.10.83
  - a324ad794566 Linux 5.10.83
  - 45b42cd05391 drm/amdgpu/gfx9: switch to golden tsc registers for renoir+
  ACK: pavel -- just an optimalization
  - 98b02755d544 net: stmmac: platform: fix build warning when with !CONFIG_PM_SLEEP
  ACK: pavel
  - a15261d2a121 shm: extend forced shm destroy to support objects from several IPC nses
  UR: pavel
  - aa20e966d8a1 s390/mm: validate VMA in PGSTE manipulation functions
  - a94e4a7b77ed tty: hvc: replace BUG_ON() with negative return value
  UR: pavel -- c/e
  - 1c5f722a8fdf xen/netfront: don't trust the backend response data blindly
  UR: pavel -- check, just a robustness against Xen backend
  - 334b0f278761 xen/netfront: disentangle tx_skb_freelist
  UR: pavel
  - e17ee047eea7 xen/netfront: don't read data from request on the ring page
  UR: pavel
  - f5e493709800 xen/netfront: read response from backend only once
  ACK: pavel -- just a robustness against Xen backend
  - 1ffb20f0527d xen/blkfront: don't trust the backend response data blindly
  UR: pavel -- c/e, just a robustness against Xen backend
  - 8e147855fcf2 xen/blkfront: don't take local copy of a request from the ring page
  ACK: pavel -- just a robustness against Xen backend
  - 273f04d5d135 xen/blkfront: read response from backend only once
  ACK: pavel -- just a robustness against Xen backend
  - b98284aa3fc5 xen: sync include/xen/interface/io/ring.h with Xen's newest version
  UR: pavel -- just a preparation?
  - 406f2d5fe368 tracing: Check pid filtering when creating events
  ACK: pavel
  - 4fd0ad08ee33 vhost/vsock: fix incorrect used length reported to the guest
  ACK: pavel
  - fbc0514e1a34 iommu/amd: Clarify AMD IOMMUv2 initialization messages
  ACK: pavel -- just a printk tweaks, removes credits
  - 5655b8bccb8a smb3: do not error on fsync when readonly
  UR: pavel -- function to hide common code would be nice
  - c380062d0850 ceph: properly handle statfs on multifs setups
  ACK: pavel
  - 22423c966e02 f2fs: set SBI_NEED_FSCK flag when inconsistent node block found
  ACK: pavel -- just a robustness
  - e6ee7abd6bfe sched/scs: Reset task stack state in bringup_cpu()
  ACK: pavel -- just a KASAN fixes
  - 71e38a0c7cf8 tcp: correctly handle increased zerocopy args struct size
  ACK: pavel
  - 72f2117e450b net: mscc: ocelot: correctly report the timestamping RX filters in ethtool
  ACK: pavel
  - 73115a2b38dd net: mscc: ocelot: don't downgrade timestamping RX filters in SIOCSHWTSTAMP
  ACK: pavel
  - 62343dadbb96 net: hns3: fix VF RSS failed problem after PF enable multi-TCs
  ACK: pavel
  - 215167df4512 net/smc: Don't call clcsock shutdown twice when smc shutdown
  ACK: pavel
  - 6e800ee43218 net: vlan: fix underflow for the real_dev refcnt
  ACK: pavel
  - ae2659d2c670 net/sched: sch_ets: don't peek at classes beyond 'nbands'
  ACK: pavel
  - e3509feb46fa tls: fix replacing proto_ops
  ACK: pavel
  - 22156242b104 tls: splice_read: fix record type check
  ACK: pavel -- not a minimum fix
  - 3b6c71c097da MIPS: use 3-level pgtable for 64KB page size on MIPS_VA_BITS_48
  - a6a5d853f1e6 MIPS: loongson64: fix FTLB configuration
  - 5e823dbee23c igb: fix netpoll exit with traffic
  ACK: pavel -- just a possible WARN fix
  - f2a58ff3e3ad nvmet: use IOCB_NOWAIT only if the filesystem supports it
  ACK: pavel
  - 12ceb52f2cc4 net/smc: Fix loop in smc_listen
  ACK: pavel
  - c94cbd262b6a net/smc: Fix NULL pointer dereferencing in smc_vlan_by_tcpsk()
  ACK: pavel
  - 3d4937c6a328 net: phylink: Force retrigger in case of latched link-fail indicator
  ACK: pavel
  - 50162ff3c80f net: phylink: Force link down and retrigger resolve on interface change
  ACK: pavel
  - 95ba8f0d57ce lan743x: fix deadlock in lan743x_phy_link_status_change()
  ACK: pavel
  - c5e4316d9c02 tcp_cubic: fix spurious Hystart ACK train detections for not-cwnd-limited flows
  ACK: pavel
  - 318762309609 drm/amd/display: Set plane update flags for all planes in reset
  ACK: pavel
  - f634c755a0ee PM: hibernate: use correct mode for swsusp_close()
  UR: pavel --! needed in 4.4, too!
  - 440bd9faad29 net/ncsi : Add payload to be 32-bit aligned to fix dropped packets
  ACK: pavel -- not a minimum fix
  - ac88cb3c44b6 nvmet-tcp: fix incomplete data digest send
  ACK: pavel
  - 8889ff80fde3 net: marvell: mvpp2: increase MTU limit when XDP enabled
  ACK: pavel -- just a support for bigger MTUs
  - 90d0736876c5 mlxsw: spectrum: Protect driver from buggy firmware
  ACK: pavel -- just a robustness
  - 33d89128a960 mlxsw: Verify the accessed index doesn't exceed the array length
  ACK: pavel -- just a robustness
  - 29e1b5734795 net/smc: Ensure the active closing peer first closes clcsock
  ACK: pavel -- just a performance improvement
  - 77d9c2efa870 erofs: fix deadlock when shrink erofs slab
  ACK: pavel
  - 9f540c7ffb1e scsi: scsi_debug: Zero clear zones at reset write pointer
  ACK: pavel
  - 725ba1289508 scsi: core: sysfs: Fix setting device state to SDEV_RUNNING
  ACK: pavel
  - e65a8707b4cd ice: avoid bpf_prog refcount underflow
  ACK: pavel -- not a minimum fix
  - 1eb5395add78 ice: fix vsi->txq_map sizing
  ACK: pavel
  - 26ed13d06422 net: nexthop: release IPv6 per-cpu dsts when replacing a nexthop group
  ACK: pavel
  - 3c40584595f8 net: ipv6: add fib6_nh_release_dsts stub
  ACK: pavel -- just a preparation for next patch
  - dc2f7e9d8d20 net: stmmac: retain PTP clock time during SIOCSHWTSTAMP ioctls
  UR: pavel -- rewrite of time handling :-(.
  - 79068e6b1cfb net: stmmac: fix system hang caused by eee_ctrl_timer during suspend/resume
  UR: pavel -- nop when device may wakeup, how does this work?; causes warning, fixed below
  - cc301ad31207 nfp: checking parameter process for rx-usecs/tx-usecs is invalid
  ACK: pavel
  - 9b44cb67d387 ipv6: fix typos in __ip6_finish_output()
  ACK: pavel -- not sure someone did required checking in 4.19
  - 6d9e8dabd46f firmware: smccc: Fix check for ARCH_SOC_ID not implemented
  ACK: pavel
  - bbd1683e795c mptcp: fix delack timer
  ACK: pavel
  - 061542815af1 ALSA: intel-dsp-config: add quirk for JSL devices based on ES8336 codec
  ACK: pavel
  - f5af2def7e05 iavf: Prevent changing static ITR values if adaptive moderation is on
  UR: pavel -- c/e
  - 5dca8eff4627 net: marvell: prestera: fix double free issue on err path
  ACK: pavel
  - b33c5c828144 drm/vc4: fix error code in vc4_create_object()
  ACK: pavel
  - 2bf9c5a5039c scsi: mpt3sas: Fix kernel panic during drive powercycle test
  ACK: pavel
  - 29ecb4c0f0d7 drm/nouveau/acr: fix a couple NULL vs IS_ERR() checks
  ACK: pavel
  - 0effb7f51b65 ARM: socfpga: Fix crash with CONFIG_FORTIRY_SOURCE
  ACK: pavel -- just a tweak for FORTIFY_SOURCE
  - 86c5adc78083 NFSv42: Don't fail clone() unless the OP_CLONE operation failed
  ACK: pavel
  - c9ba7864d3a2 firmware: arm_scmi: pm: Propagate return value to caller
  ACK: pavel -- just a robustness
  - 8730a679c3cb net: ieee802154: handle iftypes as u32
  ACK: pavel
  - 2925aadd1f32 ASoC: codecs: wcd934x: return error code correctly from hw_params
  ACK: pavel -- just a robustness
  - 3a25def06de8 ASoC: topology: Add missing rwsem around snd_ctl_remove() calls
  ACK: pavel
  - 4a4f900e0415 ASoC: qdsp6: q6asm: fix q6asm_dai_prepare error handling
  UR: pavel --! are you surely sure? May return >0 on success. Does not cleanup on late failure
  - 9196a6858150 ASoC: qdsp6: q6routing: Conditionally reset FrontEnd Mixer
  UR: pavel -- c/e
  - 2be17eca48ae ARM: dts: bcm2711: Fix PCIe interrupts
  ACK: pavel -- does not fix user-visible problem
  - 9db1d4a3c270 ARM: dts: BCM5301X: Add interrupt properties to GPIO node
  ACK: pavel -- not sure if it fixes user-visible problem
  - b2cd6fdcbe0a ARM: dts: BCM5301X: Fix I2C controller interrupt
  ACK: pavel
  - b7ef25e8c271 netfilter: flowtable: fix IPv6 tunnel addr match
  ACK: pavel
  - d689176e0e18 netfilter: ipvs: Fix reuse connection if RS weight is 0
  ACK: pavel
  - 994065f6efdc netfilter: ctnetlink: do not erase error code with EINVAL
  ACK: pavel -- just an error code tweak
  - a3d829e5f375 netfilter: ctnetlink: fix filtering with CTA_TUPLE_REPLY
  ACK: pavel
  - a8a917058faf proc/vmcore: fix clearing user buffer by properly using clear_user()
  ACK: pavel
  - 1f520a0d78fc PCI: aardvark: Fix link training
  UR: pavel
  - aec0751f61f5 PCI: aardvark: Simplify initialization of rootcap on virtual bridge
  ACK: pavel -- just a cleanup
  - df5748098878 PCI: aardvark: Implement re-issuing config requests on CRS response
  UR: pavel
  - e7f2e2c758ea PCI: aardvark: Update comment about disabling link training
  ACK: pavel -- just a whitespace tweak
  - 2b7bc1c4b2c8 PCI: aardvark: Deduplicate code in advk_pcie_rd_conf()
  UR: pavel --a just a cleanup / preparation?
  - dfe906da9a1a powerpc/32: Fix hardlockup on vmap stack overflow
  ACK: pavel -- just a robustness
  - bf00edd9e6c9 mdio: aspeed: Fix "Link is Down" issue
  UR: pavel -- c/e
  - 14c3ce30ddbd mmc: sdhci: Fix ADMA for PAGE_SIZE >= 64KiB
  UR: pavel --a comment style, assert that max_adma >= 32K?
  - 63195705b334 mmc: sdhci-esdhc-imx: disable CMDQ support
  ACK: pavel -- just a temporary workaround
  - 092a58f0d9ef tracing: Fix pid filtering when triggers are attached
  ACK: pavel -- not a minimum fix
  - 68fa6bf7f179 tracing/uprobe: Fix uprobe_perf_open probes iteration
  ACK: pavel
  - b777c866aafc KVM: PPC: Book3S HV: Prevent POWER7/8 TLB flush flushing SLB
  ACK: pavel
  - bfed9c2f2f2e xen: detect uninitialized xenbus in xenbus_init
  ACK: pavel -- two fixes in one
  - e1d492c27519 xen: don't continue xenstore initialization in case of errors
  UR: pavel -- check err is initialized
  - 8f4d0719f323 fuse: release pipe buf after last use
  ACK: pavel
  - 8d0163cec7de staging: rtl8192e: Fix use after free in _rtl92e_pci_disconnect()
  - 0bfed81b2ccd staging: greybus: Add missing rwsem around snd_ctl_remove() calls
  - 146283f16b7e staging/fbtft: Fix backlight
  - 8fc5e3c7cacc HID: wacom: Use "Confidence" flag to prevent reporting invalid contacts
  ACK: pavel
  - 6ca32e2e776e Revert "parisc: Fix backtrace to always include init funtion names"
  ACK: pavel
  - 3a4aeb37a7a6 media: cec: copy sequence field for the reply
  ACK: pavel
  - 3798218a1af4 ALSA: hda/realtek: Fix LED on HP ProBook 435 G7
  ACK: pavel
  - 60274e248e3d ALSA: hda/realtek: Add quirk for ASRock NUC Box 1100
  ACK: pavel
  - 172167bc8dac ALSA: ctxfi: Fix out-of-range access
  ACK: pavel -- pretty sure bug was not user-visible
  - 4402cf040252 binder: fix test regression due to sender_euid change
  UR: pavel
  - aea184ae6408 usb: hub: Fix locking issues with address0_mutex
  UR: pavel
  - 5bf3a0c7789e usb: hub: Fix usb enumeration issue due to address0 race
  UR: pavel
  - 00f1038c72f8 usb: typec: fusb302: Fix masking of comparator and bc_lvl interrupts
  ACK: pavel
  - 56fbab4937e0 usb: chipidea: ci_hdrc_imx: fix potential error pointer dereference in probe
  ACK: pavel
  - b70ff391deee net: nexthop: fix null pointer dereference when IPv6 is not enabled
  ACK: pavel -- "interesting" code
  - 0755f3f32277 usb: dwc3: gadget: Fix null pointer exception
  ACK: pavel
  - 140e2df472ba usb: dwc3: gadget: Check for L1/L2/U3 for Start Transfer
  ACK: pavel -- just a warning fix
  - 3abf746e800b usb: dwc3: gadget: Ignore NoStream after End Transfer
  ACK: pavel
  - 2b7ab82f5173 usb: dwc2: hcd_queue: Fix use of floating point literal
  ACK: pavel
  - 4b18ccad9671 usb: dwc2: gadget: Fix ISOC flow for elapsed frames
  UR: pavel --a elimiating duplicated code might be reasonable
  - 16f1cac8f702 USB: serial: option: add Fibocom FM101-GL variants
  ACK: pavel
  - ff721286369e USB: serial: option: add Telit LE910S1 0x9200 composition
  ACK: pavel
  - 854c14b2a15c ACPI: Get acpi_device's parent from the parent field
  ACK: pavel
  - 33fe044f6a9e bpf: Fix toctou on read-only map's constant scalar tracking
  ACK: pavel
