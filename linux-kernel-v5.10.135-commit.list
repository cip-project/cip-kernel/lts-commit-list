# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v5.10.135
  - 4fd9cb57a3f5 Linux 5.10.135
  - 4bfc9dc60873 selftests: bpf: Don't run sk_lookup in verifier tests
  ACK: pavel
  - 6d3fad2b44eb bpf: Add PROG_TEST_RUN support for sk_lookup programs
  ACK: pavel
  - 6aad811b37ee bpf: Consolidate shared test timing code
  UR: pavel --! calling variable i is strange,
  - 545fc3524ccc x86/bugs: Do not enable IBPB at firmware entry when IBPB is not available
  ACK: pavel
  - 14b494b7aaf2 xfs: Enforce attr3 buffer recovery order
  ACK: pavel
  - e5f9d4e0f895 xfs: logging the on disk inode LSN can make it go backwards
  UR: pavel -- check we have prerequisites, lSN->LSN?
  - c1268acaa0dd xfs: remove dead stale buf unpin handling code
  UR: pavel -- do we have prerequisites?
  - c85cbb0b21a1 xfs: hold buffer across unpin and potential shutdown processing
  ACK: pavel
  - d8f5bb0a09b7 xfs: force the log offline when log intent item recovery fails
  ACK: pavel
  - eccacbcbfd70 xfs: fix log intent recovery ENOSPC shutdowns when inactivating inodes
  ACK: pavel
  - 17c8097fb041 xfs: prevent UAF in xfs_log_item_in_current_chkpt
  ACK: pavel
  - 6d3605f84edd xfs: xfs_log_force_lsn isn't passed a LSN
  ACK: pavel -- just a preparation, buggy -- fixed by next patch
  - 41fbfdaba94a xfs: refactor xfs_file_fsync
  UR: pavel -- just a preparation
  - aadc39fd5b6d docs/kernel-parameters: Update descriptions for "mitigations=" param with retbleed
  ACK: pavel -- just a documentation fix
  - c4cd52ab1e6d EDAC/ghes: Set the DIMM label unconditionally
  ACK: pavel
  - c45463917205 ARM: 9216/1: Fix MAX_DMA_ADDRESS overflow
  ACK: pavel
  - e500aa9f2d76 mt7601u: add USB device ID for some versions of XiaoDu WiFi Dongle.
  ACK: pavel
  - 2670f76a5631 page_alloc: fix invalid watermark check on a negative value
  - 8014246694bb ARM: crypto: comment out gcc warning that breaks clang builds
  UR: pavel
  - 6f3505588d66 sctp: leave the err path free in sctp_stream_init to sctp_stream_free
  UR: pavel -- not a minimum fix
  - 510e5b3791f6 sfc: disable softirqs for ptp TX
  UR: pavel -- notes that bug does not exist
  - 3ec42508a67b perf symbol: Correct address for bss symbols
  ACK: pavel
  - 6807897695d4 virtio-net: fix the race between refill work and close
  UR: pavel --! wow. We have milder locking primitives for this
  - 440dccd80f62 netfilter: nf_queue: do not allow packet truncation below transport header offset
  ACK: pavel
  - aeb2ff9f9f70 sctp: fix sleep in atomic context bug in timer handlers
  ACK: pavel
  - fad6caf9b19f i40e: Fix interface init with MSI interrupts (no MSI-X)
  ACK: pavel
  - e4a7acd6b443 tcp: Fix data-races around sysctl_tcp_reflect_tos.
  ACK: uli
  - f310fb69a0a8 tcp: Fix a data-race around sysctl_tcp_comp_sack_nr.
  ACK: uli
  - d2476f2059c2 tcp: Fix a data-race around sysctl_tcp_comp_sack_slack_ns.
  ACK: uli
  - 483239789127 tcp: Fix a data-race around sysctl_tcp_comp_sack_delay_ns.
  ACK: uli
  - 530a4da37ece net: macsec: fix potential resource leak in macsec_add_rxsa() and macsec_add_txsa()
  ACK: uli
  - 6e0e0464f1da macsec: always read MACSEC_SA_ATTR_PN as a u64
  ACK: uli
  - 2daf0a1261c7 macsec: limit replay window size with XPN
  ACK: uli
  - 0755c9d05ab2 macsec: fix error message in macsec_add_rxsa and _txsa
  ACK: uli
  - 54c295a30f00 macsec: fix NULL deref in macsec_add_rxsa
  ACK: uli
  - 034bfadc8f51 Documentation: fix sctp_wmem in ip-sysctl.rst
  ACK: uli -- docs only
  - 4aea33f40459 tcp: Fix a data-race around sysctl_tcp_invalid_ratelimit.
  ACK: uli
  - c4e6029a85c8 tcp: Fix a data-race around sysctl_tcp_autocorking.
  ACK: uli
  - 83edb788e69a tcp: Fix a data-race around sysctl_tcp_min_rtt_wlen.
  ACK: uli
  - f47e7e5b49e3 tcp: Fix a data-race around sysctl_tcp_min_tso_segs.
  ACK: uli
  - 5584fe9718a4 net: sungem_phy: Add of_node_put() for reference returned by of_get_parent()
  ACK: uli
  - b399ffafffba igmp: Fix data-races around sysctl_igmp_qrv.
  ACK: uli
  - 4c1318dabeb9 net/tls: Remove the context from the list in tls_device_down
  ACK: uli
  - 8008e797ec6f ipv6/addrconf: fix a null-ptr-deref bug for ip6_ptr
  ACK: pavel
  - a84b8b53a50b net: ping6: Fix memleak in ipv6_renew_options().
  ACK: pavel
  - c37c7f35d7b7 tcp: Fix a data-race around sysctl_tcp_challenge_ack_limit.
  ACK: pavel
  - 9ffb4fdfd80a tcp: Fix a data-race around sysctl_tcp_limit_output_bytes.
  ACK: pavel
  - 3e933125830a tcp: Fix data-races around sysctl_tcp_moderate_rcvbuf.
  ACK: pavel
  - 77ac046a9ad3 Revert "tcp: change pingpong threshold to 3"
  UR: pavel
  - 54a73d65440e scsi: ufs: host: Hold reference returned by of_parse_phandle()
  ACK: pavel
  - 160f79561e87 ice: do not setup vlan for loopback VSI
  ACK: uli
  - 9ed6f97c8d77 ice: check (DD | EOF) bits on Rx descriptor rather than (EOP | RS)
  ACK: uli
  - 2b4b373271e5 tcp: Fix data-races around sysctl_tcp_no_ssthresh_metrics_save.
  ACK: uli
  - 3fb21b67c0fc tcp: Fix a data-race around sysctl_tcp_nometrics_save.
  ACK: uli
  - 81c45f49e678 tcp: Fix a data-race around sysctl_tcp_frto.
  ACK: uli
  - 312ce3901fd8 tcp: Fix a data-race around sysctl_tcp_adv_win_scale.
  ACK: uli
  - 3cddb7a7a5d5 tcp: Fix a data-race around sysctl_tcp_app_win.
  ACK: uli
  - f10a5f905a97 tcp: Fix data-races around sysctl_tcp_dsack.
  ACK: uli
  - 7fa8999b3167 watch_queue: Fix missing locking in add_watch_to_object()
  ACK: uli
  - 45a84f04a9a0 watch_queue: Fix missing rcu annotation
  ACK: uli
  - b38a8802c52d nouveau/svm: Fix to migrate all requested pages
  ACK: uli
  - bd46ca41461b s390/archrandom: prevent CPACF trng invocations in interrupt context
  IGN: uli
  - 1228934cf259 ntfs: fix use-after-free in ntfs_ucsncmp()
  UR: pavel --! does it need checking against overflows?
  - 5528990512a2 Revert "ocfs2: mount shared volume without ha stack"
  UR: pavel -- why did we have it in stable?
  - de5d4654ac6c Bluetooth: L2CAP: Fix use-after-free caused by l2cap_chan_put
  ACK: pavel
