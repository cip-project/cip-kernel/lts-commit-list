# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v6.1.31
  - d2869ace6eeb8 Linux 6.1.31
  - 2f32b89d81208 net: phy: mscc: add VSC8502 to MODULE_DEVICE_TABLE
  ACK: pavel -- just a autoloading fix
  - 3bcb97e4241b7 3c589_cs: Fix an error handling path in tc589_probe()
  ACK: pavel
  - 9540765d1882d net/smc: Reset connection when trying to use SMCRv2 fails.
  ACK: pavel
  - be4022669e66f regulator: mt6359: add read check for PMIC MT6359
  ACK: pavel -- just a robustness
  - 22157f7445810 firmware: arm_ffa: Set reserved/MBZ fields to zero in the memory descriptors
  ACK: pavel -- it would be better to just memset
  - 1ae70faa86fd0 arm64: dts: imx8mn-var-som: fix PHY detection bug by adding deassert delay
  ACK: pavel
  - 3e8a82fb55a64 net/mlx5: Devcom, serialize devcom registration
  - eaa365c104590 net/mlx5: Devcom, fix error flow in mlx5_devcom_register_device
  - 411e4d6caa7f7 net/mlx5: Collect command failures data only for known commands
  - 390aa5c006b3d net/mlx5: Fix error message when failing to allocate device memory
  - 59dd110ca2413 net/mlx5: DR, Check force-loopback RC QP capability independently from RoCE
  - b17294e7aa8c3 net/mlx5: Handle pairing of E-switch via uplink un/load APIs
  - e501ab136691c net/mlx5: DR, Fix crc32 calculation to work on big-endian (BE) CPUs
  - 6f0dce5f7822f net/mlx5e: do as little as possible in napi poll when budget is 0
  UR: pavel -- c/e
  - 00959a1bad58e net/mlx5e: Use correct encap attribute during invalidation
  - 362063df6ceec net/mlx5e: Fix deadlock in tc route query code
  - 2051f762c5080 net/mlx5e: Fix SQ wake logic in ptp napi_poll context
  - 47b4f741a3f6e platform/mellanox: mlxbf-pmc: fix sscanf() error checking
  - 04238c23853a2 forcedeth: Fix an error handling path in nv_probe()
  ACK: pavel
  - 0392c9185d712 sctp: fix an issue that plpmtu can never go to complete state
  ACK: pavel
  - c9e09b070d0b2 cxl: Wait Memory_Info_Valid before access memory related info
  UR: pavel
  - ad72cb5899ce5 ASoC: Intel: avs: Access path components under lock
  ACK: pavel
  - 6ae9cf40b4e0b ASoC: Intel: avs: Fix declaration of enum avs_channel_config
  ACK: pavel -- just a cleanup
  - 5eaaad19c82c1 ASoC: Intel: Skylake: Fix declaration of enum skl_ch_cfg
  ACK: pavel
  - d8cfe5ccc98e9 x86/show_trace_log_lvl: Ensure stack pointer is aligned, again
  ACK: pavel
  - a7edc86e149e8 xen/pvcalls-back: fix double frees with pvcalls_new_active_socket()
  ACK: pavel
  - 53384076f7433 x86/pci/xen: populate MSI sysfs entries
  ACK: pavel
  - 84b211b028948 ARM: dts: imx6qdl-mba6: Add missing pvcie-supply regulator
  ACK: pavel
  - 225a5f394b09d coresight: Fix signedness bug in tmc_etr_buf_insert_barrier_packet()
  ACK: pavel
  - 55224690958cd platform/x86: ISST: Remove 8 socket limit
  ACK: pavel
  - f34428b5a3737 regulator: pca9450: Fix BUCK2 enable_mask
  ACK: pavel -- just a cleanup
  - ccc6e9ded63bf fs: fix undefined behavior in bit shift for SB_NOUSER
  ACK: pavel -- no enduser impact, not a minimum fix
  - dfc5aaa57f52a firmware: arm_ffa: Fix FFA device names for logical partitions
  ACK: pavel -- will rename partitions in /devices/arm-ffa-8001
  - ad73dc7263ea9 firmware: arm_ffa: Check if ffa_driver remove is present before executing
  ACK: pavel
  - 06ec5be891183 optee: fix uninited async notif value
  ACK: pavel
  - 9c744c6ff238f power: supply: sbs-charger: Fix INHIBITED bit for Status reg
  ACK: pavel
  - 71e60a58d7f6e power: supply: bq24190: Call power_supply_changed() after updating input current
  ACK: pavel
  - 1f02bfd5d94c2 power: supply: bq25890: Call power_supply_changed() after updating input current or voltage
  ACK: pavel
  - 57842035d2987 power: supply: bq27xxx: After charger plug in/out wait 0.5s for things to stabilize
  ACK: pavel
  - 221f7cb122852 power: supply: bq27xxx: Ensure power_supply_changed() is called on current sign changes
  ACK: pavel
  - 3c573e7910c60 power: supply: bq27xxx: Move bq27xxx_battery_update() down
  ACK: pavel -- just a preparation
  - 9108ede08d7ae power: supply: bq27xxx: Add cache parameter to bq27xxx_battery_current_and_status()
  UR: pavel -- just a preparation
  - d952a1eaafcc5 power: supply: bq27xxx: Fix poll_interval handling and races on remove
  ACK: pavel
  - e65fee45687fa power: supply: bq27xxx: Fix I2C IRQ race on remove
  ACK: pavel
  - d746fbf4f09c1 power: supply: bq27xxx: Fix bq27xxx_battery_update() race condition
  UR: pavel
  - e1073f81478fc power: supply: mt6360: add a check of devm_work_autocancel in mt6360_charger_probe
  ACK: pavel
  - 2ac38f130e5b3 power: supply: leds: Fix blink to LED on transition
  ACK: pavel
  - 94373413e13d3 cifs: mapchars mount option ignored
  ACK: pavel -- just an interface tweak
  - 91dd8aab9c9f1 ipv6: Fix out-of-bounds access in ipv6_find_tlv()
  ACK: pavel
  - 9bc1dbfd9158c lan966x: Fix unloading/loading of the driver
  ACK: pavel
  - 1a9e80f757bbb bpf: fix a memory leak in the LRU and LRU_PERCPU hash maps
  ACK: pavel
  - 177ee41f6162b bpf: Fix mask generation for 32-bit narrow loads of 64-bit fields
  ACK: pavel
  - a1d7c357f4dc4 octeontx2-pf: Fix TSOv6 offload
  ACK: pavel
  - 4883d9e2a2212 selftests: fib_tests: mute cleanup error message
  - 722af06e6100c drm: fix drmm_mutex_init()
  ACK: pavel
  - cc18b4685910d net: fix skb leak in __skb_tstamp_tx()
  ACK: pavel
  - 8d81d3b0ed361 ASoC: lpass: Fix for KASAN use_after_free out of bounds
  UR: pavel -- c/e, less '!'s would be welcome
  - 53764a17f5d8f media: radio-shark: Add endpoint checks
  ACK: pavel -- just a robustness against bad hw
  - d5dba4b7bf904 USB: sisusbvga: Add endpoint checks
  ACK: pavel -- just a robustness against bad hw
  - 09e9d1f52f974 USB: core: Add routines for endpoint checks in old drivers
  UR: pavel -- just a preparation
  - 2a112f04629f7 udplite: Fix NULL pointer dereference in __sk_mem_raise_allocated().
  ACK: pavel
  - ed66e6327a69f net: fix stack overflow when LRO is disabled for virtual interfaces
  ACK: pavel
  - c8fdf7feca77c fbdev: udlfb: Fix endpoint check
  ACK: pavel -- just a robustness against bad hw
  - d7fff52c99d52 debugobjects: Don't wake up kswapd from fill_pool()
  ACK: pavel
  - 8694853768e36 irqchip/mips-gic: Use raw spinlock for gic_lock
  ACK: pavel
  - dc1b7641a9893 irqchip/mips-gic: Don't touch vl_map if a local interrupt is not routable
  ACK: pavel
  - 4ca6b06e9be25 x86/topology: Fix erroneous smp_num_siblings on Intel Hybrid platforms
  ACK: pavel -- just a support for big/little
  - ed0ef89508d26 perf/x86/uncore: Correct the number of CHAs on SPR
  ACK: pavel
  - f3078be2febb7 drm/amd/amdgpu: limit one queue per gang
  - 34570f85a282e selftests/memfd: Fix unknown type name build failure
  - 931ea1ed31be9 binder: fix UAF of alloc->vma in race with munmap()
  ACK: pavel
  - e1e198eff1fba binder: fix UAF caused by faulty buffer cleanup
  ACK: pavel
  - d7cee853bcb07 binder: add lockless binder_alloc_(set|get)_vma()
  ACK: pavel
  - 72a94f8c14a1e Revert "android: binder: stop saving a pointer to the VMA"
  ACK: pavel -- just a performance tweak, fixed below
  - 7e6b8548549e1 Revert "binder_alloc: add missing mmap_lock calls when using the VMA"
  ACK: pavel -- just a performance tweak, reintroduces bug, fixed below
  - 8069bcaa5b392 drm/amd/pm: Fix output of pp_od_clk_voltage
  UR: pavel -- check. adding einval to size
  - 6acfbdda4d06d drm/amd/pm: add missing NotifyPowerSource message mapping for SMU13.0.7
  ACK: pavel
  - 8756863c7fe0f drm/radeon: reintroduce radeon_dp_work_func content
  ACK: pavel
  - 3897ac532af08 drm/mgag200: Fix gamma lut not initialized.
  ACK: pavel
  - 3970ee926e7ef dt-binding: cdns,usb3: Fix cdns,on-chip-buff-size type
  ACK: pavel -- just a documentation fix
  - 937264cd9aab5 btrfs: use nofs when cleaning up aborted transactions
  ACK: pavel
  - 63e12910b7f5e gpio: mockup: Fix mode of debugfs files
  ACK: pavel -- just an interface tweak
  - b49706d1799ab parisc: Handle kprobes breakpoints only in kernel context
  - 5596e2ef5f1a4 parisc: Enable LOCKDEP support
  - d935edd510d78 parisc: Allow to reboot machine after system halt
  - c49ffd89b66e8 parisc: Fix flush_dcache_page() for usage from irq context
  - c0993b463fe75 parisc: Handle kgdb breakpoints only in kernel context
  - e1f14a4071406 parisc: Use num_present_cpus() in alternative patching code
  - bd90ac0002d1a xtensa: add __bswap{si,di}2 helpers
  ACK: pavel -- just a gcc-13 support
  - 522bbbfcb612b xtensa: fix signal delivery to FDPIC process
  ACK: pavel
  - 0845660508912 m68k: Move signal frame following exception on 68020/030
  ACK: pavel
  - 6147745d43ff4 net: cdc_ncm: Deal with too low values of dwNtbOutMaxSize
  UR: pavel -- just a robustness/cleanup
  - da1e8adab3665 ASoC: rt5682: Disable jack detection interrupt during suspend
  ACK: pavel
  - 72c28207c19c2 power: supply: bq25890: Fix external_power_changed race
  ACK: pavel
  - 0456b912121e4 power: supply: axp288_fuel_gauge: Fix external_power_changed race
  ACK: pavel
  - 7d5e0150eeec9 mmc: block: ensure error propagation for non-blk
  ACK: pavel -- not sure this fixes user-visible bug
  - a24aec210aa53 mmc: sdhci-esdhc-imx: make "no-mmc-hs400" works
  ACK: pavel
  - 0d97634ad4988 SUNRPC: Don't change task->tk_status after the call to rpc_exit_task
  ACK: pavel
  - 40599969ff585 ALSA: hda/realtek: Enable headset onLenovo M70/M90
  ACK: pavel
  - 7d3d306f159e7 ALSA: hda: Fix unhandled register update during auto-suspend period
  ACK: pavel
  - 5222e81afa268 ALSA: hda/ca0132: add quirk for EVGA X299 DARK
  ACK: pavel
  - 688c9af6e5fc5 platform/x86/intel/ifs: Annotate work queue on stack so object debug does not complain
  ACK: pavel -- just a warning workaround
  - c26b9e193172f x86/mm: Avoid incomplete Global INVLPG flushes
  ACK: pavel -- just a hw bug workaround
  - 4eb600f386efd arm64: Also reset KASAN tag if page is not PG_mte_tagged
  ACK: pavel
  - 8bdf47f9dbeac ocfs2: Switch to security_inode_init_security()
  UR: pavel --!a just a cleanup, needed?
  - 28ee628fff1e7 drm/amd/display: hpd rx irq not working with eDP interface
  ACK: pavel -- not a minimum fix
  - 7bfd4c0ebcb4f net: dsa: mv88e6xxx: Add RGMII delay to 88E6320
  ACK: pavel
  - 66ede2e4235f8 platform/x86: hp-wmi: Fix cast to smaller integer type warning
  ACK: pavel -- just a warning workaround
  - 0dbc898f5917c skbuff: Proactively round up to kmalloc bucket size
  UR: pavel -- just a tweak for sanitizers
  - ac2f5739fdca4 drm/amdgpu/mes11: enable reg active poll
  ACK: pavel -- inadequate changelog
  - a2fe4534bb38f drm/amd/amdgpu: update mes11 api def
  ACK: pavel -- just a preparation
  - ae9e65319f99b watchdog: sp5100_tco: Immediately trigger upon starting.
  ACK: pavel
  - 7cd46930b8bf3 tpm: Prevent hwrng from activating during resume
  NAK: pavel -- woodo mb, would need to be before statement, confused code
  - 25d38d5eaa1f7 tpm: Re-enable TPM chip boostrapping non-tpm_tis TPM drivers
  ACK: pavel
  - e76f61a2c523b tpm, tpm_tis: startup chip before testing for interrupts
  UR: pavel -- c/e, buggy, fixed by commit below
  - 9953dbf65f92b tpm_tis: Use tpm_chip_{start,stop} decoration inside tpm_tis_resume
  ACK: pavel -- redundant if
  - c5a5d33886a73 tpm, tpm_tis: Only handle supported interrupts
  ACK: pavel -- not a minimum fix
  - 5c4c8075bc8a2 tpm, tpm_tis: Avoid cache incoherency in test for interrupts
  NAK: pavel -- the code is quite confused, test_bit takes bit number
  - 1ec145277a262 usb: dwc3: fix gadget mode suspend interrupt handler issue
  ACK: pavel
