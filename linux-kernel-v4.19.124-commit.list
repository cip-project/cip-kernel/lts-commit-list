# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v4.19.124
  - 1bab61d3e8cd Linux 4.19.124
  - bf7d61e56eb5 Makefile: disallow data races on gcc-10 as well
  ACK: pavel -- just a workaround for new gcc
  - 7709b2c659fa KVM: x86: Fix off-by-one error in kvm_vcpu_ioctl_x86_setup_mce
  ACK: pavel
  - 1c87f4f38d73 ARM: dts: r8a7740: Add missing extal2 to CPG node
  ACK: pavel -- does not have an impact on boards supported by in-tree code
  - 48ca4ccf0275 arm64: dts: renesas: r8a77980: Fix IPMMU VIP[01] nodes
  ACK: pavel
  - 609d20896e00 ARM: dts: r8a73a4: Add missing CMT1 interrupts
  ACK: pavel
  - 1af815dfa636 arm64: dts: rockchip: Rename dwc3 device nodes on rk3399 to make dtc happy
  ACK: pavel -- just a warning fix
  - 1178a33eb23b arm64: dts: rockchip: Replace RK805 PMIC node name with "pmic" on rk3328 boards
  ACK: pavel -- cleanup, not a bugfix
  - 11b6715f971f clk: Unlink clock if failed to prepare or enable
  ACK: pavel
  - eea2b01912c1 Revert "ALSA: hda/realtek: Fix pop noise on ALC225"
  ACK: pavel
  - 3655034daad1 usb: gadget: legacy: fix error return code in cdc_bind()
  ACK: pavel
  - d08742fefe87 usb: gadget: legacy: fix error return code in gncm_bind()
  ACK: pavel
  - c570aea01396 usb: gadget: audio: Fix a missing error return value in audio_bind()
  ACK: pavel
  - 804bbfc3f28c usb: gadget: net2272: Fix a memory leak in an error handling path in 'net2272_plat_probe()'
  ACK: pavel
  - 5ac0e17eba00 dwc3: Remove check for HWO flag in dwc3_gadget_ep_reclaim_trb_sg()
  ACK: pavel
  - 4ebb32efb638 clk: rockchip: fix incorrect configuration of rk3228 aclk_gpu* clocks
  UR: pavel
  - bfdb18282b6f exec: Move would_dump into flush_old_exec
  ACK: pavel
  - 7f95c1106041 x86/unwind/orc: Fix error handling in __unwind_start()
  ACK: pavel
  - 15b4f26b7590 x86: Fix early boot crash on gcc-10, third try
  NAK: pavel -- uses crazy code to workaround compiler problem. mb() should be barrier().
  - ad149b6e08f1 cifs: fix leaked reference on requeued write
  ACK: pavel
  - 643ca7097dae ARM: dts: imx27-phytec-phycard-s-rdk: Fix the I2C1 pinctrl entries
  ACK: pavel
  - 7abecb94bc72 ARM: dts: dra7: Fix bus_dma_limit for PCIe
  ACK: pavel
  - 563cdec83585 usb: xhci: Fix NULL pointer dereference when enqueuing trbs from urb sg list
  ACK: pavel
  - a105bb549252 USB: gadget: fix illegal array access in binding with UDC
  ACK: pavel
  - 8fd38d318e15 usb: host: xhci-plat: keep runtime active when removing host
  ACK: pavel
  - 073a30cb2e68 usb: core: hub: limit HUB_QUIRK_DISABLE_AUTOSUSPEND to USB5534B
  ACK: pavel
  - e5c0fbcd2cb5 ALSA: usb-audio: Add control message quirk delay for Kingston HyperX headset
  ACK: pavel
  - a507658fdb2a ALSA: rawmidi: Fix racy buffer resize under concurrent accesses
  ACK: pavel
  - f8685c334d53 ALSA: hda/realtek - Limit int mic boost for Thinkpad T530
  ACK: pavel
  - f81c4cc9b040 gcc-10: avoid shadowing standard library 'free()' in crypto
  ACK: pavel -- just a warning workaround
  - 28b0bceefe0d gcc-10: disable 'restrict' warning for now
  ACK: pavel -- just a patch to disable warnings
  - 8a5530c2f0c6 gcc-10: disable 'stringop-overflow' warning for now
  ACK: pavel -- just a patch to disable warnings
  - fa8487621f6d gcc-10: disable 'array-bounds' warning for now
  ACK: pavel -- just a patch to disable warnings
  - 7f43fca7ea24 gcc-10: disable 'zero-length-bounds' warning for now
  ACK: pavel -- just a patch to disable warnings
  - ec22322218ec Stop the ad-hoc games with -Wno-maybe-initialized
  UR: pavel -- redoes warning logic, does not fix a bug
  - 9088569b5603 kbuild: compute false-positive -Wmaybe-uninitialized cases in Kconfig
  UR: pavel -- cleanup, not a bugfix; prepares for next patch?
  - 51cc5495ff35 gcc-10 warnings: fix low-hanging fruit
  ACK: pavel -- just a warning fix
  - bd2cefcd8815 pnp: Use list_for_each_entry() instead of open coding
  ACK: pavel -- just a cleanup and warning fix
  - 89148bf73d76 hwmon: (da9052) Synchronize access with mfd
  ACK: pavel
  - 893e55eb245e IB/mlx4: Test return value of calls to ib_get_cached_pkey
  IGN: pavel
  - 358254300b7b netfilter: nft_set_rbtree: Introduce and use nft_rbtree_interval_start()
  NAK: pavel -- preparation for patch that is not there?
  - 63e320a09544 arm64: fix the flush_icache_range arguments in machine_kexec
  ACK: pavel
  - d2413ec1f789 netfilter: conntrack: avoid gcc-10 zero-length-bounds warning
  ACK: pavel -- just a warning fix
  - 0840f6728363 NFSv4: Fix fscache cookie aux_data to ensure change_attr is included
  ACK: pavel
  - d8b16f4ab334 nfs: fscache: use timespec64 in inode auxdata
  ACK: pavel -- cleanup, does not fix much; preparation for next patch
  - f61787f77dd7 NFS: Fix fscache super_cookie index_key from changing after umount
  ACK: pavel -- just a performance improvement for caching after umount
  - 57a5bd6c26dc mmc: block: Fix request completion in the CQE timeout path
  ACK: pavel -- very very very unlikely bug
  - d8c167dd2fa3 mmc: core: Check request type before completing the request
  ACK: pavel
  - 3d03be751577 i40iw: Fix error handling in i40iw_manage_arp_cache()
  ACK: pavel
  - 6f5c38c7b002 pinctrl: cherryview: Add missing spinlock usage in chv_gpio_irq_handler
  ACK: pavel
  - 823fb483dfee pinctrl: baytrail: Enable pin configuration setting for GPIO chip
  ACK: pavel
  - ecf4cb653e63 gfs2: Another gfs2_walk_metadata fix
  ACK: pavel -- not a minimal fix
  - 0a79765b560f ALSA: hda/realtek - Fix S3 pop noise on Dell Wyse
  ACK: pavel
  - 15e0db6e61c0 ipc/util.c: sysvipc_find_ipc() incorrectly updates position index
  ACK: pavel
  - e00fb78e1265 drm/qxl: lost qxl_bo_kunmap_atomic_page in qxl_image_init_helper()
  ACK: pavel
  - 1b8a1d1351a4 ALSA: hda/hdmi: fix race in monitor detection during probe
  ACK: pavel
  - 648e920654f4 cpufreq: intel_pstate: Only mention the BIOS disabling turbo mode once
  ACK: pavel -- just a printk fix; also changes message severity
  - 1cb0aed8cd18 dmaengine: mmp_tdma: Reset channel error on release
  ACK: pavel
  - 53d9db75d7a9 dmaengine: pch_dma.c: Avoid data race between probe and irq handler
  ACK: pavel
  - 29130f67c174 riscv: fix vdso build with lld
  ACK: pavel
  - 99779c24bf4c tcp: fix SO_RCVLOWAT hangs with fat skbs
  ACK: pavel
  - 31080a892c54 net: tcp: fix rx timestamp behavior for tcp_recvmsg
  UR: pavel -- interface change; not really a bugfix
  - 0ccd25035949 netprio_cgroup: Fix unlimited memory leak of v2 cgroups
  ACK: pavel
  - c1386e64c82a net: ipv4: really enforce backoff for redirects
  ACK: pavel
  - 5cdcb1777289 net: dsa: loop: Add module soft dependency
  ACK: pavel
  - 2afbedce3184 hinic: fix a bug of ndo_stop
  UR: pavel -- also removes error handling?
  - 791195cc821f virtio_net: fix lockdep warning on 32 bit
  UR: pavel -- fixes warning at cost of performance; does not fix any real problem
  - d4ebd0fc9384 tcp: fix error recovery in tcp_zerocopy_receive()
  ACK: pavel
  - 4347527e0115 Revert "ipv6: add mtu lock check in __ip6_rt_update_pmtu"
  ACK: pavel
  - 3d6322ab1b5c pppoe: only process PADT targeted at local interfaces
  ACK: pavel
  - 9a949267f271 net: phy: fix aneg restart in phy_ethtool_set_eee
  ACK: pavel
  - caf6c20c6421 netlabel: cope with NULL catmap
  ACK: pavel
  - 2ef834fec2ad net: fix a potential recursive NETDEV_FEAT_CHANGE
  ACK: pavel
  - cfd57f735cec mmc: sdhci-acpi: Add SDHCI_QUIRK2_BROKEN_64_BIT_DMA for AMDI0040
  ACK: pavel -- not a minimum fix
  - 34fcb4291e23 scsi: sg: add sg_remove_request in sg_write
  ACK: pavel
  - 3c11fec81519 virtio-blk: handle block_device_operations callbacks after hot unplug
  ACK: pavel
  - e459398d6066 drop_monitor: work around gcc-10 stringop-overflow warning
  UR: pavel
  - 7b77f42f3f45 net: moxa: Fix a potential double 'free_irq()'
  UR: pavel -- check it is allocated by devm
  - 453be5262a1e net/sonic: Fix a resource leak in an error handling path in 'jazz_sonic_probe()'
  ACK: pavel
  - 4ad0f43edf60 shmem: fix possible deadlocks on shmlock_user_lock
  ACK: pavel -- a just a tweak to address lockdep warning. Needs more annotation for other checkers?
  - 8ab7974fd821 net: dsa: Do not make user port errors fatal
  ACK: pavel -- should dev_info be added?
