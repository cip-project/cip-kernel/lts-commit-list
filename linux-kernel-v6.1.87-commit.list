# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v6.1.87
  - 6741e066ec763 Linux 6.1.87
  - 724fbc7c0cb88 drm/amd/display: fix disable otg wa logic in DCN316
  ACK: pavel
  - 90819b1830bc2 drm/amdgpu: always force full reset for SOC21
  UR: pavel --! return true
  - 7cc89dbcb8eab drm/amdgpu: Reset dGPU if suspend got aborted
  ACK: pavel -- interesting workaround
  - 29bd4d05f2c5b drm/i915: Disable port sync when bigjoiner is used
  ACK: pavel -- just a feature disable
  - 2bc1796f8eeba drm/i915/cdclk: Fix CDCLK programming order when pipes are active
  ACK: pavel
  - d844df110084e x86/bugs: Replace CONFIG_SPECTRE_BHI_{ON,OFF} with CONFIG_MITIGATION_SPECTRE_BHI
  UR: pavel --! the patch hit mainline 6 days ago
  - 7f18a0df76217 x86/bugs: Remove CONFIG_BHI_MITIGATION_AUTO and spectre_bhi=auto
  ACK: pavel -- just tweak to match mainline
  - d737d8cd8e64e x86/bugs: Clarify that syscall hardening isn't a BHI mitigation
  ACK: pavel -- just an API tweak
  - 4b0b5d621e89e x86/bugs: Fix BHI handling of RRSBA
  ACK: pavel
  - dc2db3e978c5a x86/bugs: Rename various 'ia32_cap' variables to 'x86_arch_cap_msr'
  UR: pavel
  - b1b32586f797b x86/bugs: Cache the value of MSR_IA32_ARCH_CAPABILITIES
  ACK: pavel
  - 662e341e57ccb x86/bugs: Fix BHI documentation
  ACK: pavel -- just a documentation fix
  - 0d433e40827d3 x86/bugs: Fix return type of spectre_bhi_state()
  ACK: pavel -- just a warning fix
  - d447d8de840c2 irqflags: Explicitly ignore lockdep_hrtimer_exit() argument
  ACK: pavel -- just a warning workaround
  - 22f51ddb0cc12 x86/apic: Force native_apic_mem_read() to use the MOV instruction
  ACK: pavel -- workaround for missing emulation
  - 881b495ed26be selftests: timers: Fix abs() warning in posix_timers test
  ACK: pavel -- just a warning fix
  - 9c09773917fbb x86/cpu: Actually turn off mitigations by default for SPECULATION_MITIGATIONS=n
  ACK: pavel
  - 0c182182d6d9e perf/x86: Fix out of range data
  ACK: pavel
  - a2c1c0cfab054 vhost: Add smp_rmb() in vhost_enable_notify()
  ACK: pavel
  - f6e2d61dc1598 vhost: Add smp_rmb() in vhost_vq_avail_empty()
  ACK: pavel
  - 18c8cc6680ce9 drm/client: Fully protect modes[] with dev->mode_config.mutex
  ACK: pavel
  - 8a6fea3fcb577 drm/ast: Fix soft lockup
  ACK: pavel -- simply continues on timeout
  - d29b50a32c274 drm/amdkfd: Reset GPU on queue preemption failure
  ACK: pavel
  - 4b53d7d620c45 drm/i915/vrr: Disable VRR when using bigjoiner
  ACK: pavel -- just disables broken feature
  - 62029bc9ff2c1 kprobes: Fix possible use-after-free issue on kprobe registration
  UR: pavel -- c/e
  - 88dd8bb129fca io_uring/net: restore msg_control on sendzc retry
  ACK: pavel
  - c00146b399a51 btrfs: qgroup: convert PREALLOC to PERTRANS after record_root_in_trans
  ACK: pavel
  - 06fe999854273 btrfs: record delayed inode root in transaction
  ACK: pavel
  - cb3131b5a204d btrfs: qgroup: correctly model root qgroup rsv in convert
  ACK: pavel
  - 5f1205b86bd04 iommu/vt-d: Allocate local memory for page request queue
  ACK: pavel -- just a perfromance fix
  - 91580ea48b6dc tracing: hide unused ftrace_event_id_fops
  UR: pavel --! missing in 5.10?
  - 19ff8fed33388 net: ena: Fix incorrect descriptor free behavior
  ACK: pavel
  - 7d44e12efb7d7 net: ena: Wrong missing IO completions check order
  UR: pavel -- not a minimum bugfix, not really a fix
  - 4dea83d483d57 net: ena: Fix potential sign extension issue
  ACK: pavel
  - b75722be422c2 af_unix: Fix garbage collector racing against connect()
  ACK: pavel
  - fb6d14e23d489 af_unix: Do not use atomic ops for unix_sk(sk)->inflight.
  ACK: pavel -- just a preparation
  - 19643bf8c9b5b net: dsa: mt7530: trap link-local frames regardless of ST Port State
  UR: pavel -- long
  - 8edb087c44a43 net: sparx5: fix wrong config being used when reconfiguring PCS
  ACK: pavel
  - 88a50c8a50482 net/mlx5e: HTB, Fix inconsistencies with QoS SQs number
  ACK: pavel
  - ad26f26abd353 net/mlx5e: Fix mlx5e_priv_init() cleanup flow
  ACK: pavel -- just a warning fix
  - 2e8dc5cffc844 net/mlx5: Properly link new fs rules into the tree
  ACK: pavel -- just a robustness
  - c760089aa9828 netfilter: complete validation of user input
  ACK: pavel
  - 9d42f37339121 Bluetooth: L2CAP: Fix not validating setsockopt user input
  ACK: pavel
  - 7bc65d23ba20d Bluetooth: SCO: Fix not validating setsockopt user input
  ACK: pavel
  - de76ae9ea1a6c ipv6: fix race condition between ipv6_get_ifaddr and ipv6_del_addr
  ACK: pavel
  - 03d564999fa8a ipv4/route: avoid unused-but-set-variable warning
  ACK: pavel -- just a warning workaround
  - 2c46877f5f935 ipv6: fib: hide unused 'pn' variable
  ACK: pavel -- just a warning workaround
  - 7e33f68791eb8 octeontx2-af: Fix NIX SQ mode and BP config
  ACK: pavel
  - 84a352b7eba11 af_unix: Clear stale u->oob_skb.
  ACK: pavel
  - 492337a4fbd14 net: ks8851: Handle softirqs at the end of IRQ thread to fix hang
  ACK: pavel
  - be033154523f5 net: ks8851: Inline ks8851_rx_skb()
  ACK: pavel -- just a preparation
  - ecedcd7e39854 bnxt_en: Reset PTP tx_avail after possible firmware reset
  ACK: pavel
  - 4a1b65d1e55d5 geneve: fix header validation in geneve[6]_xmit_skb
  ACK: pavel
  - 2a523f14a3f53 xsk: validate user input for XDP_{UMEM|COMPLETION}_FILL_RING
  ACK: pavel
  - ac1c10b4ebdf7 u64_stats: fix u64_stats_init() for lockdep when used repeatedly in one file
  ACK: pavel -- just a debugging fix
  - 0b445005599d9 net: openvswitch: fix unwanted error log on timeout policy probing
  ACK: pavel -- just a printk tweak
  - 9fc74e367be42 scsi: qla2xxx: Fix off by one in qla_edif_app_getstats()
  ACK: pavel
  - 438b9a71b25ab scsi: hisi_sas: Modify the deadline for ata_wait_after_reset()
  ACK: pavel
  - b7dc2e6b87985 nouveau: fix function cast warning
  ACK: pavel -- just a warning workaround
  - 84fb60063509e Revert "drm/qxl: simplify qxl_fence_wait"
  UR: pavel
  - 24c0c5867ad27 arm64: dts: imx8-ss-conn: fix usdhc wrong lpcg clock order
  ACK: pavel
  - 49054b3ed2495 media: cec: core: remove length check of Timer Status
  UR: pavel --a just support for more messages
  - bccc8d1550927 PM: s2idle: Make sure CPUs will wakeup directly on resume
  ACK: pavel
  - bd9b94055c3de drm/amd/pm: fixes a random hang in S4 for SMU v13.0.4/11
  ACK: pavel
  - 66fab1e120b39 Bluetooth: Fix memory leak in hci_req_sync_complete()
  ACK: pavel
  - 2d5f12de4cf58 ring-buffer: Only update pages_touched when a new page is touched
  ACK: pavel
  - 3fe79b2c83461 batman-adv: Avoid infinite loop trying to resize local TT
  ACK: pavel
  - 0559b2d759be0 ata: libata-scsi: Fix ata_scsi_dev_rescan() error path
  ACK: pavel
  - ca5962bdc53c8 smb3: fix Open files on server counter going negative
  ACK: pavel
