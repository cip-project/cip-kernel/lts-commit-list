# Stable Kernel Patches Review Status
Please list your name and review result below the patch item

  * UR:  Under Review
  * ACK: Acknowledge (if the patch is accepted)
  * TBB: To be backported (if other patches should be also backported)
  * NAK: Negative acknowledge (if the patch is rejected, please list the reason)
  * IGN: Patch was not reviewed as it is out of scope for CIP project


## v4.4.183
  - 3087432 Linux 4.4.183
  - df7ba81 Abort file_remove_privs() for non-reg. files
  - 8f6345a coredump: fix race condition between mmget_not_zero()/get_task_mm() and core dumping
  - 3692bc3 Revert "crypto: crypto4xx - properly set IV after de- and encrypt"
  - 39c7c90 scsi: libsas: delete sas port if expander discover failed
  - a272035 scsi: libcxgbi: add a check for NULL pointer in cxgbi_check_route()
  - af8cd61 net: sh_eth: fix mdio access in sh_eth_close() for R-Car Gen2 and RZ/A1 SoCs
  - b25c62c KVM: PPC: Book3S: Use new mutex to synchronize access to rtas token list
  - 7ef8d77 ia64: fix build errors by exporting paddr_to_nid()
  - 432030b configfs: Fix use-after-free when accessing sd->s_dentry
  - d5a18c8 i2c: dev: fix potential memory leak in i2cdev_ioctl_rdwr
  - be1b9df net: tulip: de4x5: Drop redundant MODULE_DEVICE_TABLE()
  - 0bb6c29 gpio: fix gpio-adp5588 build errors
  - dd961ef perf/ring_buffer: Add ordering to rb->nest increment
  - 8abd8a9 perf/ring_buffer: Fix exposing a temporarily decreased data_head
  - 3e74b3f x86/CPU/AMD: Don't force the CPB cap when running under a hypervisor
  - f4678b7 mISDN: make sure device name is NUL terminated
  - e2f9c0f sunhv: Fix device naming inconsistency between sunhv_console and sunhv_reg
  - 753aa75 neigh: fix use-after-free read in pneigh_get_next
  - 709d877 lapb: fixed leak of control-blocks.
  - 1777c35 ipv6: flowlabel: fl6_sock_lookup() must use atomic_inc_not_zero
  - 6f057ab be2net: Fix number of Rx queues used for flow hashing
  - 7139a9f ax25: fix inconsistent lock state in ax25_destroy_timer
  - f1a5c6b USB: serial: option: add Telit 0x1260 and 0x1261 compositions
  - a67d659 USB: serial: option: add support for Simcom SIM7500/SIM7600 RNDIS mode
  - b8ee804 USB: serial: pl2303: add Allied Telesis VT-Kit3
  - f14747c USB: usb-storage: Add new ID to ums-realtek
  - 038c192 USB: Fix chipmunk-like voice when using Logitech C270 for recording audio.
  - 8f6b0e5 drm/vmwgfx: NULL pointer dereference from vmw_cmd_dx_view_define()
  - 6e0c7e73 drm/vmwgfx: integer underflow in vmw_cmd_dx_set_shader() leading to an invalid read
  - ce391e4 KVM: s390: fix memory slot handling for KVM_SET_USER_MEMORY_REGION
  - a1b9c6b KVM: x86/pmu: do not mask the value that is written to fixed PMUs
  - 7caddac usbnet: ipheth: fix racing condition
  - f80a35f scsi: bnx2fc: fix incorrect cast to u64 on shift operation
  - da51a32 scsi: lpfc: add check for loss of ndlp when sending RRQ
  - 3a2ff10 Drivers: misc: fix out-of-bounds access in function param_set_kgdbts_var
  - 1f8233c ASoC: cs42xx8: Add regcache mask dirty
  - 91f1fc1 cgroup: Use css_tryget() instead of css_tryget_online() in task_get_css()
  - 8b47af9 bcache: fix stack corruption by PRECEDING_KEY()
  - a2aabf2 i2c: acorn: fix i2c warning
  - d774bd2 ptrace: restore smp_rmb() in __ptrace_may_access()
  - 5aff00e signal/ptrace: Don't leak unitialized kernel memory with PTRACE_PEEK_SIGINFO
  - 0b871fc fs/ocfs2: fix race in ocfs2_dentry_attach_lock()
  - c05fed5 mm/list_lru.c: fix memory leak in __memcg_init_list_lru_node
  - be499b8 libata: Extend quirks for the ST1000LM024 drives with NOLPM quirk
  - d7a1881 ALSA: seq: Cover unsubscribe_port() in list_mutex
  - 4282a0b Revert "Bluetooth: Align minimum encryption key size for LE and BR/EDR connections"
  - cd23996 futex: Fix futex lock the wrong page
  - 38fbd5c ARM: exynos: Fix undefined instruction during Exynos5422 resume
  - 5767587 pwm: Fix deadlock warning when removing PWM device
  - 113a78a ARM: dts: exynos: Always enable necessary APIO_1V8 and ABB_1V8 regulators on Arndale Octa
  - 7818495 pwm: tiehrpwm: Update shadow register for disabling PWMs
  - d24e22c dmaengine: idma64: Use actual device for DMA transfers
  - 5331716 gpio: gpio-omap: add check for off wake capable gpios
  - 3b4652b PCI: xilinx: Check for __get_free_pages() failure
  - c869210 video: imsttfb: fix potential NULL pointer dereferences
  - 5c7fbc5 video: hgafb: fix potential NULL pointer dereference
  - bea8fa8 PCI: rcar: Fix a potential NULL pointer dereference
  - 76208df PCI: rpadlpar: Fix leaked device_node references in add/remove paths
  - 1004686 ARM: dts: imx6qdl: Specify IMX6QDL_CLK_IPG as "ipg" clock to SDMA
  - 927d2be ARM: dts: imx6sx: Specify IMX6SX_CLK_IPG as "ipg" clock to SDMA
  - 8aee025 ARM: dts: imx6sx: Specify IMX6SX_CLK_IPG as "ahb" clock to SDMA
  - b49501f5 clk: rockchip: Turn on "aclk_dmac1" for suspend on rk3288
  - 4270dc6 soc: mediatek: pwrap: Zero initialize rdata in pwrap_init_cipher
  - 8456763 platform/chrome: cros_ec_proto: check for NULL transfer function
  - c7155e5 x86/PCI: Fix PCI IRQ routing table memory leak
  - 198a54f nfsd: allow fh_want_write to be called twice
  - e6779b2 fuse: retrieve: cap requested size to negotiated max_write
  - 742cb74b nvmem: core: fix read buffer in place
  - 750c55e ALSA: hda - Register irq handler after the chip initialization
  - 09ad374 iommu/vt-d: Set intel_iommu_gfx_mapped correctly
  - 9e4ed17 f2fs: fix to do sanity check on valid block count of segment
  - 534ef92 f2fs: fix to avoid panic in do_recover_data()
  - 90a238a ntp: Allow TAI-UTC offset to be set to zero
  - 25be7d5 drm/bridge: adv7511: Fix low refresh rate selection
  - 8fdebdd perf/x86/intel: Allow PEBS multi-entry in watermark mode
  - dee1ba9 mfd: twl6040: Fix device init errors for ACCCTL register
  - 4110c41 mfd: intel-lpss: Set the device in reset state when init
  - 1bef191 kernel/sys.c: prctl: fix false positive in validate_prctl_map()
  - 937fa16 mm/cma_debug.c: fix the break condition in cma_maxchunk_get()
  - fceb0be mm/cma.c: fix crash on CMA allocation if bitmap allocation fails
  - 9c8d4d7 hugetlbfs: on restore reserve error path retain subpool reservation
  - d8129a5 ipc: prevent lockup on alloc_msg and free_msg
  - 50c0db53 sysctl: return -EINVAL if val violates minmax
  - 8b9241b fs/fat/file.c: issue flush after the writeback of FAT
